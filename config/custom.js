/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

// Declare global variables for a couple common libraries.

// Moment
global["moment"] = require("moment-timezone");
require("moment-duration-format");
require("moment-recur-ts");

// Needle
global["needle"] = require("needle");

// WWSU queue
const WWSU = require("../assets/plugins/wwsu-sails/js/wwsu.js");
global["WWSUqueue"] = new WWSU.WWSUqueue();

// Load discord. You should perform the connections in bootstrap initializer.
global["Discord"] = require("discord.js");

// WARNING! You should never modify any of these values directly; use the appropriate sails.models instead. Otherwise, changes will not persist.
module.exports.custom = {
  // Populate these from sails.models in bootstrap.js, and keep up to date with lifecycle callbacks
  analytics: {},
  basic: {},
  breaks: [],
  categories: [],
  discord: {},
  meta: {},
  radiodjs: [],
  displaysigns: [],
  sports: [],
  status: {},
  nws: [],

  // This is populated by bootstrap crons by taking all of the configuration from categories and finding RadioDJ subcategory IDs.
  subcats: {},
  // Populated by bootstrap crons via sports and getting RadioDJ subcategory IDs
  sportscats: {},
  // Populated by bootstrap crons via Show Openers, Show Returns, and Show Closers
  showcats: {},
  // Populated by bootstrap crons; contains randomly generated secrets to use for this process
  secrets: {},

  // Other config set manually in env files
  baseUrl: `https://server.wwsu1069.org/dev/`,
  basePath: `/dev/`,
  internalEmailAddress: "wwsu4@wright.edu",

  // Leave at false! Used to track when sails has fully lifted.
  lifted: false,

  // Default system categories (Configcategories model)
  // WARNING: Do NOT remove/rename any of the keys marked "SYSTEM"; but you may modify its default value object.
  systemCategories: {
    // SYSTEM: All RadioDJ categories / subcategories containing music which can (also) be requested.
    music: {
      Music: [],
    },

    // SYSTEM: Top Adds, or recently-added music for heavy promotion via "Top Add" button in DJ Controls.
    adds: {
      Music: ["Heavy Rotation"],
    },

    // SYSTEM: Required FCC Station ID at the top of every hour that includes call sign, frequency, and market area
    IDs: {
      "Station IDs": [],
    },

    // SYSTEM: A non-profit "commercial" that promotes a cause, organization, or social issue.
    PSAs: {
      PSAs: [],
    },

    // SYSTEM: Fun audio clips, generally 15-30 seconds, identifying the station, played as defined in break configuration.
    sweepers: {
      Jingles: [],
    },

    // SYSTEM: A promotional audio clip usually paid for by the organization / company. Used in the underwriting system (and only tracks in these categories can be added into underwritings).
    underwritings: {
      Underwritings: [],
    },

    // SYSTEM: Short (usually < 7 seconds) station identification that plays in between music tracks during non-break times in automation.
    liners: {
      Liners: [],
    },

    // SYSTEM: Short (usually < 7 seconds) audio clips that play before the system plays requested tracks (if in automation).
    requestLiners: {
      Liners: ["Requests"]
    },

    // SYSTEM: An audio clip (usually 30 - 60 seconds) promoting a radio show or broadcast, played as defined in break configuration.
    promos: {
      Promos: ["Radio Show Promos"],
    },

    // SYSTEM: Music used for sports haltime and other extended sports breaks
    halftime: {
      Music: ["Sports"]
    },

    // SYSTEM: Liners played when the system is sent to break because of a technical issue (remote broadcasts)
    technicalIssues: {
      Liners: ["Technical Issues"]
    },

    // (ALL SYSTEM) EAS stuff
    easNewAlertIntro: {
      EAS: ["Intro"],
    },
    easNewAlertStream: {
      EAS: ["New Alerts"],
    },
    easAlertUpdateStream: {
      EAS: ["Active Alerts"],
    },

    // SYSTEM: When the system changes to a new playlist or genre, all tracks will be removed from the current queue EXCEPT tracks that are in these defined RadioDJ categories / subcategories.
    noClearGeneral: {
      Sweepers: [],
      "Station IDs": [],
      Jingles: [],
      Promos: [],
      Commercials: [],
      News: [],
      "Radio Shows": [],
    },

    // SYSTEM: When someone starts a show/broadcast, all tracks in the queue will be removed EXCEPT tracks in these categories / subcategories.
    noClearShow: {
      Sweepers: [],
      "Station IDs": [],
      Jingles: [],
      Promos: [],
      Commercials: [],
    },

    // SYSTEM: When a DJ or producer requests to resume a broadcast from a break, all tracks in these defined categories and subcategories will be removed from the queue
    clearBreak: {
      PSAs: [],
    },

    // SYSTEM: Whenever a track from any of these categories / subcategories play, the metadata will show configured alt(state) text instead of the track info.
    // NOTE: This also determines when the system determines when someone has gone on the air; the first track not existing in here is deemed when someone has started the broadcast.
    noMeta: {
      Jingles: [],
      "Station IDs": [],
      Sweepers: [],
      Promos: [],
      Commercials: [],
    },

    // SYSTEM: CRON will routinely check tracks in the specified categories. If there is a set fade in or fade out on these tracks, the system will reset these to zero (eg. no fading).
    noFade: {
      "Station IDs": [],
      Promos: [],
      Commercials: [],
    },
  },
};

/**
 * Datastores
 * (sails.config.datastores)
 *
 * A set of datastore configurations which tell Sails where to fetch or save
 * data when you execute built-in model methods like `.find()` and `.create()`.
 *
 *  > This file is mainly useful for configuring your development database,
 *  > as well as any additional one-off databases used by individual models.
 *  > Ready to go live?  Head towards `config/env/production.js`.
 *
 * For more information on configuring datastores, check out:
 * https://sailsjs.com/config/datastores
 */

module.exports.datastores = {
  /***************************************************************************
   *                                                                          *
   * Your app's default datastore.                                            *
   *                                                                          *
   * Sails apps read and write to local disk by default, using a built-in     *
   * database adapter called `sails-disk`.  This feature is purely for        *
   * convenience during development; since `sails-disk` is not designed for   *
   * use in a production environment.                                         *
   *                                                                          *
   * To use a different db _in development_, follow the directions below.     *
   * Otherwise, just leave the default datastore as-is, with no `adapter`.    *
   *                                                                          *
   * (For production configuration, see `config/env/production.js`.)          *
   *                                                                          *
   ***************************************************************************/

  default: { // Used for models with no datastore defined
    /***************************************************************************
     *                                                                          *
     * Want to use a different database during development?                     *
     *                                                                          *
     * 1. Choose an adapter:                                                    *
     *    https://sailsjs.com/plugins/databases                                 *
     *                                                                          *
     * 2. Install it as a dependency of your Sails app.                         *
     *    (For example:  npm install sails-mysql --save)                        *
     *                                                                          *
     * 3. Then pass it in, along with a connection URL.                         *
     *    (See https://sailsjs.com/config/datastores for help.)                 *
     *                                                                          *
     ***************************************************************************/
  },

  // WWSU-specific datastores which should be configured in local.js
  wwsusails: {}, // Main application database (you should use a production database like MariaDB / MySQL)
  radiodj: {}, // Model regarding the RadioDJ database; leave empty if not using RadioDJ.
  wwsusailsconfig: {}, // Model storing all of the configuration for the application (probably best to use a flat file db)
  ram: {}, // Data which should not persist between server reboots (you should use a memory store)

  /*
    MIGRATION NOTES when using the radiodj datastore:

    **Be sure to create and then execute the following procedure on the radiodj database to ensure createdAt and updatedAt columns are added:
    (modify table_schema to the name of the RadioDJ database)**

    DELIMITER //

    CREATE PROCEDURE AddSailsColumns()
    BEGIN
        DECLARE done INT DEFAULT 0;
        DECLARE tableName VARCHAR(255);
        DECLARE cur CURSOR FOR SELECT table_name FROM information_schema.tables WHERE table_schema = 'radiodj';
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        OPEN cur;
        read_loop: LOOP
            FETCH cur INTO tableName;
            IF done THEN
                LEAVE read_loop;
            END IF;

            SET @query = CONCAT('ALTER TABLE ', tableName, '
                                ADD createdAt DATETIME DEFAULT CURRENT_TIMESTAMP,
                                ADD updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;');
            PREPARE stmt FROM @query;
            EXECUTE stmt;
            DEALLOCATE PREPARE stmt;
        END LOOP;

        CLOSE cur;
    END //

    DELIMITER ;

    **Also run the following SQL query (but do not execute the procedure):***
    DROP PROCEDURE IF EXISTS `UpdateTracks2`;
    DELIMITER |
    CREATE PROCEDURE `UpdateTracks2`(
    IN `trackID` INT,
    IN `curArtist` VARCHAR(250) CHARSET utf8,
    IN `curTitle` VARCHAR(250) CHARSET utf8,
    IN `tType` INT,
    IN `curListeners` INT,
    IN `historyDays` INT,
    IN `pWeight` DOUBLE
    )
    BEGIN
    UPDATE songs SET count_played=count_played+1, date_played=NOW() WHERE ID=trackID;
    SET @tAlbum = (SELECT album FROM songs WHERE ID=trackID);
    IF tType = 0 OR tType = 9 THEN
    UPDATE songs SET artist_played=NOW() WHERE artist=curArtist;
    UPDATE songs SET title_played=NOW() WHERE title=curTitle;
    IF @tAlbum <> '' THEN
    UPDATE songs SET album_played=NOW() WHERE album=@tAlbum;
    END IF;
    END IF;
    IF tType = 9 THEN
    UPDATE requests SET played=1 WHERE songID=trackID;
    END IF;
    UPDATE songs SET enabled=0, play_limit=0 WHERE enabled=1 AND play_limit>0 AND count_played>=play_limit AND limit_action=1;
    DELETE FROM songs WHERE play_limit>0 AND count_played>=play_limit AND limit_action=2;
    UPDATE songs SET id_subcat=ABS(limit_action), play_limit=0, limit_action=0 WHERE enabled=1 AND limit_action<0 AND count_played>=play_limit;
    IF pWeight>0 THEN
    UPDATE songs SET weight=weight-pWeight WHERE ID=trackID AND (weight-pWeight)>=0;
    END IF;
    IF historyDays > 0 THEN
    INSERT INTO history(trackID, date_played, song_type, id_subcat, id_genre, duration, artist, original_artist, title, album, composer, label, `year`, track_no, disc_no, publisher, copyright, isrc, listeners)
    SELECT ID, NOW(), song_type, id_subcat, id_genre, duration, curArtist, original_artist, curTitle, album, composer, label, `year`, track_no, disc_no, publisher, copyright, isrc, curListeners FROM songs WHERE ID=trackID;
    END IF;
    END |
    DELIMITER ;

    **Also the settings table in RadioDJ needs an "ID" column added (not null, PRIMARY, auto-increment)**
  */
};

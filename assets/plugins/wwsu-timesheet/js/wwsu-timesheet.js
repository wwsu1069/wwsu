"use strict";

// This class manages director timesheets from WWSU.
// NOTE: unlike most other WWSU models, this does not use traditional WWSUdb extends. Otherwise, memory can be quickly eaten up by timesheets.

// REQUIRES these WWSUmodules: noReq (WWSUreq), directorReq (WWSUreq), adminDirectorReq (WWSUreq), WWSUMeta, WWSUhosts, WWSUutil, WWSUanimations
class WWSUtimesheet extends WWSUevents {
	/**
	 * Construct the class.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();

		this.manager = manager;

		this.endpoints = {
			add: "POST /api/timesheet",
			get: "GET /api/timesheet",
			edit: "PUT /api/timesheet/:ID",
			remove: "DELETE /api/timesheet/:ID",
		};
		this.data = {
			add: {},
			get: {},
			edit: {},
			remove: {},
		};

		this.tables = {
			hours: undefined,
			records: undefined,
		};

		this.modals = {
			edit: new WWSUmodal(`Edit Timesheet`, null, ``, true, {
				zindex: 1200,
				width: 800,
			}),
			clock: new WWSUmodal(`Clock In / Out`, null, ``, true, {
				zindex: 1100,
			}),
		};

		this.fields = {
			start: undefined,
			end: undefined,
			browse: undefined,
		};

		this.manager.socket.on("timesheet", (data) => {
			this.updateTables();
		});

		this.cache = [];
	}

	/**
	 * Get the timezone we should use.
	 */
	get timezone() {
		return this.manager.has("WWSUMeta")
			? this.manager.get("WWSUMeta").timezone
			: moment.tz.guess();
	}

	/**
	 * Show clock-in or clock-out form for the specified director.
	 *
	 * @param {number} directorID ID of the director clocking in or out
	 */
	clockForm(directorID) {
		// Find the director
		let director = this.manager
			.get(this.manager.get("directorReq").db)
			.find({ ID: directorID }, true);

		this.modals.clock.iziModal("open");

		this.modals.clock.title = `${director.name} - Clock ${
			!director.present
				? this.manager.get("WWSUhosts") &&
				  this.manager.get("WWSUhosts").client &&
				  this.manager.get("WWSUhosts").client.authorized
					? `In`
					: `In Remotely`
				: `Out`
		}`;

		this.modals.clock.body = ``;

		$(this.modals.clock.body).alpaca({
			schema: {
				title: `Clock ${
					!director.present
						? this.manager.get("WWSUhosts") &&
						  this.manager.get("WWSUhosts").client &&
						  this.manager.get("WWSUhosts").client.authorized
							? `In`
							: `In Remotely`
						: `Out`
				}`,
				type: "object",
				properties: {
					name: {
						type: "string",
						required: true,
					},
					password: {
						type: "string",
						required: true,
						format: "password",
						title: "Password",
					},
					timestamp: {
						title: `Clock-${director.present ? `Out` : `In`} Time`,
						format: "datetime",
					},
					notes: {
						type: "string",
						title: "Accomplishments / Notes",
						// Notes are only required for clocking out; hide it otherwise.
						required: director.present ? true : false,
						hidden: director.present ? false : true,
					},
				},
			},
			options: {
				fields: {
					name: {
						type: "hidden",
					},
					timestamp: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						picker: {
							inline: true,
							sideBySide: true,
						},
						helpers: [
							`Be aware if you specify a time 30+ minutes from now, this record will be marked unapproved and will need approved by an admin director.`,
							`Use the station timezone of ${this.timezone}.`,
						],
					},
					notes: {
						type: "tinymce",
						options: {
							toolbar:
								"undo redo restoredraft | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | fullscreen preview | image link | ltr rtl",
							plugins:
								"autoresize autosave preview paste importcss searchreplace autolink save directionality visualblocks visualchars fullscreen image link table hr pagebreak nonbreaking toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help quickbars",
							menubar: "file edit view insert format tools table help",
							a11y_advanced_options: true,
							autosave_ask_before_unload: false,
							autosave_retention: "180m",
						},
						helper:
							"Please briefly describe what you accomplished during your time clocked in. Optionally include things you planned to do but could not accomplish (and why), and things you plan to do next time you are in.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: `Clock ${
								!director.present
									? this.manager.get("WWSUhosts") &&
									  this.manager.get("WWSUhosts").client &&
									  this.manager.get("WWSUhosts").client.authorized
										? `In`
										: `In Remotely`
									: `Out`
							}`,
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								// Add timezone to timestamp and convert to ISO String
								if (value.timestamp) {
									value.timestamp = `${moment
										.utc(value.timestamp, "YYYY-MM-DD hh:mm A")
										.format("YYYY-MM-DD[T]HH:mm:ss")}${moment
										.tz(value.timestamp, this.timezone)
										.format("ZZ")}`;
								}

								this.manager
									.get("directorReq")
									._authorize(value.name, value.password, (body) => {
										if (body === 0 || typeof body.token === `undefined`) {
											if (this.manager.has("WWSUehhh"))
												this.manager.get("WWSUehhh").play();
											$(document).Toasts("create", {
												class: "bg-warning",
												title: "Authorization Error",
												body: "There was an error authorizing you. Did you type your password in correctly? Please contact the engineer if this is a bug.",
												delay: 15000,
												autohide: true,
											});
										} else {
											this.manager.get("directorReq")._tryRequest(
												this.endpoints.add,
												{
													data: {
														timestamp: moment(value.timestamp).toISOString(
															true
														),
														notes: value.notes,
														remote:
															!this.manager.get("WWSUhosts") ||
															!this.manager.get("WWSUhosts").client ||
															!this.manager.get("WWSUhosts").client.authorized,
													},
												},
												{},
												(body2, resp) => {
													if (resp.statusCode < 400) {
														$(document).Toasts("create", {
															class: "bg-success",
															title: `Clocked ${
																director.present ? `Out` : `In`
															}`,
															autohide: true,
															delay: 10000,
															body: `You successfully clocked ${
																director.present ? `Out` : `In`
															}. ${
																director.present
																	? `Have a good day!`
																	: `Welcome!`
															}`,
														});
														this.modals.clock.iziModal("close");
													}
												}
											);
										}
									});
							},
						},
					},
				},
			},
			data: {
				name: director.name,
				password: ``,
				timestamp: moment(this.manager.get("WWSUMeta").meta.time).toISOString(
					true
				),
				notes: ``,
			},
		});
	}

	/**
	 * Edit a timesheet via WWSU API.
	 *
	 * @param {object} data Data to pass to WWSU
	 * @param {?function} cb Callback function with true for success, false for failure
	 */
	edit(data, cb) {
		this.manager
			.get("adminDirectorReq")
			.request(this.endpoints.edit, { data }, {}, (body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") {
						cb(false);
					}
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "timesheet edited",
						autohide: true,
						delay: 15000,
						body: `The timesheet was edited.`,
					});
					if (typeof cb === "function") {
						cb(true);
					}
				}
			});
	}

	/**
	 * Remove a timesheet via WWSU API.
	 *
	 * @param {object} data Data to pass to WWSU
	 * @param {?function} cb Callback function with true for success, false for failure
	 */
	remove(data, cb) {
		this.manager
			.get("adminDirectorReq")
			.request(this.endpoints.remove, { data }, {}, (body, resp) => {
				if (resp.statusCode >= 400) {
					if (typeof cb === "function") {
						cb(false);
					}
				} else {
					$(document).Toasts("create", {
						class: "bg-success",
						title: "timesheet removed",
						autohide: true,
						delay: 10000,
						body: `The timesheet was removed.`,
					});
					if (typeof cb === "function") {
						cb(true);
					}
				}
			});
	}

	/**
	 * Initialize tables.
	 *
	 * @param {string} hoursTable DOM query string for the table indicating total hours for each director
	 * @param {string} recordsTable DOM query string of the table indicating each timesheet record and actions for it
	 * @param {string} startField DOM query for the start date input field
	 * @param {string} endField DOM query for the end date input field
	 * @param {string} browseButton DOM query for the browse button to load timesheet records in the tables
	 */
	init(hoursTable, recordsTable, startField, endField, browseButton) {
		this.manager.get("WWSUanimations").add("timesheet-init-tables", () => {
			// Set fields
			this.fields.start = startField;
			this.fields.end = endField;
			this.fields.browse = browseButton;

			// Browse button click event
			$(this.fields.browse).click(() => {
				this.getRecords();
			});

			// Init html
			$(hoursTable).html(
				`<table id="section-timesheets-hours-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);
			$(recordsTable).html(
				`<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				}.</p><table id="section-timesheets-records-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);

			// Setup hours table
			this.manager
				.get("WWSUutil")
				.waitForElement(`#section-timesheets-hours-table`, () => {
					this.tables.hours = $(`#section-timesheets-hours-table`).DataTable({
						paging: false,
						data: [],
						columns: [
							{ title: "Director" },
							{ title: "In-Office Hours" },
							{ title: "Remote Hours" },
							{ title: "Total Hours" },
						],
						order: [[0, "asc"]],
						buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
					});

					this.tables.hours
						.buttons()
						.container()
						.appendTo(
							$(`#section-timesheets-hours-table_wrapper .col-md-6:eq(0)`)
						);
				});

			// Setup records table
			this.manager
				.get("WWSUutil")
				.waitForElement(`#section-timesheets-records-table`, () => {
					// Extra information
					const format = (d) => {
						return `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
					<tr>
						<td>Accomplishments / Notes:</td>
						<td>${d.notes}</td>
					</tr>
					</table>`;
					};
					this.tables.records = $(
						`#section-timesheets-records-table`
					).DataTable({
						paging: true,
						data: [],
						columns: [
							{
								className: "details-control",
								orderable: false,
								data: null,
								defaultContent: "",
							},
							{ title: "ID", data: "ID" },
							{ title: "Director", data: "director" },
							{ title: "Statuses", data: "status" },
							{ title: "Clock In", data: "clockIn" },
							{ title: "Clock Out", data: "clockOut" },
							{ title: "Actions", data: "actions" },
						],
						columnDefs: [{ responsivePriority: 1, targets: 6 }],
						order: [[1, "asc"]],
						pageLength: 100,
						buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
						drawCallback: () => {
							// Action button click events
							$(".btn-timesheet-edit").unbind("click");
							$(".btn-timesheet-delete").unbind("click");

							$(".btn-timesheet-edit").click((e) => {
								let id = parseInt($(e.currentTarget).data("id"));
								let record = this.cache.find((rec) => rec.ID === id);
								if (record) {
									this.timesheetForm(record);
								} else {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									$(document).Toasts("create", {
										class: "bg-danger",
										title: "Error accessing timesheet edit form",
										body: "There was an error loading the form to edit that timesheet. Please report this to the engineer.",
										autohide: true,
										delay: 10000,
										icon: "fas fa-skull-crossbones fa-lg",
									});
								}
							});

							$(".btn-timesheet-delete").click((e) => {
								let id = parseInt($(e.currentTarget).data("id"));
								this.manager.get("WWSUutil").confirmDialog(
									`<p>Are you sure you want to <strong>permanently</strong> remove the timesheet record ${id}?</p>
                                <p>This will remove the timesheet record, and its logged hours will no longer tally.</p>`,
									null,
									() => {
										this.remove({ ID: id }, (success) => {
											if (success) {
												this.getRecords();
											}
										});
									}
								);
							});
						},
					});

					this.tables.records
						.buttons()
						.container()
						.appendTo(
							$(`#section-timesheets-records-table_wrapper .col-md-6:eq(0)`)
						);

					// Additional info rows
					$("#section-timesheets-records-table tbody").on(
						"click",
						"td.details-control",
						(e) => {
							let tr = $(e.target).closest("tr");
							let row = this.tables.records.row(tr);

							if (row.child.isShown()) {
								// This row is already open - close it
								row.child.hide();
								tr.removeClass("shown");
							} else {
								// Open this row
								row.child(format(row.data())).show();
								tr.addClass("shown");
							}
						}
					);
				});
		});
	}

	/**
	 * Get records for a range, and update tables if initialized.
	 *
	 * @param {?string} start The ISO date of the earliest timesheet records to get
	 * @param {?string} end The ISO date of the latest timesheet records to get
	 * @param {?function} cb Callback function to execute with records
	 */
	getRecords(cb) {
		this.manager.get("noReq").request(
			this.endpoints.get,
			{
				data: {
					start: $(this.fields.start).val(),
					end: $(this.fields.end).val(),
				},
			},
			{},
			(body, resp) => {
				if (resp.statusCode < 400 && body && body.constructor === Array) {
					// Cache the current timesheets
					this.cache = body;

					if (typeof cb === "function") {
						cb(body);
					}

					// Update hours table if it exists
					if (this.tables.hours) {
						// Calculate hours for directors
						let directors = {};
						body.map((record) => {
							// Set up tamplate if new director
							if (typeof directors[record.name] === "undefined") {
								directors[record.name] = {
									office: { approved: 0, unapproved: 0, total: 0 },
									remote: { approved: 0, unapproved: 0, total: 0 },
									total: { approved: 0, unapproved: 0, total: 0 },
								};
							}

							// Add actual hours
							if (record.timeIn) {
								if (record.remote) {
									directors[record.name].remote[
										record.approved === 1 ? "approved" : "unapproved"
									] += moment(record.timeOut || undefined).diff(
										record.timeIn,
										"hours",
										true
									);
									directors[record.name].remote.total += moment(
										record.timeOut || undefined
									).diff(record.timeIn, "hours", true);
								} else {
									directors[record.name].office[
										record.approved === 1 ? "approved" : "unapproved"
									] += moment(record.timeOut || undefined).diff(
										record.timeIn,
										"hours",
										true
									);
									directors[record.name].office.total += moment(
										record.timeOut || undefined
									).diff(record.timeIn, "hours", true);
								}
								directors[record.name].total[
									record.approved === 1 ? "approved" : "unapproved"
								] += moment(record.timeOut || undefined).diff(
									record.timeIn,
									"hours",
									true
								);
								directors[record.name].total.total += moment(
									record.timeOut || undefined
								).diff(record.timeIn, "hours", true);
							}
						});

						// Update table
						this.tables.hours.clear();
						for (let director in directors) {
							if (Object.prototype.hasOwnProperty.call(directors, director)) {
								this.tables.hours.rows.add([
									[
										director,
										`<ul>
										<li>Approved: ${
											Math.round(
												(directors[director].office.approved + Number.EPSILON) *
													100
											) / 100
										}</li>
										<li>Un-approved: ${
											Math.round(
												(directors[director].office.unapproved +
													Number.EPSILON) *
													100
											) / 100
										}</li>
										<li>Total: ${
											Math.round(
												(directors[director].office.total + Number.EPSILON) *
													100
											) / 100
										}</li>
										</ul>`,
										`<ul>
										<li>Approved: ${
											Math.round(
												(directors[director].remote.approved + Number.EPSILON) *
													100
											) / 100
										}</li>
										<li>Un-approved: ${
											Math.round(
												(directors[director].remote.unapproved +
													Number.EPSILON) *
													100
											) / 100
										}</li>
										<li>Total: ${
											Math.round(
												(directors[director].remote.total + Number.EPSILON) *
													100
											) / 100
										}</li>
										</ul>`,
										`<ul>
										<li>Approved: ${
											Math.round(
												(directors[director].total.approved + Number.EPSILON) *
													100
											) / 100
										}</li>
										<li>Un-approved: ${
											Math.round(
												(directors[director].total.unapproved +
													Number.EPSILON) *
													100
											) / 100
										}</li>
										<li>Total: ${
											Math.round(
												(directors[director].total.total + Number.EPSILON) * 100
											) / 100
										}</li>
										</ul>`,
									],
								]);
							}
						}
						this.tables.hours.draw(false);
					}

					// Update records table if it exists
					if (this.tables.records) {
						this.tables.records.clear();
						this.tables.records.rows.add(
							body.map((record) => {
								let status = `<span class="badge badge-secondary">Unknown</span>`;
								switch (record.approved) {
									case -1:
										status = `<span class="badge badge-danger">Unexcused Absence</span>`;
										break;
									case 0:
										status = `<span class="badge badge-warning">Unapproved</span>`;
										break;
									case 1:
										status = `<span class="badge badge-success">Approved</span>`;
										break;
									case 2:
										status = `<span class="badge badge-secondary">Cancelled</span>`;
										break;
								}
								return {
									notes: record.notes,
									ID: record.ID,
									director: record.name,
									status: `${
										record.remote
											? `<span class="badge bg-indigo">Remote</span>`
											: ``
									}${status}`,
									clockIn: `<ul>
									<li>Scheduled: ${
										record.scheduledIn
											? moment
													.tz(
														record.scheduledIn,
														this.manager.has("WWSUMeta")
															? this.manager.get("WWSUMeta").meta.timezone
															: moment.tz.guess()
													)
													.format("YYYY-MM-DD h:mm A")
											: `Unscheduled`
									}</li>
										<li>Actual: ${
											record.timeIn
												? moment
														.tz(
															record.timeIn,
															this.manager.has("WWSUMeta")
																? this.manager.get("WWSUMeta").meta.timezone
																: moment.tz.guess()
														)
														.format("YYYY-MM-DD h:mm A")
												: `Absent`
										}</li>
										</ul>`,
									clockOut: `<ul>
									<li>Scheduled: ${
										record.scheduledOut
											? moment
													.tz(
														record.scheduledOut,
														this.manager.has("WWSUMeta")
															? this.manager.get("WWSUMeta").meta.timezone
															: moment.tz.guess()
													)
													.format("YYYY-MM-DD h:mm A")
											: `Unscheduled`
									}</li>
										<li>Actual: ${
											record.timeOut
												? moment
														.tz(
															record.timeOut,
															this.manager.has("WWSUMeta")
																? this.manager.get("WWSUMeta").meta.timezone
																: moment.tz.guess()
														)
														.format("YYYY-MM-DD h:mm A")
												: record.timeIn
												? `Clocked In`
												: `Absent`
										}</li>
										</ul>`,
									actions: `<div class="btn-group">
                                    <button class="btn btn-sm btn-warning btn-timesheet-edit" data-id="${record.ID}" title="Edit Timesheet Record"><i class="fas fa-edit"></i></button><button class="btn btn-sm btn-danger btn-timesheet-delete" data-id="${record.ID}" title="Remove Timesheet record"><i class="fas fa-trash"></i></button></div>`,
								};
							})
						);
						this.tables.records.draw(false);
					}
				}
			}
		);
	}

	/**
	 * Make a "Edit Timesheet" Alpaca form in a modal.
	 *
	 * @param {object} data The original timesheet record
	 */
	timesheetForm(data) {
		this.modals.edit.iziModal("open");
		this.modals.edit.body = ``;

		// Correct timezones in data
		if (data) {
			data.scheduledIn = moment
				.tz(
					data.scheduledIn,
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				)
				.toISOString(true);
			data.scheduledOut = moment
				.tz(
					data.scheduledOut,
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				)
				.toISOString(true);
			data.timeIn = moment
				.tz(
					data.timeIn,
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				)
				.toISOString(true);
			data.timeOut = moment
				.tz(
					data.timeOut,
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				)
				.toISOString(true);
		}

		$(this.modals.edit.body).alpaca({
			schema: {
				title: "Edit Timesheet",
				type: "object",
				properties: {
					ID: {
						type: "number",
						required: true,
					},
					name: {
						type: "string",
						title: "Director",
						readonly: true,
					},
					scheduledIn: {
						format: "datetime",
						title: "Scheduled Time In",
						readonly: true,
					},
					scheduledOut: {
						format: "datetime",
						title: "Scheduled Time Out",
						readonly: true,
					},
					remote: {
						type: "boolean",
						title: "Is Remote Hours?",
					},
					approved: {
						type: "number",
						required: true,
						enum: [-1, 2, 0, 1],
						title: "Approval Status",
					},
					timeIn: {
						format: "datetime",
						title: "Clocked Time In",
					},
					timeOut: {
						format: "datetime",
						title: "Clocked Time Out",
					},
					notes: {
						type: "string",
						title: "Accomplishments / Notes",
					},
				},
			},
			options: {
				fields: {
					ID: {
						type: "hidden",
					},
					scheduledIn: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						helper: `The date/time the director was scheduled to clock in (station timezone of ${this.timezone}).`,
					},
					scheduledOut: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						helper: `The date/time the director was scheduled to clock out (station timezone of ${this.timezone}).`,
					},
					approved: {
						optionLabels: [
							"Unexcused Absence",
							"Excused Cancellation",
							"Unapproved Hours",
							"Approved Hours",
						],
					},
					timeIn: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						picker: {
							inline: true,
							sideBySide: true,
						},
						helpers: [
							`The date/time the director clocked in. Field required for approved hours and unapproved hours approval statuses.`,
							`Use the station timezone of ${this.timezone}.`,
						],
						validator: function (callback) {
							let value = this.getValue();

							let approved =
								this.getParent().childrenByPropertyId["approved"].getValue();
							if (
								(approved === 0 || approved === 1) &&
								(!value || value === "")
							) {
								callback({
									status: false,
									message:
										"Field is required when approval status is approved hours or unapproved hours.",
								});
								return;
							}
							callback({
								status: true,
							});
						},
					},
					timeOut: {
						dateFormat: `YYYY-MM-DD hh:mm A`,
						picker: {
							inline: true,
							sideBySide: true,
						},
						helpers: [
							"The date/time the director clocked out. If clocked time in is filled in, and this is left empty, we assume the director is still clocked in.",
							`Use the station timezone of ${this.timezone}.`,
						],
					},
					notes: {
						type: "tinymce",
						options: {
							toolbar:
								"undo redo restoredraft | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | fullscreen preview | image link | ltr rtl",
							plugins:
								"autoresize autosave preview paste importcss searchreplace autolink save directionality visualblocks visualchars fullscreen image link table hr pagebreak nonbreaking toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help quickbars",
							menubar: "file edit view insert format tools table help",
							a11y_advanced_options: true,
							autosave_ask_before_unload: false,
							autosave_retention: "180m",
						},
						helper:
							"Accomplishments during this timesheet period. Optionally plans that could not be completed (and why), and plans for the future.",
					},
				},
				form: {
					buttons: {
						submit: {
							title: "Edit Timesheet",
							click: (form, e) => {
								form.refreshValidationState(true);
								if (!form.isValid(true)) {
									if (this.manager.has("WWSUehhh"))
										this.manager.get("WWSUehhh").play();
									form.focus();
									return;
								}
								let value = form.getValue();

								// Add timezone to timeIn and timeOut and convert to ISO String
								if (value.timeIn) {
									value.timeIn = `${moment
										.utc(value.timeIn, "YYYY-MM-DD hh:mm A")
										.format("YYYY-MM-DD[T]HH:mm:ss")}${moment
										.tz(value.timeIn, this.timezone)
										.format("ZZ")}`;
								}
								if (value.timeOut) {
									value.timeOut = `${moment
										.utc(value.timeOut, "YYYY-MM-DD hh:mm A")
										.format("YYYY-MM-DD[T]HH:mm:ss")}${moment
										.tz(value.timeOut, this.timezone)
										.format("ZZ")}`;
								}

								this.edit(value, (success) => {
									if (success) {
										this.modals.edit.iziModal("close");
										this.getRecords();
									}
								});
							},
						},
					},
				},
			},
			data: data,
		});
	}
}

"use strict";

/**
 * This class manages reporting silence to WWWSU.
 * For silence detection, use wwsu-audio/wwsu-silence (WWSUsilenceaudio class).
 */

// REQUIRES these WWSUmodules: hostReq (WWSUreq)
class WWSUSilence extends WWSUevents {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();
		this.manager = manager;

		this.endpoints = {
			active: "PUT /api/silence/active",
			inactive: "PUT /api/silence/inactive",
		};
	}

	/**
	 * Tell WWSU there is unacceptable silence active. This should be re-triggered every minute until silence is no longer active (this.inactive should then be triggered).
	 *
	 * @param {?function} cb Callback executed after API call is made.
	 */
	active(cb) {
		this.manager
			.get("hostReq")
			.request(this.endpoints.active, {}, {}, (body, resp) => {
				if (resp.statusCode < 400) {
					if (typeof cb === "function") {
						cb(body);
					}
				}
			});
	}

	/**
	 * Tell WWSU there is no longer silence.
	 *
	 * @param {?function} cb Callback function after API call is made.
	 */
	inactive(cb) {
		this.manager
			.get("hostReq")
			.request(
				this.endpoints.inactive,
				{},
				{ hideErrorToast: true },
				(body, resp) => {
					if (resp.statusCode < 400) {
						if (typeof cb === "function") {
							cb(body);
						}
					}
				}
			);
	}
}

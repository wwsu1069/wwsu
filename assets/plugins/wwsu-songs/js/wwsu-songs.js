"use strict";

// This class manages tasks relating to RadioDJ tracks.

// REQUIRES these WWSU modules: WWSUMeta, noReq (WWSUreq), hostReq (if using track reports) (WWSUreq), WWSUrequestsweb (if using requestForm = true in showTrackInfo)
// REQUIRES these libraries: JQuery Block
class WWSUsongs extends WWSUevents {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();

		this.manager = manager;

		this.endpoints = {
			get: "GET /api/songs",
			getGenres: "GET /api/songs/genres",
			getReports: "GET /api/songs/reports",
		};

		this.reportsTable;

		this.modals = {
			trackInfo: new WWSUmodal(
				`Track Information`,
				null,
				`<table class="table table-striped">
			<thead>
			  <tr>
				<th scope="col"><strong>Name</strong></th>
				<th scope="col"><strong>Value</strong></th>
			  </tr>
			</thead>
			<tbody>
			  <tr>
				<td>Track ID</td>
				<td id="song-info-ID"></td>
			  </tr>
			  <tr>
				<td>Status</td>
				<td id="song-info-status"></td>
			  </tr>
			  <tr>
				<td>Artist</td>
				<td id="song-info-artist"></td>
			  </tr>
			  <tr>
				<td>Title</td>
				<td id="song-info-title"></td>
			  </tr>
			  <tr>
				<td>Album</td>
				<td id="song-info-album"></td>
			  </tr>
			  <tr>
				<td>Genre</td>
				<td id="song-info-genre"></td>
			  </tr>
			  <tr>
				<td>Duration</td>
				<td id="song-info-duration"></td>
			  </tr>
			  <tr>
				<td>Date Last Played</td>
				<td id="song-info-lastplayed"></td>
			  </tr>
			  <tr>
				<td>Track Limits</td>
				<td id="song-info-limits"></td>
			  </tr>
			  <tr>
				<td>Spins</td>
				<td>
        <div id="song-info-spins-numbers"></div>
        <div id="song-info-spins" class="bg-info" style="height: 20vh; overflow-y: scroll;"></div>
        </td>
			  </tr>
			</tbody>
		  </table>
      <div id="modal-trackinfo-footer"></div>`,
				true,
				{
					headerColor: "",
					zindex: 1200,
				}
			),
		};
	}

	/**
	 * Initialize data table of track reports.
	 *
	 * @param {string} table DOM query string of the div container which to place the table
	 */
	initReportsTable(table) {
		this.manager.get("WWSUanimations").add("track-reports-init-table", () => {
			// Init html
			$(table).html(
				`<p class="wwsumeta-timezone-display">Times are shown in the timezone ${
					this.manager.has("WWSUMeta")
						? this.manager.get("WWSUMeta").meta.timezone
						: moment.tz.guess()
				}.</p><table id="section-track-reports-table" class="table table-striped display responsive" style="width: 100%;"></table>`
			);

			this.manager
				.get("WWSUutil")
				.waitForElement(`#section-track-reports-table`, () => {
					// Extra information
					const format = (d) => {
						return `<p>Spin Breakdown:</p>
								<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
										<tr>
											<td>Spins via RadioDJ:</td>
											<td>${d.spinsRadioDJ}</td>
										</tr>
										<tr>
											<td>Spins via /api/meta (other automation systems):</td>
											<td>${d.spinsMeta}</td>
										</tr>
										<tr>
											<td>Spins via logs by show hosts:</td>
											<td>${d.spinsLog}</td>
										</tr>
										<tr>
											<td>Spin times:</td>
											<td><div style="height: 200px; overflow-y: scroll;"><ul>${d.spinTimes
												.map(
													(spinTime) =>
														`<li>${moment
															.tz(
																spinTime,
																this.manager.has("WWSUMeta")
																	? this.manager.get("WWSUMeta").meta.timezone
																	: moment.tz.guess()
															)
															.format("LLLL")}</li>`
												)
												.join("\n")}</ul></div></td>
										</tr>
										</table>`;
					};

					// Generate table
					this.reportsTable = $(`#section-track-reports-table`).DataTable({
						paging: true,
						data: [],
						columns: [
							{
								className: "details-control",
								orderable: false,
								data: null,
								defaultContent: "",
							},
							{ title: "Artist", data: "artist" },
							{ title: "Title", data: "title" },
							{ title: "Album", data: "album" },
							{ title: "Label", data: "label" },
							{ title: "Spins", data: "spins" },
							{ title: "Requests", data: "requests" },
							{ title: "Likes", data: "likes" },
						],
						columnDefs: [{ responsivePriority: 1, targets: [1, 2, 5] }],
						order: [
							[5, "desc"],
							[6, "desc"],
							[7, "desc"],
						],
						buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
						pageLength: 100,
					});

					this.reportsTable
						.buttons()
						.container()
						.appendTo(
							$(`#section-track-reports-table_wrapper .col-md-6:eq(0)`)
						);

					// Additional info rows
					$("#section-track-reports-table tbody").on(
						"click",
						"td.details-control",
						(e) => {
							let tr = $(e.target).closest("tr");
							let row = this.reportsTable.row(tr);

							if (row.child.isShown()) {
								// This row is already open - close it
								row.child.hide();
								tr.removeClass("shown");
							} else {
								// Open this row
								row.child(format(row.data())).show();
								tr.addClass("shown");
							}
						}
					);
				});
		});
	}

	/**
	 * Generate a report on the table initialized via initReportsTable.
	 *
	 * @param {string} blockDom DOM query string to block while processing the report
	 * @param {string} ignoreAutomation DOM query string of the ignore automation checkbox filter.
	 * @param {string} search DOM query string of the search text box.
	 * @param {string} start DOM query string of the start date selector.
	 * @param {string} end DOM query string of the end date selector.
	 */
	generateReport(
		blockDom,
		ignoreAutomation,
		search,
		start,
		end
	) {
		if (!this.reportsTable)
			throw new Error("You must call WWSUsongs.initReportsTable first.");

		this.getReports(
			{
				ignoreAutomation: $(ignoreAutomation).is(":checked"),
				search: $(search).val(),
				start: $(start).val() !== "" ? $(start).val() : null,
				end: $(end).val() !== "" ? $(end).val() : null,
			},
			blockDom,
			(body) => {
				this.reportsTable.clear();
				this.reportsTable.rows.add(body);
				this.reportsTable.draw(false);
			}
		);
	}

	/**
	 * Get information about a song or songs in RadioDJ from the WWSU API.
	 *
	 * @param {Object} data Data to be passed to the API.
	 * @param {function} cb Function called after request is made; parameter is the data returned from the server.
	 */
	get(data, cb) {
		this.manager
			.get("noReq")
			.request(this.endpoints.get, { data }, {}, (body, resp) => {
				if (resp.statusCode < 400) cb(body);
			});
	}

	/**
	 * Get the available genres in the WWSU system.
	 *
	 * @param {object} data Data to pass to WWSU
	 * @param {function} cb Callback with an array of genres as parameter (ID and name as properties)
	 */
	getGenres(data, cb) {
		this.manager
			.get("noReq")
			.request(this.endpoints.getGenres, { data }, {}, (body, resp) => {
				if (resp.statusCode < 400) cb(body);
			});
	}

	/**
	 * Get track reports.
	 *
	 * @param {Object} data Data to be passed to the API.
	 * @param {string} dom DOM query string to block while processing the request
	 * @param {function} cb Function called after request is made; parameter is the data returned from the server.
	 */
	getReports(data, dom, cb) {
		this.manager
			.get("hostReq")
			.request(
				this.endpoints.getReports,
				{ data },
				{ dom: dom },
				(body, resp) => {
					if (resp.statusCode < 400) cb(body);
				}
			);
	}

	/**
	 * Show information about a track in a Modal.
	 *
	 * @param {number} trackID ID of the track to query.
	 * @param {boolean} spins Set to true to also show information on when the track aired
	 * @param {boolean} requestForm Set to true to also display a request form (or an error if the track cannot be requested right now)
	 */
	showTrackInfo(trackID, spins, requestForm) {
		this.modals.trackInfo.iziModal("open");
		$(`#modal-${this.modals.trackInfo.id} .modal-content`)
			.prepend(`<div class="overlay">
              <i class="fas fa-2x fa-sync fa-spin"></i>
              <h1 class="text-white">Getting Track Info...</h1>
        </div>`);
		this.get({ ID: trackID, ignoreSpins: !spins }, (track) => {
			try {
				track = track[0];
				$("#song-info-ID").html(track.ID);
				$("#song-info-status").html(
					track.enabled === 1 ? "Enabled" : "Disabled"
				);
				document.getElementById("song-info-status").className = `bg-${
					track.enabled === 1 ? "success" : "dark"
				}`;
				$("#song-info-artist").html(track.artist);
				$("#song-info-title").html(track.title);
				$("#song-info-album").html(track.album);
				$("#song-info-genre").html(track.genre);
				$("#song-info-duration").html(
					moment.duration(track.duration, "seconds").format("HH:mm:ss")
				);
				$("#song-info-lastplayed").html(
					moment
						.tz(
							track.date_played,
							this.manager.has("WWSUMeta")
								? this.manager.get("WWSUMeta").meta.timezone
								: moment.tz.guess()
						)
						.isAfter("2002-01-01 00:00:01")
						? moment
								.tz(
									track.date_played,
									this.manager.has("WWSUMeta")
										? this.manager.get("WWSUMeta").meta.timezone
										: moment.tz.guess()
								)
								.format("LLLL")
						: "Unknown"
				);
				$("#song-info-limits").html(`<ul>
							${
								track.limit_action > 0 && track.count_played < track.play_limit
									? `<li>Track will expire after ${
											track.play_limit - track.count_played
									  } more spins</li>`
									: ``
							}
							${
								track.limit_action > 0 && track.count_played >= track.play_limit
									? `<li>Track expired (reached spin limit)</li>`
									: ``
							}
							${
								moment
									.tz(
										track.start_date,
										this.manager.has("WWSUMeta")
											? this.manager.get("WWSUMeta").meta.timezone
											: moment.tz.guess()
									)
									.isAfter(this.manager.get("WWSUMeta").meta.time)
									? `<li>Track will not start airing until ${moment
											.tz(
												track.start_date,
												this.manager.has("WWSUMeta")
													? this.manager.get("WWSUMeta").meta.timezone
													: moment.tz.guess()
											)
											.format("LLLL")}</li>`
									: ``
							}
							${
								moment
									.tz(
										track.end_date,
										this.manager.has("WWSUMeta")
											? this.manager.get("WWSUMeta").meta.timezone
											: moment.tz.guess()
									)
									.isBefore(this.manager.get("WWSUMeta").meta.time) &&
								moment
									.tz(
										track.end_date,
										this.manager.has("WWSUMeta")
											? this.manager.get("WWSUMeta").meta.timezone
											: moment.tz.guess()
									)
									.isAfter("2002-01-01 00:00:01")
									? `<li>Track expired on ${moment
											.tz(
												track.end_date,
												this.manager.has("WWSUMeta")
													? this.manager.get("WWSUMeta").meta.timezone
													: moment.tz.guess()
											)
											.format("LLLL")}</li>`
									: moment
											.tz(
												track.end_date,
												this.manager.has("WWSUMeta")
													? this.manager.get("WWSUMeta").meta.timezone
													: moment.tz.guess()
											)
											.isAfter("2002-01-01 00:00:01")
									? `<li>Track will expire on ${moment
											.tz(
												track.end_date,
												this.manager.has("WWSUMeta")
													? this.manager.get("WWSUMeta").meta.timezone
													: moment.tz.guess()
											)
											.format("LLLL")}</li>`
									: ``
							}
							</ul>`);

				if (spins && track.spins) {
					$("#song-info-spins").html(
						`${track.spins.automation
							.map(
								(spin) =>
									`Automation: ${moment
										.tz(
											spin,
											this.manager.has("WWSUMeta")
												? this.manager.get("WWSUMeta").meta.timezone
												: moment.tz.guess()
										)
										.format("LLLL")}`
							)
							.join("<br />")}<br />${track.spins.logged
							.map(
								(spin) =>
									`Logged: ${moment
										.tz(
											spin,
											this.manager.has("WWSUMeta")
												? this.manager.get("WWSUMeta").meta.timezone
												: moment.tz.guess()
										)
										.format("LLLL")}`
							)
							.join("<br />")}`
					);
					$("#song-info-spins-numbers").html(`<ul>
              <li id="song-info-spins-7"><strong>Last 7 Days:</strong> ${track.spins[7]}</li>
              <li id="song-info-spins-30"><strong>Last 30 Days:</strong> ${track.spins[30]}</li>
              <li id="song-info-spins-YTD"><strong>Year to Date:</strong> ${track.spins.YTD}</li>
              <li id="song-info-spins-365"><strong>Last 365 Days:</strong> ${track.spins[365]}</li>
            </ul>`);
				} else {
					$("#song-info-spins").html(``);
					$("#song-info-spins-numbers").html(``);
				}

				if (requestForm) {
					if (track.request.requestable) {
						$(`#modal-trackinfo-footer`).html(`<div class="form-group">
													<h6>Request this Track</h6>
													<label for="song-request-name">Name (optional; displayed when the request plays)</label>
													<input type="text" class="form-control" id="song-request-name" tabindex="0">
													<label for="song-request-message">Message for the DJ (optional)</label>
													<textarea class="form-control" id="song-request-message" rows="2" tabindex="0"></textarea>
													</div>
													<div class="form-group"><button type="submit" id="song-request-submit" class="btn btn-primary" tabindex="0">Place Request</button></div>`);
						window.requestAnimationFrame(() => {
							$(`#song-request-submit`)
								.unbind("click")
								.click((e) => {
									$(`#song-request-submit`).block({
										message: `...`,
										css: { border: "3px solid #a00" },
										timeout: 15000,
										onBlock: () => {
											this.manager.get("WWSUrequestsweb").place(
												`#modal-${this.modals.trackInfo.id}`,
												{
													ID: track.ID,
													name: $(`#song-request-name`).val(),
													message: $(`#song-request-message`).val(),
												},
												() => {
													$(`#song-request-submit`).unblock();
													this.modals.trackInfo.iziModal("close");
												}
											);
										},
									});
								});
							$(`#song-request-submit`)
								.unbind("keydown")
								.keydown((e) => {
									if (e.code === "Space" || e.code === "Enter")
										$(`#song-request-submit`).block({
											message: `...`,
											css: { border: "3px solid #a00" },
											timeout: 15000,
											onBlock: () => {
												this.manager.get("WWSUrequestsweb").place(
													`#modal-${this.modals.trackInfo.id}`,
													{
														ID: track.ID,
														name: $(`#song-request-name`).val(),
														message: $(`#song-request-message`).val(),
													},
													() => {
														$(`#song-request-submit`).unblock();
														this.modals.trackInfo.iziModal("close");
													}
												);
											},
										});
								});
						});
					} else {
						$(`#modal-trackinfo-footer`)
							.html(`<div class="callout callout-${track.request.listDiv}">
										${track.request.message}
									</div>`);
					}
				} else {
					$(`#modal-trackinfo-footer`).html(``);
				}
			} catch (e) {
				console.error(e);
				$(document).Toasts("create", {
					class: "bg-danger",
					title: "Error loading track information",
					subtitle: trackID,
					body: "There was an error loading track information. Please report this to the engineer.",
					icon: "fas fa-skull-crossbones fa-lg",
				});
			}
			$(`#modal-${this.modals.trackInfo.id} .overlay`).remove();
		});
	}
}

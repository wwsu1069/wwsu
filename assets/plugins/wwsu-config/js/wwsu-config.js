"use strict";

// This class houses the WWSU configuration. Use WWSUconfigadmin (which extends WWSUconfig) if you want the ability to edit config.

// REQUIRES these WWSUmodules: WWSUevents, WWSUdb, WWSUutil, WWSUmodal (if changing settings), noReq (WWSUreq), directorReq (WWSUreq) (only if changing settings)
class WWSUconfig extends WWSUevents {
	/**
	 * Construct the class
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 * @param {array} options.systems Array of system names to use/load in config (undefined = all of them).
	 */
	constructor(manager, options = {}) {
		super();

		this.manager = manager;
		this.options = options;

		this.dbs = {};
    this.dbInitialized = new Map();

		// If systems is undefined or empty, populate with all of them.
		this.options.systems =
			this.options.systems && this.options.systems.length
				? this.options.systems
				: [
						"analytics",
						"basic",
						"breaks",
						"categories",
						"discord",
						"displaysigns",
						"meta",
						"nws",
						"radiodjs",
						"status",
						"sports",
				  ];

		// Initialize every system by creating a WWSUdb and assigning the socket event.
		this.options.systems.forEach((system) => {
			this.dbs[system] = new WWSUdb(`config.${system}`);
      this.dbInitialized.set(system, false);
			this.dbs[system].assignSocketEvent(
				`config-${system}`,
				this.manager.socket
			);
			this.dbs[system].on("change", "WWSUconfig", (db, query) => {
				this.emitEvent("change", [`config-${system}`, db, query]);
			});
      this.dbs[system].on("replace", "WWSUconfig", () => {
        // WWSUmodules loaded checks
				this.dbInitialized.set(system, true);
        let notInitialized = 0;
        this.dbInitialized.forEach((dbInit) => {
          if (!dbInit) notInitialized++;
        });
        if (notInitialized <= 0) {
          this.initialized = true;
          this.manager.checkInitialized();
        }
			});
		});

		this.endpoints = {
			get: "GET /api/config",
		};
	}

	/**
	 * Check if the system is using the RadioDJ automation system.
	 * @requires Class was initialized with the radiodjs system.
	 * @returns {boolean} True if using RadioDJ, false if not.
	 */
	get usingRadioDJ() {
		return this.radiodjs ? this.radiodjs.length > 0 : false;
	}

	/**
	 * Shortcut for the analytics config.
	 */
	get analytics() {
		return this.dbs.analytics ? this.dbs.analytics.find({ ID: 1 }, true) : {};
	}

	/**
	 * Shortcut for the basic config.
	 */
	get basic() {
		return this.dbs.basic ? this.dbs.basic.find({ ID: 1 }, true) : {};
	}

	/**
	 * Shortcut for the breaks config.
	 */
	get breaks() {
		return this.dbs.breaks ? this.dbs.breaks.db().get() : [];
	}

	/**
	 * Shortcut for the categories config.
	 */
	get categories() {
		return this.dbs.categories ? this.dbs.categories.db().get() : [];
	}

	/**
	 * Shortcut for the Discord config.
	 */
	get discord() {
		return this.dbs.discord ? this.dbs.discord.find({ ID: 1 }, true) : {};
	}

	/**
	 * Shortcut for the display signs config.
	 */
	get displaysigns() {
		return this.dbs.displaysigns ? this.dbs.displaysigns.db().get() : [];
	}

	/**
	 * Shortcut for the meta config.
	 */
	get meta() {
		return this.dbs.meta ? this.dbs.meta.find({ ID: 1 }, true) : {};
	}

	/**
	 * Shortcut for the NWS config.
	 */
	get nws() {
		return this.dbs.nws ? this.dbs.nws.db().get() : [];
	}

	/**
	 * Shortcut for the radiodjs config.
	 */
	get radiodjs() {
		return this.dbs.radiodjs ? this.dbs.radiodjs.db().get() : [];
	}

	/**
	 * Shortcut for the status config.
	 */
	get status() {
		return this.dbs.status ? this.dbs.status.find({ ID: 1 }, true) : {};
	}

	/**
	 * Shortcut for the sports config
	 */
	get sports() {
		return this.dbs.sports ? this.dbs.sports.db().get() : [];
	}

	// Initialize the config class. Call this on socket connect event.
	init() {
    this.initialized = false;
    this.dbInitialized.forEach((val, db) => {
      this.dbInitialized.set(db, false);
    });
    this.manager.checkInitialized();

		this.manager.get("noReq").request(
			this.endpoints.get,
			{
				data: { systems: this.options.systems },
			},
			{},
			(body, resp) => {
				// Replace data in each system's database; construct the database and assign socket event if it does not yet exist.
				if (resp.statusCode < 400) {
					for (let system in body) {
						this.dbs[system].query(body[system], true);
					}
				}
			}
		);
	}
}

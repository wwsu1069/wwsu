"use strict";

const announcementTypes = [
  "website-toast",
  "website-home",
  "website-directors",
  "website-members",
  "website-member",
  "website-partner",
  "website-nowplaying",
  "website-radioshows",
  "website-sports",
  "website-schedule",
  "website-chat",
  "website-call",
  "website-request",
  //"website-wcrd",
  "website-blogs",
];

// Connect socket
io.sails.reconnectionAttempts = 3; // Upon first loading, we should limit connection attempts to 3. But in the disconnect event, it should be infinity.
// io.sails.transports = ['polling', 'websocket'];
let socket = io.sails.connect();

let processingEvents;

/*
    WWSU MODULES
*/

let wwsumodules = new WWSUmodules(socket, '#modules-loading');
wwsumodules
  .add("WWSUonerror", WWSUonerror)
  .add("WWSUanimations", WWSUanimations, { loadingDOM: "#dom-loading" })
  .add(`WWSUutil`, WWSUutil)
  .add("WWSUNavigation", WWSUNavigation)
  .add("noReq", WWSUreq, { host: null })
  .add("WWSUMeta", WWSUMeta)
  .add("WWSUdirectors", WWSUdirectors, { host: null })
  .add("WWSUdjs", WWSUdjs)
  .add("WWSUeas", WWSUeas)
  .add("WWSUannouncements", WWSUannouncements, {
    types: announcementTypes,
  })
  // djs, directors, eas, and announcements need to be loaded first.
  .add("WWSUcalendar", WWSUcalendar, {
    timezone: moment.tz.guess(),
    init: () => {
      setTimeout(() => {
        updateDjs();
        updateDirectors();
        processEas();
        processAnnouncements();

        processEvents();
        checkCalendarChanges();
        updateDjShows();
        updateRadioShows();
        updateSportsBroadcasts();
      }, 100);
    },
  })
  .add("WWSUsubscriptions", WWSUsubscriptions)
  .add("WWSUdiscipline", WWSUdiscipline)
  .add("WWSUrecipientsweb", WWSUrecipientsweb)
  .add("WWSUrequestsweb", WWSUrequestsweb)
  .add("WWSUsongs", WWSUsongs)
  .add("WWSUmessagesweb", WWSUmessagesweb)
  .add("WWSUlikedtracks", WWSUlikedtracks)
  .add("WWSUstatus", WWSUstatus, { filter: ["stream-public"] })
  .add("WWSUflagsweb", WWSUflagsweb)
  .add("WWSUblogsweb", WWSUblogsweb)
  .add("WWSUconfig", WWSUconfig, { systems: ["basic", "radiodjs"] });

// Reference modules to variables
let navigation = wwsumodules.get("WWSUNavigation");
let animations = wwsumodules.get("WWSUanimations");
let util = wwsumodules.get("WWSUutil");
let meta = wwsumodules.get("WWSUMeta");
let noReq = wwsumodules.get("noReq");
let eas = wwsumodules.get("WWSUeas");
let announcements = wwsumodules.get("WWSUannouncements");
let directors = wwsumodules.get("WWSUdirectors");
let calendar = wwsumodules.get("WWSUcalendar");
let subscriptions = wwsumodules.get("WWSUsubscriptions");
let discipline = wwsumodules.get("WWSUdiscipline");
let recipients = wwsumodules.get("WWSUrecipientsweb");
let requests = wwsumodules.get("WWSUrequestsweb");
let songs = wwsumodules.get("WWSUsongs");
let messages = wwsumodules.get("WWSUmessagesweb");
let likedtracks = wwsumodules.get("WWSUlikedtracks");
let djs = wwsumodules.get("WWSUdjs");
let _status = wwsumodules.get("WWSUstatus");
let flags = wwsumodules.get("WWSUflagsweb");
let blogs = wwsumodules.get("WWSUblogsweb");
let config = wwsumodules.get("WWSUconfig");

subscriptions.initTable("#section-notifications-table");

/*
    AMPLITUDE.JS WEB PLAYER
*/

Amplitude.init({
  songs: [
    {
      url: "https://server.wwsu1069.org/stream",
      live: true,
    },
  ],
  callbacks: {
    play: () => {
      console.log("Amplitude Audio Player: Playing.");
    },
    playing: () => {
      console.log("Amplitude Audio Player: Resumed playing.");
    },
    stop: () => {
      console.log("Amplitude Audio Player: Stopped.");
    },
  },
  preload: "none",
});

/*
    VARIABLES
*/
let device = util.getUrlParameter(`device`); // Device UID if loading from a mobile app
let isMobile = device !== null;

let firstTime = true; // Is this the first time connecting to WWSU sockets since the page loaded?
let scheduleChanges = []; // Array of objects for current schedule changes
let cachedEvents = []; // Array of events for the next 28 days for cacheing.

// Used for messages
let messageIDs = []; // Message IDs read
let newMessages = 0; // Number of unread messages

// Used for announcements
let announcementsToastIDs = [];

/*
    SOCKET EVENTS
*/
// Socket connect
socket.on("connect", () => {
  $(".socket-connection").addClass("d-none");
  initModules();
});

// Disconnection; try to re-connect
socket.on("disconnect", () => {
  $(".socket-connection").removeClass("d-none");
  try {
    socket._raw.io._reconnection = true;
    socket._raw.io._reconnectionAttempts = Infinity;
  } catch (unusedE) {}
});

/**
 * Initialize WWSU modules. This should be called from socket connection or whenever all data needs to be reset.
 */
function initModules() {
  // Check if this user has an active discipline; display an alert if so.
  discipline.checkDiscipline(() => {
    // Initialize / re-load modules
    config.init();
    directors.init();
    calendar.init();
    djs.init();
    meta.init();
    likedtracks.init();
    messages.init();
    announcements.init();
    eas.init();
    _status.init();
    subscriptions.init();

    // Register this user with the chat system
    recipients.addRecipientWeb(null, (data) => {
      messages.updateMessages();
    });
  });
}

/*
    NAVIGATION
*/
navigation
  .addItem(
    "#nav-home",
    "#section-home",
    "Home - WWSU 106.9 FM",
    "/",
    false,
    () => initBackgroundImage("section-home-image")
  )
  /*
  .addItem(
    "#nav-wcrd",
    "#section-wcrd",
    "World College Radio Day - WWSU 106.9 FM",
    "/wcrd",
    false,
    () => initBackgroundImage("section-wcrd-image")
  )
  */
  .addItem(
    "#nav-directors",
    "#section-directors",
    "Directors / Contact - WWSU 106.9 FM",
    "/directors",
    false,
    () => initBackgroundImage("section-directors-image")
  )
  .addItem(
    "#btn-nav-directors",
    "#section-directors",
    "Directors / Contact - WWSU 106.9 FM",
    "/directors",
    false,
    () => initBackgroundImage("section-directors-image")
  )
  .addItem(
    "#nav-members",
    "#section-members",
    "Members / DJs - WWSU 106.9 FM",
    "/members",
    false,
    () => initBackgroundImage("section-members-image")
  )
  .addItem(
    "#btn-nav-members",
    "#section-members",
    "Members / DJs - WWSU 106.9 FM",
    "/members",
    false,
    () => initBackgroundImage("section-members-image")
  )
  .addItem(
    "#nav-member",
    "#section-member",
    "Become Org Member - WWSU 106.9 FM",
    "/become-member",
    false,
    () => initBackgroundImage("section-member-image")
  )
  .addItem(
    "#nav-partner",
    "#section-partner",
    "Partner With Us - WWSU 106.9 FM",
    "/partner",
    false,
    () => initBackgroundImage("section-partner-image")
  )
  .addItem(
    "#nav-blogs-all",
    "#section-blogs",
    "All Blog Posts - WWSU 106.9 FM",
    "/blogs",
    false,
    (e, popState) => {
      $(`#section-blogs-title`).html("Blog Posts");
      initBackgroundImage("section-blogs-image");
      blogs.list(!popState);
    }
  )
  .addItem(
    "#nav-blogs-sports",
    "#section-blogs",
    "Sports Blog Posts - WWSU 106.9 FM",
    "/blogs/sports",
    false,
    (e, popState) => {
      $(`#section-blogs-title`).html("Sports Blog Posts");
      initBackgroundImage("section-blogs-image");
      blogs.list(!popState, "sports");
    }
  )
  .addItem(
    "#nav-blogs-music",
    "#section-blogs",
    "Music Blog Posts - WWSU 106.9 FM",
    "/blogs/music",
    false,
    (e, popState) => {
      $(`#section-blogs-title`).html("Music Blog Posts");
      blogs.list(!popState, "music");
      initBackgroundImage("section-blogs-image");
    }
  )
  .addItem(
    "#nav-blogs-show",
    "#section-blogs",
    "Radio Show Blog Posts - WWSU 106.9 FM",
    "/blogs/show",
    false,
    (e, popState) => {
      $(`#section-blogs-title`).html("Radio Show Blog Posts");
      blogs.list(!popState, "show");
      initBackgroundImage("section-blogs-image");
    }
  )
  .addItem(
    "#nav-blogs-station",
    "#section-blogs",
    "Station Blog Posts - WWSU 106.9 FM",
    "/blogs/station",
    false,
    (e, popState) => {
      $(`#section-blogs-title`).html("Station Blog Posts");
      blogs.list(!popState, "station");
      initBackgroundImage("section-blogs-image");
    }
  )
  .addItem(
    "#nav-nowplaying",
    "#section-nowplaying",
    "Now Playing on WWSU - WWSU 106.9 FM",
    "/nowplaying",
    false,
    () => initBackgroundImage("section-nowplaying-image")
  )
  .addItem(
    "#nav-video",
    "#section-video",
    "WWSU Video Stream - WWSU 106.9 FM",
    "/video",
    false,
    () => initBackgroundImage("section-video-image")
  )
  .addItem(
    "#nav-radioshows",
    "#section-radioshows",
    "Radio Shows - WWSU 106.9 FM",
    "/radioshows",
    false,
    () => initBackgroundImage("section-radioshows-image")
  )
  .addItem(
    "#btn-nav-radioshows",
    "#section-radioshows",
    "Radio Shows - WWSU 106.9 FM",
    "/radioshows",
    false,
    () => initBackgroundImage("section-radioshows-image")
  )
  .addItem(
    "#nav-sports",
    "#section-sports",
    "Upcoming Sports Broadcasts - WWSU 106.9 FM",
    "/sports",
    false,
    () => initBackgroundImage("section-sports-image")
  )
  .addItem(
    "#btn-nav-sports",
    "#section-sports",
    "Upcoming Sports Broadcasts - WWSU 106.9 FM",
    "/sports",
    false,
    () => initBackgroundImage("section-sports-image")
  )
  .addItem(
    "#nav-calendar",
    "#section-calendar",
    "Program Calendar - WWSU 106.9 FM",
    "/calendar",
    false,
    () => initBackgroundImage("section-calendar-image")
  )
  .addItem(
    "#btn-nav-calendar",
    "#section-calendar",
    "Program Calendar - WWSU 106.9 FM",
    "/calendar",
    false,
    () => initBackgroundImage("section-calendar-image")
  )
  .addItem(
    "#nav-chat",
    "#section-chat",
    "Chat Room - WWSU 106.9 FM",
    "/chat",
    false,
    () => initBackgroundImage("section-chat-image")
  )
  .addItem(
    "#nav-call",
    "#section-call",
    "Call the Studio - WWSU 106.9 FM",
    "/call",
    false,
    () => initBackgroundImage("section-call-image")
  )
  .addItem(
    "#nav-notifications",
    "#section-notifications",
    "Manage Notifications - WWSU 106.9 FM",
    "/notifications",
    false,
    () => initBackgroundImage("section-notifications-image")
  )
  .addItem(
    "#nav-request",
    "#section-request",
    "Request a Track - WWSU 106.9 FM",
    "/request",
    false,
    () => {
      initBackgroundImage("section-request-image");

      // Update genres in dropdown
      songs.getGenres({}, (response) => {
        try {
          let html = `<option value="0">Any Genre</option>`;
          response.map((subcat) => {
            html += `<option value="${subcat.ID}">${subcat.name}</option>`;
          });
          $("#request-genre").html(html);
        } catch (e) {
          console.error(e);
          $(document).Toasts("create", {
            class: "bg-danger",
            title: "Error loading genres for request system",
            body: "There was an error loading the available genres to filter by in the request system. Please report this to wwsu4@wright.edu.",
            icon: "fas fa-skull-crossbones fa-lg",
          });
        }
      });
    }
  );

/**
 * Initialize deferred background image loading.
 *
 * @param {string} domID The ID of the div class for which we want to load the image.
 */
function initBackgroundImage(domID) {
  let imgDefer = document.getElementById(domID);
  let style =
    "background: url({url}) right bottom / cover no-repeat; text-shadow: 2px 2px #000000;";
  imgDefer.setAttribute(
    "style",
    style.replace("{url}", imgDefer.getAttribute("data-src"))
  );
}

/*
  Callback for navigation
*/

if (typeof window.wwsuNavigationCallback === "function")
  window.wwsuNavigationCallback(wwsumodules);

/*
    CLICK / CHANGE EVENTS
*/
$(".nav-item-schedule-change").on("click", () => {
  scheduleChanges.forEach((change) => {
    $(document).Toasts("create", {
      class: change.class,
      title: change.title,
      autohide: true,
      delay: 15000 * scheduleChanges.length,
      body: change.message,
    });
  });
});
$(".nav-item-eas-alert").on("click", () => {
  eas.db().each((easAlert) => {
    let bgClass = `bg-secondary`;
    switch (easAlert.severity) {
      case "Extreme":
        bgClass = `bg-danger`;
        break;
      case "Severe":
        bgClass = `bg-orange`;
        break;
      case "Moderate":
        bgClass = `bg-warning`;
        break;
      case "Minor":
        bgClass = `bg-info`;
        break;
      default:
        bgClass = `bg-secondary`;
        break;
    }
    $(document).Toasts("create", {
      class: bgClass,
      title: easAlert.alert,
      autohide: true,
      delay: 15000 * eas.db().get().length,
      body: `A ${easAlert.alert} is in effect in the WWSU listening area for ${
        easAlert.counties
      }, effective ${moment(easAlert.starts)
        .tz(meta.meta.timezone)
        .format("llll")} - ${moment(easAlert.expires)
        .tz(meta.meta.timezone)
        .format("llll")}`,
    });
  });
});
$(`#section-calendar-select`).on("change", () => {
  updateProgramCalendar();
});
$(`.button-broadcast-report`).on("click", (e) => {
  flags.flagForm({
    attendanceID: meta.meta.attendanceID,
    meta: `${meta.meta.line1} / ${meta.meta.line2}`,
  });
});
$(`.button-broadcast-subscribe`).on("click", (e) => {
  subscriptions.askByUnique(meta.meta.calendarUnique);
});

/*
    META
*/
function displayNowPlayingToast() {
  $(document).Toasts("create", {
    class: "bg-info",
    title: "Now Playing on WWSU",
    autohide: true,
    delay: 15000,
    body: `<strong>${meta.meta.line1}</strong><br />${meta.meta.line2}<br /><br />Click the badge in the top right corner of the page anytime to see what is playing.`,
  });
}

meta.on("newMeta", "renderer", (updated, fullMeta) => {
  if (typeof updated.state !== "undefined") {
    animations.add("metaTick-state", () => {
      // Update now playing card state info
      if (updated.state.startsWith("live_")) {
        $(".section-nowplaying-card-ribbon").html(
          `<div class="ribbon bg-danger">LIVE</div>`
        );
      } else if (updated.state.startsWith("prerecord_")) {
        $(".section-nowplaying-card-ribbon").html(
          `<div class="ribbon bg-fuchsia">PRERECORD</div>`
        );
      } else if (updated.state.startsWith("remote_")) {
        $(".section-nowplaying-card-ribbon").html(
          `<div class="ribbon bg-indigo">REMOTE</div>`
        );
      } else if (
        updated.state.startsWith("sports_") ||
        updated.state.startsWith("sportsremote_")
      ) {
        $(".section-nowplaying-card-ribbon").html(
          `<div class="ribbon bg-success">SPORTS</div>`
        );
      } else if (
        updated.state === "automation_on" ||
        updated.state === "automation_break"
      ) {
        $(".section-nowplaying-card-ribbon").html(
          `<div class="ribbon bg-secondary">MUSIC</div>`
        );
      } else if (updated.state === "automation_genre") {
        $(".section-nowplaying-card-ribbon").html(
          `<div class="ribbon bg-info">GENRE</div>`
        );
      } else if (updated.state === "automation_playlist") {
        $(".section-nowplaying-card-ribbon").html(
          `<div class="ribbon bg-blue">PLAYLIST</div>`
        );
      } else if (updated.state.startsWith("automation_")) {
        $(".section-nowplaying-card-ribbon").html(
          `<div class="ribbon bg-warning">SHOW STARTING</div>`
        );
      } else {
        $(".section-nowplaying-card-ribbon").html(
          `<div class="ribbon bg-secondary">UNKNOWN</div>`
        );
      }
    });
  }

  // Update now playing card info
  if (
    typeof updated.line1 !== "undefined" ||
    typeof updated.line2 !== "undefined"
  ) {
    animations.add("metaTick-lines", () => {
      $(".nav-item-nowplaying").html(
        `<strong>${fullMeta.line1}</strong><br />${fullMeta.line2}`
      );
        $(".section-nowplaying-card-line1").html(fullMeta.line1);
        $(".section-nowplaying-card-line2").html(fullMeta.line2);
    });
  }
  if (typeof updated.topic !== "undefined") {
    animations.add("metaTick-topic", () => {
      $(".section-nowplaying-card-topic").html(fullMeta.topic);
    });
  }
  if (typeof updated.showLogo !== "undefined") {
    animations.add("metaTick-logo", () => {
      if (fullMeta.showLogo && fullMeta.showLogo !== "") {
        $(".section-nowplaying-card-showlogo").html(
          `<img src="/api/uploads/${fullMeta.showLogo}" alt="Show logo" style="width: 100%" loading="lazy">`
        );
      } else {
        $(".section-nowplaying-card-showlogo").html(``);
      }
    });
  }
  if (typeof updated.history !== "undefined") {
    updateTrackHistory();
  }

  // Update whether or not video stream is live
  if (typeof updated.viewers !== "undefined") {
    animations.add("metaTick-viewers", () => {
      if (updated.viewers === null) {
        $(".video-live").addClass("d-none");
      } else {
        $(".video-live").removeClass("d-none");
      }
    });
  }
});

meta.on("metaTick", "renderer", (meta) => {
  // Re-process events and announcements every 5 minutes
  if (
    moment(meta.time).second() === 0 &&
    moment(meta.time).minute() % 5 === 0
  ) {
    processEvents();
    processAnnouncements();
  }
});

/*
    CALENDAR
*/

calendar.on("calendarUpdated", "renderer", () => {
  processEvents();
  checkCalendarChanges();
  updateDjShows();
  updateRadioShows();
  updateSportsBroadcasts();
});

/**
 * Re-process calendar events for the next 28 days and update UI as necessary with the new info. Plus, cache the events.
 */
function processEvents() {
  animations.add("processEvents", () => {
    // The 1-second timer prevents running multiple processEvents if it fires multiple times in a 1-second period.
    if (processingEvents && processingEvents !== "active") {
      clearTimeout(processingEvents);
    }
    processingEvents = setTimeout(() => {
      processingEvents = "active";

      // Update dates on select options
      for (var i = 1; i < 28; i++) {
        $(`#section-calendar-select-${i}`).html(
          moment
            .tz(meta.meta.time, moment.tz.guess())
            .startOf(`day`)
            .add(i, "days")
            .format(`dddd, MMMM D`)
        );
      }

      // Get events for the next 28 days
      calendar.getEvents(
        (events) => {
          // Update cache
          cachedEvents = events;

          // Update things that use the cache
          updateDirectorHours();
          updateProgramCalendar();

          processingEvents = undefined;
        },
        moment.tz(meta.meta.time, moment.tz.guess()).startOf("day"),
        moment.tz(meta.meta.time, moment.tz.guess()).add(4, "weeks"),
        undefined,
        undefined,
        undefined
      );
    }, 1000);
  });
}

/**
 * Check for, and display on now playing page, event re-schedules and cancellations that take place right now.
 */
function checkCalendarChanges() {
  animations.add("checkCalendarChanges", () => {
    calendar.whatShouldBePlaying(
      (events) => {
        let _scheduleChanges = []; // Do not update the global variable of schedule changes until we fully processed the calendar events.
        events
          .filter(
            (event) =>
              [
                "canceled",
                "canceled-system",
                "canceled-changed",
                "updated",
                "updated-system",
              ].indexOf(event.scheduleType) !== -1
          )
          .map((event) => {
            let scheduleObject;
            if (
              ["canceled", "canceled-system"].indexOf(event.scheduleType) !== -1
            ) {
              scheduleObject = {
                class: `bg-danger`,
                title: `CANCELED: ${event.hosts} - ${event.name}`,
                message: `<p>Are you here for ${event.hosts} - ${
                  event.name
                }? The ${moment(event.originalTime).format(
                  "LLL (Z)"
                )} broadcast was canceled.</p>
            <p>Reason: ${
              event.scheduleReason || "Unknown / not specified."
            }</p>`,
              };
            } else if (event.scheduleType === "canceled-changed") {
              scheduleObject = {
                class: `bg-warning`,
                title: `RE-SCHEDULED: ${event.hosts} - ${event.name}`,
                message: `<p>Are you here for ${event.hosts} - ${
                  event.name
                } for the ${moment(event.start).format("LLL (Z)")} broadcast? ${
                  event.scheduleReason
                }</p>`,
              };
            } else if (
              ["updated", "updated-system"].indexOf(event.scheduleType) &&
              event.originalDuration &&
              event.originalDuration !== event.duration
            ) {
              scheduleObject = {
                class: `bg-info`,
                title: `END TIME CHANGED: ${event.hosts} - ${event.name}`,
                message: `<p>Are you here for ${event.hosts} - ${
                  event.name
                } for the ${moment(event.originalTime).format(
                  "LLL (Z)"
                )} broadcast? It will end at ${moment(event.start)
                  .add(event.duration, "minutes")
                  .format(
                    "LT"
                  )} instead of its originally scheduled end time.</p>
                    <p>Reason: ${
                      event.scheduleReason || "Unknown / unspecified."
                    }</p>`,
              };
            }
            if (scheduleObject) _scheduleChanges.push(scheduleObject);
          });

        scheduleChanges = _scheduleChanges;

        let nowPlayingHtml = ``;

        // Display ! icon on nav bar if we have a schedule change
        if (scheduleChanges.length > 0) {
          $(".nav-item-schedule-change").removeClass("d-none");

          // Also add schedule changes to now playing announcements html
          scheduleChanges.forEach((change) => {
            nowPlayingHtml += `<div class="alert ${change.class}">
          <h5>${change.title}</h5>
          <p>${change.message}</p>
          </div>`;
          });
        } else {
          $(".nav-item-schedule-change").addClass("d-none");
        }

        $(".announcements-nowplaying-schedules").html(nowPlayingHtml);
      },
      false,
      true
    );
  });
}

/**
 * Update the listing of member hosted shows on the member page.
 */
function updateDjShows() {
  animations.add("updateDjShows", () => {
    djs
      .db({ active: true })
      .get()
      .forEach((dj) => {
        let calendarEvents = calendar.calendar.find(
          (record) =>
            [
              record.hostDJ,
              record.cohostDJ1,
              record.cohostDJ2,
              record.cohostDJ3,
            ].indexOf(dj.ID) !== -1 && record.active
        );

        $(`.calendar-shows-dj-${dj.ID}`).html(
          calendarEvents.length > 0
            ? calendarEvents
                .map((event) => `<li>${event.type}: ${event.name}</li>`)
                .join("")
            : "None"
        );
      });
  });
}

/**
 * Update listing of radio shows on WWSU
 */
function updateRadioShows() {
  animations.add("updateRadioShows", () => {
    let shows = [];
    let html = ``;
    let tophtml = ``;
    let topShow;
    let topShowScore = 0;

    calendar.calendar
      .find(
        (cal) =>
          cal.ID > 1 && // Skip test event
          cal.active && // Only list active events
          ["show", "remote", "prerecord"].indexOf(cal.type) !== -1
      ) // Only include shows, remotes, and prerecords; other events are not shows
      .sort((a, b) => b.ID - a.ID)
      .forEach((cal) => {
        // Determine if this is the top show of the week
        if (cal.scoreTrack > topShowScore) {
          topShow = cal.ID;
          topShowScore = cal.scoreTrack;
        }

        // Start by getting all the airtimes
        let schedules = {};

        calendar.schedule
          .find((sch) => sch.calendarID === cal.ID && !sch.scheduleType)
          .forEach((sch) => {
            schedules[sch.ID] = {
              schedule: calendar.generateScheduleText(sch),
              updates: [],
            };
          });

        // Skip shows without any schedules
        if (Object.values(schedules).length === 0) return;

        // Now get cancellations and re-schedules and add them to the primary schedule updates array
        calendar.schedule
          // For find, do not include cancellations / re-schedules that passed
          .find(
            (sch) =>
              sch.calendarID === cal.ID &&
              sch.scheduleType &&
              sch.originalTime &&
              (moment(sch.originalTime).isSameOrAfter(moment(meta.meta.time)) ||
                (sch.newTime &&
                  moment(sch.newTime).isSameOrAfter(moment(meta.meta.time))))
          )
          .forEach((sch) => {
            if (typeof schedules[sch.scheduleID] === "undefined") return; // parent schedule does not exist? Ignore this.

            switch (sch.scheduleType) {
              case "updated":
              case "updated-system":
                if (!sch.duration && !sch.newTime) break; // Ignore if it is not an actual re-schedule but just an edit.
                schedules[sch.scheduleID].updates.push(
                  `<span class="badge badge-warning p-1">RE-SCHEDULED</span> ${moment(
                    sch.originalTime
                  )
                    .tz(meta.meta.timezone)
                    .format("LLL")} -> ${moment(sch.newTime || sch.originalTime)
                    .tz(meta.meta.timezone)
                    .format("LLL")}${
                    sch.duration
                      ? ` - ${moment(sch.newTime || sch.originalTime)
                          .tz(meta.meta.timezone)
                          .add(sch.duration, "minutes")
                          .format("LT")}`
                      : ``
                  }`
                );
                break;
              case "canceled":
              case "canceled-system":
                schedules[sch.scheduleID].updates.push(
                  `<span class="badge badge-danger p-1">CANCELED</span> ${moment(
                    sch.originalTime
                  )
                    .tz(meta.meta.timezone)
                    .format("LLL")}`
                );
                break;
            }
          });

        // Push the info
        shows.push({
          cal,
          schedules,
        });
      });

    // Now, run through our array
    if (shows.length > 0) {
      shows.forEach((show) => {
        let { cal, schedules } = show;
        let temphtml = ``;

        // Populate HTML
        temphtml += `<div class="col-12">
      <div class="card mb-3${
        topShow === cal.ID ? ` border border-primary` : ``
      }">
      <div class="ribbon-wrapper ribbon-lg">
        <div class="ribbon bg-${calendar.getColorClass(cal)}">${cal.type}</div>
      </div>
      ${
        topShow === cal.ID
          ? `<div class="ribbon-wrapper ribbon-xl">
      <div class="ribbon bg-primary">SHOW OF THE WEEK</div>
    </div>`
          : ``
      }
        <div class="row no-gutters">
          <div class="col-md-3">
            ${
              cal.logo
                ? `<img src="/api/uploads/${cal.logo}" alt="Radio show logo for ${cal.name}" style="width: 100%" class="rounded" loading="lazy" />`
                : `<div class="bg-${calendar.getColorClass(
                    cal
                  )} text-center rounded" alt="Radio show Avatar for ${
                    cal.name
                  }" style="width: 140px; height: 140px;"><i class="p-1 ${calendar.getIconClass(
                    cal
                  )}" style="font-size: 100px;"></i></div>`
            }
          </div>
          <div class="col-md-8">
            <div class="card-header">
              <h2 class="h3 d-block text-wrap" style="max-width: 80%;">${
                cal.name
              }</h2>
              <h3 class="h4 d-block text-wrap" style="max-width: 80%;"><small>${
                cal.hosts
              }</small></h3>
            </div>
            <div class="card-body">
                ${
                  cal.banner
                    ? `<div class="text-center">
                <img src="/api/uploads/${cal.banner}" style="width: 80%;" alt="Radio show banner for ${cal.name}" loading="lazy" />
              </div>`
                    : ``
                }
                <p>${cal.description || `No description provided.`}</p>
                <h3 class="h5"><strong>Broadcast Schedule:</strong></h3>
                <ul>${Object.values(schedules)
                  .map(
                    (sch) =>
                      `<li>${sch.schedule}${
                        sch.updates.length
                          ? `<ul>${sch.updates
                              .map((ud) => `<li>${ud}</li>`)
                              .join("")}</ul>`
                          : ``
                      }</li>`
                  )
                  .join("")}</ul>
                  </div>
            </div>
          </div>
        <div class="card-footer">
        <p>
        <button type="button" class="btn bg-blue btn-radioshow-subscribe" data-id="${
          cal.ID
        }" title="Subscribe to receive notifications for ${
          cal.name
        }"><i class="fas fa-bell p-1"></i> Subscribe</button>
        </p>
        </div>
    </div>
    </div>`;

        if (topShow === cal.ID) {
          html += tophtml;
          tophtml = temphtml;
        } else {
          html += temphtml;
        }
      });
    }

    $("#section-radioshows-featured").html(tophtml);
    $("#section-radioshows-cards").html(html);

    window.requestAnimationFrame(() => {
      // Subscription buttons click event
      $(".btn-radioshow-subscribe").unbind("click");
      $(".btn-radioshow-subscribe").click((e) => {
        let event = calendar.calendar.find(
          (cal) => cal.ID === parseInt($(e.currentTarget).data("id")),
          true
        );

        if (event) {
          subscriptions.checkStatus(() => {
            util.confirmDialog(
              `<p>Do you want to receive notifications for ${event.name}?</p>
        <p>You will receive notifications when this event airs, is re-scheduled, is cancelled, or has other news.</p>
        <p>To subscribe only for a specific date/time, use the subscribe buttons on the program calendar page</p>`,
              null,
              () => {
                subscriptions.subscribe(
                  "calendar",
                  `${event.ID}`,
                  `${event.name} (all broadcasts)`
                );
              }
            );
          });
        }
      });
    });
  });
}

/**
 * Update listings of Sports Broadcasts on WWSU
 */
function updateSportsBroadcasts() {
  animations.add("updateSportsBroadcasts", () => {
    let html = ``;

    calendar.calendar
      .find((cal) => cal.active && cal.type === "sports")
      .sort((a, b) => b.ID - a.ID)
      .forEach((cal) => {
        // Start by getting all the airtimes
        let schedules = {};

        calendar.schedule
          .find((sch) => sch.calendarID === cal.ID && !sch.scheduleType)
          .forEach((sch) => {
            schedules[sch.ID] = {
              schedule: calendar.generateScheduleText(sch),
              updates: [],
            };
          });

        // Skip shows without any schedules
        if (Object.values(schedules).length === 0) return;

        // Now get cancellations and re-schedules
        calendar.schedule
          // For find, do not include cancellations / re-schedules that passed
          .find(
            (sch) =>
              sch.calendarID === cal.ID &&
              sch.scheduleType &&
              sch.originalTime &&
              (moment(sch.originalTime).isSameOrAfter(moment(meta.meta.time)) ||
                (sch.newTime &&
                  moment(sch.newTime).isSameOrAfter(moment(meta.meta.time))))
          )
          .forEach((sch) => {
            if (typeof schedules[sch.scheduleID] === "undefined") return; // parent schedule does not exist? Ignore this.

            switch (sch.scheduleType) {
              case "updated":
              case "updated-system":
                if (!sch.duration && !sch.newTime) break; // Ignore if it is not an actual re-schedule but just an edit.
                schedules[sch.scheduleID].updates.push(
                  `<span class="badge badge-warning p-1">RE-SCHEDULED</span> ${moment(
                    sch.originalTime
                  )
                    .tz(meta.meta.timezone)
                    .format("LLL")} -> ${moment(sch.newTime || sch.originalTime)
                    .tz(meta.meta.timezone)
                    .format("LLL")}${
                    sch.duration
                      ? ` - ${moment(sch.newTime || sch.originalTime)
                          .tz(meta.meta.timezone)
                          .add(sch.duration, "minutes")
                          .format("LT")}`
                      : ``
                  }`
                );
                break;
              case "canceled":
              case "canceled-system":
                schedules[sch.scheduleID].updates.push(
                  `<span class="badge badge-danger p-1">CANCELED</span> ${moment(
                    sch.originalTime
                  )
                    .tz(meta.meta.timezone)
                    .format("LLL")}`
                );
                break;
            }
          });

        // Populate HTML
        html += `<div class="col-12">
        <div class="card mb-3">
        <div class="ribbon-wrapper ribbon-lg">
          <div class="ribbon bg-${calendar.getColorClass(cal)}">${
          cal.type
        }</div>
        </div>
          <div class="row no-gutters">
            <div class="col-md-3">
              ${
                cal.logo
                  ? `<img src="/api/uploads/${cal.logo}" alt="Sports logo for ${cal.name}" style="width: 100%" class="rounded" loading="lazy" />`
                  : `<div class="bg-${calendar.getColorClass(
                      cal
                    )} text-center rounded" alt="Sports Avatar for ${
                      cal.name
                    }" style="width: 140px; height: 140px;"><i class="p-1 ${calendar.getIconClass(
                      cal
                    )}" style="font-size: 100px;"></i></div>`
              }
            </div>
            <div class="col-md-8">
              <div class="card-header">
                <h2 class="h3 d-block text-wrap" style="max-width: 80%;">${
                  cal.name
                }</h2>
              </div>
              <div class="card-body">
                <h3 class="h5"><strong>Broadcast Schedule:</strong></h3>
                  <ul>${Object.values(schedules)
                    .map(
                      (sch) =>
                        `<li>${sch.schedule}${
                          sch.updates.length
                            ? `<ul>${sch.updates
                                .map((ud) => `<li>${ud}</li>`)
                                .join("")}</ul>`
                            : ``
                        }</li>`
                    )
                    .join("")}</ul>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <p>
            <button type="button" class="btn bg-blue btn-sports-subscribe" data-id="${
              cal.ID
            }" title="Subscribe to receive notifications for ${
          cal.name
        }"><i class="fas fa-bell p-1"></i> Subscribe</button>
            </p>
        </div>
      </div>
      </div>`;
      });

    $("#section-sports-cards").html(html);

    window.requestAnimationFrame(() => {
      // Subscription buttons click event
      $(".btn-sports-subscribe").unbind("click");
      $(".btn-sports-subscribe").click((e) => {
        let event = calendar.calendar.find(
          (cal) => cal.ID === parseInt($(e.currentTarget).data("id")),
          true
        );

        if (event) {
          subscriptions.checkStatus(() => {
            util.confirmDialog(
              `<p>Do you want to receive notifications for ${event.name}?</p>
        <p>You will receive notifications when this event airs, is re-scheduled, is cancelled, or has other news.</p>
        <p>To subscribe only for a specific date/time, use the subscribe buttons on the program calendar page</p>`,
              null,
              () => {
                subscriptions.subscribe(
                  "calendar",
                  `${event.ID}`,
                  `${event.name} (all broadcasts)`
                );
              }
            );
          });
        }
      });
    });
  });
}

/**
 * Update events on program calendar page.
 */
function updateProgramCalendar() {
  animations.add("updateProgramCalendar", () => {
    // Get the value of the currently selected calendar item
    let selectedOption = $("#section-calendar-select")
      .children("option:selected")
      .val();
    selectedOption = parseInt(selectedOption);

    let html = ``;

    // List events
    cachedEvents
      .filter(
        (event) =>
          [
            "show",
            "prerecord",
            "remote",
            "sports",
            "genre",
            "playlist",
          ].indexOf(event.type) !== -1 &&
          moment(event.start)
            .tz(moment.tz.guess())
            .isBefore(
              moment()
                .startOf(`day`)
                .add(selectedOption + 1, `days`)
            ) &&
          moment(event.start)
            .tz(moment.tz.guess())
            .isSameOrAfter(moment().startOf(`day`).add(selectedOption, `days`))
      )
      .forEach((cal) => {
        html += `<div class="col-12">
      <div class="card mb-3${
        ["canceled", "canceled-system", "canceled-changed"].indexOf(
          cal.scheduleType
        ) !== -1 || moment(meta.meta.time).isAfter(moment(cal.end))
          ? ` bg-dark`
          : ``
      }">
      <div class="ribbon-wrapper ribbon-lg">
        <div class="ribbon bg-${calendar.getColorClass(cal)}">${cal.type}</div>
      </div>
        <div class="row no-gutters">
          <div class="col-md-3">
            ${
              cal.logo
                ? `<img src="/api/uploads/${cal.logo}" alt="Radio show logo for ${cal.name}" style="width: 100%" class="rounded" loading="lazy" />`
                : `<div class="bg-${calendar.getColorClass(
                    cal
                  )} text-center rounded" alt="Radio show Avatar for ${
                    cal.name
                  }" style="width: 140px; height: 140px;"><i class="p-1 ${calendar.getIconClass(
                    cal
                  )}" style="font-size: 100px;"></i></div>`
            }
          </div>
          <div class="col-md-8">
            <div class="card-header">
              <h2 class="h3 d-block text-wrap" style="max-width: 80%;">${
                cal.name
              }</h2>
              <h3 class="h4 d-block text-wrap" style="max-width: 80%;"><small>${
                cal.hosts
              }</small></h3>
            </div>
            <div class="card-body">
              <h3>${moment(cal.start)
                .tz(moment.tz.guess())
                .format("LT")} - ${moment(cal.end)
          .tz(moment.tz.guess())
          .format("LT")}</h3>
        ${
          ["canceled", "canceled-system"].indexOf(cal.scheduleType) !== -1
            ? `<div class="alert alert-danger">CANCELED: ${
                cal.scheduleReason || "Unknown / unspecified reason."
              }</div>`
            : ``
        }
        ${
          cal.scheduleType === "canceled-changed"
            ? `<div class="alert alert-warning">${
                cal.scheduleReason ||
                "Re-scheduled: Unknown / unspecified reason."
              }</div>`
            : ``
        }
        ${
          cal.timeChanged
            ? `<div class="alert alert-info">Not the regular broadcast time; this is the updated time from a re-schedule.</div>`
            : ``
        }
                ${
                  cal.banner
                    ? `<div class="text-center">
                <img src="/api/uploads/${cal.banner}" style="width: 80%;" alt="Radio show banner for ${cal.name}" loading="lazy" />
              </div>`
                    : ``
                }
                <p>${cal.description || `No description provided.`}</p>
            </div>
          </div>
        </div>
        <div class="card-footer">
                <p>
                <button type="button" class="btn bg-blue btn-calendar-subscribe" data-unique="${
                  cal.unique
                }" title="Subscribe to receive notifications for ${
          cal.name
        }"><i class="fas fa-bell p-1"></i> Subscribe</button>
                </p>
        </div>
    </div>
    </div>`;
      });

    $(`#section-calendar-events`).html(html);

    window.requestAnimationFrame(() => {
      // Subscription buttons click event
      $(".btn-calendar-subscribe").unbind("click");
      $(".btn-calendar-subscribe").click((e) => {
        subscriptions.askByUnique($(e.currentTarget).data("unique"));
      });
    });
  });
}

/*
    DIRECTORS
*/
directors.on("change", "renderer", () => {
  updateDirectors();
});

/**
 * Update the listing of directors
 */
function updateDirectors() {
  animations.add("updateDirectors", () => {
    let html = ``;

    directors
      .db()
      .get()
      .sort((a, b) => {
        // Admin directors first
        if (a.admin && !b.admin) return -1;
        if (b.admin && !a.admin) return 1;

        // Non-assistant directors after admin directors
        if (a.assistant && !b.assistant) return 1;
        if (b.assistant && !a.assistant) return -1;

        // Finally, sort by name
        return a.name - b.name;
      })
      .forEach((director) => {
        // Determine which color class to use depending on director's clock-in status
        let clockClass = `danger`;
        if (director.present === 1) clockClass = `success`;
        if (director.present === 2) clockClass = `indigo`;

        html += `<div class="col-xl-6 col-12">
            <div class="card mb-3 border ${
              director.present ? `border-success` : ``
            }">
                <div class="ribbon-wrapper ribbon-lg">
                ${
                  director.present
                    ? director.present === 2
                      ? `<div class="ribbon bg-indigo">REMOTE</div>`
                      : `<div class="ribbon bg-success">IN OFFICE</div>`
                    : `<div class="ribbon bg-danger">OUT OF OFFICE</div>`
                }
                </div>
              <div class="row no-gutters">
                <div class="col-md-3">
                  ${
                    director.avatar
                      ? `<img src="/api/uploads/${director.avatar}" alt="Director Avatar for ${director.name}" style="width: 100%" class="rounded" loading="lazy" />`
                      : `<div class="bg-${clockClass} rounded" alt="Director Avatar for ${
                          director.name
                        }" style="width: 140px;">${jdenticon.toSvg(
                          `Director ${director.name}`,
                          140
                        )}</div>`
                  }
                </div>
                <div class="col-md-8">
                  <div class="card-header">
                    <h2 class="h3 d-block text-wrap" style="max-width: 80%;">${
                      director.name
                    }</h2>
                    <h3 class="h4 d-block text-wrap" style="max-width: 80%;"><small>${
                      director.position
                    }</small></h3>
                  </div>
                  <div class="card-body">
                      ${director.profile || `<p>No profile provided.<p>`}
                    <p>
                      ${
                        director.email
                          ? `<strong>Email: </strong><a href="mailto:${director.email}">${director.email}</a>`
                          : ``
                      }
                    </p>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <h3 class="h5"><strong>Office Hours (next 7 days):</strong></h3>
                <div class="container-fluid director-office-hours-${
                  director.ID
                }">
                  <p>LOADING...</p>
                </div>
              </div>
            </div>
          </div>`;
      });
    $("#section-directors-cards").html(html);

    window.requestAnimationFrame(() => updateDirectorHours());
  });
}

/**
 * Update the office hours for each director using cached events.
 */
function updateDirectorHours() {
  animations.add("updateDirectorHours", () => {
    let html = ``;

    let events = cachedEvents.filter(
      (event) =>
        event.type === "office-hours" &&
        moment(event.start)
          .subtract(1, "weeks")
          .isBefore(moment(meta.meta.time))
    );

    directors
      .db()
      .get()
      .forEach((director) => {
        html = `<div class="row">
      <div class="col"><strong>Day</strong></div>
      <div class="col"><strong>Date</strong></div>
      <div class="col"><strong>Time</strong></div>
      </div>`;

        let officeHours = events.filter(
          (event) => event.director === director.ID
        );

        officeHours = officeHours.map((event) => {
          // null start or end? Use a default to prevent errors.
          if (!moment(event.start).isValid()) {
            event.start = moment(meta.meta.time).startOf("day");
          }
          if (!moment(event.end).isValid()) {
            event.end = moment(meta.meta.time).add(1, "days").startOf("day");
          }

          event.startT =
            moment(event.start).tz(meta.meta.timezone).minutes() === 0
              ? moment(event.start).tz(meta.meta.timezone).format("h")
              : moment(event.start).tz(meta.meta.timezone).format("h:mm");
          if (
            (moment(event.start).tz(meta.meta.timezone).hours() < 12 &&
              moment(event.end).tz(meta.meta.timezone).hours() >= 12) ||
            (moment(event.start).tz(meta.meta.timezone).hours() >= 12 &&
              moment(event.end).hours() < 12)
          ) {
            event.startT += moment(event.start)
              .tz(meta.meta.timezone)
              .format(" A");
          }
          event.endT =
            moment(event.end).tz(meta.meta.timezone).minutes() === 0
              ? moment(event.end).tz(meta.meta.timezone).format("h A")
              : moment(event.end).tz(meta.meta.timezone).format("h:mm A");

          // Update strings if need be, if say, start time was before this day, or end time is after this day.
          if (
            moment(event.end)
              .tz(meta.meta.timezone)
              .isAfter(
                moment(event.start)
                  .tz(meta.meta.timezone)
                  .startOf("day")
                  .add(1, "days")
              )
          ) {
            event.endT = `${moment(event.end)
              .tz(meta.meta.timezone)
              .format("MM/DD ")} ${event.endT}`;
          }

          let hoursObject = {
            day: moment(event.start).tz(meta.meta.timezone).format("ddd"),
            date: moment(event.start).tz(meta.meta.timezone).format("MM/DD"),
            time: `${event.startT} - ${event.endT}`,
          };

          if (event.timeChanged) {
            hoursObject.time = `<span class="text-info">${event.startT} - ${event.endT}</span><span class="badge badge-info m-1" title="These are temporary hours.">TEMP HOURS</span>`;
          }
          if (moment(meta.meta.time).isAfter(moment(event.end))) {
            hoursObject.time = `<strike><span class="text-black-50">${event.startT} - ${event.endT}</span></strike>`;
          }
          if (event.scheduleType && event.scheduleType.startsWith("canceled")) {
            hoursObject.time = `<strike><span class="text-danger">${event.startT} - ${event.endT}</span></strike><span class="badge badge-danger m-1" title="These hours were canceled/rescheduled.">CANCELED</span>`;
          }

          return hoursObject;
        });

        html += officeHours
          .map(
            (schedule) => `<div class="row">
                      <div class="col">${schedule.day}</div>
                      <div class="col">${schedule.date}</div>
                      <div class="col">${schedule.time}</div>
                    </div>`
          )
          .join("");

        $(`.director-office-hours-${director.ID}`).html(html);
      });
  });
}

/*
    DJs / MEMBERS
*/
djs.on("change", "renderer", () => {
  updateDjs();
});

/**
 * Should be called whenever a DJ or the calendar has changed.
 */
function updateDjs() {
  animations.add("updateDjs", () => {
    let html = ``;

    // Process member blocks on web page
    djs
      .db({ active: true })
      .get()
      .sort((a, b) => b.ID - a.ID) // Newest DJs first
      .forEach((dj) => {
        // Determine if the dj is a director and/or sports broadcaster so we can add ribbons and borders to their block.
        let isDirector = directors.find(
          (director) => director.name === dj.realName,
          true
        );
        let isBroadcaster =
          dj.permissions && dj.permissions.indexOf("sportsbroadcaster") !== -1;

        html += `<div class="col-xl-6 col-12">
              <div class="card mb-3${
                isDirector || isBroadcaster ? ` border` : ``
              }${
          isDirector
            ? ` border-warning`
            : isBroadcaster
            ? ` border-success`
            : ``
        }">
              ${
                isDirector
                  ? `<div class="ribbon-wrapper ribbon-lg">
              <div class="ribbon bg-warning">DIRECTOR</div>
            </div>`
                  : ``
              }
              ${
                isBroadcaster
                  ? `<div class="ribbon-wrapper ribbon-xl">
              <div class="ribbon bg-success">SPORTS BROADCASTER</div>
            </div>`
                  : ``
              }
                <div class="row no-gutters">
                  <div class="col-md-3">
                    ${
                      dj.avatar
                        ? `<img src="/api/uploads/${dj.avatar}" alt="Member Avatar for ${dj.name}" style="width: 100%" class="rounded" loading="lazy" />`
                        : `<div class="bg-danger rounded" alt="Member Avatar for ${
                            dj.name
                          }" style="width: 140px;">${jdenticon.toSvg(
                            `Member ${dj.name}`,
                            140
                          )}</div>`
                    }
                  </div>
                  <div class="col-md-8">
                    <div class="card-header">
                      <h2 class="h3 d-block text-wrap" style="max-width: 80%;">${
                        dj.name
                      }</h2>
                      <h3 class="h4 d-block text-wrap" style="max-width: 80%;"><small>${
                        dj.realName || "Real name not specified"
                      }</small></h3>
                    </div>
                    <div class="card-body">
                        ${dj.profile || `<p>No profile provided.</p>`}
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <h3 class="h5"><strong>Hosts / Co-hosts these shows:</strong></h3>
                    <ul class="calendar-shows-dj-${dj.ID}"></ul></div>
                </div>
            </div>`;
      });
    $("#section-members-cards").html(html);
    window.requestAnimationFrame(() => updateDjShows());
  });
}

/*
    LIKED TRACKS / TRACK HISTORY
*/

likedtracks.on("init", "renderer", () => {
  updateTrackHistory();
});
likedtracks.on("likedTrack", "renderer", () => {
  updateTrackHistory();
});
likedtracks.on("likedTrackManual", "renderer", () => {
  updateTrackHistory();
});

/**
 * Update the table of recently played tracks.
 */
function updateTrackHistory() {
  animations.add("updateTrackHistory", () => {
    let tableBody = ``;
    meta.meta.history.forEach((track) => {
      tableBody += `<tr>
          <td>${moment.tz(track.time, moment.tz.guess()).format("LLL")}</td>
          <td>${track.track}</td>
          <td><div class="btn-group" role="group" aria-label="Basic example">${
            track.ID !== 0
              ? `${
                  !likedtracks.likedTracks.find(
                    (lt) =>
                      (lt.trackID && lt.trackID === track.ID) ||
                      lt.track === track.track
                  )
                    ? `<button type="button" ${
                        track.ID
                          ? `data-id="${track.ID}"`
                          : `data-track="${util.escapeHTML(track.track)}}"`
                      } class="btn btn-success btn-small button-track-like" tabindex="0" title="Like this track; liked tracks play more often on WWSU."><i class="fas fa-thumbs-up p-1"></i> Like</button>`
                    : `<button type="button" class="btn btn-outline-info btn-small disabled" tabindex="0" title="You already liked this track recently."><i class="far fa-thumbs-up p-1"></i> Liked</button>`
                }`
              : ``
          }<button type="button"${track.ID ? ` data-track="${track.ID}"` : ``}${
        track.attendanceID ? ` data-attendance="${track.attendanceID}"` : ``
      } data-meta="${util.escapeHTML(
        track.track
      )}" class="btn btn-danger btn-small button-track-report" tabindex="0" title="Report this track for inappropriate content."><i class="fas fa-flag p-1"></i> Report</button></div></td>
        </tr>`;
    });
    $(".section-nowplaying-card-history").html(tableBody);

    // Action button click events
    $(".button-track-like").unbind("click");
    $(".button-track-like").click((e) => {
      likedtracks.likeTrack(
        $(e.currentTarget).data("id")
          ? parseInt($(e.currentTarget).data("id"))
          : $(e.currentTarget).data("track")
      );
    });
    $(".button-track-report").unbind("click");
    $(".button-track-report").click((e) => {
      flags.flagForm({
        trackID: $(e.currentTarget).data("track")
          ? parseInt($(e.currentTarget).data("track"))
          : undefined,
        attendanceID: $(e.currentTarget).data("attendance")
          ? parseInt($(e.currentTarget).data("attendance"))
          : undefined,
        meta: $(e.currentTarget).data("meta"),
      });
    });
  });
}

/*
    MESSAGES
*/

// Initialize Messages
messages.initComponents(
  ".chat-status",
  ".chat-messages",
  ".chat-form",
  ".chat-new-messages",
  ".chat-messages-icon"
);

/*
    REQUESTS
*/

// Initialize requests
requests.initTable(
  ".section-request-card-tracks",
  "#request-name",
  "#request-genre",
  "#request-search",
  "#request-more"
);

/*
    ANNOUNCEMENTS
*/

announcements.on("change", "renderer", (db) => {
  processAnnouncements();
});

/**
 *  Update all announcements for the website.
 */
function processAnnouncements() {
  animations.add("processAnnouncements", () => {
    // Initialize types
    let html = {};
    announcementTypes.forEach((type) => {
      let typeB = type.replace("website-", "");
      let tmp = {};
      tmp[typeB] = "";
      html = Object.assign(html, tmp);
    });

    // Loop through all announcements
    announcements.db().each((announcement) => {
      // Check if an announcement is active
      if (
        moment(meta.meta.time).isAfter(moment(announcement.starts)) &&
        moment(meta.meta.time).isBefore(moment(announcement.expires))
      ) {
        // Make sure we are only checking website announcements
        if (announcement.type.startsWith("website-")) {
          let type = announcement.type.replace("website-", "");

          // Toast announcements should pop up if we did not already show them.
          if (
            type === "toast" &&
            announcementsToastIDs.indexOf(announcement.ID) === -1
          ) {
            announcementsToastIDs.push(announcement.ID);
            $(document).Toasts("create", {
              class: `bg-${announcement.level}`,
              title: announcement.title,
              subtitle: `Announcement`,
              autohide: true,
              delay: announcement.displayTime * 1000 || 15000,
              body: announcement.announcement,
              icon: "fas fa-bullhorn fa-lg",
            });

            // Else, it is a webpage announcement
          } else {
            if (typeof html[type] === "undefined") html[type] = "";
            html[type] += `<div class="alert alert-${announcement.level}">
                    <p class="h5">${announcement.title}</p>
                    ${announcement.announcement}
                  </div>`;
          }
        }
      }
    });

    // Display announcements on website
    for (let announcementType in html) {
      if (Object.prototype.hasOwnProperty.call(html, announcementType)) {
        $(`.announcements-${announcementType}`).html(
          html[announcementType] || ""
        );
      }
    }
  });
}

/*
    STATUS
*/
_status.on("change", "renderer", (db) => {
  // Display message about potential stream outage if stream status is less than 4
  let item = db.get().find((record) => record.name === "stream-public");
  if (!item || item.status < 4) {
    $(".status-stream-public").removeClass("d-none");
  } else {
    $(".status-stream-public").addClass("d-none");
  }
});

/*
    EAS
*/

eas.on("change", "renderer", (db) => {
  processEas();
});

/**
 *  Process EAS alerts
 */
function processEas() {
  animations.add("processEas", () => {
    let records = eas.db().get();

    // If there are EAS alerts in effect, show the weather bolt badge and colorize it according to most severe alert
    if (records.length > 0) {
      $(".nav-item-eas-alert").removeClass([
        "d-none",
        "pulse-danger",
        "pulse-warning",
      ]);

      // Determine severity of most severe alert
      let mostSevere = 5;
      records.forEach((record) => {
        switch (record.severity) {
          case "Extreme":
            if (mostSevere > 1) mostSevere = 1;
            break;
          case "Severe":
            if (mostSevere > 2) mostSevere = 2;
            break;
          case "Moderate":
            if (mostSevere > 3) mostSevere = 3;
            break;
          case "Minor":
            if (mostSevere > 4) mostSevere = 4;
            break;
          default:
            if (mostSevere > 5) mostSevere = 5;
            break;
        }
      });

      // Now, colorize and pulse the bolt icon according to most severe alert
      switch (mostSevere) {
        case 1:
          $(".nav-item-eas-alert").addClass("pulse-danger");
          break;
        case 2:
          $(".nav-item-eas-alert").addClass("pulse-warning");
          break;
      }
    } else {
      $(".nav-item-eas-alert").addClass("d-none");
    }
  });
}

/*
    BLOGS
*/

blogs.init(
  `#section-blogs-container`,
  `#section-blogs-list`,
  `#section-blogs-more`,
  `#section-blogs-search`,
  `#section-blogs-search-query`
);

/*
	  CONFIG
*/
config.on("change", "renderer", (system, db, query) => {
  animations.add(`configChange-${system}`, () => {
    if (system === "config-radiodjs") {
      // Only show certain elements when we are using RadioDJ
      if (!config.usingRadioDJ) {
        $(".config-control-usingradiodj").addClass("d-none");
      } else {
        $(".config-control-usingradiodj").removeClass("d-none");
      }
    }

    if (system === "config-basic") {
      let basic = config.basic;

      // Only display video stream navigation if owncast URL is defined.
      if (basic.owncastStream && basic.owncastStream !== "") {
        $(".config-control-usingowncast").removeClass("d-none");
        $(".config-basic-owncaststream").attr("href", basic.owncastStream);
      } else {
        $(".config-control-usingowncast").addClass("d-none");
      }

      // Update websites
      $(".config-basic-recordings").attr("href", basic.recordings);
      $(".config-basic-facebook").attr("href", basic.facebook);
      $(".config-basic-twitter").attr("href", basic.twitter);
      $(".config-basic-youtube").attr("href", basic.youtube);
      $(".config-basic-instagram").attr("href", basic.instagram);
      $(".config-basic-discord").attr("href", basic.discord);
    }
  });
});

"use strict";

/**
 * This class manages climaCell weather data from the WWSU API.
 *
 * @requires $ jQuery
 * @requires WWSUdb WWSU TAFFYdb wrapper
 * @requires WWSUanimations WWSU animations management
 * @requires moment moment.js time/date library
 */

// REQUIRES these WWSUmodules: noReq (WWSUreq), WWSUMeta, WWSUanimations, WWSUclocks (if doing 12-hour forecast clock)
class WWSUclimacell extends WWSUdb {
	/**
	 * The class constructor.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options = {}) {
		super(); // We do not use localStorage as climacell weather is very timely

		this.options = options;

		// Map weather code to condition string
		this.weatherCodeString = {
			0: "Unknown",
			1000: "Clear",
			1001: "Cloudy",
			1100: "Mostly Clear",
			1101: "Partly Cloudy",
			1102: "Mostly Cloudy",
			2000: "Fog",
			2100: "Light Fog",
			3000: "Light Wind",
			3001: "Wind",
			3002: "Strong Wind",
			4000: "Drizzle",
			4001: "Rain",
			4200: "Light Rain",
			4201: "Heavy Rain",
			5000: "Snow",
			5001: "Flurries",
			5100: "Light Snow",
			5101: "Heavy Snow",
			6000: "Freezing Drizzle",
			6001: "Freezing Rain",
			6200: "Light Freezing Rain",
			6201: "Heavy Freezing Rain",
			7000: "Ice Pellets",
			7101: "Heavy Ice Pellets",
			7102: "Light Ice Pellets",
			8000: "Downpour Rain",

			// Custom
			8100: "Whiteout Snow",
			8200: "Freezing Downpour",
			8300: "Hail",
		};

		// Map weather codes to icons (short)
		this.weatherCodeShort = {
			0: "Unknown",
			1000: "Clear",
			1001: "Cloudy",
			1100: "M. Clear",
			1101: "P. Cloudy",
			1102: "M. Cloudy",
			2000: "Fog",
			2100: "L. Fog",
			3000: "Breezy",
			3001: "Windy",
			3002: "V. Windy",
			4000: "Drizzle",
			4001: "Rain",
			4200: "L. Rain",
			4201: "H. Rain",
			5000: "Snow",
			5001: "Flurries",
			5100: "L. Snow",
			5101: "H. Snow",
			6000: "F. Drizzle",
			6001: "F. Rain",
			6200: "L.F. Rain",
			6201: "H.F. Rain",
			7000: "Ice",
			7101: "H. Ice",
			7102: "L. Ice",
			8000: "Downpour",

			// Custom
			8100: "Whiteout",
			8200: "F. Downpour",
			8300: "Hail",
		};

		// Map weather codes to colors
		this.weatherCodeColor = {
			0: "#000000",
			1000: "#FFD700",
			1001: "#665600",
			1100: "#FFD700",
			1101: "#B29600",
			1102: "#B29600",
			2000: "#665600",
			2100: "#665600",
			3000: "#7FBF7F",
			3001: "#008000",
			3002: "#004000",
			4000: "#B2B2FF",
			4001: "#6666FF",
			4200: "#B2B2FF",
			4201: "#0000FF",
			5000: "#787878",
			5001: "#AEAEAE",
			5100: "#AEAEAE",
			5101: "#484848",
			6000: "#E2A3FF",
			6001: "#CF66FF",
			6200: "#E2A3FF",
			6201: "#B000FF",
			7000: "#CF66FF",
			7101: "#B000FF",
			7102: "#E2A3FF",
			8000: "#FF0000",
			8100: "#000000",
			8200: "#4400FF",
			8300: "#4400FF",
		};

		// Map precipitation type to string
		this.precipitationTypeString = {
			0: "N/A",
			1: "Rain",
			2: "Snow",
			3: "Freezing Rain",
			4: "Ice Pellets",
		};

		// Map epaHealthConcern to string
		this.epaHealthConcernString = {
			0: "Good",
			1: "Moderate",
			2: "Sensitive",
			3: "Unhealthy",
			4: "Very Unhealthy",
			5: "Critical",
		};

		// UV Index Levels
		this.uvIndexString = {
			0: "None",
			1: "Low",
			2: "Low",
			3: "Moderate",
			4: "Moderate",
			5: "Moderate",
			6: "High",
			7: "High",
			8: "Very High",
			9: "Very High",
			10: "Very High",
			11: "Extreme",
			12: "Extreme",
			13: "Extreme",
			14: "Extreme",
			15: "Extreme",
		};

		// Determine min and max for progress bars
		this.progressRanges = {
			temperature: { min: -130, max: 140 },
			temperatureApparent: { min: -130, max: 140 },
			dewPoint: { min: 32, max: 120 },
			humidity: { min: 0, max: 100 },
			windSpeed: { min: 0, max: 73 },
			windGust: { min: 0, max: 73 },
			precipitationIntensity: { min: 0, max: 2 },
			precipitationProbability: { min: 0, max: 100 },
			visibility: { min: 0, max: 10 },
			cloudCover: { min: 0, max: 100 },
			pressureSurfaceLevel: { min: 28, max: 32 },
			pressureSeaLevel: { min: 27.6, max: 31.1 },
			uvIndex: { min: 0, max: 11 },
			snowDepth: { min: 0, max: 12 },
		};

		this.clock;

		this.manager = manager;

		this.endpoints = {
			get: "GET /api/weather",
		};
		this.data = {
			get: {},
		};

		this.ncTimer;

		if (!this.cache) {
			this.assignSocketEvent("climacell", this.manager.socket);

			// Data operations
			this.on("insert", "WWSUclimacell", (query) => {
				clearTimeout(this.ncTimer);
				this.ncTimer = setTimeout(() => {
					this.updateClock();
					this.updateData();
				}, 5000);
			});
			this.on("update", "WWSUclimacell", (query) => {
				clearTimeout(this.ncTimer);
				this.ncTimer = setTimeout(() => {
					this.updateClock();
					this.updateData();
				}, 5000);
			});
			this.on("remove", "WWSUclimacell", (query) => {
				clearTimeout(this.ncTimer);
				this.ncTimer = setTimeout(() => {
					this.updateClock();
					this.updateData();
				}, 5000);
			});
			this.on("replace", "WWSUclimacell", (db) => {
				clearTimeout(this.ncTimer);
				this.ncTimer = setTimeout(() => {
					this.updateClock();
					this.updateData();
				}, 5000);

				// WWSUmodules loading DOM check
				this.initialized = true;
				this.manager.checkInitialized();
			});
		} else {
			this.on("change", "WWSUclimacell", () => {
				clearTimeout(this.ncTimer);
				this.ncTimer = setTimeout(() => {
					this.updateClock();
					this.updateData();
				}, 5000);
			});
		}

		this.updateData();
	}

	/**
	 * Modify the weather record because tomorrow.io sometimes does weird things.
	 * Least priority modifications should be at the top.
	 *
	 * @param {object} record The weather record to adjust
	 * @returns {object} The adjusted weather record
	 */
	adjustedWeather(record) {
		// Modify weather based on wind gust
		if (record.data.windGust >= 34 || record.data.windSpeed >= 22)
			record.data.weatherCode = 3000; // Light Wind
		if (record.data.windGust >= 46 || record.data.windSpeed >= 31)
			record.data.weatherCode = 3001; // Wind (wind advisory criteria)
		if (record.data.windGust >= 58 || record.data.windSpeed >= 40)
			record.data.weatherCode = 3002; // Strong Wind (high wind warning criteria)

		// If a precip Type is set and intensity is > 0, weather code should reflect the precipitation event
		// We are also using our own scale of how heavy the precip is.
		if (
			record.data.precipitationType &&
			record.data.precipitationIntensity > 0
		) {
			switch (record.data.precipitationType) {
				case 1: // Rain
					record.data.weatherCode = 4000; // Default to lowest: drizzle
					if (record.data.precipitationIntensity >= 0.009)
						record.data.weatherCode = 4200; // Light Rain
					if (record.data.precipitationIntensity >= 0.19)
						record.data.weatherCode = 4001; // Rain
					if (record.data.precipitationIntensity >= 0.7)
						record.data.weatherCode = 4201; // Heavy Rain
					if (record.data.precipitationIntensity >= 4.7)
						record.data.weatherCode = 8000; // Downpour
					break;
				case 2: // Snow
					record.data.weatherCode = 5001; // Default to lowest: flurries
					if (record.data.precipitationIntensity >= 0.014)
						record.data.weatherCode = 5100; // Light Snow
					if (record.data.precipitationIntensity >= 0.07)
						record.data.weatherCode = 5000; // Snow
					if (record.data.precipitationIntensity >= 0.6)
						record.data.weatherCode = 5101; // Heavy Snow
					if (record.data.precipitationIntensity >= 2.36)
						record.data.weatherCode = 8100; // Whiteout Snow
					break;
				case 3: // Freezing Rain
					record.data.weatherCode = 6000; // Default to lowest: freezing drizzle
					if (record.data.precipitationIntensity >= 0.014)
						record.data.weatherCode = 6200; // Light Freezing Rain
					if (record.data.precipitationIntensity >= 0.07)
						record.data.weatherCode = 6001; // Freezing Rain
					if (record.data.precipitationIntensity >= 0.6)
						record.data.weatherCode = 6201; // Heavy Freezing Rain
					if (record.data.precipitationIntensity >= 2.36)
						record.data.weatherCode = 8200; // Freezing Downpour
					break;
				case 4: // Ice
					record.data.weatherCode = 6000; // Default to lowest: freezing drizzle
					if (record.data.precipitationIntensity >= 0.014)
						record.data.weatherCode = 7102; // Light Ice Pellets
					if (record.data.precipitationIntensity >= 0.07)
						record.data.weatherCode = 7000; // Ice Pellets
					if (record.data.precipitationIntensity >= 0.6)
						record.data.weatherCode = 7101; // Heavy Ice Pellets
					if (record.data.precipitationIntensity >= 2.36)
						record.data.weatherCode = 8300; // Hail
					break;
			}
		}

		return record;
	}

	// Initialize connection. Call this on socket connect event.
	init() {
		this.initialized = false;
		this.manager.checkInitialized();

		this.replaceData(
			this.manager.get("noReq"),
			this.endpoints.get,
			this.data.get
		);
	}

	/**
	 * Initialize a 12-hour forecast clock.
	 *
	 * @param {string} name Name to use for the clock
	 * @param {string} dom DOM container (must be position relative) to place the forecast clock.
	 * @param {string} size The size of the clock height/width
	 */
	initClockForecast(name, dom) {
		this.clock = name;
		this.manager.get("WWSUclocks").new(
			name,
			dom,
			{
				labels: ["Not Yet Loaded"],
				datasets: [
					{
						data: [720],
						backgroundColor: ["#000000"],
					},
				],
			},
			{
				maintainAspectRatio: false,
				responsive: true,
				cutoutPercentage: 80,
				plugins: {
					tooltip: false,
					legend: false,
				},
				animation: {
					animateRotate: false,
					animateScale: false,
				},
				elements: {
					arc: {
						borderWidth: 0,
					},
				},
			}
		);

		this.updateClock();
	}

	/**
	 * Update the Donut 12-hour forecast
	 */
	updateClock() {
		if (!this.clock) return;
		this.manager.get("WWSUanimations").add("climacell-forecast-update", () => {
			// If cache, just load cache clockwheel info into the clock and exit.
			if (this.cache) {
				// Update the clockwheel
				let data = this.find({ ID: "climacell-clockwheel" }, true);
				console.dir(data);
				if (data) {
					// Update the donut
					this.manager.get("WWSUclocks").updateChart(this.clock, data);
				}

				// Update the forecast description
				let innerHtml = ``;
				data = this.find({ ID: "climacell-forecast-hourly-12h" }, true);
				if (data && data.forecast) {
					for (let key in data.forecast) {
						if (
							!Object.prototype.hasOwnProperty.call(data.forecast, key) ||
							typeof data.forecast[key] !== "object" ||
							!data.forecast[key].weather ||
							!data.forecast[key].temperature
						)
							continue;

						innerHtml += `<div class="row">
			<div class="col-3"><strong>${key}</strong></div>
			<div class="col-6">${data.forecast[key].weather}</div>
			<div class="col-3">${data.forecast[key].temperature}</div>
			</div>`;
					}

					$(`#weather-forecast-description`).html(
						`<div class="container-fluid">${innerHtml}</div>`
					);
				}
				return;
			}

			let segments = [];

			// Initialize segments array
			for (let i = 0; i < 720; i++) {
				segments[i] = {};
			}

			let updateClockwheel = (weather, start, length) => {
				while (length > 0) {
					length--;
					start++;
					segments[start] = weather;
					if (start >= 720) {
						start -= 720;
					}
				}
			};

			// Determine what the exact date/time is for the "12" (start of the doughnut chart) on the clock
			let topOfClock = moment
				.parseZone(this.manager.get("WWSUMeta").meta.time)
				.startOf("day")
				.add(1, "days");
			if (
				moment.parseZone(this.manager.get("WWSUMeta").meta.time).hours() < 12
			) {
				topOfClock = moment.parseZone(topOfClock).subtract(12, "hours");
			}

			// Determine number of minutes from current time to topOfClock
			let untilTopOfClock = moment(topOfClock).diff(
				moment(this.manager.get("WWSUMeta").meta.time),
				"minutes"
			);

			// Initialize variables for tracking when the weather changes
			let weatherCode = 0;
			let weatherData = {};
			let totalLength = 0;
			let startTime = moment();

			// Initialize the above with current conditions
			let currently = this.db({ dataClass: "current-0" }).first();
			if (currently) {
				currently = this.adjustedWeather(currently);
				startTime = currently.dataTime;
				weatherCode = currently.data.weatherCode;
			}

			// Now process every record sorted by dataTime
			let currentTime = this.manager.get("WWSUMeta").meta.time;
			let innerHtml = ``;
			let shortTerm = this.db()
				.get()
				.sort((a, b) => {
					if (!a.dataTime) return 1;
					if (!b.dataTime) return -1;
					if (moment(a.dataTime).isBefore(moment(b.dataTime))) return -1;
					if (moment(b.dataTime).isBefore(moment(a.dataTime))) return 1;
					return 0;
				})
				.map((record) => {
					record = this.adjustedWeather(record);
					// Add weather descriptions
					if (record.dataClass === `current-0`) {
						innerHtml += `<div class="row">
			<div class="col-3"><strong>Now:</strong></div>
			<div class="col-6">${this.weatherCodeString[record.data.weatherCode]}</div>
			<div class="col-3">${parseInt(record.data.temperature)}°F</div>
			</div>`;
					} else if (
						record.dataClass.startsWith("1h-") &&
						record.dataClass !== `1h-0` &&
						moment(currentTime)
							.add(12, "hours")
							.isSameOrAfter(moment(record.dataTime))
					) {
						innerHtml += `<div class="row">
			<div class="col-3"><strong>${moment(record.dataTime).format(
				"h:mm A"
			)}:</strong></div>
			<div class="col-6">${this.weatherCodeString[record.data.weatherCode]}</div>
			<div class="col-3">${parseInt(record.data.temperature)}°F</div>
			</div>`;
					}

					// Determine clockwheel segments
					if (
						!record.dataClass.startsWith("1h-") ||
						moment(currentTime)
							.add(6, "hours")
							.isBefore(moment(record.dataTime))
					) {
						if (
							moment(record.dataTime).isSameOrBefore(
								moment(currentTime),
								"minutes"
							)
						) {
							weatherCode = record.data.weatherCode;
							weatherData = record.data;
							startTime = currentTime;

							// If weatherCode changed, create a new segment on the chart
						} else if (weatherCode !== record.data.weatherCode) {
							// Calculate segment length and start
							let length = moment(record.dataTime).diff(startTime, "minutes");
							let start =
								720 -
								untilTopOfClock +
								moment(startTime).diff(currentTime, "minutes");

							// Correct length if it goes beyond 12 hours
							if (
								moment(record.dataTime).isAfter(
									moment.parseZone(currentTime).add(12, "hours"),
									"minutes"
								)
							) {
								let correction = moment(record.dataTime).diff(
									moment.parseZone(currentTime).add(12, "hours"),
									"minutes"
								);
								length -= correction;
							}

							if (start >= 720) {
								start -= 720;
							}

							// Add segment
							updateClockwheel(weatherData, start, length);

							// Update memory info
							startTime = record.dataTime;
							weatherCode = record.data.weatherCode;
							weatherData = record.data;
							totalLength += length;
						}
					}
				});

			$(`#weather-forecast-description`).html(
				`<div class="container-fluid">${innerHtml}</div>`
			);

			// Now, begin updating clockwheel
			let clockwheelDonutData = {
				labels: [],
				datasets: [
					{
						data: [],
						backgroundColor: [],
					},
				],
			};

			// Process donut segments
			let currentSegment = { weatherCode: null, minutes: 0 };
			segments.map((segment) => {
				// If we have a new id at this minute, create a new segment
				if (segment.weatherCode !== currentSegment.weatherCode) {
					clockwheelDonutData.labels.push(
						this.weatherCodeString[currentSegment.weatherCode]
					);
					clockwheelDonutData.datasets[0].data.push(currentSegment.minutes);
					clockwheelDonutData.datasets[0].backgroundColor.push(
						this.weatherCodeColor[currentSegment.weatherCode]
					);
					currentSegment = Object.assign({ minutes: 1 }, segment);
				} else {
					currentSegment.minutes++;
				}
			});
			// Push the last remaining segment into data
			clockwheelDonutData.labels.push(
				this.weatherCodeString[currentSegment.weatherCode]
			);
			clockwheelDonutData.datasets[0].data.push(currentSegment.minutes);
			clockwheelDonutData.datasets[0].backgroundColor.push(
				this.weatherCodeColor[currentSegment.weatherCode]
			);

			// Update the donut
			this.manager
				.get("WWSUclocks")
				.updateChart(this.clock, clockwheelDonutData);
		});
	}

	/**
	 * Refresh all data on DOM.
	 */
	updateData() {
		this.manager.get("WWSUanimations").add(`update-climacell`, () => {
			if (!this.cache) {
				this.db()
					.get()
					.map((query) => {
						query = this.adjustedWeather(query);
						for (let value in query.data) {
							if (!Object.prototype.hasOwnProperty.call(query.data, value))
								return;

							if (query.dataClass === "current-0") {
								if (value === "weatherCode") {
									$(`.climacell-quick-weather-string`).html(
										this.weatherCodeShort[query.data[value]]
									);
								}
								if (value === "temperature") {
									$(`.climacell-quick-weather-temperature`).html(
										Math.round(query.data[value])
									);
								}
							}

							$(`.climacell-${query.dataClass}-${value}`).html(
								query.data[value]
							);
							if (
								query.dataClass === "current-0" &&
								typeof this.progressRanges[value] !== "undefined"
							) {
								let height = 0;
								if (query.data[value] <= this.progressRanges[value].min) {
									height = 0;
								} else if (
									query.data[value] >= this.progressRanges[value].max
								) {
									height = 100;
								} else {
									let maxAtZero =
										this.progressRanges[value].max -
										this.progressRanges[value].min;
									let differential = 0 - this.progressRanges[value].min;
									let adjustedValue = query.data[value] + differential;
									height =
										(maxAtZero !== 0 ? adjustedValue / maxAtZero : 0) * 100;
								}
								$(`.climacell-${query.dataClass}-${value}-progress`).css(
									"height",
									`${height}%`
								);
							}

							if (value === "windDirection") {
								$(`.climacell-${query.dataClass}-${value}-card`).html(
									this.degToCard(parseInt(query.data[value]))
								);

								let blowingTowards =
									query.data[value] >= 180
										? query.data[value] - 180
										: query.data[value] + 180;
								$(`.climacell-${query.dataClass}-${value}-arrow`).css(
									"transform",
									`rotate(${blowingTowards}deg)`
								);
							}
							if (value === "weatherCode") {
								$(`.climacell-${query.dataClass}-${value}-string`).html(
									this.weatherCodeString[query.data[value]]
								);
								$(`.climacell-${query.dataClass}-${value}-icon`).attr(
									"src",
									`${this.options.iconPath}/${query.data[value]}.svg`
								);
							}
							if (value === "precipitationType") {
								$(`.climacell-${query.dataClass}-${value}-string`).html(
									this.precipitationTypeString[query.data[value]]
								);
							}
							if (value === "epaHealthConcern") {
								$(`.climacell-${query.dataClass}-${value}-string`).html(
									this.epaHealthConcernString[query.data[value]]
								);
							}
							if (value === "uvIndex") {
								$(`.climacell-${query.dataClass}-${value}-string`).html(
									this.uvIndexString[query.data[value]]
								);
							}
						}
					});
			} else {
				let data = this.find({ ID: "climacell-current-0" }, true);
				if (data) {
					for (let value in data) {
						if (!Object.prototype.hasOwnProperty.call(data, value)) return;

						$(`.climacell-current-0-${value}`).html(data[value]);

						if (value === "weatherCode") {
							$(`.climacell-quick-weather-string`).html(
								this.weatherCodeShort[data[value]]
							);
						}
						if (value === "temperature") {
							$(`.climacell-quick-weather-temperature`).html(
								Math.round(data[value])
							);
						}

						if (typeof this.progressRanges[value] !== "undefined") {
							let height = 0;
							if (data[value] <= this.progressRanges[value].min) {
								height = 0;
							} else if (data[value] >= this.progressRanges[value].max) {
								height = 100;
							} else {
								let maxAtZero =
									this.progressRanges[value].max -
									this.progressRanges[value].min;
								let differential = 0 - this.progressRanges[value].min;
								let adjustedValue = data[value] + differential;
								height =
									(maxAtZero !== 0 ? adjustedValue / maxAtZero : 0) * 100;
							}
							$(`.climacell-current-0-${value}-progress`).css(
								"height",
								`${height}%`
							);
						}
						if (value === "windDirection") {
							$(`.climacell-current-0-${value}-card`).html(
								this.degToCard(parseInt(data[value]))
							);
							let blowingTowards =
								data[value] >= 180 ? data[value] - 180 : data[value] + 180;
							$(`.climacell-current-0-${value}-arrow`).css(
								"transform",
								`rotate(${blowingTowards}deg)`
							);
						}
						if (value === "weatherCode") {
							$(`.climacell-current-0-${value}-string`).html(
								this.weatherCodeString[data[value]]
							);
							$(`.climacell-current-0-${value}-icon`).attr(
								"src",
								`${this.options.iconPath}/${data[value]}.svg`
							);
						}
						if (value === "precipitationType") {
							$(`.climacell-current-0-${value}-string`).html(
								this.precipitationTypeString[data[value]]
							);
						}

						if (value === "uvIndex") {
							$(`.climacell-current-0-${value}-string`).html(
								this.uvIndexString[data[value]]
							);
						}
					}
				}
			}
		});
	}

	/**
	 * Utility function to convert wind direction degrees to cardinal direction.
	 *
	 * @param {number} d Degrees
	 * @returns {string} cardinal direction
	 */
	degToCard(d) {
		if (11.25 <= d && d < 33.75) {
			return "NNE";
		} else if (33.75 <= d && d < 56.25) {
			return "NE";
		} else if (56.25 <= d && d < 78.75) {
			return "ENE";
		} else if (78.75 <= d && d < 101.25) {
			return "E";
		} else if (101.25 <= d && d < 123.75) {
			return "ESE";
		} else if (123.75 <= d && d < 146.25) {
			return "SE";
		} else if (146.25 <= d && d < 168.75) {
			return "SSE";
		} else if (168.75 <= d && d < 191.25) {
			return "S";
		} else if (191.25 <= d && d < 213.75) {
			return "SSW";
		} else if (213.75 <= d && d < 236.25) {
			return "SW";
		} else if (236.25 <= d && d < 258.75) {
			return "WSW";
		} else if (258.75 <= d && d < 281.25) {
			return "W";
		} else if (281.25 <= d && d < 303.75) {
			return "WNW";
		} else if (303.75 <= d && d < 326.25) {
			return "NW";
		} else if (326.25 <= d && d < 348.75) {
			return "NNW";
		} else {
			return "N";
		}
	}
}

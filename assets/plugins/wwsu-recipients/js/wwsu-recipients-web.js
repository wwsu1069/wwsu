"use strict";

// This class is a recipients helper for web-based recipients, such as listeners.

// REQUIRES these WWSUmodules: noReq (WWSUreq), WWSUevents
class WWSUrecipientsweb extends WWSUevents {
	/**
	 * The class constructor.
	 *
	 * @param {WWSUmodules} manager The modules class which initiated this module
	 * @param {object} options Options to be passed to this module
	 */
	constructor(manager, options) {
		super();

		this.manager = manager;

		this.endpoints = {
			addDisplay: "POST /api/recipients/display",
			addWeb: "POST /api/recipients/web",
			editWeb: "PUT /api/recipients/web",
		};

		this.recipient = {};
	}

	/**
	 * Add this host as a display recipient to the WWSU API (register as online).
	 *
	 * @param {string} host Name of the host being registered
	 * @param {function} cb Callback; recipient data as first parameter, boolean true = success, false = no success as second parameter
	 */
	addRecipientDisplay(host, cb) {
    this.initialized = false;
    this.manager.checkInitialized();

		this.manager.get("noReq").request(
			this.endpoints.addDisplay,
			{
				data: { host: host },
			},
			{},
			(body, resp) => {
        this.initialized = true;
        this.manager.checkInitialized();

				if (resp.statusCode < 400) {
					this.recipient = body;
					this.emitEvent("recipientChanged", [this.recipient]);
					if (body.label) {
						cb(body, true);
					} else {
						cb(body, false);
					}
				} else {
					cb({}, false);
				}
			}
		);
	}

	/**
	 * Add this host as a web recipient to the WWSU API (register as online).
	 *
	 * @param {?string} device OneSignal App ID if applicable (for notifications)
	 * @param {function} cb Callback; recipient data as first parameter, boolean true = success, false = no success as second parameter
	 */
	addRecipientWeb(device, cb) {
    this.initialized = false;
    this.manager.checkInitialized();

		this.manager.get("noReq").request(
			this.endpoints.addWeb,
			{
				data: { device: device },
			},
			{},
			(body, resp) => {
        this.initialized = true;
        this.manager.checkInitialized();

				if (resp.statusCode < 400) {
					this.recipient = body;
					this.emitEvent("recipientChanged", [this.recipient]);
					if (body.label) {
						cb(body, true);
					} else {
						cb(body, false);
					}
				} else {
					this.recipient = {};
					cb({}, false);
				}
			}
		);
	}

	/**
	 * Edit the nickname for a web recipient.
	 *
	 * @param {object} data The data to send to the API.
	 * @param {string} data.label The new nickname for this recipient if changing nickname
	 * @param {string} data.device The new device Id from subscriptions if changing device
	 * @param {?function} cb Callback.
	 */
	editRecipientWeb(data, cb) {
		this.manager.get("noReq").request(
			this.endpoints.editWeb,
			{
				data: data,
			},
			{ hideErrorToast: true },
			(body, resp) => {
				if (resp.statusCode < 400) {
					if (data.label) {
						this.recipient.label = `Web (${data.label})`;
					}
					if (data.device) {
						this.recipient.device = data.device;
					}
					this.emitEvent("recipientChanged", [this.recipient]);
					if (typeof cb === "function") cb();
				}
			}
		);
	}
}

"use strict";

// TODO: Find a way to get these values without using WWSUhosts
const productionHosts = [125];
const onairHosts = [120];

let disconnected = true;
let updateTimer;

// AnimateCSS JQuery extension
$.fn.extend({
  animateCss: function (animationName, callback) {
    let animationEnd = (function (el) {
      let animations = {
        animation: "animationend",
        OAnimation: "oAnimationEnd",
        MozAnimation: "mozAnimationEnd",
        WebkitAnimation: "webkitAnimationEnd",
      };

      for (let t in animations) {
        if (el.style[t] !== undefined) {
          return animations[t];
        }
      }
    })(document.createElement("div"));

    this.addClass("animated " + animationName).one(animationEnd, function () {
      $(this).removeClass("animated " + animationName);

      if (typeof callback === "function") {
        callback();
      }
    });

    return this;
  },
});

// Create restart function to restart the screen after 15 seconds if it does not connect.
let restart = setTimeout(() => {
  window.location.reload(true);
}, 15000);

// Define connections
//io.sails.url = "https://server.wwsu1069.org";
let socket = io.sails.connect();

// Add WWSU modules
let wwsumodules = new WWSUmodules(socket, "#modules-loading");
wwsumodules
  .add("WWSUonerror", WWSUonerror)
  .add("WWSUanimations", WWSUanimations, { loadingDOM: "#dom-loading" })
  .add(`WWSUutil`, WWSUutil)
  .add("noReq", WWSUreq, { host: "bookings" })
  .add("WWSUMeta", WWSUMeta)
  .add("WWSUdirectors", WWSUdirectors, { host: "bookings" })
  .add("WWSUdjs", WWSUdjs)
  .add("WWSUannouncements", WWSUannouncements, {
    types: [`bookings`],
  })
  .add("WWSUcalendar", WWSUcalendar)
  .add("WWSUlockdown", WWSUlockdown)
  .add("directorReq", WWSUreq, {
    host: "bookings",
    db: "WWSUdirectors",
    filter: null,
    usernameField: "name",
    authPath: "GET /api/auth/director",
    authName: "Director",
  })
  .add("djReq", WWSUreq, {
    host: "bookings",
    db: "WWSUdjs",
    filter: { active: true },
    usernameField: "name",
    authPath: "GET /api/auth/dj",
    authName: "DJ",
  })
  .add("WWSUconfig", WWSUconfig, { systems: ["basic"] });

// Reference modules to variables
let animations = wwsumodules.get("WWSUanimations");
let util = wwsumodules.get("WWSUutil");
let Meta = wwsumodules.get("WWSUMeta");
let noReq = wwsumodules.get("noReq");
let djReq = wwsumodules.get("djReq");
let directorReq = wwsumodules.get("directorReq");
let Announcements = wwsumodules.get("WWSUannouncements");
let Directors = wwsumodules.get("WWSUdirectors");
let Djs = wwsumodules.get("WWSUdjs");
let Calendar = wwsumodules.get("WWSUcalendar");
let Lockdown = wwsumodules.get("WWSUlockdown");
let Config = wwsumodules.get("WWSUconfig");

let modal = new WWSUmodal(`Book studio`, ``, ``, true, {
  overlayClose: false,
});

socket.on("connect", () => {
  $(".content-wrapper").removeClass("d-none");
  $("#reconnecting").addClass("d-none");

  Config.init();
  Meta.init();
  Announcements.init();
  Directors.init();
  Djs.init();
  Calendar.init();
  Lockdown.initSimple();

  if (disconnected) {
    // noConnection.style.display = "none";
    disconnected = false;
    clearTimeout(restart);
  }
});

socket.on("disconnect", () => {
  console.log("Lost connection");
  $(".content-wrapper").addClass("d-none");
  $("#reconnecting").removeClass("d-none");
  try {
    socket._raw.io._reconnection = true;
    socket._raw.io._reconnectionAttempts = Infinity;
  } catch (e) {
    console.error(e);
    $(document).Toasts("create", {
      class: "bg-danger",
      title: "Error reconnecting",
      body: "There was an error attempting to reconnect to WWSU. Please report this to the engineer at wwsu4@wright.edu.",
      icon: "fas fa-skull-crossbones fa-lg",
    });
  }
  if (!disconnected) {
    disconnected = true;
  }
});

socket.on("display-refresh", () => {
  window.location.reload(true);
});

$("#reserve-production").on("click", () => {
  showBookingForm("prod-booking");
});

$("#reserve-onair").on("click", () => {
  showBookingForm("onair-booking");
});

Meta.on("newMeta", "renderer", (newMeta, fullMeta) => {
  // Update station time
  animations.add("meta-time", () => {
    $(".meta-time").html(moment.parseZone(fullMeta.time).format("llll"));
  });
});

Meta.on("metaTick", "renderer", (fullMeta) => {
  // Update station time
  animations.add("meta-time", () => {
    $(".meta-time").html(moment.parseZone(fullMeta.time).format("llll"));
  });

  // Re-check bookings every minute
  if (moment(fullMeta.time).second() === 0) recalculateBookings();
});

Lockdown.on("change", "renderer", () => {
  recalculateBookings();
});

Calendar.on("calendarUpdated", "renderer", () => {
  recalculateBookings();
});

// Re-calculate status of the studio bookings
function recalculateBookings() {
  animations.add(`recalculateBookings`, () => {
    if (updateTimer) return;
    updateTimer = setTimeout(() => {
      // Get upcoming bookings
      let reservations = Calendar.getReservations();

      // Update reservation tables
      $("#reservations-production").html(
        reservations
          .filter((res) => res.type === "prod-booking")
          .map(
            (res) => `<tr>
        <td>${res.type}</td>
        <td>${res.hosts}</td>
        <td>${moment
          .tz(res.start, Meta.meta.timezone)
          .format("MM/DD LT")} - ${moment
              .tz(res.end, Meta.meta.timezone)
              .format("LT")}</td>
        </tr>`
          )
          .join("")
      );

      $("#reservations-onair").html(
        reservations
          .filter(
            (res) =>
              ["onair-booking", "show", "sports"].indexOf(res.type) !== -1
          )
          .map(
            (res) => `<tr>
          <td>${res.type}</td>
          <td>${res.hosts}</td>
          <td>${moment
            .tz(res.start, Meta.meta.timezone)
            .format("MM/DD LT")} - ${moment
              .tz(res.end, Meta.meta.timezone)
              .format("LT")}</td>
          </tr>`
          )
          .join("")
      );

      // Checking production studio
      (() => {
        // Check if in use
        let inUse = Lockdown.db()
          .get()
          .find(
            (record) =>
              !record.clockOut && productionHosts.indexOf(record.host) !== -1
          );
        if (inUse) {
          $("#status-production-card").removeClass([
            "bg-danger",
            "bg-orange",
            "bg-warning",
            "bg-success",
          ]);
          $("#status-production-card").addClass("bg-danger");

          // Get current bookings
          let reservedEvent;
          if (inUse.type === "DJ") {
            reservedEvent = reservations.find((record) => {
              // Record is not a scheduled prod booking; false
              if (
                record.type !== "prod-booking" ||
                !moment(Meta.meta.time).isBefore(record.end) ||
                !moment(Meta.meta.time)
                  .add(5, "minutes")
                  .isSameOrAfter(record.start)
              )
                return false;

              // Logged in DJ matches scheduled prod booking; return true.
              if (
                [
                  record.hostDJ,
                  record.cohostDJ1,
                  record.cohostDJ2,
                  record.cohostDJ3,
                ].indexOf(inUse.typeID) !== -1
              )
                return true;

              // Edge case: booking was scheduled for a director, but the director logged in as a DJ
              if (!record.director) return false;
              let directorRecord = Directors.find(
                { ID: record.director },
                true
              );
              if (!directorRecord) return false;

              let djRecord = Djs.find({ realName: directorRecord.name }, true);
              if (djRecord && djRecord.ID === inUse.typeID) return true;

              return false;
            });
          } else if (inUse.type === "director") {
            reservedEvent = reservations.find((record) => {
              // Record is not a scheduled prod booking; false
              if (
                record.type !== "prod-booking" ||
                !moment(Meta.meta.time).isBefore(record.end) ||
                !moment(Meta.meta.time)
                  .add(5, "minutes")
                  .isSameOrAfter(record.start)
              )
                return false;

              // Signed in director is scheduled for the booking; return true.
              if (record.director === inUse.typeID) return true;

              // Edge case: booking was scheduled for a DJ, but they logged in as a director
              if (
                !record.hostDJ &&
                !record.cohostDJ &&
                !record.cohostDJ2 &&
                !record.cohostDJ3
              )
                return false;
              let directorRecord = Directors.find({ ID: inUse.typeID }, true);
              if (!directorRecord) return false;
              let djRecord = Djs.find({ realName: directorRecord.name }, true);
              if (!djRecord) return false;
              if (
                [
                  record.hostDJ,
                  record.cohostDJ1,
                  record.cohostDJ2,
                  record.cohostDJ3,
                ].indexOf(djRecord.ID) !== -1
              )
                return true;

              return false;
            });
          }
          if (reservedEvent) {
            $("#status-production-text").html(
              `IN USE (next ${moment
                .duration(
                  moment(reservedEvent.end).diff(
                    moment(Meta.meta.time),
                    "minutes"
                  ),
                  "minutes"
                )
                .format("h [hrs], m [mins]")})`
            );
          } else {
            $("#status-production-text").html("IN USE");
          }

          return;
        }

        // Not in use? Check for an upcoming reservation.
        let upcoming = reservations.find(
          (record) =>
            record.type === "prod-booking" &&
            moment(Meta.meta.time)
              .add(60, "minutes")
              .isSameOrAfter(record.start) &&
            moment(Meta.meta.time).isBefore(record.end)
        );
        if (upcoming) {
          if (moment(Meta.meta.time).isBefore(upcoming.start)) {
            $("#status-production-card").removeClass([
              "bg-danger",
              "bg-orange",
              "bg-warning",
              "bg-success",
            ]);
            $("#status-production-card").addClass("bg-warning");
            $("#status-production-text").html(
              `AVAILABLE (for ${moment
                .duration(
                  moment(upcoming.start).diff(Meta.meta.time, "minutes"),
                  "minutes"
                )
                .format("m [mins]")})`
            );
          } else {
            $("#status-production-card").removeClass([
              "bg-danger",
              "bg-orange",
              "bg-warning",
              "bg-success",
            ]);
            $("#status-production-card").addClass("bg-orange");
            $("#status-production-text").html(
              `RESERVED (next ${moment
                .duration(
                  moment(upcoming.end).diff(moment(Meta.meta.time), "minutes"),
                  "minutes"
                )
                .format("m [mins]")})`
            );
          }

          return;
        }

        // At this point, studio is available
        $("#status-production-card").removeClass([
          "bg-danger",
          "bg-orange",
          "bg-warning",
          "bg-success",
        ]);
        $("#status-production-card").addClass("bg-success");
        $("#status-production-text").html("AVAILABLE next hour");
      })();

      // Checking on-air studio
      (() => {
        // Check if in use
        let inUse = Lockdown.db()
          .get()
          .find(
            (record) =>
              !record.clockOut && onairHosts.indexOf(record.host) !== -1
          );
        if (inUse) {
          $("#status-onair-card").removeClass([
            "bg-danger",
            "bg-orange",
            "bg-warning",
            "bg-success",
          ]);
          $("#status-onair-card").addClass("bg-danger");

          // Get current bookings
          let reservedEvent;
          if (inUse.type === "DJ") {
            reservedEvent = reservations.find((record) => {
              // Record is not a scheduled onair booking; false
              if (
                ["onair-booking", "show", "sports"].indexOf(record.type) ===
                  -1 ||
                !moment(Meta.meta.time).isBefore(record.end) ||
                !moment(Meta.meta.time)
                  .add(5, "minutes")
                  .isSameOrAfter(record.start)
              )
                return false;

              // Logged in DJ matches scheduled onair booking; return true.
              if (
                [
                  record.hostDJ,
                  record.cohostDJ1,
                  record.cohostDJ2,
                  record.cohostDJ3,
                ].indexOf(inUse.typeID) !== -1
              )
                return true;

              // Edge case: booking was scheduled for a director, but the director logged in as a DJ
              if (!record.director) return false;
              let directorRecord = Directors.find(
                { ID: record.director },
                true
              );
              if (!directorRecord) return false;

              let djRecord = Djs.find({ realName: directorRecord.name }, true);
              if (djRecord && djRecord.ID === inUse.typeID) return true;

              return false;
            });
          } else if (inUse.type === "director") {
            reservedEvent = reservations.find((record) => {
              // Record is not a scheduled onair booking; false
              if (
                ["onair-booking", "show", "sports"].indexOf(record.type) ===
                  -1 ||
                !moment(Meta.meta.time).isBefore(record.end) ||
                !moment(Meta.meta.time)
                  .add(5, "minutes")
                  .isSameOrAfter(record.start)
              )
                return false;

              // Signed in director is scheduled for the booking; return true.
              if (record.director === inUse.typeID) return true;

              // Edge case: booking was scheduled for a DJ, but they logged in as a director
              if (
                !record.hostDJ &&
                !record.cohostDJ &&
                !record.cohostDJ2 &&
                !record.cohostDJ3
              )
                return false;
              let directorRecord = Directors.find({ ID: inUse.typeID }, true);
              if (!directorRecord) return false;
              let djRecord = Djs.find({ realName: directorRecord.name }, true);
              if (!djRecord) return false;
              if (
                [
                  record.hostDJ,
                  record.cohostDJ1,
                  record.cohostDJ2,
                  record.cohostDJ3,
                ].indexOf(djRecord.ID) !== -1
              )
                return true;

              return false;
            });
          }
          if (reservedEvent) {
            $("#status-onair-text").html(
              `IN USE (next ${moment
                .duration(
                  moment(reservedEvent.end).diff(
                    moment(Meta.meta.time),
                    "minutes"
                  ),
                  "minutes"
                )
                .format("h [hrs], m [mins]")})`
            );
          } else {
            $("#status-onair-text").html("IN USE");
          }

          return;
        }

        // Not in use? Check for an upcoming reservation.
        let upcoming = reservations.find(
          (record) =>
            ["onair-booking", "show", "sports"].indexOf(record.type) !== -1 &&
            moment(Meta.meta.time)
              .add(60, "minutes")
              .isSameOrAfter(record.start) &&
            moment(Meta.meta.time).isBefore(record.end)
        );
        if (upcoming) {
          if (moment(Meta.meta.time).isBefore(upcoming.start)) {
            $("#status-onair-card").removeClass([
              "bg-danger",
              "bg-orange",
              "bg-warning",
              "bg-success",
            ]);
            $("#status-onair-card").addClass("bg-warning");
            $("#status-onair-text").html(
              `AVAILABLE (for ${moment
                .duration(
                  moment(upcoming.start).diff(Meta.meta.time, "minutes"),
                  "minutes"
                )
                .format("m [mins]")})`
            );
          } else {
            $("#status-onair-card").removeClass([
              "bg-danger",
              "bg-orange",
              "bg-warning",
              "bg-success",
            ]);
            $("#status-onair-card").addClass("bg-orange");
            $("#status-onair-text").html(
              `RESERVED (next ${moment
                .duration(
                  moment(upcoming.end).diff(moment(Meta.meta.time), "minutes"),
                  "minutes"
                )
                .format("m [mins]")})`
            );
          }

          return;
        }

        // At this point, studio is available
        $("#status-onair-card").removeClass([
          "bg-danger",
          "bg-orange",
          "bg-warning",
          "bg-success",
        ]);
        $("#status-onair-card").addClass("bg-success");
        $("#status-onair-text").html("AVAILABLE next hour");
      })();

      updateTimer = undefined;
    }, 1000);
  });
}

function showBookingForm(type) {
  modal.body = ``;
  modal.title = type;
  modal.iziModal("open");

  $(modal.body).alpaca({
    schema: {
      title: type,
      type: "object",
      properties: {
        type: {
          type: "string",
        },
        start: {
          title: "Start Date / Time",
          format: "date",
          required: true,
        },
        duration: {
          title: "End Date / Time",
          format: "time",
          required: true,
        },
        reason: {
          title: "Reason(s) for booking",
          type: "string",
          required: true,
          maxLength: 256,
        },
      },
    },
    options: {
      fields: {
        type: {
          type: "hidden",
        },
        start: {
          dateFormat: `YYYY-MM-DD hh:mm A`,
          picker: {
            inline: true,
            sideBySide: true,
          },
          helper: `Use the station timezone.`,
        },
        duration: {
          dateFormat: `YYYY-MM-DD hh:mm A`,
          picker: {
            inline: true,
            sideBySide: true,
          },
          helper: `Use the station timezone.`,
          validator: function (callback) {
            let value = this.getValue();
            let start =
              this.getParent().childrenByPropertyId["start"].getValue();

            if (moment(value).diff(moment(start), "minutes") <= 0) {
              callback({
                status: false,
                message: "End Date / Time must be after Start Date / Time.",
              });
              return;
            }

            callback({
              status: true,
            });
          },
        },
        reason: {
          type: "textarea",
          helper: "Specify for what you will be using the studio.",
        },
      },
      form: {
        buttons: {
          dj: {
            title: "Book as Org Member",
            styles: "btn bg-primary",
            click: (form, e) => {
              form.refreshValidationState(true);
              if (!form.isValid(true)) {
                form.focus();
                return;
              }
              let value = form.getValue();

              value.duration = moment(value.duration).diff(
                moment(value.start),
                "minutes"
              );

              // Add timezone to start and convert to ISO String
              if (value.start) {
                value.start = `${moment
                  .utc(value.start, "YYYY-MM-DD hh:mm A")
                  .format("YYYY-MM-DD[T]HH:mm:ss")}${moment
                  .tz(value.start, Meta.timezone)
                  .format("ZZ")}`;
              }

              // Request authentication
              djReq._promptLogin((username, password) => {
                djReq._authorize(username, password, (token, resp, url) => {
                  if (
                    resp.statusCode >= 400 ||
                    typeof token.token === "undefined"
                  ) {
                    $(document).Toasts("create", {
                      class: "bg-danger",
                      title: "Error Authorizing",
                      body: "There was an error authorizing. Did you type your password in correctly?",
                      autohide: true,
                      delay: 10000,
                      icon: "fas fa-skull-crossbones fa-lg",
                    });
                  } else {
                    // Block modal while we attempt the request
                    $(`#modal-${modal.id} .modal-content`)
                      .prepend(`<div class="overlay">
                                  <i class="fas fa-2x fa-sync fa-spin"></i>
                                  <h1 class="text-white">Processing...</h1>
                                </div>`);
                    djReq._tryRequest(
                      "POST /api/bookings/dj",
                      {
                        data: value,
                      },
                      {},
                      (body2, resp) => {
                        $(`#modal-${modal.id} .overlay`).remove();
                        if (resp.statusCode < 400) {
                          modal.iziModal("close");
                          $(document).Toasts("create", {
                            class: "bg-success",
                            title: "Studio Reserved",
                            body: `You successfully reserved the studio.<br /><br /><strong>Please email wwsu2@wright.edu if you must cancel or re-schedule.</strong> Also, you will be emailed if your booking changes.`,
                            autohide: true,
                            delay: 20000,
                          });
                        } else {
                          $(document).Toasts("create", {
                            class: "bg-warning",
                            title: "Error Reserving Studio",
                            body: body2,
                            autohide: true,
                            delay: 20000,
                          });
                        }
                      }
                    );
                  }
                });
              });
            },
          },
          director: {
            title: "Book as Director",
            styles: "btn bg-warning",
            click: (form, e) => {
              form.refreshValidationState(true);
              if (!form.isValid(true)) {
                form.focus();
                return;
              }
              let value = form.getValue();

              value.duration = moment(value.duration).diff(
                moment(value.start),
                "minutes"
              );

              // Add timezone to start and convert to ISO String
              if (value.start) {
                value.start = `${moment
                  .utc(value.start, "YYYY-MM-DD hh:mm A")
                  .format("YYYY-MM-DD[T]HH:mm:ss")}${moment
                  .tz(value.start, Meta.timezone)
                  .format("ZZ")}`;
              }

              // Request authentication
              directorReq._promptLogin((username, password) => {
                directorReq._authorize(
                  username,
                  password,
                  (token, resp, url) => {
                    if (
                      resp.statusCode >= 400 ||
                      typeof token.token === "undefined"
                    ) {
                      $(document).Toasts("create", {
                        class: "bg-danger",
                        title: "Error Authorizing",
                        body: "There was an error authorizing. Did you type your password in correctly?",
                        autohide: true,
                        delay: 10000,
                        icon: "fas fa-skull-crossbones fa-lg",
                      });
                    } else {
                      // Block modal while we attempt the request
                      $(`#modal-${modal.id} .modal-content`)
                        .prepend(`<div class="overlay">
                                  <i class="fas fa-2x fa-sync fa-spin"></i>
                                  <h1 class="text-white">Processing...</h1>
                                </div>`);
                      directorReq._tryRequest(
                        "POST /api/bookings/director",
                        {
                          data: value,
                        },
                        {},
                        (body2, resp) => {
                          $(`#modal-${modal.id} .overlay`).remove();
                          if (resp.statusCode < 400) {
                            modal.iziModal("close");
                            $(document).Toasts("create", {
                              class: "bg-success",
                              title: "Studio Reserved",
                              body: `You successfully reserved the studio.<br /><br /><strong>Please email wwsu2@wright.edu if you must cancel or re-schedule.</strong> Also, you will be emailed if your booking changes.`,
                              autohide: true,
                              delay: 20000,
                            });
                          } else {
                            $(document).Toasts("create", {
                              class: "bg-warning",
                              title: "Error Reserving Studio",
                              body: body2,
                              autohide: true,
                              delay: 20000,
                            });
                          }
                        }
                      );
                    }
                  }
                );
              });
            },
          },
        },
      },
    },
    data: { type: type },
  });
}

"use strict";

// This class manages the WWSU basketball shootout scoreboard
// REQUIRES: noReq, directorReq (if using web interface to control scoreboard)
class WWSUshootout extends WWSUdb {
  /**
   * Create the announcements class.
   *
   * @param {WWSUmodules} manager The modules class which initiated this module
   * @param {object} options Options to be passed to this module
   * @param {string} options.username If this is the web interface, the DOM query string for the text field containing the director
   * @param {string} options.password If this is the web interface, the DOM query string for the text field containing the password
   */
  constructor(manager, options) {
    super(); // Create the db

    this.manager = manager;

    this.endpoints = {
      get: "GET /api/shootout",
      set: "PUT /api/shootout/:name",
    };
    this.data = {
      get: {},
    };

    this.assignSocketEvent("shootout", this.manager.socket);

    this.username = options ? options.username : undefined;
    this.password = options ? options.password : undefined;

    this.on("replace", "WWSUshootout", () => {
      // WWSUmodules loading DOM check
      this.initialized = true;
      this.manager.checkInitialized();
    });
  }

  // Initialize the connection and get initial data; should be called on socket connect event.
  init() {
    this.initialized = false;
    this.manager.checkInitialized();

    this.replaceData(
      this.manager.get("noReq"),
      this.endpoints.get,
      this.data.get
    );
  }

  /**
   * Set something in the API for the shootout.
   *
   * @requires options.user and options.password to be set upon creation of the class.
   * @param {string} name Name of the value to set as defined in the shootout model.
   * @param {string} value Value to set.
   */
  set(name, value) {
    let req = this.manager.get("directorReq");
    req._authorize(
      $(this.username).val(),
      $(this.password).val(),
      (body, res) => {
        if (res.statusCode < 400) {
          req._tryRequest(
            this.endpoints.set,
            {
              data: { name: name, value: value },
            },
            {},
            (body2, resp2) => {}
          );
        } else {
          $(document).Toasts("create", {
            class: "bg-danger",
            title: "Error authorizing",
            body: "Authorization credentials are incorrect.",
            autohide: true,
            delay: 15000,
            icon: "fas fa-skull-crossbones",
          });
        }
      }
    );
  }
}

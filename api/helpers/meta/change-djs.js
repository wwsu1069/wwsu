module.exports = {
  friendlyName: "meta.changeDjs",

  description:
    "Update meta DJ ID numbers based on provided show name. Create DJs that do not exist. Update DJ lastSeen records.",

  inputs: {
    show: {
      type: "string",
      required: true,
      description:
        "The name of the show in the format hosts (separated by ;) - show name",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Helper meta.changeDjs called.");

    var returnData = {
      hostDJ: null,
      cohostDJ1: null,
      cohostDJ2: null,
      cohostDJ3: null,
    };
    var show;
    var temp;

    var temp = ["Unknown Hosts"];

    if (inputs.show.includes(" - ")) {
      // Split DJ and show
      var temp = inputs.show.split(" - ")[0];
      show = inputs.show.split(" - ")[1];
      temp = temp.split("; ");

      // Determine who the DJs are and create them if they do not exist
      if (temp[0] && temp[0] !== "Unknown Hosts") {
        returnData.hostDJ = await sails.models.djs.findOrCreate(
          { name: temp[0] },
          { name: temp[0], lastSeen: moment().format("YYYY-MM-DD HH:mm:ss") }
        );
        returnData.hostDJ = returnData.hostDJ.ID;
      }
      if (temp[1]) {
        returnData.cohostDJ1 = await sails.models.djs.findOrCreate(
          { name: temp[1] },
          { name: temp[1], lastSeen: moment().format("YYYY-MM-DD HH:mm:ss") }
        );
        returnData.cohostDJ1 = returnData.cohostDJ1.ID;
      }
      if (temp[2]) {
        returnData.cohostDJ2 = await sails.models.djs.findOrCreate(
          { name: temp[2] },
          { name: temp[2], lastSeen: moment().format("YYYY-MM-DD HH:mm:ss") }
        );
        returnData.cohostDJ2 = returnData.cohostDJ2.ID;
      }
      if (temp[3]) {
        returnData.cohostDJ3 = await sails.models.djs.findOrCreate(
          { name: temp[3] },
          { name: temp[3], lastSeen: moment().format("YYYY-MM-DD HH:mm:ss") }
        );
        returnData.cohostDJ3 = returnData.cohostDJ3.ID;
      }

      // Update lastSeen record for the DJs
      if (returnData.hostDJ !== null) {
        await sails.models.djs
          .update(
            { ID: returnData.hostDJ },
            { lastSeen: moment().format("YYYY-MM-DD HH:mm:ss") }
          )
          .fetch();
      }
      if (returnData.cohostDJ1 !== null) {
        await sails.models.djs
          .update(
            { ID: returnData.cohostDJ1 },
            { lastSeen: moment().format("YYYY-MM-DD HH:mm:ss") }
          )
          .fetch();
      }
      if (returnData.cohostDJ2 !== null) {
        await sails.models.djs
          .update(
            { ID: returnData.cohostDJ2 },
            { lastSeen: moment().format("YYYY-MM-DD HH:mm:ss") }
          )
          .fetch();
      }
      if (returnData.cohostDJ3 !== null) {
        await sails.models.djs
          .update(
            { ID: returnData.cohostDJ3 },
            { lastSeen: moment().format("YYYY-MM-DD HH:mm:ss") }
          )
          .fetch();
      }
    }

    await sails.helpers.meta.change.with(returnData);
    return returnData;
  },
};

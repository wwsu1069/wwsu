const weatherCodeString = {
  0: "Unknown",
  1000: "Clear",
  1001: "Cloudy",
  1100: "Mostly Clear",
  1101: "Partly Cloudy",
  1102: "Mostly Cloudy",
  2000: "Fog",
  2100: "Light Fog",
  3000: "Light Wind",
  3001: "Wind",
  3002: "Strong Wind",
  4000: "Drizzle",
  4001: "Rain",
  4200: "Light Rain",
  4201: "Heavy Rain",
  5000: "Snow",
  5001: "Flurries",
  5100: "Light Snow",
  5101: "Heavy Snow",
  6000: "Freezing Drizzle",
  6001: "Freezing Rain",
  6200: "Light Freezing Rain",
  6201: "Heavy Freezing Rain",
  7000: "Ice Pellets",
  7101: "Heavy Ice Pellets",
  7102: "Light Ice Pellets",
  8000: "Downpour Rain",

  // Custom
  8100: "Whiteout Snow",
  8200: "Freezing Downpour",
  8300: "Hail",
};

const weatherCodeColor = {
  0: "#000000",
  1000: "#FFD700",
  1001: "#665600",
  1100: "#FFD700",
  1101: "#B29600",
  1102: "#B29600",
  2000: "#665600",
  2100: "#665600",
  3000: "#7FBF7F",
  3001: "#008000",
  3002: "#004000",
  4000: "#B2B2FF",
  4001: "#6666FF",
  4200: "#B2B2FF",
  4201: "#0000FF",
  5000: "#787878",
  5001: "#AEAEAE",
  5100: "#AEAEAE",
  5101: "#484848",
  6000: "#E2A3FF",
  6001: "#CF66FF",
  6200: "#E2A3FF",
  6201: "#B000FF",
  7000: "#CF66FF",
  7101: "#B000FF",
  7102: "#E2A3FF",
  8000: "#FF0000",
  8100: "#000000",
  8200: "#4400FF",
  8300: "#4400FF",
};

const adjustedWeather = (record) => {
  // Modify weather based on wind gust
  if (record.data.windGust >= 34 || record.data.windSpeed >= 22)
    record.data.weatherCode = 3000; // Light Wind
  if (record.data.windGust >= 46 || record.data.windSpeed >= 31)
    record.data.weatherCode = 3001; // Wind (wind advisory criteria)
  if (record.data.windGust >= 58 || record.data.windSpeed >= 40)
    record.data.weatherCode = 3002; // Strong Wind (high wind warning criteria)

  // If a precip Type is set and intensity is > 0, weather code should reflect the precipitation event
  // We are also using our own scale of how heavy the precip is.
  if (record.data.precipitationType && record.data.precipitationIntensity > 0) {
    switch (record.data.precipitationType) {
      case 1: // Rain
        record.data.weatherCode = 4000; // Default to lowest: drizzle
        if (record.data.precipitationIntensity >= 0.009)
          record.data.weatherCode = 4200; // Light Rain
        if (record.data.precipitationIntensity >= 0.19)
          record.data.weatherCode = 4001; // Rain
        if (record.data.precipitationIntensity >= 0.7)
          record.data.weatherCode = 4201; // Heavy Rain
        if (record.data.precipitationIntensity >= 4.7)
          record.data.weatherCode = 8000; // Downpour
        break;
      case 2: // Snow
        record.data.weatherCode = 5001; // Default to lowest: flurries
        if (record.data.precipitationIntensity >= 0.014)
          record.data.weatherCode = 5100; // Light Snow
        if (record.data.precipitationIntensity >= 0.07)
          record.data.weatherCode = 5000; // Snow
        if (record.data.precipitationIntensity >= 0.6)
          record.data.weatherCode = 5101; // Heavy Snow
        if (record.data.precipitationIntensity >= 2.36)
          record.data.weatherCode = 8100; // Whiteout Snow
        break;
      case 3: // Freezing Rain
        record.data.weatherCode = 6000; // Default to lowest: freezing drizzle
        if (record.data.precipitationIntensity >= 0.014)
          record.data.weatherCode = 6200; // Light Freezing Rain
        if (record.data.precipitationIntensity >= 0.07)
          record.data.weatherCode = 6001; // Freezing Rain
        if (record.data.precipitationIntensity >= 0.6)
          record.data.weatherCode = 6201; // Heavy Freezing Rain
        if (record.data.precipitationIntensity >= 2.36)
          record.data.weatherCode = 8200; // Freezing Downpour
        break;
      case 4: // Ice
        record.data.weatherCode = 6000; // Default to lowest: freezing drizzle
        if (record.data.precipitationIntensity >= 0.014)
          record.data.weatherCode = 7102; // Light Ice Pellets
        if (record.data.precipitationIntensity >= 0.07)
          record.data.weatherCode = 7000; // Ice Pellets
        if (record.data.precipitationIntensity >= 0.6)
          record.data.weatherCode = 7101; // Heavy Ice Pellets
        if (record.data.precipitationIntensity >= 2.36)
          record.data.weatherCode = 8300; // Hail
        break;
    }
  }

  return record;
};

module.exports = {
  friendlyName: "Update",

  description: "Update climacell weather data.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    if (
      !sails.config.custom.basic.tomorrowioAPI ||
      sails.config.custom.basic.tomorrowioAPI === "" ||
      !sails.config.custom.basic.tomorrowioLocation ||
      sails.config.custom.basic.tomorrowioLocation === ""
    ) {
      await sails.helpers.status.modify.with({
        name: "tomorrowio",
        label: "Tomorrow.io",
        status: 4,
        summary: `Disabled; API and/or location not configured.`,
        data: `Tomorrow.io weather is disabled because API and/or location are not configured.
        <br /><strong>TO FIX / CHECK:</strong>
        <ul>
          <li>Set the API key and location in administration DJ Controls -> System Settings -> Tomorrow.io.</li>
        </ul>`,
      });
      return;
    }

    // Get data from climacell
    let url = new URL("https://api.tomorrow.io/v4/timelines");
    url.searchParams.append("apikey", sails.config.custom.basic.tomorrowioAPI);
    url.searchParams.append(
      "location",
      `${sails.config.custom.basic.tomorrowioLocation}`
    );
    url.searchParams.append("units", "imperial");
    [
      "temperature",
      "temperatureApparent",
      "dewPoint",
      "humidity",
      "windSpeed",
      "windDirection",
      "windGust",
      "precipitationIntensity",
      "precipitationProbability",
      "precipitationType",
      "snowDepth",
      "visibility",
      "cloudCover",
      "weatherCode",
      "pressureSurfaceLevel",
      "pressureSeaLevel",
      "uvIndex",

      // No longer available in the free version of tomorrow.io's API
      // "epaIndex",
      // "epaHealthConcern",
    ].forEach((field) => {
      url.searchParams.append("fields", field);
    });
    ["current", "5m", "1h"].forEach((timestep) => {
      url.searchParams.append("timesteps", timestep);
    });
    let { body } = await needle("get", url.toString(), {
      json: true,
    });

    // Exit if there was an error, there was no body, or the body timelines constructor is not an array
    if (
      !body ||
      body.errorCode ||
      !body.data ||
      !body.data.timelines ||
      body.data.timelines.constructor !== Array
    ) {
      sails.log.error(new Error(body));
      await sails.helpers.status.modify.with({
        name: "tomorrowio",
        label: "Tomorrow.io",
        status: 3,
        summary: `Error parsing data / API offline.`,
        data: `Tomorrow.io did not return a proper body with data and a timeline.
        <br /><strong>TO FIX / CHECK:</strong>
        <ul>
          <li>Is tomorrow.io responding and the server network connection good?</li>
          <li>Do you have the correct API key? If not, set it in administration DJ Controls -> System Settings -> Tomorrow.io.</li>
          <li>Is the account in good standing?</li>
          <li>Did a rate limit get reached?</li>
        </ul>`,
      });
      return;
    }

    // Run through operations in the body for each timestep in the array
    let maps = body.data.timelines.map(async (timeline) => {
      // Skip if there are no intervals
      if (!timeline.intervals || timeline.intervals.constructor !== Array)
        return;

      // Run through each interval in the timeline
      let iMaps = timeline.intervals.map(async (interval, index) => {
        // No values? Exit.
        if (!interval.values) return;

        let dataClass = `${timeline.timestep}-${index}`;

        // Create the record if it does not exist
        sails.models.climacell
          .findOrCreate(
            { dataClass: dataClass },
            {
              dataClass: dataClass,
              data: interval.values,
              dataTime: moment(interval.startTime).format(
                "YYYY-MM-DD HH:mm:ss"
              ),
            }
          )
          .exec(async (err, record, wasCreated) => {
            // Exit on error or if the record was new / created
            if (err) {
              sails.log.error(err);
              return;
            }
            if (wasCreated) {
              return;
            }

            // Update only if the data changed
            if (
              record &&
              !_.isEqual(
                { data: record.data, dataTime: record.dataTime },
                {
                  data: interval.values,
                  dataTime: moment(interval.startTime).format(
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                }
              )
            )
              await sails.models.climacell
                .update(
                  { dataClass: dataClass },
                  {
                    dataClass: dataClass,
                    data: interval.values,
                    dataTime: moment(interval.startTime).format(
                      "YYYY-MM-DD HH:mm:ss"
                    ),
                  }
                )
                .fetch();
          });
      });
      await Promise.all(iMaps);
    });
    await Promise.all(maps);

    await sails.helpers.status.modify.with({
      name: "tomorrowio",
      label: "Tomorrow.io",
      status: 5,
      summary: `Operational.`,
      data: "Tomorrow.io is operational.",
    });

    return maps;
  },
};

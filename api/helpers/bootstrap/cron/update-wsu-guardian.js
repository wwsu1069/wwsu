module.exports = {
  friendlyName: "helpers.bootstrap.cron.updateWsuGuardian",

  description: "Update the latest posts on The Guardian Media Group.",

  schedule: "11 */5 * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: "rss-wsuguardian",
        summary: `Disabled; system in maintenance mode.`,
        data: `The Guardian Media Group RSS feed fetching is disabled; maintenance mode is active. Status will take up to 5 minutes to clear after maintenance mode is disabled.`,
        status: 4,
      });
      return "SKIPPED; maintenance mode active.";
    }

    // Update weather
    await sails.helpers.rss.guardian.update();
  },
};

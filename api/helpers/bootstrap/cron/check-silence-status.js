module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkSilenceStatus",

  description:
    "Update the silence status to critical when DJ Controls has not reported info about silence detection.",

  schedule: "13 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: "silence",
        label: "Silence",
        summary: `Checks disabled; system in maintenance mode.`,
        data: `Silence checks are disabled; maintenance mode is active. Status will take up to one minute to clear after maintenance mode is disabled.`,
        status: 4,
      });
      return "SKIPPED; maintenance mode active.";
    }

    let silence = await sails.models.status.findOne({ name: "silence" });
    let responsible = await sails.models.hosts.count({
      silenceDetection: true,
    });
    if (
      moment(silence.updatedAt).add(3, "minutes").isBefore(moment()) &&
      responsible > 0
    ) {
      await sails.helpers.status.modify.with({
        name: "silence",
        label: "Silence",
        summary: `DJ Controls responsible for reporting silence is not reporting silence status.`,
        data: `There has been no information received about the silence detection system for over 3 minutes!
        <br /><strong>TO FIX:</strong>
        <ul>
          <li>Make sure the computer / DJ Controls responsible for silence detection is online / running and connected to the internet.</li>
          <li>If DJ Controls is running, you may have to restart it due to an error.</li>
        </ul>`,
        status: 2,
      });
    } else if (responsible < 1) {
      await sails.helpers.status.modify.with({
        name: `silence`,
        status: 2,
        label: `Silence`,
        summary: `No hosts are configured to monitor for silence.`,
        data: `There are no hosts currently set for silence detection. You should set one.
        <br /><strong>TO FIX: </strong>
        <ol>
          <li>In an administration DJ Controls, go to Hosts and edit the host to be responsible for silence detection.</li>
          <li>Check "Monitor / Report Silence".</li>
          <li>On the DJ Controls responsible for silence detection, go to DJ Controls Settings -> Audio, make sure the correct input audio devices are checked for silence detection (the ones receiving on-air programming).</li>
          <li>Ensure you have the silence detection settings set to where you want (threshold and delay).</li>
        </ol>`,
      });
    }
  },
};

module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkDiscordVoice",

  description:
    "Check if the Discord bot is in a voice channel. If not, move it there to stream the radio station.",

  schedule: "15 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // TODO
    return "SKIPPED; needs work.";

    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance)
      return "SKIPPED; maintenance mode active.";

    if (
      typeof DiscordClient === "undefined" ||
      !DiscordClient ||
      !DiscordClient.readyTimestamp ||
      DiscordClient.ws.status === 5
    )
      return "FAILED; Discord bot not active or offline.";

    if (!DiscordClient.voice || DiscordClient.voice.connections.size === 0) {
      let channel = DiscordClient.channels.resolve(
        sails.config.custom.discord.channelRadio
      );
      if (!channel) return resolve();

      let connection = await channel.join();
      connection
        .play("https://server.wwsu1069.org/stream") // TODO; add to configuration.
        .then((dispatcher) => {
          dispatcher.on("debug", (e) => sails.log.debug(e));
          dispatcher.on("error", (e) => sails.log.error(e));
          dispatcher.on("start", () =>
            sails.log.debug("Discord bot started playing radio stream")
          );
        });
    }
  },
};

module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkSongs",

  description:
    "Count the number of corrupt / bad tracks in RadioDJ and calculate the number of music tracks whose duration is too long.",

  schedule: "9 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: "music-library",
        label: "Music Library",
        summary: `Checks disabled; system in maintenance mode.`,
        data: `RadioDJ track library checks are disabled; maintenance mode is active.`,
        status: 4,
      });
      return "SKIPPED; maintenance mode active.";
    }

    // Disable if not using RadioDJ
    if (!sails.config.custom.radiodjs.length) {
      await sails.helpers.status.modify.with({
        name: "music-library",
        label: "Music Library",
        summary: `Checks disabled; no RadioDJ instances have been configured.`,
        data: `RadioDJ track library checks are disabled; no RadioDJs are configured / RadioDJ is not being used.
        <br /><strong>TO FIX:</strong> Go to administration DJ Controls -> System Settings -> RadioDJs.`,
        status: 4,
      });
      return "SKIPPED; No RadioDJs are configured.";
    }

    let problems = [];
    let problemSummaries = [];
    let status = 5;

    try {
      // Count the number of -1 enabled tracks
      let found = await sails.models.songs
        .count({ enabled: -1 })
        .tolerate(() => {});
      if (found && found >= sails.config.custom.status.musicLibraryVerify1) {
        status = 1;
        problemSummaries.push(`RadioDJ track library has ${found} bad tracks (critically high).`);
        problems.push(
          `RadioDJ track library has ${found} bad tracks. This is critically high and should be fixed immediately! Tracks are marked bad when either corrupt or cannot be accessed by RadioDJ. Please ensure all tracks used by RadioDJ are saved on a [network] drive that can be accessed by all RadioDJs. <strong>TO FIX: </strong> In RadioDJ, go to Utilities -> Verify Tracks. Run it without "delete invalid tracks" to see which tracks are bad (this will also re-enable tracks that are now accessible).`
        );
      } else if (
        found &&
        found >= sails.config.custom.status.musicLibraryVerify2
      ) {
        status = status > 2 ? 2 : status;
        problemSummaries.push(`RadioDJ track library has ${found} bad tracks (very high).`);
        problems.push(
          `RadioDJ track library has ${found} bad tracks. This is quite high. Tracks are marked bad when either corrupt or cannot be accessed by RadioDJ. Please ensure all tracks used by RadioDJ are saved on a [network] drive that can be accessed by all RadioDJs. <strong>TO FIX: </strong> In RadioDJ, go to Utilities -> Verify Tracks. Run it without "delete invalid tracks" to see which tracks are bad (this will also re-enable tracks that are now accessible).`
        );
      } else if (
        found &&
        found >= sails.config.custom.status.musicLibraryVerify3
      ) {
        status = status > 3 ? 3 : status;
        problemSummaries.push(`RadioDJ track library has ${found} bad tracks (high).`);
        problems.push(
          `RadioDJ track library has ${found} bad tracks. Tracks are marked bad when either corrupt or cannot be accessed by RadioDJ. Please ensure all tracks used by RadioDJ are saved on a [network] drive that can be accessed by all RadioDJs. <strong>TO FIX: </strong> In RadioDJ, go to Utilities -> Verify Tracks. Run it without "delete invalid tracks" to see which tracks are bad (this will also re-enable tracks that are now accessible).`
        );
      } else if (found && found > 0) {
        status = status > 4 ? 4 : status;
        problemSummaries.push(`RadioDJ track library has ${found} bad tracks.`);
        problems.push(
          `RadioDJ track library has ${found} bad tracks. Tracks are marked bad when either corrupt or cannot be accessed by RadioDJ. Please ensure all tracks used by RadioDJ are saved on a [network] drive that can be accessed by all RadioDJs. <strong>TO FIX: </strong> In RadioDJ, go to Utilities -> Verify Tracks. Run it without "delete invalid tracks" to see which tracks are bad (this will also re-enable tracks that are now accessible)..`
        );
      }
    } catch (e) {
      sails.log.error(e);
      status = 1;
      problemSummaries.push(`Error checking for number of bad tracks in the RadioDJ track library.`);
      problems.push(
        `There was an error checking the RadioDJ track library for bad tracks. This usually indicates a database problem. Please check the server logs.`
      );
    }

    // Now check for long tracks
    try {
      let found = await sails.models.songs
        .count({
          enabled: 1,
          id_subcat: sails.config.custom.subcats.music,
          duration: { ">=": 60 * sails.config.custom.basic.breakCheck },
        })
        .tolerate(() => {});

      if (found && found >= sails.config.custom.status.musicLibraryLong1) {
        status = 1;
        problemSummaries.push(`There are ${found} long tracks in the RadioDJ track library (critically high). These can prevent top-of-hour ID from airing.`);
        problems.push(
          `RadioDJ track library has ${found} long tracks in the system music category (that are enabled). This is critically high and should be fixed immediately! Tracks which are longer than 10 minutes should be shortened, disabled, or removed. Excessively long tracks can prevent the FCC required top-of-hour ID from airing on time. <strong>TO FIX:</strong> In administration DJ Controls -> Maintenance, use the "Long Music Tracks" utility.`
        );
      } else if (
        found &&
        found >= sails.config.custom.status.musicLibraryLong2
      ) {
        status = status > 2 ? 2 : status;
        problemSummaries.push(`There are ${found} long tracks in the RadioDJ track library (very high). These can prevent top-of-hour ID from airing.`);
        problems.push(
          `RadioDJ track library has ${found} long tracks in the system music category (that are enabled). This is high. Tracks which are longer than 10 minutes should be shortened, disabled, or removed. Excessively long tracks can prevent the FCC required top-of-hour ID from airing on time. <strong>TO FIX:</strong> In administration DJ Controls -> Maintenance, use the "Long Music Tracks" utility.`
        );
      } else if (
        found &&
        found >= sails.config.custom.status.musicLibraryLong3
      ) {
        status = status > 3 ? 3 : status;
        problemSummaries.push(`There are ${found} long tracks in the music library (high). These can prevent top-of-hour ID from airing.`);
        problems.push(
          `RadioDJ track library has ${found} long tracks in the system music category (that are enabled). Tracks which are longer than 10 minutes should be shortened, disabled, or removed. Excessively long tracks can prevent the FCC required top-of-hour ID from airing on time. <strong>TO FIX:</strong> In administration DJ Controls -> Maintenance, use the "Long Music Tracks" utility.`
        );
      } else if (found && found > 0) {
        status = status > 4 ? 4 : status;
        problemSummaries.push(`There are ${found} long tracks in the music library. These can prevent top-of-hour ID from airing.`);
        problems.push(
          `RadioDJ track library has ${found} long tracks (that are enabled). Tracks which are longer than 10 minutes should be shortened, disabled, or removed. Excessively long tracks can prevent the FCC required top-of-hour ID from airing on time. <strong>TO FIX:</strong> In administration DJ Controls -> Maintenance, use the "Long Music Tracks" utility.`
        );
      }
    } catch (e) {
      sails.log.error(e);
      status = 1;
      problemSummaries.push(`Error checking for long music tracks in the library.`);
      problems.push(
        `There was an error checking the RadioDJ music library for long tracks. This usually indicates a database problem. Please see the server error logs.`
      );
    }

    await sails.helpers.status.modify.with({
      name: "music-library",
      status: status,
      summary: problemSummaries.join("; "),
      data: `<ul>${problems
        .map((problem) => `<li>${problem}</li>`)
        .join("")}</ul>`,
    });
  },
};

module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkInventory",

  description: "Check for overdue checkouts and missing items.",

  schedule: "15 */5 * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if shutting down or in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      await sails.helpers.status.modify.with({
        name: `inventory`,
        label: `Inventory`,
        status: 4,
        summary: `Checks disabled; system in maintenance mode.`,
        data: `Inventory checks are disabled; maintenance mode is active. Status will take up to 5 minutes to clear after maintenance mode is disabled.`,
      });
      return "SKIPPED; maintenance mode active.";
    }

    let status = 5;
    let issues = [];

    // Check for items not checked in yet that are overdue
    let records = await sails.models.checkout
      .find({
        checkInDate: null,
        checkInDue: { "!=": null },
      })
      .populate("item");
    records
      .filter((record) => moment(record.checkInDue).isBefore(moment()))
      .map((record) => {
        if (status > 4) status = 4;
        issues.push(
          `${record.checkOutQuantity} of ${record.item.name} (${
            record.item.location
          } / ${
            record.item.subLocation
          }) are still checked out. They were supposed to be returned by ${moment(
            record.checkInDue
          ).format(
            "LLLL"
          )}. Please review the check-out records in the inventory system, retrieve the items, and check them in on the inventory system.`
        );
      });

    // Check for missing items
    let items = await sails.models.items.find();
    records = await sails.models.checkout
      .find({
        checkOutDate: { "!=": null },
        checkInDate: { "!=": null },
      })
      .populate("item");
    items.map((item) => {
      let quantity = item.quantity;
      let record = records.find((record) => record.item.ID === item.ID);
      if (record) quantity -= record.checkOutQuantity - record.checkInQuantity;
      if (quantity < item.quantity) {
        if (status > 4) status = 4;
        issues.push(
          `${item.name} (${item.location} / ${item.subLocation}) is missing items! Expected ${item.quantity} quantity, but only ${quantity} quantity was checked back in. Please ensure all items were returned and either edit the check-in records or fix the total quantity of the item.`
        );
      }
    });

    // Update status
    await sails.helpers.status.modify.with({
      name: `inventory`,
      label: `Inventory`,
      status: status,
      summary: `${issues.length} issues detected with missing quantities or items checked out past due date. See DJ Controls.`,
      data:
        issues.length === 0
          ? `No issues.`
          : `<ul>${issues.map((issue) => `<li>${issue}</li>`).join("")}</ul>`,
    });
  },
};

const queryString = require("query-string");

module.exports = {
  friendlyName: "helpers.bootstrap.cron.songMaintenance",

  description:
    "Perform daily maintenance on songs, such as removing special characters from metadata, removing fade from noFade tracks, and disabling tracks with negative durations.",

  schedule: "12 1 0 * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance)
      return "SKIPPED; maintenance mode active.";

    if (!sails.config.custom.radiodjs.length)
      return "SKIPPED; not using RadioDJ."

    let songs = await sails.models.songs.find();
    songs.map(async (song) => {
      try {
        let toUpdate = {};

        // Loop through each metadata property we want to check and ensure is sanitized
        [
          "artist",
          "original_artist",
          "title",
          "album",
          "composer",
          "label",
          "year",
          "publisher",
          "copyright",
        ].map((prop) => {
          let sanitized = song[prop].replace(/[^\x20-\x7E]/g, ""); // Remove all non-printable ASCII characters
          if (sanitized !== song[prop])
            // Only mark for update if the sanitized version is different from the current version
            toUpdate[prop] = sanitized;
        });

        // Disable the track as a bad track if its duration is negative; this will crash RadioDJ.
        if (song.duration < 0 && song.enabled !== -1) {
          toUpdate.enabled = -1;
        }

        // If the track is in a noFade subcategory, then ensure it has no fading.
        if (sails.config.custom.subcats.noFade.indexOf(song.id_subcat) !== -1) {
          var cueData = queryString.parse(song.cue_times);

          // If fade in and fade out are already 0, then we have nothing to do.
          if (
            cueData.fin &&
            cueData.fin !== 0 &&
            cueData.fou &&
            cueData.fou !== 0
          ) {
            // Get rid of any fading, and reset the xta cue point
            cueData.fin = 0;
            cueData.fou = 0;
            cueData.xta = cueData.end || record.duration;

            cueData = `&${queryString.stringify(cueData)}`;

            toUpdate.cue_times = cueData;
          }
        }

        // Finally, update the song with the new properties if there is anything to update
        if (Object.keys(toUpdate).length > 0) {
          await sails.models.songs.updateOne({ ID: song.ID }, toUpdate);
        }
      } catch (e) {
        sails.log.error(e);
      }
    });
  },
};

module.exports = {
  friendlyName: "helpers.bootstrap.cron.checkDatastores",

  description: "Check the status of the models / datastores.",

  schedule: "7 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Initialize by loading each model in according to datastore.
    let datastores = {};
    for (let datastore in sails.config.datastores) {
      if (
        !Object.prototype.hasOwnProperty.call(
          sails.config.datastores,
          datastore
        )
      )
        continue;
      datastores[datastore] = {
        status: datastore === "default" ? 5 : 4, // Do not trigger "no models" error for the default datastore
        data: [],
        summary: [],
        models: [],
      };
    }

    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) {
      for (let datastore in datastores) {
        if (!Object.prototype.hasOwnProperty.call(datastores, datastore))
          continue;

        await sails.helpers.status.modify.with({
          name: `db-${datastore}`,
          status: 4,
          summary: `Checks disabled; system in maintenance mode.`,
          data: `Datastore checks are disabled; maintenance mode is active. Status will take up to one minute to clear after maintenance mode is disabled.`,
        });
      }
      return "SKIPPED; maintenance mode active.";
    }

    for (let model in sails.models) {
      if (!Object.prototype.hasOwnProperty.call(sails.models, model)) continue;

      if (datastores[sails.models[model].datastore]) {
        datastores[sails.models[model].datastore].models.push(
          sails.models[model]
        );
      }
    }

    // Test each database
    for (let datastore in datastores) {
      if (!Object.prototype.hasOwnProperty.call(datastores, datastore))
        continue;

      let maps = datastores[datastore].models.map(async (model) => {
        try {
          let record = await model
            .find()
            .limit(1)
            .tolerate((err) => {
              sails.log.error(err);
              datastores[datastore].status = 1;
              datastores[datastore].summary.push(
                `Model ${model.identity} failure (error querying records in the database). System is considered unstable.`
              );
              datastores[datastore].data.push(
                `Model failure (error querying records): ${model.identity}. Please ensure this table / datastore is online and not corrupt. Also check that the credentials are correct in sails.config.datastore. And check the server logs.`
              );
            });
          if (
            !model.ignoreEmptyTable &&
            (typeof record[0] === "undefined" ||
              typeof record[0][model.primaryKey] === "undefined")
          ) {
            if (datastores[datastore].status > 2) {
              datastores[datastore].status = 2;
            }
            datastores[datastore].summary.push(
              `Model ${model.identity} failure (empty database table but expected at least 1 record).`
            );
            datastores[datastore].data.push(
              `Model failure (empty table / no records returned): ${model.identity}. Expected this model to return at least 1 row of data (with a valid primary key in column ${model.primaryKey}), but didn't. Please ensure the datastore / table is not corrupt. If this table should be allowed to be empty, set module.exports.ignoreEmptyTable to true in its api/models js file.`
            );
          }

          if (datastores[datastore].status === 4) {
            datastores[datastore].status = 5;
          }
        } catch (err) {
          datastores[datastore].status = 1;
          datastores[datastore].summary.push(
            `Model ${model.identity} failure (internal error). System is considered unstable.`
          );
          datastores[datastore].data.push(
            `Model failure (internal error): ${model.identity}. Please ensure the datastore / table is online and not corrupt, and the credentials in sails.config.datastore are correct. Check the server logs.`
          );
          sails.log.error(err);
        }
      });
      await Promise.all(maps);

      // Process final status
      await sails.helpers.status.modify.with({
        name: `db-${datastore}`,
        status: datastores[datastore].status,
        summary:
          datastores[datastore].status !== 4
            ? datastores[datastore].summary.length > 0
              ? `${datastores[datastore].summary.join("; ")}`
              : `This datastore and all models are operational.`
            : `Checks disabled; no models assigned to this datastore.`,
        data:
          datastores[datastore].status !== 4
            ? datastores[datastore].data.length > 0
              ? `<ul>${datastores[datastore].data
                  .map((dat) => `<li>${dat}</li>`)
                  .join("")}</ul>`
              : `This datastore and all models are operational.`
            : `No sailsjs models are assigned to this datastore; no checks were run. If this is unexpected, check to ensure all models are correctly loaded in the Sailsjs application.`,
      });
    }
  },
};

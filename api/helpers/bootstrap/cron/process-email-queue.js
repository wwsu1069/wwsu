module.exports = {
  friendlyName: "helpers.bootstrap.cron.processEmailQueue",

  description: "Send an email that is in the queue.",

  schedule: "14 * * * * *",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance) return "SKIPPED; maintenance mode active.";

    // Find one email to send
    let records = await sails.models.emails
      .find({
        sent: false,
        or: [{ sendAt: null, sendAt: { "<=": moment().format("YYYY-MM-DD HH:mm:ss") } }],
      })
      .limit(1);

    // Send the email if we have one
    if (records && records.length > 0) {
      await sails.helpers.emails.send(records[0].ID);
      return "DONE; An email was sent.";
    } else {
      return "DONE; no emails are ready to be sent.";
    }
  },
};

module.exports = {
  friendlyName: "helpers.bootstrap.cron.sendWeeklyAnalytics",

  description:
    "Send out an email to directors of weekly analytics. Also recalculate show scores (for show of the week).",

  schedule: "45 0 0 * * 0",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Do not execute if in maintenance mode.
    if (sails.config.custom.basic.maintenance)
      return "SKIPPED; maintenance mode active.";

    sails.models.status.tasks.analyticsEmail++;
    try {
      // Recalculate weekly stats
      let stats = await sails.helpers.analytics.calculateStats();
      let weekly = stats[0];
      let overall = stats[1];
      let overall2 = stats[2];

      // Reset the scoreTrack of the top show
      let featuredShow = await sails.models.calendar
        .find({ type: ["show", "remote", "prerecord"], active: true })
        .sort("scoreTrack DESC")
        .limit(1);
      if (featuredShow[0])
        await sails.models.calendar.updateOne(
          { ID: featuredShow[0].ID },
          { scoreTrack: 0 }
        );

      // Update score tracking for all shows
      let calRecords = await sails.models.calendar.find({
        type: ["show", "remote", "prerecord"],
        active: true,
      });
      let maps = overall2.map(async (record) => {
        let recordB = calRecords.find((rec) => rec.ID === record.ID);
        if (!recordB) return;
        await sails.models.calendar.updateOne(
          { ID: record.ID },
          { scoreTrack: recordB.scoreTrack + record.showScore }
        );
      });
      await Promise.all(maps);

      // Determine new top show
      featuredShow = await sails.models.calendar
        .find({ type: ["show", "remote", "prerecord"], active: true })
        .sort("scoreTrack DESC")
        .limit(1);

      // Build email body
      let body = `<p>Dear directors,</p>
            <p>Here are the key WWSU analytics for this past week.</p>
    
            <hr />
            <p><strong>TOP STATS</strong></p>
    
            <p><strong>Show of the Week*:</strong> ${
              featuredShow && featuredShow[0]
                ? `${featuredShow[0].hosts} - ${featuredShow[0].name}`
                : ``
            }</p>
            **Show of the week is not necessarily chosen based on top performing show this week. Instead, higher-performing shows will be chosen more often, but lower-performing shows will also be chosen from time to time. It is recommended to use this show as the show of the week for social posting and promotion. This will also be the show displayed as featured on the website and display signs this week.
            
            <p><strong>Order of shows/remotes/prerecords based on performance; higher is better:</strong>
            <ul>
            ${weekly.topShows
              .map(
                (show, index) =>
                  `<li>${index + 1}. ${show.name} (Score: ${show.score})</li>`
              )
              .join("")}
            </ul>
            Scores are calculated based on listener:showtime ratio, viewer:showtime ratio for video streams, messages sent/received (interactivity), number of breaks taken in an hour (3 is best), and reputation (followed all on-air regulations, did show on-time, etc)</p>
    
            <p><strong>Top Genre of the week:</strong> ${
              weekly.topGenre.name
            } (Score: ${weekly.topGenre.score})</p>
            <p><strong>Top Playlist of the week:</strong> ${
              weekly.topPlaylist.name
            } (Score: ${weekly.topPlaylist.score})</p>
    
            <hr />
            <p><strong>ALL PROGRAMMING</strong></p>
    
            <p><strong>Total online listener time*:</strong> ${moment
              .duration(overall[0].listenerMinutes, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Listener to showtime ratio (higher is better):</strong> ${
              overall[0].ratio
            }</p>
            <p><strong>Peak online listeners:</strong> ${
              overall[0].listenerPeak
            } at ${moment(overall[0].listenerPeakTime).format("LLLL")}</p>
            <p><strong>Total video streaming time:</strong> ${moment
              .duration(overall[0].videoTime, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Total video stream watch time*:</strong> ${moment
              .duration(overall[0].viewerMinutes, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Video stream watch time to showtime ratio (higher is better):</strong> ${
              overall[0].viewerRatio
            }</p>
            <p><strong>Peak video stream viewers:</strong> ${
              overall[0].viewerPeak
            } at ${moment(overall[0].viewerPeakTime).format("LLLL")}</p>
    
            <hr />
            <p><strong>LIVE SHOWS / REMOTES / PRERECORDS</strong></p>
    
            <p><strong>Live shows performed:</strong> ${overall[-1].shows}</p>
            <p><strong>Remote shows performed:</strong> ${
              overall[-1].remotes
            }</p>
            <p><strong>Prerecorded shows aired:</strong> ${
              overall[-1].prerecords
            }</p>
            <p><strong>Total on-air time of shows/remotes/prerecords:</strong> ${moment
              .duration(overall[-1].showTime, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Total online listener time* during shows/remotes/prerecords:</strong> ${moment
              .duration(overall[-1].listenerMinutes, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Listener to showtime ratio of shows/remotes/prerecords (higher is better):</strong> ${
              overall[-1].ratio
            }</p>
            <p><strong>Total video streaming time during shows/remotes/prerecords:</strong> ${moment
              .duration(overall[-1].videoTime, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Total video stream watch time* during shows/remotes/prerecords:</strong> ${moment
              .duration(overall[-1].viewerMinutes, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Video stream watch time to showtime ratio of shows/remotes/prerecords (higher is better):</strong> ${
              overall[-1].viewerRatio
            }</p>
    
            <hr />
            <p><strong>SPORTS BROADCASTS</strong></p>
    
            <p><strong>Sports broadcasts performed:</strong> ${
              overall[-2].sports
            }</p>
            <p><strong>Total on-air time of sports broadcasts:</strong> ${moment
              .duration(overall[-2].showTime, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Total online listener time* during sports:</strong> ${moment
              .duration(overall[-2].listenerMinutes, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Listener to showtime ratio of sports (higher is better):</strong> ${
              overall[-2].ratio
            }</p>
            <p><strong>Total video streaming time during sports:</strong> ${moment
              .duration(overall[-2].videoTime, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Total video stream watch time* during sports:</strong> ${moment
              .duration(overall[-2].viewerMinutes, "minutes")
              .format("h [hours], m [minutes]")}</p>
            <p><strong>Video stream watch time to showtime ratio of sports (higher is better):</strong> ${
              overall[-2].viewerRatio
            }</p>
    
            <hr />
            <p><strong>OTHER PROGRAMMING</strong></p>
    
            <p><strong>Playlists aired:</strong> ${overall[-4].playlists}</p>
    
            <hr />
            <p><strong>INTERACTIVITY</strong></p>
    
            <p><strong>Tracks liked on the website:</strong> ${
              weekly.tracksLiked
            }</p>
            <p><strong>Track requests placed on the website:</strong> ${
              weekly.tracksRequested
            }</p>
            <p><strong>Messages sent/received on the website:</strong> ${
              weekly.webMessagesExchanged
            }</p>
            <p><strong>Messages sent/received in the Discord server:</strong> ${
              weekly.discordMessagesExchanged
            }</p>
            <p><strong>Number of Discord members in the WWSU server:</strong> ${
              typeof DiscordClient !== "undefined" &&
              DiscordClient &&
              DiscordClient.readyTimestamp &&
              DiscordClient.ws.status !== 5 &&
              DiscordClient.guilds.resolve(
                sails.config.custom.discord.guildWWSU
              )
                ? DiscordClient.guilds.resolve(
                    sails.config.custom.discord.guildWWSU
                  ).memberCount
                : `UNKNOWN`
            }</p>
            
            <p><small>*Online listener/viewer time combines the total lisatening time of all online listeners / video stream watchers. For example, 3 listeners/viewers tuned in for 30 minutes (3 * 30) and 1 other listener/viewer tuned in for one hour (1 * 60) is 2 hours and 30 minutes of online listener / viewer time.</small></p>`;

      // Queue email
      await sails.helpers.emails.queueWeeklyAnalytics(
        `Weekly Analytics Report`,
        body
      );

      sails.models.status.tasks.analyticsEmail--;
    } catch (e) {
      sails.models.status.tasks.analyticsEmail--;
      sails.log.error(e);
    }
  },
};

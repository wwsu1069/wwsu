module.exports = {
  friendlyName: "helpers.bootstrap.initializers.refreshDisplays",

  description:
    "Request a display sign refresh in sockets after 30 seconds in case HTML assets changed.",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    setTimeout(() => {
      sails.sockets.broadcast("display-refresh", "display-refresh", true);
    }, 30000);
  },
};

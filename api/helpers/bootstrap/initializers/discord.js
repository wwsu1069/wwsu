module.exports = {
  friendlyName: "helpers.bootstrap.initializers.discord",

  description: "Discord initializer",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Create the global Discord client if config permits.
    global["DiscordClient"] =
      sails.config.custom.discord.clientOptions &&
      sails.config.custom.discord.token
        ? new Discord.Client(sails.config.custom.discord.clientOptions)
        : undefined;

    // Skip if we do not have a DiscordClient
    if (typeof DiscordClient === "undefined" || !DiscordClient) {
      await sails.helpers.status.modify.with({
        name: "discord",
        label: "Discord Bot",
        status: 4,
        summary: `Disabled; clientOptions and/or token not configured. Schedules / programming, messages, analytics, etc will not be posted in Discord until fixed.`,
        data: `Discord bot is disabled because clientOptions and/or token is not defined in settings.
        <br /><strong>TO FIX / CHECK:</strong>
        <ul>
          <li>Go to the Discord Developer Portal to create a new application for the bot. Also create a bot user. Make sure the bot is NOT a public bot!</li>
          <li>Add/invite the bot to the WWSU Discord server with permissions View Channels, Manage Channels, Manage Roles, Change Nickname, Send Messages, Embed Links, Attach Files, Add Reactions, mention everyone/here and All Roles, Manage Messages, Read Message History, Speak, Use Voice Activity, and Priority Speaker.</li>
          <li>Make sure the bot has the Presence, Server Members, and Message Content intents.</li>
          <li>Grab the bot token, and set it in admin DJ Controls -> Server Settings -> Discord.</li>
          <li>Set Discord client options as per the Discord.js Documentation on ClientOptions in admin DJ Controls -> Server Settings -> Discord.</li>
          <li>Reboot the sailsjs app / server (can be done in DJ Controls -> Maintenance).</li>
        </ul>`,
      });
      return "Skipped; Discord configuration and/or token was not defined.";
    }

    // Initialize DiscordClient event handlers (every Discord.js event is handled in a sails.helpers.events file)
    if (
      sails.helpers &&
      sails.helpers.discord &&
      sails.helpers.discord.events
    ) {
      for (var event in sails.helpers.discord.events) {
        if (
          Object.prototype.hasOwnProperty.call(
            sails.helpers.discord.events,
            event
          )
        ) {
          // Needs to be in a self-calling function to provide the proper value of event
          let temp = (async (event2) => {
            // ready should only ever fire once whereas other events should be allowed to fire multiple times.
            if (["ready"].indexOf(event2) !== -1) {
              DiscordClient.once(event2, async (...args) => {
                await sails.helpers.discord.events[event2](...args);
              });
            } else {
              DiscordClient.on(event2, async (...args) => {
                await sails.helpers.discord.events[event2](...args);
              });
            }
          })(event);
        }
      }
    }

    // Try logging in Discord
    const discordLogin = async () => {
      try {
        await DiscordClient.login(sails.config.custom.discord.token);
      } catch (e) {
        sails.log.error(e);

        // Try again in 5 minutes
        setTimeout(async () => discordLogin(), 1000 * 60 * 5);

        // Set status
        await sails.helpers.status.modify.with({
          name: "discord",
          status: 3,
          summary: `Bot could not connect to Discord. Schedules / programming, messages, analytics, etc will not be posted in Discord until fixed.`,
          data: `There was a problem connecting to Discord (will retry every 5 minutes). The Discord bot will not be updating the WWSU server with schedule changes, messages, analytics, etc until resolved.
          <br /><strong>TO FIX / CHECK:</strong>
          <ul>
            <li>Check https://discordstatus.com/ for any reported Discord issues.</li>
            <li>Is the bot token correct? Go to the Discord Developer Portal and insert the bot token in administration DJ Controls -> System Settings -> Discord.</li>
            <li>Did the bot hit a Discord API rate limit?</li>
            <li>Did Discord ban the bot?</li>
          </ul>`,
        });
      }
    };
    discordLogin();
  },
};

const sh = require("shorthash");

module.exports = {
  friendlyName: "Status",

  description: "Status initializers.",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    // Load default status template into memory. Add radioDJ and DJ Controls instances to template as well.
    sails.config.custom.radiodjs.forEach((radiodj) => {
      sails.models.status.template.push({
        name: `radiodj-${radiodj.name}`,
        label: `RadioDJ ${radiodj.label}`,
        status: radiodj.errorLevel,
        data: "This RadioDJ has not reported online since initialization.",
        time: null,
      });
    });

    // Also add datastores to template
    for (let datastore in sails.config.datastores) {
      if (
        !Object.prototype.hasOwnProperty.call(
          sails.config.datastores,
          datastore
        )
      )
        continue;

      sails.models.status.template.push({
        name: `db-${datastore}`,
        label: `DB ${datastore}`,
        status: 4,
        data: "This datastore has not been checked yet since initialization.",
        time: null,
      });
    }

    // Also add hosts to template who have responsibilities that necessitate status checking
    var clients = await sails.models.hosts
      .find({ authorized: true })
      .tolerate((err) => {
        // Don't throw errors, but log them
        sails.log.error(err);
      });
    if (clients.length > 0) {
      clients.forEach((client) => {
        var offStatus = 4;
        if (
          client.silenceDetection ||
          client.recordAudio ||
          client.answerCalls
        ) {
          if (client.silenceDetection || client.recordAudio) {
            offStatus = 2;
          } else {
            offStatus = 3;
          }
          sails.models.status.template.push({
            name: `computer-${sh.unique(
              client.host + sails.config.custom.basic.hostSecret
            )}`,
            label: `Host ${client.friendlyname}`,
            status: offStatus,
            data: "This host has not reported online since initialization.",
            time: null,
          });
        }
      });
    }

    // Also add display signs
    sails.config.custom.displaysigns.forEach((display) => {
      sails.models.status.template.push({
        name: `display-${display.name}`,
        label: `Display ${display.label}`,
        status: display.level,
        data: "This display sign has not reported online since initialization.",
        time: null,
      });
    });

    // Add the template to the database
    await sails.models.status.createEach(sails.models.status.template);

    // Check for reported issues
    await sails.helpers.status.checkReported();

    // Check if we do not have a host set to monitor recording
    let recordHosts = await sails.models.hosts.count({ recordAudio: true });
    if (!recordHosts) {
      await sails.helpers.status.modify.with({
        name: `recorder`,
        status: 2,
        label: `Recorder`,
        summary: `No hosts are configured to automatically record on-air programming. Prepare to manually record on-air programming.`,
        data: `There are no hosts currently set for recording audio. <strong>Be prepared to manually record your broadcasts</strong> until this is resolved. <br />
            <strong>TO FIX:</strong>
              <ol>
                <li>Go in administration DJ Controls -> Hosts.</li>
                <li>Edit the host that should record audio and check the box for record audio.</li>
                <li>On the DJ Controls recording audio, in DJ Controls Settings -> Audio, make sure the appropriate input audio devices receiving on-air audio have "Record Audio" checked. Also make sure the Recorder Settings are correct.</li>
              </ol>`,
      });
    }
  },
};

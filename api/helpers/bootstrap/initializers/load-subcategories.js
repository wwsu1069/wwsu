module.exports = {
  friendlyName: "Load subcategories",

  description: "",

  inputs: {},

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    await sails.helpers.songs.reloadSubcategories();
    sails.log.verbose(sails.config.custom.subcats);
  },
};

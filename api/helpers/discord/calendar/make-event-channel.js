module.exports = {
  friendlyName: "Make Event channel",

  description: "Get or create the discord channel of a calender event",

  inputs: {
    event: {
      type: "json",
      required: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.helpers.runSync('Discord makeEventChannel', async () => {
      // Exit immediately if there is no Discord client.
      if (
        typeof DiscordClient === "undefined" ||
        !DiscordClient ||
        !DiscordClient.readyTimestamp ||
        DiscordClient.ws.status === 5
      )
        return;

      // Do not make channels for events that are not a broadcast or are a sports broadcast
      if (
        ["show", "remote", "prerecord", "playlist"].indexOf(
          inputs.event.type
        ) === -1
      )
        return;

      sails.log.verbose(`Discord makeEventChannel: Passed initial guards`);

      let channel;
      let reason = `New event`;

      // Things could have changed by other processes; check for this
      let updatedRecord = await sails.models.calendar.findOne({
        ID: inputs.event.calendarID || inputs.event.ID,
      });

      sails.log.verbose(`Discord makeEventChannel: Retrieved updated record`);

      // If discordChannel is set, try to retrieve it and return it if it exists.
      if (updatedRecord.discordChannel) {
        reason = `Event channel no longer existed`;
        channel = DiscordClient.channels.resolve(updatedRecord.discordChannel);

        sails.log.verbose(`Discord makeEventChannel: Resolved (or not) channel`);

        // If channel exists, update its name and description in case they were edited. And return the channel.
        if (channel) {
          channel = await channel.edit(
            {
              name: await sails.helpers.discord.toChannelName(
                inputs.event.name
              ),
              topic: `${inputs.event.type}: ${inputs.event.description}`,
            },
            "Event changed in the system"
          );

          sails.log.verbose(`Discord makeEventChannel: Edited channel details`);
          return channel;
        }
      }

      sails.log.verbose(`Discord makeEventChannel: New channel must be created...`);

      // At this point, the channel does not exist. Create it in our configured guild but only if the event is active.
      if (!inputs.event.active) return;
      let guild = DiscordClient.guilds.resolve(
        sails.config.custom.discord.guildWWSU
      );
      sails.log.verbose(`Discord makeEventChannel: Resolved (or not) guild`);
      if (!guild) return;
      channel = await guild.channels.create(
        await sails.helpers.discord.toChannelName(inputs.event.name),
        {
          topic: `${inputs.event.type}: ${inputs.event.description}`,
          rateLimitPerUser: 15,
          reason: reason,
          parent: sails.config.custom.discord.categoryShowDefault,
        }
      );
      sails.log.verbose(`Discord makeEventChannel: Channel created`);

      // Update database with the new channel, but do not fetch or this will result in duplicate messages (the calling function should do a full lifecycle fetch)
      await sails.models.calendar.update(
        { ID: inputs.event.calendarID || inputs.event.ID },
        { discordChannel: channel.id }
      );

      sails.log.verbose(`Discord makeEventChannel: Snowflake added to calendar event`);

      // Be sure to update current meta DiscordChannel if the event calendar ID is on the air right now.
      if (sails.models.meta.memory.calendarID === inputs.event.calendarID) {
        await sails.helpers.meta.change.with({ discordChannel: channel.id });
        sails.log.verbose(`Discord makeEventChannel: Meta updated`);
      }

      sails.log.verbose(`Discord makeEventChannel: Returning channel`);

      return channel;
    });
  },
};

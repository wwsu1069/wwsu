module.exports = {
  friendlyName: "Post event",

  description:
    "Make a discord channel for an event if it does not exist. And post or edit the message for its info / description",

  inputs: {
    event: {
      type: "json",
      required: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Do not make channels for events that are not a broadcast or are a sports broadcast
    if (
      ["show", "remote", "prerecord", "playlist"].indexOf(
        inputs.event.type
      ) === -1
    )
      return;

    let channel;
    let message;

    // Get or create the Discord channel for the event
    channel = await sails.helpers.discord.calendar.makeEventChannel(
      inputs.event
    );

    if (!channel) return;

    return await sails.helpers.runSync('Discord postEvent', async () => {

      // Things could have changed by other processes; check for this
      let updatedRecord = await sails.models.calendar.findOne({
        ID: inputs.event.calendarID || inputs.event.ID,
      });

      // Construct an embed containing the details of the event
      let embed = new Discord.MessageEmbed()
        .setColor(
          `${sails.models.calendar.calendardb.getColor(inputs.event) || "#000000"
          }`
        )
        .setTitle(
          `${inputs.event.type}: ${inputs.event.hosts} - ${inputs.event.name}`
        )
        .setDescription(
          `${inputs.event.description || "No Description Provided"}`
        )
        .setFooter(
          `This message will be edited automatically when the details of the broadcast changes. This message was pinned to the channel for easy access.`
        );
      if (inputs.event.banner)
        embed = embed.setImage(
          `https://server.wwsu1069.org/api/uploads/${inputs.event.banner}`
        );
      if (inputs.event.logo)
        embed = embed.setThumbnail(
          `https://server.wwsu1069.org/api/uploads/${inputs.event.logo}`
        );

      // Update existing message if applicable
      if (updatedRecord.discordCalendarMessage) {
        try {
          message = await channel.messages.fetch(
            updatedRecord.discordCalendarMessage
          );
        } catch (e) {
          /* Ignore errors */
        }
        if (message) {
          message = await message.edit({ embeds: [embed] });

          // Also add schedule message
          WWSUqueue.add(() => {
            sails.helpers.discord.calendar
              .postSchedule(inputs.event, channel)
              .then(() => { });
          });

          return message;
        }
      }

      // At this point, message does not exist. Create it if the event is active, and update the database.
      message = await channel.send({ embeds: [embed] });
      await sails.models.calendar
        .update(
          { ID: inputs.event.calendarID || inputs.event.ID },
          { discordChannel: channel.id, discordCalendarMessage: message.id }
        )
        .fetch();

      message = await message.pin();

      // NOTE: It is assumed via the above update that this helper will be called again (afterCreate lifecycle), this time with discordCalendarMessage present.
      // Therefore, we will not call sails.helpers.discord.calendar.postSchedule here as it would be called on the second iteration.
      // If we called it, we would end up with two schedule messages.

      return message;
    });
  },
};

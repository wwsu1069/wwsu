module.exports = {


  friendlyName: 'Shard disconnect',


  description: '',


  inputs: {
    event: {
      type: "ref"
    },
    id: {
      type: "number"
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    await sails.helpers.status.modify.with({
      name: "discord",
      status: 3,
      summary: `Shard disconnected and will not re-connect. Schedules / programming, messages, analytics, etc might not be posted to Discord until fixed.`,
      data: `A shard has disconnected and will not reconnect: ${inputs.id}. Please check the network / Discord status. You may need to reboot the WWSU application.`,
    });
  }


};


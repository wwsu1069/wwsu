module.exports = {
  friendlyName: "Ready",

  description: "Discord ready event",

  inputs: {
    client: {
      type: "ref",
    },
  },

  fn: async function (inputs) {
    sails.log.debug(`DISCORD: Bot is online.`);

    // Guild check
    let guild = await inputs.client.guilds.fetch(
      sails.config.custom.discord.guildWWSU
    );
    if (guild) {
      await sails.helpers.status.modify.with({
        name: "discord",
        status: 5,
        summary: `Online.`,
        data: "The Discord Bot is online and operational.",
      });
    } else {
      await sails.helpers.status.modify.with({
        name: "discord",
        status: 3,
        summary: `Misconfigured Discord server setting. Schedules / programming, messages, analytics, etc will not be posted to Discord until fixed.`,
        data: `The Discord bot is online, but either there is no WWSU server configured, or the configuration is wrong. The bot will not update the WWSU server with schedule changes, messages, analytics, etc until fixed.
        <br /><strong>TO FIX:</strong>
        <ol>
          <li>Get the snowflake ID of the WWSU server (In Discord Settings -> Advanced, activate "Developer Mode". Then, right-click the WWSU server and click "Copy ID").</li>
          <li>Put the ID in configuration... administration DJ Controls -> System Settings -> Discord and set the Server / Guild ID to the copied snowflake.</li>
          <li>Restart the application in administration DJ Controls -> Maintenance -> Restart Node / SailsJS Application.</li>
        </ol>`,
      });
    }
  },
};

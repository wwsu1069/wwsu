let rateLimitTimeoutUntil;
let rateLimitTimeout;

module.exports = {
  friendlyName: "Rate limit",

  description: "",

  inputs: {
    rateLimitData: {
      type: "ref",
    },
  },

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs) {
    await sails.helpers.status.modify.with({
      name: "discord",
      status: 4,
      summary: `API rate limit encountered.`,
      data: `The Discord bot hit a rate limit: ${inputs.rateLimitData.method} ${inputs.rateLimitData.path} ${inputs.rateLimitData.route}. This should resolve itself over time.`,
    });

    if (
      moment(rateLimitTimeoutUntil).isBefore(
        moment().add(inputs.rateLimitData.timeout, "milliseconds")
      )
    ) {
      rateLimitTimeoutUntil = moment().add(
        inputs.rateLimitData.timeout,
        "milliseconds"
      );
      clearTimeout(rateLimitTimeout);

      rateLimitTimeout = setTimeout(async () => {
        if (
          typeof DiscordClient === "undefined" ||
          !DiscordClient ||
          !DiscordClient.readyTimestamp ||
          DiscordClient.ws.status === 5
        )
          return;

        await sails.helpers.status.modify.with({
          name: "discord",
          status: 5,
          summary: `Online.`,
          data: `The Discord Bot is online and operational.`,
        });
      }, inputs.rateLimitData.timeout);
    }
  },
};

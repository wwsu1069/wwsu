const WWSU = require("../../assets/plugins/wwsu-sails/js/wwsu.js");
const throttleQueue = new WWSU.WWSUqueue();

module.exports = {
  friendlyName: "Throttle",

  description:
    "Potentially throttle or reject a function if the server is overloaded.",

  inputs: {
    fn: {
      type: "ref",
      required: true,
      custom: function (value) {
        return _.isFunction(value);
      },
      description: "Function being requested",
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Helper throttle called.");

    // Continue immediately if the task queue is good and throttleAPI is not active.
    if (
      throttleQueue.taskList.length < 1000 &&
      !sails.models.status.errorCheck.throttleAPI
    ) {
      sails.log.debug("No throttling necessary at this time.");
      inputs.fn();
      return {
        result: "SUCCESS",
        queue: throttleQueue.taskList.length,
        status: sails.models.status.errorCheck.throttleAPI,
      };
    }

    // Too many pending requests, or throttleAPI set to 2? Reject the request.
    if (
      throttleQueue.taskList.length >= 1000 ||
      sails.models.status.errorCheck.throttleAPI === 2
    ) {
      sails.log.debug("Throttled request rejected.");
      return {
        result: "REJECTED",
        queue: throttleQueue.taskList.length,
        status: sails.models.status.errorCheck.throttleAPI,
      };
    }

    // At this point, we must throttle the request
    sails.log.debug("Request throttled.");
    throttleQueue.add(inputs.fn);
    return {
      result: "THROTTLED",
      queue: throttleQueue.taskList.length,
      status: sails.models.status.errorCheck.throttleAPI,
    };
  },
};

const sh = require("shorthash");

module.exports = {
  friendlyName: "sails.helpers.getHost()",

  description: "Get the sh unique string for a host based on the session ID, socket ID, or the IP address.",

  inputs: {
    req: {
      type: "ref",
      required: true,
      description: "The req object.",
    },
  },

  fn: async function (inputs) {
    if (inputs.req.session && inputs.req.session.id) {
      return sh.unique(inputs.req.session.id + sails.config.custom.basic.hostSecret);
    }

    let fromIP = await sails.helpers.getIp(inputs.req);
    return sh.unique(fromIP + sails.config.custom.basic.hostSecret);
  },
};

const { constants } = require("os");

module.exports = {
  friendlyName: "analytics.showTime",

  description: "Get analytics about shows and DJs.",

  inputs: {
    djs: {
      type: "json",
      required: false,
      custom: function (value) {
        var valid = true;
        if (value.length > 0) {
          value.map((val) => {
            if (isNaN(val)) valid = false;
          });
        }
        return valid;
      },
      description: `Array of DJ IDs if you want showtime records for specific DJs. If not provided, will return all applicable DJs.`,
    },
    calendarIDs: {
      type: "json",
      required: false,
      custom: function (value) {
        var valid = true;
        if (value.length > 0) {
          value.map((val) => {
            if (isNaN(val)) valid = false;
          });
        }
        return valid;
      },
      description: `Array of calendar IDs of a calendar if you only want showtime records for specific shows/calendars. If not provided, will return all applicable shows.`,
    },
    start: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      description: `moment() parsable string of the start date/time for analytics. Defaults to one week ago.`,
    },
    end: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      description: `moment() parsable string of the end date/time for analytics. Defaults to now.`,
    },
    immediate: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, analytics will be calculated immediately without using a task queue. WARNING! This could freeze all other operations temporarily.",
    },
  },

  fn: async function (inputs) {
    // Initialize object storage for analytics
    let DJs = {};
    let shows = {};

    // Define functions for creating new analytic records
    const initializeShow = (id) => {
      if (
        id &&
        typeof shows[id] === "undefined" &&
        (!inputs.calendarIDs ||
          inputs.calendarIDs.find((cal) => parseInt(cal) === parseInt(id)))
      ) {
        sails.log.debug(`analytics.showtime: INITIALIZED SHOW ${id}`);
        shows[id] = _.cloneDeep(template);
      }
    };
    const initializeDJ = (id) => {
      if (
        id &&
        typeof DJs[id] === "undefined" &&
        (!inputs.djs || inputs.djs.find((dj) => parseInt(dj) === parseInt(id)))
      ) {
        sails.log.debug(`analytics.showtime: INITIALIZED DJ ${id}`);
        DJs[id] = _.cloneDeep(template);
      }
    };

    // Define an analytics template (should be read-only constant to prevent accidental overwrite)
    const template = {
      name: "Unknown", // Name of event or DJ
      type: "event", // Type of event (if applicable)
      discordChannel: null, // Snowflake ID of the relevant Discord channel if applicable
      showTime: 0, // On-Air time in minutes
      tuneIns: 0, // Number of times someone tuned in to the online stream during the show
      listenerMinutes: 0, // Number of online listener minutes during programming
      listenerPeak: 0, // Peak number of online listeners
      listenerPeakTime: null, // ISO time of when the peak online listeners took place
      videoTime: 0, // Number of minutes of video stream time during the broadcast(s)
      viewerTuneIns: 0, // Number of times someone tuned in to the video stream during the show (if a video stream was used)
      viewerMinutes: 0, // Number of video viewer minutes during programming
      viewerPeak: 0, // Peak number of viewers on the video stream
      viewerPeakTime: null, // ISO time of when the peak viewers on the video stream took place
      ratio: 0, // Overall ratio of listenerMinutes to showtime
      ratioPeak: 0, // Highest listenerMinutes to showtime ratio on a per-broadcast basis
      ratioPeakTime: null, // Date/time the ratioPeak happened
      viewerRatio: 1, // Overall ratio of viewerMinutes to showtime
      viewerRatioPeak: 0, // Highest viewerMinutes to showtime ratio on a per-broadcast basis
      viewerRatioPeakTime: null, // Date/time the viewerRatioPeak happened
      webMessages: 0, // Messages sent and received
      remoteCredits: 0, // Remote credits earned during the time period
      warningPoints: 0, // Warning points issued during the time period
      shows: 0, // Number of live shows
      prerecords: 0, // Number of pre-recorded shows
      remotes: 0, // Number of remote broadcasts
      genres: 0, // Number of genre rotations
      playlists: 0, // Number of playlists
      sports: 0, // Number of sports broadcasts
      breaks: 0, // Number of breaks taken
      earlyStart: [], // array of ISO times a broadcast started early
      lateStart: [], // array of ISO times a broadcast started late
      earlyEnd: [], // array of ISO times a broadcast ended early
      lateEnd: [], // array of ISO times a broadcast ended late
      absences: [], // array of ISO times with unexcused absences
      cancellations: [], // array of ISO times a broadcast was cancelled
      unauthorized: [], // array of ISO times a broadcast went on the air unauthorized/unscheduled
      missedIDs: [], // array of ISO times a top of the hour ID break was missed
      silences: [], // array of ISO times the silence detection was triggered
      dumps: [], // array of ISO times the dump button was triggered on the delay system
      warnings: [], // array of objects containing warnings issued against the DJ (if applicable)
      reputationScore: 0, // Number of reputation points earned
      reputationScoreMax: 0, // Maximum number of reputation points that could have been earned
      reputationPercent: 0, // Reputation percent (higher usually means more responsible DJs)
    };

    // Determine time range
    let start = inputs.start
      ? moment(inputs.start).format("YYYY-MM-DD HH:mm:ss")
      : moment().subtract(1, "weeks").format("YYYY-MM-DD HH:mm:ss");
    let end = inputs.end
      ? moment(inputs.end).format("YYYY-MM-DD HH:mm:ss")
      : moment().format("YYYY-MM-DD HH:mm:ss");

    // filter out all falsey values from inputs
    if (inputs.djs && typeof inputs.djs.filter !== "undefined")
      inputs.djs = inputs.djs.filter((dj) => typeof dj === "number");
    if (inputs.calendarIDs && typeof inputs.calendarIDs.filter !== "undefined")
      inputs.calendarIDs = inputs.calendarIDs.filter(
        (cal) => cal && typeof cal === "number"
      );

    // Preload show groups
    shows[0] = Object.assign(_.cloneDeep(template), {
      name: "All programming",
    });
    shows[-1] = Object.assign(_.cloneDeep(template), {
      name: "Live shows, remotes, and prerecords",
    });
    shows[-2] = Object.assign(_.cloneDeep(template), {
      name: "Sports broadcasts",
    });
    shows[-3] = Object.assign(_.cloneDeep(template), {
      name: "Genre rotations",
    });
    shows[-4] = Object.assign(_.cloneDeep(template), { name: "Playlists" });

    // Preload provided djs and calendarIDs even if they end up having zeroed analytics
    if (inputs.djs) inputs.djs.forEach((dj) => initializeDJ(dj));
    if (inputs.calendarIDs)
      inputs.calendarIDs.forEach((cal) => initializeShow(cal));

    // Form a query
    let query = {};
    if (inputs.djs && inputs.djs.length > 0) {
      query.or = [
        { dj: inputs.djs },
        { cohostDJ1: inputs.djs },
        { cohostDJ2: inputs.djs },
        { cohostDJ3: inputs.djs },
      ];
    }
    if (inputs.calendarIDs && inputs.calendarIDs.length > 0) {
      query.calendarID = inputs.calendarIDs;
    }

    let criteria = _.cloneDeep(query);

    // Get the attendance records
    let attendance = await sails.models.attendance.find(criteria);

    // Waterline ORM would not be able to filter this properly in the query.
    attendance = attendance.filter((record) =>
      record.actualStart
        ? moment(record.actualStart).isSameOrAfter(moment(start)) &&
          moment(record.actualStart).isBefore(moment(end))
        : moment(record.scheduledStart || record.createdAt).isSameOrAfter(
            moment(start)
          ) &&
          moment(record.scheduledStart || record.createdAt).isBefore(
            moment(end)
          )
    );

    // Showtime, tuneins, listenerMinutes, videoTime, viewerMinutes, and webMessages calculations
    // Note: While we will count the number of sports broadcasts and genres, the other analytics will not be considered
    const process2 = new Promise(async (resolve, reject) => {
      // Ignore attendance records without showTime or listenerMinutes or viewerMinutes analytics
      let records = attendance.filter(
        (record) =>
          record.showTime !== null &&
          (record.listenerMinutes !== null || record.viewerMinutes !== null)
      );

      let tasksLeft = records.length;
      if (tasksLeft <= 0) return resolve();

      const process2_2 = (record) => {
        ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
          initializeDJ(record[dj]);
          if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;
          if (
            !record.event.toLowerCase().startsWith("sports: ") &&
            !record.event.toLowerCase().startsWith("genre: ")
          ) {
            DJs[record[dj]].showTime += record.showTime;
            DJs[record[dj]].tuneIns += record.tuneIns;
            DJs[record[dj]].listenerMinutes += record.listenerMinutes;
            DJs[record[dj]].videoTime += record.videoTime;
            DJs[record[dj]].viewerTuneIns += record.viewerTuneIns;
            DJs[record[dj]].viewerMinutes += record.viewerMinutes;
            DJs[record[dj]].webMessages += record.webMessages;

            if (record.listenerPeak > DJs[record[dj]].listenerPeak) {
              DJs[record[dj]].listenerPeak = record.listenerPeak;
              DJs[record[dj]].listenerPeakTime = record.listenerPeakTime;
            }
            if (record.viewerPeak > DJs[record[dj]].viewerPeak) {
              DJs[record[dj]].viewerPeak = record.viewerPeak;
              DJs[record[dj]].viewerPeakTime = record.viewerPeakTime;
            }

            // Calculate peak ratios
            if (record.showTime > 0) {
              let ratio = record.listenerMinutes / record.showTime;
              if (ratio > DJs[record[dj]].ratioPeak) {
                DJs[record[dj]].ratioPeak = ratio;
                DJs[record[dj]].ratioPeakTime = moment(
                  record.actualStart
                ).toISOString(true);
              }

              ratio = record.viewerMinutes / record.showTime;
              if (ratio > DJs[record[dj]].viewerRatioPeak) {
                DJs[record[dj]].viewerRatioPeak = ratio;
                DJs[record[dj]].viewerRatioPeakTime = moment(
                  record.actualStart
                ).toISOString(true);
              }
            }
          }
        });

        shows[0].showTime += record.showTime;
        shows[0].tuneIns += record.tuneIns;
        shows[0].listenerMinutes += record.listenerMinutes;
        shows[0].videoTime += record.videoTime;
        shows[0].viewerTuneIns += record.viewerTuneIns;
        shows[0].viewerMinutes += record.viewerMinutes;
        shows[0].webMessages += record.webMessages;

        if (record.listenerPeak > shows[0].listenerPeak) {
          shows[0].listenerPeak = record.listenerPeak;
          shows[0].listenerPeakTime = record.listenerPeakTime;
        }
        if (record.viewerPeak > shows[0].viewerPeak) {
          shows[0].viewerPeak = record.viewerPeak;
          shows[0].viewerPeakTime = record.viewerPeakTime;
        }

        // Calculate peak ratios
        if (record.showTime > 0) {
          let ratio = record.listenerMinutes / record.showTime;
          if (ratio > shows[0].ratioPeak) {
            shows[0].ratioPeak = ratio;
            shows[0].ratioPeakTime = moment(record.actualStart).toISOString(
              true
            );
          }

          ratio = record.viewerMinutes / record.showTime;
          if (ratio > shows[0].viewerRatioPeak) {
            shows[0].viewerRatioPeak = ratio;
            shows[0].viewerRatioPeakTime = moment(
              record.actualStart
            ).toISOString(true);
          }
        }

        if (
          record.event.toLowerCase().startsWith("show: ") ||
          record.event.toLowerCase().startsWith("remote: ") ||
          record.event.toLowerCase().startsWith("prerecord: ")
        ) {
          shows[-1].showTime += record.showTime;
          shows[-1].tuneIns += record.tuneIns;
          shows[-1].listenerMinutes += record.listenerMinutes;
          shows[-1].videoTime += record.videoTime;
          shows[-1].viewerTuneIns += record.viewerTuneIns;
          shows[-1].viewerMinutes += record.viewerMinutes;
          shows[-1].webMessages += record.webMessages;

          if (record.listenerPeak > shows[-1].listenerPeak) {
            shows[-1].listenerPeak = record.listenerPeak;
            shows[-1].listenerPeakTime = record.listenerPeakTime;
          }
          if (record.viewerPeak > shows[-1].viewerPeak) {
            shows[-1].viewerPeak = record.viewerPeak;
            shows[-1].viewerPeakTime = record.viewerPeakTime;
          }

          // Calculate peak ratios
          if (record.showTime > 0) {
            let ratio = record.listenerMinutes / record.showTime;
            if (ratio > shows[-1].ratioPeak) {
              shows[-1].ratioPeak = ratio;
              shows[-1].ratioPeakTime = moment(record.actualStart).toISOString(
                true
              );
            }

            ratio = record.viewerMinutes / record.showTime;
            if (ratio > shows[-1].viewerRatioPeak) {
              shows[-1].viewerRatioPeak = ratio;
              shows[-1].viewerRatioPeakTime = moment(
                record.actualStart
              ).toISOString(true);
            }
          }
        }

        if (record.event.toLowerCase().startsWith("sports: ")) {
          shows[-2].showTime += record.showTime;
          shows[-2].tuneIns += record.tuneIns;
          shows[-2].listenerMinutes += record.listenerMinutes;
          shows[-2].videoTime += record.videoTime;
          shows[-2].viewerTuneIns += record.viewerTuneIns;
          shows[-2].viewerMinutes += record.viewerMinutes;
          shows[-2].webMessages += record.webMessages;

          if (record.listenerPeak > shows[-2].listenerPeak) {
            shows[-2].listenerPeak = record.listenerPeak;
            shows[-2].listenerPeakTime = record.listenerPeakTime;
          }
          if (record.viewerPeak > shows[-2].viewerPeak) {
            shows[-2].viewerPeak = record.viewerPeak;
            shows[-2].viewerPeakTime = record.viewerPeakTime;
          }

          // Calculate peak ratios
          if (record.showTime > 0) {
            let ratio = record.listenerMinutes / record.showTime;
            if (ratio > shows[-2].ratioPeak) {
              shows[-2].ratioPeak = ratio;
              shows[-2].ratioPeakTime = moment(record.actualStart).toISOString(
                true
              );
            }

            ratio = record.viewerMinutes / record.showTime;
            if (ratio > shows[-2].viewerRatioPeak) {
              shows[-2].viewerRatioPeak = ratio;
              shows[-2].viewerRatioPeakTime = moment(
                record.actualStart
              ).toISOString(true);
            }
          }
        }

        if (record.event.toLowerCase().startsWith("genre: ")) {
          shows[-3].showTime += record.showTime;
          shows[-3].tuneIns += record.tuneIns;
          shows[-3].listenerMinutes += record.listenerMinutes;
          shows[-3].videoTime += record.videoTime;
          shows[-3].viewerTuneIns += record.viewerTuneIns;
          shows[-3].viewerMinutes += record.viewerMinutes;
          shows[-3].webMessages += record.webMessages;

          if (record.listenerPeak > shows[-3].listenerPeak) {
            shows[-3].listenerPeak = record.listenerPeak;
            shows[-3].listenerPeakTime = record.listenerPeakTime;
          }
          if (record.viewerPeak > shows[-3].viewerPeak) {
            shows[-3].viewerPeak = record.viewerPeak;
            shows[-3].viewerPeakTime = record.viewerPeakTime;
          }

          // Calculate peak ratios
          if (record.showTime > 0) {
            let ratio = record.listenerMinutes / record.showTime;
            if (ratio > shows[-3].ratioPeak) {
              shows[-3].ratioPeak = ratio;
              shows[-3].ratioPeakTime = moment(record.actualStart).toISOString(
                true
              );
            }

            ratio = record.viewerMinutes / record.showTime;
            if (ratio > shows[-3].viewerRatioPeak) {
              shows[-3].viewerRatioPeak = ratio;
              shows[-3].viewerRatioPeakTime = moment(
                record.actualStart
              ).toISOString(true);
            }
          }
        }

        if (record.event.toLowerCase().startsWith("playlist: ")) {
          shows[-4].showTime += record.showTime;
          shows[-4].tuneIns += record.tuneIns;
          shows[-4].listenerMinutes += record.listenerMinutes;
          shows[-4].videoTime += record.videoTime;
          shows[-4].viewerTuneIns += record.viewerTuneIns;
          shows[-4].viewerMinutes += record.viewerMinutes;
          shows[-4].webMessages += record.webMessages;

          if (record.listenerPeak > shows[-4].listenerPeak) {
            shows[-4].listenerPeak = record.listenerPeak;
            shows[-4].listenerPeakTime = record.listenerPeakTime;
          }
          if (record.viewerPeak > shows[-4].viewerPeak) {
            shows[-4].viewerPeak = record.viewerPeak;
            shows[-4].viewerPeakTime = record.viewerPeakTime;
          }

          // Calculate peak ratios
          if (record.showTime > 0) {
            let ratio = record.listenerMinutes / record.showTime;
            if (ratio > shows[-4].ratioPeak) {
              shows[-4].ratioPeak = ratio;
              shows[-4].ratioPeakTime = moment(record.actualStart).toISOString(
                true
              );
            }

            ratio = record.viewerMinutes / record.showTime;
            if (ratio > shows[-4].viewerRatioPeak) {
              shows[-4].viewerRatioPeak = ratio;
              shows[-4].viewerRatioPeakTime = moment(
                record.actualStart
              ).toISOString(true);
            }
          }
        }

        initializeShow(record.calendarID);
        if (
          record.calendarID &&
          typeof shows[record.calendarID] !== "undefined"
        ) {
          shows[record.calendarID].showTime += record.showTime;
          shows[record.calendarID].tuneIns += record.tuneIns;
          shows[record.calendarID].listenerMinutes += record.listenerMinutes;
          shows[record.calendarID].videoTime += record.videoTime;
          shows[record.calendarID].viewerTuneIns += record.viewerTuneIns;
          shows[record.calendarID].viewerMinutes += record.viewerMinutes;
          shows[record.calendarID].webMessages += record.webMessages;

          if (record.listenerPeak > shows[record.calendarID].listenerPeak) {
            shows[record.calendarID].listenerPeak = record.listenerPeak;
            shows[record.calendarID].listenerPeakTime = record.listenerPeakTime;
          }
          if (record.viewerPeak > shows[record.calendarID].viewerPeak) {
            shows[record.calendarID].viewerPeak = record.viewerPeak;
            shows[record.calendarID].viewerPeakTime = record.viewerPeakTime;
          }

          // Calculate peak ratios
          if (record.showTime > 0) {
            let ratio = record.listenerMinutes / record.showTime;
            if (ratio > shows[record.calendarID].ratioPeak) {
              shows[record.calendarID].ratioPeak = ratio;
              shows[record.calendarID].ratioPeakTime = moment(
                record.actualStart
              ).toISOString(true);
            }

            ratio = record.viewerMinutes / record.showTime;
            if (ratio > shows[record.calendarID].viewerRatioPeak) {
              shows[record.calendarID].viewerRatioPeak = ratio;
              shows[record.calendarID].viewerRatioPeakTime = moment(
                record.actualStart
              ).toISOString(true);
            }
          }
        }

        tasksLeft--;
        if (tasksLeft <= 0) resolve();
      };

      records.map((record) => {
        if (!inputs.immediate) {
          WWSUqueue.add(() => process2_2(_.cloneDeep(record)));
        } else {
          process2_2(_.cloneDeep(record));
        }
      });
    });

    // Calculate broadcast counts and maximum reputation score
    const process3 = new Promise(async (resolve, reject) => {
      var unique = {};

      let records = attendance.filter(
        (record) =>
          (inputs.djs &&
            inputs.djs.length > 0 &&
            (inputs.djs.indexOf(record.dj) !== -1 ||
              inputs.djs.indexOf(record.cohostDJ1) !== -1 ||
              inputs.djs.indexOf(record.cohostDJ2) !== -1 ||
              inputs.djs.indexOf(record.cohostDJ3) !== -1)) ||
          ((!inputs.djs || inputs.djs.length === 0) &&
            (record.dj ||
              record.cohostDJ1 ||
              record.cohostDJ2 ||
              record.cohostDJ3))
      );

      let tasksLeft = records.length;
      if (tasksLeft <= 0) return resolve();

      await new Promise(async (resolve2) => {
        // Calculate how many duplicate records for the same show exists and add reputation score to offset a penalty
        const process3_2 = (record) => {
          if (record.unique !== null && record.unique !== ``) {
            if (
              record.unique in unique &&
              record.showTime &&
              record.scheduledStart !== null &&
              record.scheduledEnd !== null
            ) {
              // Update showTime and breaks to be correct as it is necessary for break reputation calculations
              if (record.showTime) {
                !unique[record.unique].showTime
                  ? (unique[record.unique].showTime = record.showTime)
                  : (unique[record.unique].showTime += record.showTime);
              }
              if (record.breaks) {
                !unique[record.unique].breaks
                  ? (unique[record.unique].breaks = record.breaks)
                  : (unique[record.unique].breaks += record.breaks);
              }

              ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
                initializeDJ(record[dj]);
                if (!record[dj] || typeof DJs[record[dj]] === "undefined") {
                  return;
                }

                DJs[record[dj]].reputationScoreMax +=
                  sails.config.custom.analytics.signOnLatePenalty / 2;
              });

              shows[0].reputationScoreMax +=
                sails.config.custom.analytics.signOnLatePenalty / 2;

              if (
                record.event.toLowerCase().startsWith("show: ") ||
                record.event.toLowerCase().startsWith("remote: ") ||
                record.event.toLowerCase().startsWith("prerecord: ")
              ) {
                shows[-1].reputationScoreMax +=
                  sails.config.custom.analytics.signOnLatePenalty / 2;
              }

              if (record.event.toLowerCase().startsWith("sports: ")) {
                shows[-2].reputationScoreMax +=
                  sails.config.custom.analytics.signOnLatePenalty / 2;
              }

              if (record.event.toLowerCase().startsWith("genre: ")) {
                shows[-3].reputationScoreMax +=
                  sails.config.custom.analytics.signOnLatePenalty / 2;
              }

              if (record.event.toLowerCase().startsWith("playlist: ")) {
                shows[-4].reputationScoreMax +=
                  sails.config.custom.analytics.signOnLatePenalty / 2;
              }

              initializeShow(record.calendarID);
              if (
                record.calendarID &&
                typeof shows[record.calendarID] !== "undefined"
              ) {
                shows[record.calendarID].reputationScoreMax +=
                  sails.config.custom.analytics.signOnLatePenalty / 2;
              }
            }

            unique[record.unique] = _.cloneDeep(record);
          }

          tasksLeft--;
          if (tasksLeft <= 0) resolve2();
        };

        records.map((record) => {
          if (!inputs.immediate) {
            WWSUqueue.add(() => process3_2(_.cloneDeep(record)));
          } else {
            process3_2(_.cloneDeep(record));
          }
        });
      });

      let tasksLeft2 = 0;

      // Calculate breaks reputation
      await new Promise(async (resolve2) => {
        const process3_3 = (record) => {
          if (
            record.actualStart !== null &&
            record.actualEnd !== null &&
            record.happened === 1
          ) {
            // Determine how many reputation points should be earned for breaks
            let breakPoints =
              Math.min(
                1,
                record.showTime > 0
                  ? record.breaks /
                      (sails.config.custom.analytics.idealBreaks *
                        (record.showTime / 60))
                  : 0
              ) * sails.config.custom.analytics.breakReputation;

            if (record.event.toLowerCase().startsWith("show: ")) {
              ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
                initializeDJ(record[dj]);
                if (!record[dj] || typeof DJs[record[dj]] === "undefined")
                  return;
                DJs[record[dj]].shows += 1;
                DJs[record[dj]].breaks += record.breaks;
              });

              shows[0].shows += 1;
              shows[0].breaks += record.breaks;
              shows[-1].shows += 1;
              shows[-1].breaks += record.breaks;

              initializeShow(record.calendarID);
              if (
                record.calendarID &&
                typeof shows[record.calendarID] !== "undefined"
              ) {
                shows[record.calendarID].shows += 1;
                shows[record.calendarID].breaks += record.breaks;
              }
              if (
                record.scheduledStart !== null &&
                record.scheduledEnd !== null
              ) {
                ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
                  initializeDJ(record[dj]);
                  if (!record[dj] || typeof DJs[record[dj]] === "undefined")
                    return;
                  DJs[record[dj]].reputationScoreMax +=
                    sails.config.custom.analytics.showReputation +
                    sails.config.custom.analytics.breakReputation;
                  DJs[record[dj]].reputationScore -=
                    sails.config.custom.analytics.breakReputation - breakPoints;
                });
                shows[0].reputationScoreMax +=
                  sails.config.custom.analytics.showReputation +
                  sails.config.custom.analytics.breakReputation;
                shows[0].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
                shows[-1].reputationScoreMax +=
                  sails.config.custom.analytics.showReputation +
                  sails.config.custom.analytics.breakReputation;
                shows[-1].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
                initializeShow(record.calendarID);
                if (
                  record.calendarID &&
                  typeof shows[record.calendarID] !== "undefined"
                ) {
                  shows[record.calendarID].reputationScoreMax +=
                    sails.config.custom.analytics.showReputation +
                    sails.config.custom.analytics.breakReputation;
                  shows[record.calendarID].reputationScore -=
                    sails.config.custom.analytics.breakReputation - breakPoints;
                }
              }
            } else if (record.event.toLowerCase().startsWith("prerecord: ")) {
              ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
                initializeDJ(record[dj]);
                if (!record[dj] || typeof DJs[record[dj]] === "undefined")
                  return;
                DJs[record[dj]].prerecords += 1;
                DJs[record[dj]].breaks += record.breaks;
                DJs[record[dj]].reputationScoreMax +=
                  sails.config.custom.analytics.prerecordReputation +
                  sails.config.custom.analytics.breakReputation;
                DJs[record[dj]].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
              });

              shows[0].prerecords += 1;
              shows[0].reputationScoreMax +=
                sails.config.custom.analytics.prerecordReputation +
                sails.config.custom.analytics.breakReputation;
              shows[0].reputationScore -=
                sails.config.custom.analytics.breakReputation - breakPoints;
              shows[0].breaks += record.breaks;
              shows[-1].prerecords += 1;
              shows[-1].reputationScoreMax +=
                sails.config.custom.analytics.prerecordReputation +
                sails.config.custom.analytics.breakReputation;
              shows[-1].reputationScore -=
                sails.config.custom.analytics.breakReputation - breakPoints;
              shows[-1].breaks += record.breaks;

              initializeShow(record.calendarID);
              if (
                record.calendarID &&
                typeof shows[record.calendarID] !== "undefined"
              ) {
                shows[record.calendarID].prerecords += 1;
                shows[record.calendarID].reputationScoreMax +=
                  sails.config.custom.analytics.prerecordReputation +
                  sails.config.custom.analytics.breakReputation;
                shows[record.calendarID].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
                shows[record.calendarID].breaks += record.breaks;
              }
            } else if (record.event.toLowerCase().startsWith("remote: ")) {
              ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
                initializeDJ(record[dj]);
                if (!record[dj] || typeof DJs[record[dj]] === "undefined")
                  return;
                DJs[record[dj]].remotes += 1;
                DJs[record[dj]].breaks += record.breaks;
              });

              shows[0].remotes += 1;
              shows[0].breaks += record.breaks;
              shows[-1].remotes += 1;
              shows[-1].breaks += record.breaks;

              initializeShow(record.calendarID);
              if (
                record.calendarID &&
                typeof shows[record.calendarID] !== "undefined"
              ) {
                shows[record.calendarID].remotes += 1;
                shows[record.calendarID].breaks += record.breaks;
              }
              if (
                record.scheduledStart !== null &&
                record.scheduledEnd !== null
              ) {
                ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
                  initializeDJ(record[dj]);
                  if (!record[dj] || typeof DJs[record[dj]] === "undefined")
                    return;
                  DJs[record[dj]].reputationScoreMax +=
                    sails.config.custom.analytics.remoteReputation +
                    sails.config.custom.analytics.breakReputation;
                  DJs[record[dj]].reputationScore -=
                    sails.config.custom.analytics.breakReputation - breakPoints;
                });
                shows[0].reputationScoreMax +=
                  sails.config.custom.analytics.remoteReputation +
                  sails.config.custom.analytics.breakReputation;
                shows[0].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
                shows[-1].reputationScoreMax +=
                  sails.config.custom.analytics.remoteReputation +
                  sails.config.custom.analytics.breakReputation;
                shows[-1].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
                initializeShow(record.calendarID);
                if (
                  record.calendarID &&
                  typeof shows[record.calendarID] !== "undefined"
                ) {
                  shows[record.calendarID].reputationScoreMax +=
                    sails.config.custom.analytics.remoteReputation +
                    sails.config.custom.analytics.breakReputation;
                  shows[record.calendarID].reputationScore -=
                    sails.config.custom.analytics.breakReputation - breakPoints;
                }
              }
            } else if (record.event.toLowerCase().startsWith("sports: ")) {
              ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
                initializeDJ(record[dj]);
                if (!record[dj] || typeof DJs[record[dj]] === "undefined")
                  return;
                DJs[record[dj]].sports += 1;
              });

              shows[0].sports += 1;
              shows[0].breaks += record.breaks;
              shows[0].reputationScoreMax +=
                sails.config.custom.analytics.sportsReputation +
                sails.config.custom.analytics.breakReputation;
              shows[0].reputationScore -=
                sails.config.custom.analytics.breakReputation - breakPoints;
              shows[-2].sports += 1;
              shows[-2].breaks += record.breaks;
              shows[-2].reputationScoreMax +=
                sails.config.custom.analytics.sportsReputation +
                sails.config.custom.analytics.breakReputation;
              shows[-2].reputationScore -=
                sails.config.custom.analytics.breakReputation - breakPoints;

              initializeShow(record.calendarID);
              if (
                record.calendarID &&
                typeof shows[record.calendarID] !== "undefined"
              ) {
                shows[record.calendarID].sports += 1;
                shows[record.calendarID].breaks += record.breaks;
                shows[record.calendarID].reputationScoreMax +=
                  sails.config.custom.analytics.sportsReputation +
                  sails.config.custom.analytics.breakReputation;
                shows[record.calendarID].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
              }
            } else if (record.event.toLowerCase().startsWith("genre: ")) {
              shows[0].genres += 1;
              shows[0].breaks += record.breaks;
              shows[0].reputationScoreMax +=
                sails.config.custom.analytics.genreReputation +
                sails.config.custom.analytics.breakReputation;
              shows[0].reputationScore -=
                sails.config.custom.analytics.breakReputation - breakPoints;
              shows[-3].genres += 1;
              shows[-3].breaks += record.breaks;
              shows[-3].reputationScoreMax +=
                sails.config.custom.analytics.genreReputation +
                sails.config.custom.analytics.breakReputation;
              shows[-3].reputationScore -=
                sails.config.custom.analytics.breakReputation - breakPoints;

              initializeShow(record.calendarID);
              if (
                record.calendarID &&
                typeof shows[record.calendarID] !== "undefined"
              ) {
                shows[record.calendarID].genres += 1;
                shows[record.calendarID].breaks += record.breaks;
                shows[record.calendarID].reputationScoreMax +=
                  sails.config.custom.analytics.genreReputation +
                  sails.config.custom.analytics.breakReputation;
                shows[record.calendarID].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
              }
            } else if (record.event.toLowerCase().startsWith("playlist: ")) {
              ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
                initializeDJ(record[dj]);
                if (!record[dj] || typeof DJs[record[dj]] === "undefined")
                  return;
                DJs[record[dj]].playlists += 1;
                DJs[record[dj]].breaks += record.breaks;
                DJs[record[dj]].reputationScoreMax +=
                  sails.config.custom.analytics.playlistReputation +
                  sails.config.custom.analytics.breakReputation;
                DJs[record[dj]].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
              });

              shows[0].playlists += 1;
              shows[0].breaks += record.breaks;
              shows[0].reputationScoreMax +=
                sails.config.custom.analytics.playlistReputation +
                sails.config.custom.analytics.breakReputation;
              shows[0].reputationScore -=
                sails.config.custom.analytics.breakReputation - breakPoints;
              shows[-4].playlists += 1;
              shows[-4].breaks += record.breaks;
              shows[-4].reputationScoreMax +=
                sails.config.custom.analytics.playlistReputation +
                sails.config.custom.analytics.breakReputation;
              shows[-4].reputationScore -=
                sails.config.custom.analytics.breakReputation - breakPoints;

              initializeShow(record.calendarID);
              if (
                record.calendarID &&
                typeof shows[record.calendarID] !== "undefined"
              ) {
                shows[record.calendarID].playlists += 1;
                shows[record.calendarID].breaks += record.breaks;
                shows[record.calendarID].reputationScoreMax +=
                  sails.config.custom.analytics.playlistReputation +
                  sails.config.custom.analytics.breakReputation;
                shows[record.calendarID].reputationScore -=
                  sails.config.custom.analytics.breakReputation - breakPoints;
              }
            }
          }

          tasksLeft2--;
          if (tasksLeft2 <= 0) resolve2();
        };

        // Now go through each unique record to calculate show stats and add reputation
        for (let uniqueRecord in unique) {
          if (Object.prototype.hasOwnProperty.call(unique, uniqueRecord)) {
            tasksLeft2++;
            if (!inputs.immediate) {
              WWSUqueue.add(() =>
                process3_3(_.cloneDeep(unique[uniqueRecord]))
              );
            } else {
              process3_3(_.cloneDeep(unique[uniqueRecord]));
            }
          }
        }

        if (tasksLeft2 <= 0) resolve2();
      });

      resolve();
    });

    // Calculate reputation score penalties and analytics (ignores excused)
    const process4 = new Promise(async (resolve, reject) => {
      let records = attendance.filter(
        (record) =>
          (inputs.djs &&
            inputs.djs.length > 0 &&
            (inputs.djs.indexOf(record.dj) !== -1 ||
              inputs.djs.indexOf(record.cohostDJ1) !== -1 ||
              inputs.djs.indexOf(record.cohostDJ2) !== -1 ||
              inputs.djs.indexOf(record.cohostDJ3) !== -1)) ||
          ((!inputs.djs || inputs.djs.length === 0) &&
            (record.dj ||
              record.cohostDJ1 ||
              record.cohostDJ2 ||
              record.cohostDJ3))
      );

      let tasksLeft = records.length;
      if (tasksLeft <= 0) return resolve();

      const process4_2 = (record) => {
        // Cancelled broadcasts
        if (record.cancellation) {
          ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
            initializeDJ(record[dj]);
            if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;

            if (
              !record.event.toLowerCase().startsWith("genre: ") &&
              !record.event.toLowerCase().startsWith("sports: ")
            )
              DJs[record[dj]].reputationScore -=
                sails.config.custom.analytics.cancellationPenalty;

            DJs[record[dj]].cancellations.push([
              record.event,
              record.scheduledStart,
            ]);
          });

          shows[0].reputationScore -=
            sails.config.custom.analytics.cancellationPenalty;
          shows[0].cancellations.push([record.event, record.scheduledStart]);
          if (
            record.event.toLowerCase().startsWith("show: ") ||
            record.event.toLowerCase().startsWith("remote: ") ||
            record.event.toLowerCase().startsWith("prerecord: ")
          ) {
            shows[-1].reputationScore -=
              sails.config.custom.analytics.cancellationPenalty;
            shows[-1].cancellations.push([record.event, record.scheduledStart]);
          }
          if (record.event.toLowerCase().startsWith("sports: ")) {
            shows[-2].reputationScore -=
              sails.config.custom.analytics.cancellationPenalty;
            shows[-2].cancellations.push([record.event, record.scheduledStart]);
          }
          if (record.event.toLowerCase().startsWith("genre: ")) {
            shows[-3].reputationScore -=
              sails.config.custom.analytics.cancellationPenalty;
            shows[-3].cancellations.push([record.event, record.scheduledStart]);
          }
          if (record.event.toLowerCase().startsWith("playlist: ")) {
            shows[-4].reputationScore -=
              sails.config.custom.analytics.cancellationPenalty;
            shows[-4].cancellations.push([record.event, record.scheduledStart]);
          }

          initializeShow(record.calendarID);
          if (
            record.calendarID &&
            typeof shows[record.calendarID] !== "undefined"
          ) {
            shows[record.calendarID].reputationScore -=
              sails.config.custom.analytics.cancellationPenalty;
            shows[record.calendarID].cancellations.push([
              record.event,
              record.scheduledStart,
            ]);
          }
        }

        // Number of times silence alarm was triggered
        if (record.silence && record.silence.length > 0) {
          ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
            initializeDJ(record[dj]);
            if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;

            if (
              !record.event.toLowerCase().startsWith("genre: ") &&
              !record.event.toLowerCase().startsWith("sports: ")
            )
              DJs[record[dj]].reputationScore -=
                record.silence.length *
                sails.config.custom.analytics.silenceAlarmPenalty;

            record.silence.map((silence) =>
              DJs[record[dj]].silences.push([record.event, silence])
            );
          });

          shows[0].reputationScore -=
            record.silence.length *
            sails.config.custom.analytics.silenceAlarmPenalty;
          record.silence.map((silence) =>
            shows[0].silences.push([record.event, silence])
          );
          if (
            record.event.toLowerCase().startsWith("show: ") ||
            record.event.toLowerCase().startsWith("remote: ") ||
            record.event.toLowerCase().startsWith("prerecord: ")
          ) {
            shows[-1].reputationScore -=
              record.silence.length *
              sails.config.custom.analytics.silenceAlarmPenalty;
            record.silence.map((silence) =>
              shows[-1].silences.push([record.event, silence])
            );
          }
          if (record.event.toLowerCase().startsWith("sports: ")) {
            shows[-2].reputationScore -=
              record.silence.length *
              sails.config.custom.analytics.silenceAlarmPenalty;
            record.silence.map((silence) =>
              shows[-2].silences.push([record.event, silence])
            );
          }
          if (record.event.toLowerCase().startsWith("genre: ")) {
            shows[-3].reputationScore -=
              record.silence.length *
              sails.config.custom.analytics.silenceAlarmPenalty;
            record.silence.map((silence) =>
              shows[-3].silences.push([record.event, silence])
            );
          }
          if (record.event.toLowerCase().startsWith("playlist: ")) {
            shows[-4].reputationScore -=
              record.silence.length *
              sails.config.custom.analytics.silenceAlarmPenalty;
            record.silence.map((silence) =>
              shows[-4].silences.push([record.event, silence])
            );
          }

          initializeShow(record.calendarID);
          if (
            record.calendarID &&
            typeof shows[record.calendarID] !== "undefined"
          ) {
            shows[record.calendarID].reputationScore -=
              record.silence.length *
              sails.config.custom.analytics.silenceAlarmPenalty;
            record.silence.map((silence) =>
              shows[record.calendarID].silences.push([record.event, silence])
            );
          }
        }

        // Absences
        if (record.absent) {
          ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
            initializeDJ(record[dj]);
            if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;

            if (
              !record.event.toLowerCase().startsWith("genre: ") &&
              !record.event.toLowerCase().startsWith("sports: ")
            )
              DJs[record[dj]].reputationScore -=
                sails.config.custom.analytics.absencePenalty;

            DJs[record[dj]].absences.push([
              record.event,
              record.scheduledStart,
            ]);
          });

          shows[0].reputationScore -=
            sails.config.custom.analytics.absencePenalty;
          shows[0].absences.push([record.event, record.scheduledStart]);
          if (
            record.event.toLowerCase().startsWith("show: ") ||
            record.event.toLowerCase().startsWith("remote: ") ||
            record.event.toLowerCase().startsWith("prerecord: ")
          ) {
            shows[-1].reputationScore -=
              sails.config.custom.analytics.absencePenalty;
            shows[-1].absences.push([record.event, record.scheduledStart]);
          } else if (record.event.toLowerCase().startsWith("sports: ")) {
            shows[-2].reputationScore -=
              sails.config.custom.analytics.absencePenalty;
            shows[-2].absences.push([record.event, record.scheduledStart]);
          } else if (record.event.toLowerCase().startsWith("genre: ")) {
            shows[-3].reputationScore -=
              sails.config.custom.analytics.absencePenalty;
            shows[-3].absences.push([record.event, record.scheduledStart]);
          } else if (record.event.toLowerCase().startsWith("playlist: ")) {
            shows[-4].reputationScore -=
              sails.config.custom.analytics.absencePenalty;
            shows[-4].absences.push([record.event, record.scheduledStart]);
          }

          initializeShow(record.calendarID);
          if (
            record.calendarID &&
            typeof shows[record.calendarID] !== "undefined"
          ) {
            shows[record.calendarID].reputationScore -=
              sails.config.custom.analytics.absencePenalty;
            shows[record.calendarID].absences.push([
              record.event,
              record.scheduledStart,
            ]);
          }
        }

        // Unauthorized broadcasts
        if (record.unauthorized) {
          ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
            initializeDJ(record[dj]);
            if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;

            if (
              !record.event.toLowerCase().startsWith("genre: ") &&
              !record.event.toLowerCase().startsWith("sports: ")
            )
              DJs[record[dj]].reputationScore -=
                sails.config.custom.analytics.unauthorizedBroadcastPenalty;

            DJs[record[dj]].unauthorized.push([
              record.event,
              record.scheduledStart,
            ]);
          });

          shows[0].reputationScore -=
            sails.config.custom.analytics.unauthorizedBroadcastPenalty;
          shows[0].unauthorized.push([record.event, record.scheduledStart]);
          if (
            record.event.toLowerCase().startsWith("show: ") ||
            record.event.toLowerCase().startsWith("remote: ") ||
            record.event.toLowerCase().startsWith("prerecord: ")
          ) {
            shows[-1].reputationScore -=
              sails.config.custom.analytics.unauthorizedBroadcastPenalty;
            shows[-1].unauthorized.push([record.event, record.scheduledStart]);
          } else if (record.event.toLowerCase().startsWith("sports: ")) {
            shows[-2].reputationScore -=
              sails.config.custom.analytics.unauthorizedBroadcastPenalty;
            shows[-2].unauthorized.push([record.event, record.scheduledStart]);
          } else if (record.event.toLowerCase().startsWith("genre: ")) {
            shows[-3].reputationScore -=
              sails.config.custom.analytics.unauthorizedBroadcastPenalty;
            shows[-3].unauthorized.push([record.event, record.scheduledStart]);
          } else if (record.event.toLowerCase().startsWith("playlist: ")) {
            shows[-4].reputationScore -=
              sails.config.custom.analytics.unauthorizedBroadcastPenalty;
            shows[-4].unauthorized.push([record.event, record.scheduledStart]);
          }

          initializeShow(record.calendarID);
          if (
            record.calendarID &&
            typeof shows[record.calendarID] !== "undefined"
          ) {
            shows[record.calendarID].reputationScore -=
              sails.config.custom.analytics.unauthorizedBroadcastPenalty;
            shows[record.calendarID].unauthorized.push([
              record.event,
              record.scheduledStart,
            ]);
          }
        }

        // Missed top-of-hour ID breaks
        if (record.missedIDs && record.missedIDs.length > 0) {
          ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
            initializeDJ(record[dj]);
            if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;

            if (
              !record.event.toLowerCase().startsWith("genre: ") &&
              !record.event.toLowerCase().startsWith("sports: ")
            )
              DJs[record[dj]].reputationScore -=
                sails.config.custom.analytics.missedIDPenalty *
                record.missedIDs.length;

            record.missedIDs.map((missedID) =>
              DJs[record[dj]].missedIDs.push([record.event, missedID])
            );
          });

          shows[0].reputationScore -=
            sails.config.custom.analytics.missedIDPenalty *
            record.missedIDs.length;
          record.missedIDs.map((missedID) =>
            shows[0].missedIDs.push([record.event, missedID])
          );
          if (
            record.event.toLowerCase().startsWith("show: ") ||
            record.event.toLowerCase().startsWith("remote: ") ||
            record.event.toLowerCase().startsWith("prerecord: ")
          ) {
            shows[-1].reputationScore -=
              sails.config.custom.analytics.missedIDPenalty *
              record.missedIDs.length;
            record.missedIDs.map((missedID) =>
              shows[-1].missedIDs.push([record.event, missedID])
            );
          } else if (record.event.toLowerCase().startsWith("sports: ")) {
            shows[-2].reputationScore -=
              sails.config.custom.analytics.missedIDPenalty *
              record.missedIDs.length;
            record.missedIDs.map((missedID) =>
              shows[-2].missedIDs.push([record.event, missedID])
            );
          } else if (record.event.toLowerCase().startsWith("genre: ")) {
            shows[-3].reputationScore -=
              sails.config.custom.analytics.missedIDPenalty *
              record.missedIDs.length;
            record.missedIDs.map((missedID) =>
              shows[-3].missedIDs.push([record.event, missedID])
            );
          } else if (record.event.toLowerCase().startsWith("playlist: ")) {
            shows[-4].reputationScore -=
              sails.config.custom.analytics.missedIDPenalty *
              record.missedIDs.length;
            record.missedIDs.map((missedID) =>
              shows[-4].missedIDs.push([record.event, missedID])
            );
          }

          initializeShow(record.calendarID);
          if (
            record.calendarID &&
            typeof shows[record.calendarID] !== "undefined"
          ) {
            shows[record.calendarID].reputationScore -=
              sails.config.custom.analytics.missedIDPenalty *
              record.missedIDs.length;
            record.missedIDs.map((missedID) =>
              shows[record.calendarID].missedIDs.push([record.event, missedID])
            );
          }
        }

        // Early sign-ons
        if (record.signedOnEarly) {
          ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
            initializeDJ(record[dj]);
            if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;

            if (
              !record.event.toLowerCase().startsWith("genre: ") &&
              !record.event.toLowerCase().startsWith("sports: ")
            )
              DJs[record[dj]].reputationScore -=
                sails.config.custom.analytics.SignOnEarlyPenalty;

            DJs[record[dj]].earlyStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          });

          shows[0].reputationScore -=
            sails.config.custom.analytics.SignOnEarlyPenalty;
          shows[0].earlyStart.push([
            record.event,
            record.actualStart,
            record.scheduledStart,
          ]);
          if (
            record.event.toLowerCase().startsWith("show: ") ||
            record.event.toLowerCase().startsWith("remote: ") ||
            record.event.toLowerCase().startsWith("prerecord: ")
          ) {
            shows[-1].reputationScore -=
              sails.config.custom.analytics.SignOnEarlyPenalty;
            shows[-1].earlyStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          } else if (record.event.toLowerCase().startsWith("sports: ")) {
            shows[-2].reputationScore -=
              sails.config.custom.analytics.SignOnEarlyPenalty;
            shows[-2].earlyStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          } else if (record.event.toLowerCase().startsWith("genre: ")) {
            shows[-3].reputationScore -=
              sails.config.custom.analytics.SignOnEarlyPenalty;
            shows[-3].earlyStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          } else if (record.event.toLowerCase().startsWith("playlist: ")) {
            shows[-4].reputationScore -=
              sails.config.custom.analytics.SignOnEarlyPenalty;
            shows[-4].earlyStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          }

          initializeShow(record.calendarID);
          if (
            record.calendarID &&
            typeof shows[record.calendarID] !== "undefined"
          ) {
            shows[record.calendarID].reputationScore -=
              sails.config.custom.analytics.SignOnEarlyPenalty;
            shows[record.calendarID].earlyStart.push([
              record.event,
              record.actualStart,
            ]);
          }
        }

        // Late sign-offs
        if (record.signedOffLate) {
          ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
            initializeDJ(record[dj]);
            if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;

            if (
              !record.event.toLowerCase().startsWith("genre: ") &&
              !record.event.toLowerCase().startsWith("sports: ")
            )
              DJs[record[dj]].reputationScore -=
                sails.config.custom.analytics.SignOffLatePenalty;

            DJs[record[dj]].lateEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          });

          shows[0].reputationScore -=
            sails.config.custom.analytics.SignOffLatePenalty;
          shows[0].lateEnd.push([
            record.event,
            record.actualEnd,
            record.scheduledEnd,
          ]);
          if (
            record.event.toLowerCase().startsWith("show: ") ||
            record.event.toLowerCase().startsWith("remote: ") ||
            record.event.toLowerCase().startsWith("prerecord: ")
          ) {
            shows[-1].reputationScore -=
              sails.config.custom.analytics.SignOffLatePenalty;
            shows[-1].lateEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          } else if (record.event.toLowerCase().startsWith("sports: ")) {
            shows[-2].reputationScore -=
              sails.config.custom.analytics.SignOffLatePenalty;
            shows[-2].lateEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          } else if (record.event.toLowerCase().startsWith("genre: ")) {
            shows[-3].reputationScore -=
              sails.config.custom.analytics.SignOffLatePenalty;
            shows[-3].lateEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          } else if (record.event.toLowerCase().startsWith("playlist: ")) {
            shows[-4].reputationScore -=
              sails.config.custom.analytics.SignOffLatePenalty;
            shows[-4].lateEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          }

          initializeShow(record.calendarID);
          if (
            record.calendarID &&
            typeof shows[record.calendarID] !== "undefined"
          ) {
            shows[record.calendarID].reputationScore -=
              sails.config.custom.analytics.SignOffLatePenalty;
            shows[record.calendarID].lateEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          }
        }

        // Late sign-ons
        if (record.signedOnLate) {
          ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
            initializeDJ(record[dj]);
            if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;

            if (
              !record.event.toLowerCase().startsWith("genre: ") &&
              !record.event.toLowerCase().startsWith("sports: ")
            )
              DJs[record[dj]].reputationScore -=
                sails.config.custom.analytics.SignOnLatePenalty;

            DJs[record[dj]].lateStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          });
          shows[0].reputationScore -=
            sails.config.custom.analytics.SignOnLatePenalty;
          shows[0].lateStart.push([
            record.event,
            record.actualStart,
            record.scheduledStart,
          ]);
          if (
            record.event.toLowerCase().startsWith("show: ") ||
            record.event.toLowerCase().startsWith("remote: ") ||
            record.event.toLowerCase().startsWith("prerecord: ")
          ) {
            shows[-1].reputationScore -=
              sails.config.custom.analytics.SignOnLatePenalty;
            shows[-1].lateStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          } else if (record.event.toLowerCase().startsWith("sports: ")) {
            shows[-2].reputationScore -=
              sails.config.custom.analytics.SignOnLatePenalty;
            shows[-2].lateStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          } else if (record.event.toLowerCase().startsWith("genre: ")) {
            shows[-3].reputationScore -=
              sails.config.custom.analytics.SignOnLatePenalty;
            shows[-3].lateStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          } else if (record.event.toLowerCase().startsWith("playlist: ")) {
            shows[-4].reputationScore -=
              sails.config.custom.analytics.SignOnLatePenalty;
            shows[-4].lateStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          }

          initializeShow(record.calendarID);
          if (
            record.calendarID &&
            typeof shows[record.calendarID] !== "undefined"
          ) {
            shows[record.calendarID].reputationScore -=
              sails.config.custom.analytics.SignOnLatePenalty;
            shows[record.calendarID].lateStart.push([
              record.event,
              record.actualStart,
              record.scheduledStart,
            ]);
          }
        }

        // Early sign-offs
        if (record.signedOffEarly) {
          ["dj", "cohostDJ1", "cohostDJ2", "cohostDJ3"].map((dj) => {
            initializeDJ(record[dj]);
            if (!record[dj] || typeof DJs[record[dj]] === "undefined") return;

            if (
              !record.event.toLowerCase().startsWith("genre: ") &&
              !record.event.toLowerCase().startsWith("sports: ")
            )
              DJs[record[dj]].reputationScore -=
                sails.config.custom.analytics.SignOffEarlyPenalty;

            DJs[record[dj]].earlyEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          });

          shows[0].reputationScore -=
            sails.config.custom.analytics.SignOffEarlyPenalty;
          shows[0].earlyEnd.push([
            record.event,
            record.actualEnd,
            record.scheduledEnd,
          ]);
          if (
            record.event.toLowerCase().startsWith("show: ") ||
            record.event.toLowerCase().startsWith("remote: ") ||
            record.event.toLowerCase().startsWith("prerecord: ")
          ) {
            shows[-1].reputationScore -=
              sails.config.custom.analytics.SignOffEarlyPenalty;
            shows[-1].earlyEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          } else if (record.event.toLowerCase().startsWith("sports: ")) {
            shows[-2].reputationScore -=
              sails.config.custom.analytics.SignOffEarlyPenalty;
            shows[-2].earlyEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          } else if (record.event.toLowerCase().startsWith("genre: ")) {
            shows[-3].reputationScore -=
              sails.config.custom.analytics.SignOffEarlyPenalty;
            shows[-3].earlyEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          } else if (record.event.toLowerCase().startsWith("playlist: ")) {
            shows[-4].reputationScore -=
              sails.config.custom.analytics.SignOffEarlyPenalty;
            shows[-4].earlyEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          }

          initializeShow(record.calendarID);
          if (
            record.calendarID &&
            typeof shows[record.calendarID] !== "undefined"
          ) {
            shows[record.calendarID].reputationScore -=
              sails.config.custom.analytics.SignOffEarlyPenalty;
            shows[record.calendarID].earlyEnd.push([
              record.event,
              record.actualEnd,
              record.scheduledEnd,
            ]);
          }
        }

        tasksLeft--;
        if (tasksLeft <= 0) resolve();
      };

      records.map(async (record) => {
        if (!inputs.immediate) {
          WWSUqueue.add(() => process4_2(_.cloneDeep(record)));
        } else {
          process4_2(_.cloneDeep(record));
        }
      });
    });

    // Execute our parallel functions and wait for all of them to resolve.
    await Promise.all([process2, process3, process4]);

    // Calculate earned remote credits and active warning points for all DJs we are using
    // We do this last because we only want to run this on applicable DJs from calendar events (otherwise all DJs will end up in our results)
    const process5 = async () => {
      return new Promise(async (resolve, reject) => {
      let records = await sails.models.djnotes.find({
        dj: Object.keys(DJs).map((dj) => parseInt(dj)),
        date: {
          ">=": start,
          "<=": end,
        },
      });

      let tasksLeft = records.length;
      if (tasksLeft <= 0) return resolve();

      const process5_2 = (record) => {
        initializeDJ(record.dj);
        if (record.dj && typeof DJs[record.dj] !== "undefined") {
          // Remote credits
          if (record.type.startsWith("remote-")) {
            DJs[record.dj].remoteCredits += record.amount;

            // Warning points
          } else if (record.type.startsWith("warning-")) {
            DJs[record.dj].warningPoints += record.amount;
            DJs[record.dj].warnings.push({
              date: record.date,
              type: record.type,
              description: record.description,
              amount: record.amount,
            });
            DJs[record.dj].reputationScore -=
              sails.config.custom.analytics.warningPointsPenalty *
              record.amount;
          }
        }

        tasksLeft--;
        if (tasksLeft <= 0) resolve();
      };

      records.map((record) => {
        if (!inputs.immediate) {
          WWSUqueue.add(() => process5_2(_.cloneDeep(record)));
        } else {
          process5_2(_.cloneDeep(record));
        }
      });
    });
  };

    if (Object.keys(DJs).length > 0) await process5();

    // Do additional final calculations for DJs
    for (var index in DJs) {
      if (Object.prototype.hasOwnProperty.call(DJs, index)) {
        // Calculate the ratio of listeners to showtime
        DJs[index].ratio =
          DJs[index].showTime > 0
            ? DJs[index].listenerMinutes / DJs[index].showTime
            : 0;

        DJs[index].viewerRatio =
          DJs[index].showTime > 0
            ? DJs[index].viewerMinutes / DJs[index].showTime
            : 0;

        // Calculate the reputation percent
        DJs[index].reputationScore += DJs[index].reputationScoreMax;
        DJs[index].reputationPercent =
          DJs[index].reputationScore > 0
            ? Math.round(
                100 *
                  (DJs[index].reputationScore / DJs[index].reputationScoreMax)
              )
            : 0;

        // Get DJ name
        if (index > 0) {
          let name = await sails.models.djs.findOne({ ID: index });
          if (name) {
            DJs[index].name = `${name.name} (${
              name.realName || `Unknown Full Name`
            })`;
          } else {
            DJs[index].name = `Unknown DJ`;
          }
        }
      }
    }

    // Do additional final calculations for shows
    for (var index in shows) {
      if (Object.prototype.hasOwnProperty.call(shows, index)) {
        // Calculate the ratio of listeners to showtime
        shows[index].ratio =
          shows[index].showTime > 0
            ? shows[index].listenerMinutes / shows[index].showTime
            : 0;

        shows[index].viewerRatio =
          shows[index].showTime > 0
            ? shows[index].viewerMinutes / shows[index].showTime
            : 0;

        // Calculate the reputation percent
        shows[index].reputationScore += shows[index].reputationScoreMax;
        shows[index].reputationPercent =
          shows[index].reputationScore > 0
            ? Math.round(
                100 *
                  (shows[index].reputationScore /
                    shows[index].reputationScoreMax)
              )
            : 0;

        // Get show name and discord channel
        if (index > 0) {
          let name = await sails.models.calendar.findOne({ ID: index });
          if (name) {
            shows[index].name = name.name;
            shows[index].type = name.type;
            shows[index].discordChannel = name.discordChannel;
          } else {
            shows[index].name = `Unknown Event`;
            shows[index].type = `event`;
            shows[index].discordChannel = null;
          }
        }
      }
    }

    // All done. Return as an array pair.
    return [DJs, shows];
  },
};

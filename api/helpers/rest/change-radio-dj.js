module.exports = {
  friendlyName: "helper rest.changeRadioDj",

  description: "Change which RadioDJ instance is the active one.",

  inputs: {
    instance: {
      type: "json",
      description:
        "The radioDJ instance (configradiodjs record) that should be activated. If not specified, one will be chosen from a list of good-status RadioDJs.",
    },
  },

  fn: async function (inputs) {
    // Skip if there are no RadioDJs configured.
    if (!sails.config.custom.radiodjs.length) {
      sails.models.status.errorCheck.waitForGoodRadioDJ = false;
      await sails.helpers.status.modify.with({
        name: "calendar-triggerer",
        status: 4,
        summary: `No Genres / Playlists / Prerecords are being automatically started; no RadioDJ instances have been configured.`,
        data: `Since no RadioDJs are configured, the system is not automatically triggering calendar programming. You are responsible for configuring your automation system, if not using RadioDJ, to trigger automatic programming at their respective times. It should also be informing the system of now-playing metadata accordingly as indicated in the instructions under administration DJ Controls -> System Settings -> RadioDJs.
        <br /><strong>TO FIX:</strong> Go to administration DJ Controls -> System Settings -> RadioDJs. This status might not clear until a genre/playlist/prerecord starts.`,
      });
      return;
    }

    const noHealthyRadioDJs = async () => {
      // Suspend most programming operations until a healthy RadioDJ is detected
      sails.models.status.errorCheck.waitForGoodRadioDJ = true;

      // Made the most sense to stick this status on Calendar Triggerer because there might not be any RadioDJ statuses available, and calendar triggerer is never updated if no RadioDJs are healthy.
      await sails.helpers.status.modify.with({
        name: "calendar-triggerer",
        label: "Calendar Triggerer",
        status: 1,
        summary: `No operational RadioDJs! Genres / Playlists / Prerecords disabled. Shows cannot air.`,
        data: `None of the configured RadioDJs are operational! The system has suspended most programming and automation operations until at least one RadioDJ begins responding again. Any active broadcasts that were ongoing when this occurred were terminated. <br />
        <strong>TO FIX / CHECK: </strong>
        <ul>
        <li>Make sure RadioDJ is running on all computers that it should be running on.</li>
        <li>If RadioDJ is frozen, check that it can access the music library (also ensure network drives are connected). Check that sound cards are responding. Also check if any bad tracks are in the queue (corrupted or contain weird characters in their metadata). Then, restart RadioDJ.</li>
        <li>Make sure the MariaDB / MySQL database server on each computer running RadioDJ is functional.</li>
        <li>Make sure the REST server on each RadioDJ is online, configured properly, and accessible (RadioDJ Options -> Plugins -> REST server). You may have to play a track in RadioDJ if you recently restarted it before REST begins working.</li>
        <li>Make sure the network connection on the computers running RadioDJ is good, and that the server can connect.</li>
        </ul>
        This status might not clear until a genre/playlist/prerecord starts.`,
      });

      await sails.helpers.onesignal.sendMass(
        "emergencies",
        "No operational RadioDJ instances!",
        `On ${moment().format(
          "LLLL"
        )}, the system ran into a condition where there were no operational RadioDJs. Dead air was likely and may be still occurring. Please ensure at least 1 RadioDJ is functional immediately.`
      );

      // End the current show/log
      await sails.helpers.attendance.createRecord(
        undefined,
        false,
        false,
        true
      );

      // Attempt to clear the queue in case REST is actually responding but erroring because of a bad track in the queue.
      await sails.helpers.rest.cmd("ClearPlaylist", 1);
    };

    // Determine which inactive RadioDJs are healthy (status 5).
    var healthyRadioDJs = [];
    var maps = sails.config.custom.radiodjs.map(async (instance) => {
      if (instance.name === sails.models.meta.memory.radiodj) {
        return false;
      }
      var status = await sails.models.status.findOne({
        name: `radiodj-${instance.name}`,
      });
      if (status && status.status === 5) {
        healthyRadioDJs.push(instance);
      }
      return true;
    });
    await Promise.all(maps);

    // If there is at least one healthy inactive RadioDJ, choose one randomly to switch to
    if (healthyRadioDJs.length > 0) {
      var changeTo = await sails.helpers.pickRandom(healthyRadioDJs);
      // Overwrite randomly chosen RadioDJ if one was specified
      if (inputs.instance) changeTo = { item: inputs.instance };
      await sails.helpers.meta.change.with({ radiodj: changeTo.item.name });

      // Otherwise, check to see if the active RadioDJ is still status 5
    } else {
      instance = sails.config.custom.radiodjs.find(
        (inst) => inst.name === sails.models.meta.memory.radiodj
      );
      if (!instance) {
        await noHealthyRadioDJs();
      } else {
        var status = await sails.models.status.findOne({
          name: `radiodj-${instance.name}`,
        });

        // If the current RadioDJ is also not status 5, we have a huge problem! Trigger critical status, and wait for a good RadioDJ to report
        if (!status || status.status !== 5) {
          await noHealthyRadioDJs();
        }
        return true;
      }
    }
  },
};

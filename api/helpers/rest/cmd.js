module.exports = {
  friendlyName: "rest.cmd",

  description: "Execute a command on the active RadioDJ REST server.",

  inputs: {
    command: {
      type: "string",
      required: true,
      description: "Command to send over REST.",
    },
    arg: {
      type: "string",
      allowNull: true,
      description: "If the command takes an argument, arg is that argument.",
    },
    timeout: {
      type: "number",
      defaultsTo: 5000,
      description:
        "Amount of time allowed for waiting for a connection, a response header, and response data (each). If this value is set to 0, the promise will resolve immediately without waiting for needle to finish (and will return true instead of REST response), and will assume 10000 for needle timeout.",
    },
    altREST: {
      type: "json",
      description:
        "Execute the command on the provided RadioDJ instance (configdjs record) instead of the active one.",
    },
  },

  fn: async function (inputs) {
    sails.log.debug(
      `Helper rest.cmd called: ${inputs.command} / ${inputs.arg}`
    );

    // Skip if there are no RadioDJs configured.
    if (
      !inputs.altREST &&
      (!sails.config.custom.radiodjs.length ||
        !sails.models.meta.memory.radiodj ||
        sails.models.meta.memory.radiodj === "")
    )
      return;

    var endstring = ""; // appends at the end of a REST call, say, if arg was supplied

    // arg supplied? Load it in memory.
    if (typeof inputs.arg !== "undefined" && inputs.arg !== null) {
      endstring = "&arg=" + inputs.arg;
    }

    try {
      // Query REST
      // LINT: do NOT camel case; these are needle parameters.
      // eslint-disable-next-line camelcase
      var func = () => {
        return new Promise((resolve, reject) => {
          let instance =
            inputs.altREST ||
            sails.config.custom.radiodjs.find(
              (inst) => inst.name === sails.models.meta.memory.radiodj
            );
          if (!instance || !instance.restURL || !instance.restPassword)
            return resolve("RadioDJ error");

          needle(
            "get",
            instance.restURL +
              "/opt?auth=" +
              instance.restPassword +
              "&command=" +
              inputs.command +
              endstring,
            {},
            {
              open_timeout: inputs.timeout > 0 ? inputs.timeout : 5000,
              response_timeout: inputs.timeout > 0 ? inputs.timeout : 5000,
              read_timeout: inputs.timeout > 0 ? inputs.timeout : 5000,
              headers: { "Content-Type": "application/json" },
            }
          )
            .then((resp) => resolve(resp))
            .catch((e) => resolve(false));
        });
      };
      if (inputs.timeout > 0) {
        var resp = await func();
        return resp;
      }
      if (inputs.timeout === 0) {
        func();
        return true;
      }
    } catch (e) {
      return false;
    }
  },
};

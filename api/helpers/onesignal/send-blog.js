var breakdance = require("breakdance");

module.exports = {
  friendlyName: "sails.helpers.onesignal.sendBlog",

  description: "Send push notifications out for new blog posts.",

  inputs: {
    blog: {
      type: "ref",
      required: true,
      description: "the blog post",
    },
  },

  fn: async function (inputs) {
    try {
      let devices = [];

      // Determine which blog subtypes to send.
      let subtypes = ["all"];
      if (inputs.blog.categories && inputs.blog.categories.length)
        subtypes = subtypes.concat(inputs.blog.categories);

      // Find which devices to send a notification.
      let records = await sails.models.subscribers.find({
        type: "blog",
        subtype: subtypes,
      });
      records.map((record) => devices.push(record.device));

      // If we have at least 1 person to receive a push notification, continue
      if (devices.length > 0) {
        await sails.helpers.onesignal.send(
          devices,
          `message`,
          `New blog post on the WWSU website!`,
          breakdance(inputs.blog.title),
          60 * 60 * 24 * 7
        );
      }

      return true;
    } catch (e) {
      // No erroring if there's an error; just ignore it
      sails.log.error(e);
      return false;
    }
  },
};

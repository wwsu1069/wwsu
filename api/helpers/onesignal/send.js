module.exports = {
  friendlyName: "sails.helpers.onesignal.send",

  description: "Send a notification out to onesignal player devices.",

  inputs: {
    devices: {
      type: "ref",
      required: true,
      description: `A list of OneSignal IDs to send this notification to.`,
    },
    category: {
      type: "string",
      isIn: ["message", "event", "announcement", "request"],
      required: true,
      description: `The category of the notification.`,
    },
    title: {
      type: "string",
      required: true,
      description: `The title of the notification`,
    },
    content: {
      type: "string",
      required: true,
      description: `The content of the notification`,
    },
    ttl: {
      type: `number`,
      defaultsTo: 60 * 60 * 24,
      description: `The amount of time the notification persists for mobile users.`,
    },
  },

  fn: async function (inputs) {
    // Skip if oneSignal App ID or rest key not provided
    if (
      !sails.config.custom.basic.oneSignalAppID ||
      !sails.config.custom.basic.oneSignalRest
    )
      return;

    try {
      var categories = {
        message: sails.config.custom.basic.oneSignalCategoryMessage,
        event: sails.config.custom.basic.oneSignalCategoryEvent,
        announcement: sails.config.custom.basic.oneSignalCategoryAnnouncement,
        request: sails.config.custom.basic.oneSignalCategoryRequest,
      };
      // LINT: ignore camel casing errors for needle parameters; they must be like this for oneSignal
      var resp = await needle(
        "post",
        `https://onesignal.com/api/v1/notifications`,
        {
          app_id: sails.config.custom.basic.oneSignalAppID,
          include_player_ids: inputs.devices,
          headings: { en: inputs.title },
          contents: { en: inputs.content },
          url: `https://server.wwsu1069.org`,
          // android_channel_id: categories[inputs.category],
          android_group: categories[inputs.category] || undefined,
          // android_group_message: `$[notif_count] new ${inputs.category}s`,
          thread_id: categories[inputs.category] || undefined,
          summary_arg: `${inputs.category}s`,
          ttl: inputs.ttl,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Basic ${sails.config.custom.basic.oneSignalRest}`,
          },
        }
      );

      // Remove any subscriptions made by devices that were returned as invalid from oneSignal
      if (resp && typeof resp.body !== "undefined") {
        if (typeof resp.body.errors !== `undefined`) {
          sails.log.error(new Error(JSON.stringify(resp.body.errors)));

          if (typeof resp.body.errors["invalid_player_ids"] !== `undefined`) {
            await sails.models.logs
              .create({
                attendanceID: null,
                logtype: "subscribers",
                loglevel: "info",
                logsubtype: "inactive",
                logIcon: `fas fa-bell-slash`,
                title: `Notification subscribers were removed (inactive / unsubscribed).`,
                event: `Devices: ${resp.body.errors["invalid_player_ids"].join(
                  ", "
                )}`,
              })
              .fetch()
              .tolerate((err) => {
                // Don't throw errors, but log them
                sails.log.error(err);
              });
            resp.body.errors["invalid_player_ids"].map((invalid) => {
              (async (invalid2) => {
                await sails.models.subscribers.destroy({ device: invalid2 });
              })(invalid);
            });
          } else if (
            resp.body.errors.indexOf(
              "All included players are not subscribed"
            ) !== -1
          ) {
            await sails.models.logs
              .create({
                attendanceID: null,
                logtype: "subscribers",
                loglevel: "info",
                logsubtype: "inactive",
                logIcon: `fas fa-bell-slash`,
                title: `Notification subscribers were removed (inactive / unsubscribed).`,
                event: `Devices: ${inputs.devices.join(", ")}`,
              })
              .fetch()
              .tolerate((err) => {
                // Don't throw errors, but log them
                sails.log.error(err);
              });
            inputs.devices.map((invalid) => {
              (async (invalid2) => {
                await sails.models.subscribers.destroy({ device: invalid2 });
              })(invalid);
            });
          }
        }
      } else {
        throw new Error(`Error sending onesignal notification request.`);
      }
      return true;
    } catch (e) {
      // Do not error when notifications fail, but return false instead.
      sails.log.error(e);
      return false;
    }
  },
};

module.exports = {
  friendlyName: "Break",

  description: "Break state.",

  inputs: {
    halftime: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Halftime is true if this is an extended or halftime sports break, rather than a standard one.",
    },

    problem: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, will play a configured technicalIssue liner as the break begins, such as if the break was triggered because of an issue. Defaults to false.",
    },
  },

  fn: async function (inputs) {
    // Block this request if we are already trying to change states
    if (sails.models.meta.memory.changingState !== null) {
      throw new Error(
        `The system is in the process of changing states. The request was blocked to prevent clashes.`
      );
    }

    // Block if running an alert
    if (sails.models.meta.memory.altRadioDJ !== null) {
      throw new Error(
        `Cannot change states while an alert is being broadcast.`
      );
    }

    // Block the request if we are already in break and problem = true. Return non-error because a problem = true request is never user initiated.
    if (sails.models.meta.memory.state.endsWith("_break") && inputs.problem)
      return;

    // Disallow going to break for a problem if we are not using RadioDJ, but silently ignore without error.
    if (inputs.problem && !sails.config.custom.radiodjs.length) {
      return;
    }

    // Lock so that other state changing requests get blocked until we are done
    await sails.helpers.meta.change.with({
      changingState: `Going into break`,
    });

    // Do not allow a halftime break if not in a sports broadcast
    if (
      !sails.models.meta.memory.state.startsWith("sports") &&
      inputs.halftime
    ) {
      inputs.halftime = false;
    }

    // Log it in a separate self-calling async function that we do not await so it does not block the rest of the call.
    // Also, increment break number in attendance record.
    (async () => {
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "break",
          loglevel: "info",
          logsubtype: sails.models.meta.memory.show,
          logIcon: `fas fa-coffee`,
          title: `A break started.`,
          event: "",
        })
        .fetch()
        .tolerate((err) => {
          // Do not throw for errors, but log it.
          sails.log.error(err);
        });
    })();

    // Define break executioner.
    const executeBreak = async () => {
      try {
        // Skip all the RadioDJ stuff if we are not using RadioDJ.
        if (!sails.config.custom.radiodjs.length) {
          // Assume this is a Top of Hour ID break if executed between :55 and :05.
          // We have no way of checking for IDs airing when not using RadioDJ. So this is arbitrary.
          if (moment().minute() >= 55 || moment().minute() < 5) {
            await sails.helpers.meta.change.with({
              lastID: moment().format("YYYY-MM-DD HH:mm:ss"),
            });
          }
          if (inputs.halftime) {
            if (sails.models.meta.memory.state.startsWith("sportsremote_")) {
              await sails.helpers.meta.change.with({
                state: "sportsremote_halftime",
              });
            } else {
              await sails.helpers.meta.change.with({
                state: "sports_halftime",
              });
            }
          } else {
            switch (sails.models.meta.memory.state) {
              case "live_on":
                await sails.helpers.meta.change.with({ state: "live_break" });
                break;
              case "remote_on":
                await sails.helpers.meta.change.with({ state: "remote_break" });
                break;
              case "sports_on":
                await sails.helpers.meta.change.with({ state: "sports_break" });
                break;
              case "sportsremote_on":
                await sails.helpers.meta.change.with({
                  state: "sportsremote_break",
                });
                break;
            }
          }
          return;
        }
        // If this break was triggered because of a technical problem, play a technical problem liner
        if (inputs.problem) {
          await sails.helpers.songs.queue(
            sails.config.custom.subcats.technicalIssues,
            "Top",
            1,
            "noRules"
          );
        }

        // halftime break? Play a break and then begin halftime music
        if (inputs.halftime) {
          // Queue and play tracks
          await sails.helpers.rest.cmd("EnableAssisted", 1);
          await sails.helpers.break.checkClockwheel(false);
          await sails.helpers.break.executeArray(
            sails.config.custom.breaks.filter(
              (record) =>
                record.type === "sports" && record.subtype === "before"
            ),
            "Sports Before"
          );
          await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
          await sails.helpers.rest.cmd("EnableAssisted", 0);
          await sails.helpers.break.executeArray(
            sails.config.custom.breaks.filter(
              (record) =>
                record.type === "sports" && record.subtype === "duringHalftime"
            ),
            "Sports duringHalftime"
          );

          // Change state to halftime mode
          if (sails.models.meta.memory.state.startsWith("sportsremote")) {
            await sails.helpers.meta.change.with({
              state: "sportsremote_halftime",
            });
          } else {
            await sails.helpers.meta.change.with({ state: "sports_halftime" });
          }

          // Standard break
        } else {
          // Queue and play tracks
          await sails.helpers.rest.cmd("EnableAssisted", 1);

          // Execute appropriate breaks, and switch state to break
          switch (sails.models.meta.memory.state) {
            case "live_on":
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "live" && record.subtype === "during"
                ),
                "Live During"
              );
              await sails.helpers.break.checkClockwheel(false);
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "live" && record.subtype === "before"
                ),
                "Live Before"
              );
              await sails.helpers.meta.change.with({ state: "live_break" });
              break;
            case "remote_on":
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "remote" && record.subtype === "during"
                ),
                "Remote During"
              );
              await sails.helpers.break.checkClockwheel(false);
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "remote" && record.subtype === "before"
                ),
                "Remote Before"
              );
              await sails.helpers.meta.change.with({ state: "remote_break" });
              break;
            case "sports_on":
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "sports" && record.subtype === "during"
                ),
                "Sports During"
              );
              await sails.helpers.break.checkClockwheel(false);
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "sports" && record.subtype === "before"
                ),
                "Sports Before"
              );
              await sails.helpers.meta.change.with({ state: "sports_break" });
              break;
            case "sportsremote_on":
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "sports" && record.subtype === "during"
                ),
                "Sports During"
              );
              await sails.helpers.break.checkClockwheel(false);
              await sails.helpers.break.executeArray(
                sails.config.custom.breaks.filter(
                  (record) =>
                    record.type === "sports" && record.subtype === "before"
                ),
                "Sports Before"
              );
              await sails.helpers.meta.change.with({
                state: "sportsremote_break",
              });
              break;
          }

          await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
          await sails.helpers.rest.cmd("EnableAssisted", 0);
        }

        await sails.helpers.meta.change.with({ changingState: null });
      } catch (e) {
        await sails.helpers.meta.change.with({ changingState: null });
        throw e;
      }
    };

    // Because remote broadcasts are intentionally on a 1-second delay, delay actually going to break by 1 second.
    if (
      sails.models.meta.memory.state.startsWith("remote_") ||
      sails.models.meta.memory.state.startsWith("sportsremote_")
    ) {
      setTimeout(async () => executeBreak(), 1000);
    } else {
      executeBreak();
    }
  },
};

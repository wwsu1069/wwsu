module.exports = {
  friendlyName: "eas.parseCaps",

  description: "Parse alert data received from a CAPS JSON document.",

  inputs: {
    county: {
      type: "string",
      required: true,
      description: "The CAPS body applies to the specified county.",
    },
    body: {
      type: "json",
      required: true,
      description:
        "JSON returned by the needle library containing the CAPS data.",
    },
  },

  fn: async function (inputs) {
    sails.log.debug(`Helper eas.parseCaps called on county ${inputs.county}.`);
    let body = JSON.parse(inputs.body);
    console.dir(body);

    var maps = body.features
      .map(async (entry, index) => {
        var alert = entry.properties;

        // Skip any entries that do not have an ID or do not have a status of "Actual"; they're not real alerts.
        if (
          typeof alert.id !== "undefined" &&
          typeof alert.status !== "undefined" &&
          alert.status === "Actual"
        ) {
          // Skip expired alerts
          if (moment().isBefore(moment(alert.expires))) {
            sails.log.verbose(
              `Processing alert ${index} for county ${inputs.county}.`
            );
            var color = "#787878";
            if (alert.event in sails.config.custom.basic.nwsAlerts) {
              // Is the alert in our array of alerts to alert for? Get its color if so.
              color = sails.config.custom.basic.nwsAlerts[alert.event];
            } else {
              // If it is not in our array, then it is not an alert we should publish. Resolve to the next one.
              sails.log.verbose(
                `Alert ${index}/${inputs.county} (${alert.event}) is not in our process list. Ignoring.`
              );
              return false;
            }

            // Pre-add the alert (this does NOT put it in the database nor push in websockets yet; that is done via sails.helpers.eas.postParse)
            await sails.helpers.eas.addAlert(
              alert.id,
              "The National Weather Service",
              inputs.county,
              alert.event,
              alert.severity !== `Unknown`
                ? alert.severity
                : `Minor`,
              moment(alert.effective).format("YYYY-MM-DD HH:mm:ss"),
              moment(alert.expires).format("YYYY-MM-DD HH:mm:ss"),
              color
            );

            // Mark this alert as active in the caps
            sails.models.eas.activeCAPS.push(alert.id);
          } else {
            sails.log.verbose(
              `Skipped ${index}/${inputs.county} because it is expired.`
            );
          }
        } else {
          sails.log.verbose(
            `Skipped ${index}/${inputs.county} because it was not a valid alert.`
          );
        }
        return true;
      });
    await Promise.all(maps);
  },
};

const { Mutex, withTimeout } = require("async-mutex");
const mutex = withTimeout(new Mutex(), 10000); // Mutex locks will time-out with a rejection after 1 minute

module.exports = {
  friendlyName: "Run Sync",

  description:
    "Run an async function sync in a mutex lock (this helper will wait for prior calls to this helper to finish first)",

  inputs: {
    id: {
      type: 'string',
      required: true,
      description: 'A debug identifier for this call',
    },
    fn: {
      type: "ref",
      required: true,
      custom: function (value) {
        return _.isFunction(value);
      },
      description: "Function being requested (should ideally be async)",
    },
  },

  fn: async function (inputs) {
    try {
      sails.log.debug(`*** LOCK REQUESTED *** ${inputs.id}`);
      let lock = await mutex.runExclusive(async () => {
        sails.log.debug(`*** LOCKED *** ${inputs.id}`);
        let resp = await inputs.fn();
        sails.log.debug(`*** UNLOCKED *** ${inputs.id}`);
        return resp;
      });
      return lock;
    } catch (e) {
      if (e.message.includes('timeout while waiting for mutex to become available')) {
        sails.log.debug(`Mutex ${inputs.id}`);
        sails.log.error(e); // We do not want the entire application crashing for this
      } else {
        sails.log.debug(`Mutex ${inputs.id}`);
        throw e;
      }
    }
  },
};

module.exports = {
  friendlyName: "Execute array",

  description: "",

  inputs: {
    array: {
      type: "ref",
      required: true,
      description:
        "An array of break task objects to execute. The order will be reversed.",
    },
    name: {
      type: "string",
      description: "Name of break",
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Skip if no RadioDJs configured
    if (!sails.config.custom.radiodjs.length) return;

    // Empty array? No need to continue.
    if (inputs.array.length <= 0) {
      return;
    }

    let tasksDone = [];

    // Get the configured break tasks, but clone it. We're going to reverse the order, so we don't want to reverse the original object.
    var breakOpts = _.cloneDeep(inputs.array);

    // Sort the tasks by order and then by ID.
    breakOpts = breakOpts.sort((a, b) => {
      if (a.order < b.order) return -1;
      if (a.order > b.order) return 1;

      if (a.ID < b.ID) return -1;
      if (a.ID > b.ID) return 1;

      return 0;
    });

    // Reverse the order of execution so queued things are in the same order as configured.
    breakOpts.reverse();

    // Go through each task
    if (breakOpts.length > 0) {
      var asyncLoop = async function (array, callback) {
        for (let index = 0; index < array.length; index++) {
          // LINT: Callback is executed on every item in the array; do NOT return.
          // eslint-disable-next-line callback-return
          await callback(array[index], index, array);
        }
      };

      await asyncLoop(breakOpts, async (task) => {
        try {
          let results = await sails.helpers.break.execute(
            task.task,
            task.event,
            task.category,
            task.quantity,
            task.rules,
            task.doWhen
          );
          tasksDone.push(
            `${task.task}${
              task.category ? ` / ${task.category}` : ``
            }: ${results}`
          );
        } catch (error) {
          tasksDone.push(
            `${task.task}${
              task.category ? ` / ${task.category}` : ``
            }: FAILED; ${error.message}`
          );
        }
      });

      // Reverse tasksDone as they are in reverse order
      tasksDone.reverse();

      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "break",
          loglevel: "info",
          logsubtype: "automation",
          logIcon: `fas fa-coffee`,
          title: `Executed break ${inputs.name || "Unknown"}`,
          event: `<ul>${tasksDone
            .map((task) => `<li>${task}</li>`)
            .join("")}</ul>`,
        })
        .fetch()
        .tolerate(() => {});
    }
  },
};

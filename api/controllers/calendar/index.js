module.exports = {
  friendlyName: "calendar/",

  description: "Program calendar page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/calendar",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `See what programming and genres are scheduled to air on WWSU for the next 14 days.`,
        author: "WWSU 106.9 FM",
        "og:title": `Calendar - WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/calendar`,
        "og:type": "website",
        "og:description": `See what programming and genres are scheduled to air on WWSU for the next 14 days.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};

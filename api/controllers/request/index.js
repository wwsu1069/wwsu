module.exports = {
  friendlyName: "request/",

  description: "Request page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/request",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `Browse our track library and make a song / track request.`,
        author: "Request a Track - WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/request`,
        "og:type": "website",
        "og:description": `Browse our track library and make a song / track request.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};
module.exports = {
  friendlyName: "directors/panel/calendar",

  description: "Director panel calendar page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "directors/calendar",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "directors/layout",
    };
  },
};
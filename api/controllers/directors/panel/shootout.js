module.exports = {
  friendlyName: "directors/panel/shootout",

  description: "Director panel bball shootout page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "directors/shootout",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "directors/layout",
    };
  },
};
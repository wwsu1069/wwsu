module.exports = {
  friendlyName: "directors/panel",

  description: "Director panel page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "directors/timesheets",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "directors/layout",
    };
  },
};
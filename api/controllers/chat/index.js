module.exports = {
  friendlyName: "chat/",

  description: "Chat page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/chat",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `Send and receive messages with the on-air DJs and other listeners of WWSU.`,
        author: "WWSU 106.9 FM",
        "og:title": `Chat Room - WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/chat`,
        "og:type": "website",
        "og:description": `Send and receive messages with the on-air DJs and other listeners of WWSU.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};

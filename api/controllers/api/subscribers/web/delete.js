module.exports = {
  friendlyName: "DELETE api/subscribers/web",

  description: "Remove push notification subscriptions.",

  inputs: {
    device: {
      type: "string",
      required: true,
      description: "The OneSignal device ID of the subscriber.",
    },

    type: {
      type: "string",
      required: true,
      isIn: ["calendar", "blog"],
      description: "The main type of the subscription",
    },

    subtype: {
      type: "string",
      required: true,
      description: "The subtype of the subscription",
    },
  },

  fn: async function (inputs) {
    let records = await sails.models.subscribers
      .destroy({
        device: inputs.device,
        type: inputs.type,
        subtype: inputs.subtype,
      })
      .fetch();

    return records;
  },
};

const sh = require("shorthash");

module.exports = {
  friendlyName: "POST api/flags",

  description: "Flag a song or a show for inappropriate content.",

  inputs: {
    trackID: {
      type: "number",
      description: "The ID of the track being reported.",
    },
    attendanceID: {
      type: "number",
      description:
        "The ID of the broadcast being reported. Required if trackID not specified.",
    },
    meta: {
      type: "string",
      required: true,
      description:
        "The metadata of the broadcast or track being reported. Required if ID not provided.",
    },
    reason: {
      type: "string",
      required: true,
      description: "The reason provided for the report.",
    },
  },

  exits: {
    badRequest: {
      statusCode: 400,
    },
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
    tooManyRequests: {
      statusCode: 429,
    },
  },

  fn: async function (inputs) {
    // Get IP address of the person reporting
    let fromIP = await sails.helpers.getIp(this.req);

    // ID or track is required. Error if neither is provided.
    if (!inputs.trackID && !inputs.attendanceID)
      throw { badRequest: "You must provide trackID or attendanceID." };

    // Checks for if attendanceID is provided
    if (inputs.attendanceID) {
      // First, get the record from the database (and reject if it does not exist)
      let record = await sails.models.attendance.findOne({
        ID: inputs.attendanceID,
      });
      if (!record) {
        throw { notFound: "The attendanceID was not found." };
      }

      // Bail if this host already reported the broadcast in the last hour
      if (
        (await sails.models.flaggedlist.count({
          host: fromIP,
          attendanceID: inputs.attendanceID,
          trackID: null,
          createdAt: {
            ">=": moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:ss"),
          },
        })) > 0
      ) {
        throw {
          tooManyRequests:
            "This IP address already reported the attendanceID / broadcast within the last hour.",
        };
      }

      // Bail if the attendanceID was not on the air in the last 30 minutes
      if (
        !record.actualStart ||
        (record.actualEnd &&
          moment().subtract(30, "minutes").isAfter(moment(record.actualEnd)))
      ) {
        throw {
          forbidden: `The attendanceID / broadcast either did not air yet or ended over 30 minutes ago. Flagging is not allowed.`,
        };
      }
    }

    // Checks for if trackID is provided
    if (inputs.trackID && sails.config.custom.radiodjs.length) {
      // First, get the track record from the database (and reject if it does not exist)
      let track = await sails.models.songs.findOne({ ID: inputs.trackID });
      if (!track) {
        throw { notFound: "The trackID was not found." };
      }

      // Bail if the track is already disabled
      if (track.enabled !== 1) {
        throw {
          forbidden:
            "The track is already disabled in the system; it cannot be flagged.",
        };
      }

      // Bail if this host already reported the track in the last hour
      if (
        (await sails.models.flaggedlist.count({
          host: fromIP,
          trackID: inputs.trackID,
          createdAt: {
            ">=": moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:ss"),
          },
        })) > 0
      ) {
        throw {
          tooManyRequests:
            "This IP address already reported the track in the last hour.",
        };
      }

      // Bail if the track did not play in the last 30 minutes.
      let canLike = false;
      let records = await sails.models.history.count({
        trackID: inputs.trackID,
        createdAt: {
          ">=": moment().subtract(30, "minutes").format("YYYY-MM-DD HH:mm:ss"),
        },
      });
      if (records > 0) canLike = true;

      if (!canLike) {
        throw {
          forbidden:
            "The provided track did not air in the last 30 minutes; it cannot be flagged right now.",
        };
      }

      // Disable the track in RadioDJ, and add a comment about the report.
      await sails.models.songs.updateOne(
        { ID: inputs.trackID },
        {
          enabled: 0,
          comments: `${
            track.comments
          }\n\n[SYSTEM] Track reported on ${moment().format(
            "llll"
          )} for reason ${inputs.reason}.`,
        }
      );
    }

    // At this point, add the report
    let record = await sails.models.flaggedlist
      .create({
        host: fromIP,
        attendanceID: inputs.attendanceID || null,
        trackID: inputs.trackID || null,
        meta: inputs.meta,
        reason: inputs.reason,
      })
      .fetch();

    // Also log the report
    await sails.models.logs
      .create({
        attendanceID:
          inputs.attendanceID || sails.models.meta.memory.attendanceID,
        logtype: `flag`,
        logsubtype: `website-${sh.unique(fromIP)}`,
        loglevel: `warning`,
        logIcon: `fas fa-flag`,
        title: `A track or broadcast was flagged / reported by someone for inappropriate content!`,
        event: `Metadata: ${inputs.meta}${
          inputs.trackID ? ` (Track ID ${inputs.trackID})` : ``
        }${
          inputs.attendanceID
            ? ` (Operation Log / Attendance ID ${inputs.attendanceID})`
            : ``
        }<br />Reported by IP address: ${fromIP}<br />Reason: ${
          inputs.reason
        }<br />Directors: you should check the contents of the track or broadcast recordings! Reported tracks are automatically disabled in RadioDJ (re-enable for false reports), and details of this report were added to the comments section of the track info in RadioDJ. Consider issuing discipline / warning points to DJs for valid reports. For repeat invalid reports, you may want to ban this host.`,
      })
      .fetch();

    // Email directors about the report
    await sails.helpers.emails.queueFlag(
      `A track / Broadcast was flagged: ${inputs.meta}`,
      `<p>Dear directors,</p>

    <p>A track or broadcast was flagged by someone on the website for inappropriate content. Please investigate as soon as possible.</p>

    <p>Metadata: ${inputs.meta}</p>
    <p>Reported by IP address: ${fromIP}</p>
    ${inputs.trackID ? `<p>Track ID: ${inputs.trackID}</p>` : ``}
    ${
      inputs.attendanceID
        ? `<p>Operation Log / Attendance ID: ${inputs.attendanceID}</p>`
        : ``
    }
    <p>Reason: ${inputs.reason}</p>

    <p>For tracks: Tracks are automatically disabled in RadioDJ when reported, and a note about this report is included in its comments section. Please review the track. Re-enable if it's fine.</p>
    <p>For broadcasts: Review the broadcast recordings to see if the report is valid. If so, issue discipline to the hosts / DJs as necessary.</p>
    <p>If this report is invalid, and the provided host has made multiple invalid reports, consider banning them in DJ Controls -> Administration -> Bans.</p>
    <p><strong>After you are done handling this report, please mark it off in DJ Controls under Administration -> To-Do.</strong></p>`
    );

    return record;
  },
};

module.exports = {
  friendlyName: "DELETE api/uploads/:ID",

  description: "Delete an upload.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "ID of the temp file to delete",
    },
    host: {
      type: "string",
      required: true,
      description:
        "The host string deleting content (since jquery.fileupload does not support the authentication module).",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // First, validate the host.
    let hostRecords = await sails.models.hosts.count();
    let host = await sails.models.hosts.findOrCreate(
      { host: inputs.host },
      {
        host: inputs.host,

        // Automatically authorize the first host in the database and make them an admin
        authorized: hostRecords <= 0,
        admin: hostRecords <= 0,

        friendlyname: `Unknown ${inputs.host.substr(inputs.host.length - 8)}`,
      }
    );
    if (!host || !host.authorized) {
      throw { forbidden: "This host is not authorized to delete uploads." };
    }

    let record = await sails.models.uploads.destroyOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The provided upload ID was not found." };
  },
};

module.exports = {
  friendlyName: "POST api/uploads",

  description: "Upload a file to the system.",

  inputs: {
    host: {
      type: "string",
      required: true,
      description:
        "The host string uploading content (since jquery.fileupload does not support the authentication module).",
    },
    type: {
      type: "string",
      required: true,
      description: "The type of file being uploaded",
      isIn: ["calendar/logo", "calendar/banner", "directors", "djs", "blogs"],
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    internalError: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    return await new Promise(async (resolve, reject) => {
      // First, validate the host.
      let hostRecords = await sails.models.hosts.count();
      let host = await sails.models.hosts.findOrCreate(
        { host: inputs.host },
        {
          host: inputs.host,

          // Automatically authorize the first host in the database and make them an admin
          authorized: (hostRecords <= 0),
          admin: (hostRecords <= 0),

          friendlyname: `Unknown ${inputs.host.substr(inputs.host.length - 8)}`,
        }
      );
      if (!host || !host.authorized) {
        return reject({ forbidden: "This host is not authorized to upload files." });
      }

      // Now, get the files.
      this.req.file("file").upload(
        {
          dirname: `./${inputs.type}`,
          maxBytes: 1024 * 1024 * 8,
        },
        async (err, _files) => {
          if (err) return reject({ internalError: err });

          let files = _files.map(async (file, index) => {
            let record = await sails.models.uploads
              .create({
                hostID: host.ID,
                path: file.fd,
                type: inputs.type,
              })
              .fetch();

            // Alpaca format
            return {
              id: record.ID,
              name: file.filename,
              size: file.size,
              url: `${sails.config.custom.baseUrl}/api/uploads/${record.ID}`,
              thumbnailUrl: `${sails.config.custom.baseUrl}/api/uploads/${record.ID}`,
              deleteUrl: `${sails.config.custom.baseUrl}/api/uploads/${record.ID}?host=${inputs.host}`,
              deleteType: "DELETE",
            };
          });

          let filesFinal = await Promise.all(files);

          return resolve({ files: filesFinal });
        }
      );
    });
  },
};

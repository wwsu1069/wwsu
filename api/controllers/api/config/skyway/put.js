module.exports = {
  friendlyName: "PUT /api/config/skyway",

  description: "Change skyway.js options.",

  inputs: {
    skywayAPI: {
      description:
        "API key for skyway.js (service used for DJ Controls to establish audio calls for remote broadcasts). Set to null to disable skyway.js / remote broadcasting.",
      type: "string",
      allowNull: true,
    },

    skywaySecret: {
      description: "API secret for skyway.js service. Null = do not change current secret.",
      type: "string",
      allowNull: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    let criteria = {
      skywayAPI: inputs.skywayAPI || null,
      skywaySecret: inputs.skywaySecret ? inputs.skywaySecret : undefined, // If Alpaca sends null, this means "do not change", not "remove / clear".
    };

    return await sails.models.configbasic.updateOne({ ID: 1 }, criteria);
  },
};

module.exports = {
  friendlyName: "PUT api/config/songlikes",

  description: "Change configuration for the song liking system.",

  inputs: {
    songsLikedCooldown: {
      description:
        "When someone likes a track on the website, the same track cannot be liked again by the same IP address for this many days. 0 means a track can only ever be liked one time per IP address.",
      type: "number",
      min: 0,
    },

    songsLikedPriorityChange: {
      description:
        "When someone likes a track, its priority in RadioDJ will be adjusted by this amount (positive = increase, negative = decrease)",
      type: "number",
      min: -100,
      max: 100,
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        songsLikedCooldown: inputs.songsLikedCooldown,
        songsLikedPriorityChange: inputs.songsLikedPriorityChange,
      }
    );
  },
};

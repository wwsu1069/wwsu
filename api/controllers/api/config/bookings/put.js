module.exports = {
  friendlyName: "PUT /api/config/bookings",

  description: "Change settings for the studio bookings system.",

  inputs: {
    maxDurationPerBooking: {
      type: "number",
      min: 1,
      required: true,
      description:
        "Maximum duration, in minutes, an org member is allowed to book a studio at one time.",
    },

    maxDurationPerDay: {
      type: "number",
      min: 1,
      required: true,
      description:
        "Maximum duration, in minutes, an org member is allowed to book any studio on a given day.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        maxDurationPerBooking: inputs.maxDurationPerBooking,
        maxDurationPerDay: inputs.maxDurationPerDay,
      }
    );
  },
};

module.exports = {
  friendlyName: "DELETE /api/config/nws/:ID",

  description: "Remove a NWS feed for checking with the internal EAS.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID to remove.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.confignws.destroyOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The provided record ID was not found." };

    return record;
  },
};

module.exports = {
  friendlyName: "DELETE /api/config/sports/:ID",

  description: "Delete a sport from the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The sport ID to remove.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.configsports.destroyOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The provided sport ID was not found." };

    return record;
  },
};

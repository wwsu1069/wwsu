module.exports = {
  friendlyName: "DELETE /api/config/categories/:ID",

  description: "Delete a RadioDJ category group.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The category ID to delete.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.configcategories.destroyOne({
      ID: inputs.ID,
    });
    if (!record) throw { notFound: "The category ID provided was not found." };

    return record;
  },
};

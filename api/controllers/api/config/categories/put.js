module.exports = {
  friendlyName: "PUT /api/config/categories/:ID",

  description: "Edit a category.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The category ID to edit.",
    },

    name: {
      type: "string",
    },

    categories: {
      type: "json",
      description:
        "Map of RadioDJ categories and subcategories assigned to this system category. Should be an object... each key is the name of the RadioDJ main category, and value is an array of subcategory names in the main category (or empty array for all of them).",
      custom: (value) => {
        if (typeof value !== "object") return false;

        let valid = true;
        let hasOneKey = false; // There must exist at least one key

        for (let key in value) {
          if (!Object.prototype.hasOwnProperty.call(value, key)) continue;

          // All property keys must be a string
          if (typeof key !== "string") {
            valid = false;
            break;
          }

          hasOneKey = true;

          // Values must be an array
          if (value[key].constructor !== Array) {
            valid = false;
            break;
          }

          // Every item in the value array must be a string
          if (value[key].length) {
            value[key].forEach((subcat) => {
              if (typeof subcat !== "string") valid = false;
            });
          }
        }

        return hasOneKey && valid;
      },
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.configcategories.updateOne(
      { ID: inputs.ID },
      {
        name: inputs.name,
        categories: inputs.categories,
      }
    );
    if (!record) throw { notFound: "The provided category ID was not found." };

    return record;
  },
};

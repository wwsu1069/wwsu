module.exports = {
  friendlyName: "PUT /api/config/filter-profanity",

  description: "Edit the list of profane words to censor.",

  inputs: {
    filterProfanity: {
      description:
        "The specified words will be censored (including within other words) in metadata and messages.",
      type: "json",
      custom: function (value) {
        // Must be an array
        if (value.constructor !== Array) return false;

        // All entries in array must be a string. But allow empty array.
        if (value.length > 0 && value.find((val) => typeof val !== "string"))
          return false;

        return true;
      },
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        filterProfanity: inputs.filterProfanity,
      }
    );
  },
};

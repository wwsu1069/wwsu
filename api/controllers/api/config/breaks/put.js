module.exports = {
  friendlyName: "PUT /api/config/breaks/:ID",

  description: "Edit a break task.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The record to edit.",
    },

    type: {
      type: "string",
      description: "Type of the break",
      isIn: ["clockwheel", "automation", "live", "remote", "sports"],
    },

    subtype: {
      type: "string",
      custom: (value) => {
        // If the value is a number, it must be between 0 and 59 (for the minute of the hour)
        if (!isNaN(parseInt(value))) {
          let numericValue = parseInt(value);
          if (numericValue < 0 || numericValue > 59) return false;
          return true;
        }

        // If the value is otherwise an actual string, it must be an allowed value
        if (
          [
            "start",
            "before",
            "during",
            "duringHalftime",
            "after",
            "end",
          ].indexOf(value) === -1
        )
          return false;
        return true;
      },
      description:
        "The subtype (for clockwheel, the minute of the hour... for all other types, either start, before, during, duringHalftime, after, or end).",
    },

    order: {
      type: "number",
      description:
        "Tasks are executed in order from lowest to highest (ID is used when records have the same order)",
    },

    task: {
      type: "string",
      isIn: [
        "log",
        "queue",
        "queueDuplicates",
        "queueUnderwritings",
        "queueRequests",
        "queueAlerts"
      ],
    },

    event: {
      type: "string",
      allowNull: true,
      description: "For log tasks, this is the text to be logged.",
    },

    category: {
      type: "string",
      allowNull: true,
      description:
        "For queue tasks, the category (configured in the node server) to queue tracks from.",
    },

    quantity: {
      type: "number",
      description: "The number of tracks to queue for any queue related tasks.",
    },

    rules: {
      type: "string",
      isIn: ["noRules", "lenientRules", "strictRules"],
      description:
        "noRules = do not follow playlist rotation rules; lenientRules = follow rotation rules unless there are no more tracks that conform; strictRules = abandon queuing any more tracks if no more conform to rotation rules",
    },

    doWhen: {
      type: "json",
      custom: (value) => {
        if (value.constructor !== Array) return false; // Must be an array

        let invalid = value.find(
          (item) =>
            typeof item !== "string" ||
            [
              "show",
              "remote",
              "sports",
              "prerecord",
              "genre",
              "playlist",
              "default",
            ].indexOf(item) === -1
        );
        return !invalid;
      },
      description:
        "Only execute this task during the specified types of broadcasts (empty array = all of them, the default). Accepted values: show, remote, sports, prerecord, genre, playlist, default (Default automation rotation).",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.configbreaks.updateOne(
      { ID: inputs.ID },
      {
        type: inputs.type,
        subtype: inputs.subtype,
        order: inputs.order,
        task: inputs.task,
        event: inputs.event || null,
        category: inputs.category || null,
        quantity: inputs.quantity,
        rules: inputs.rules,
        doWhen: inputs.doWhen,
      }
    );
    if (!record)
      throw { notFound: "The provided break task ID was not found." };

    return record;
  },
};

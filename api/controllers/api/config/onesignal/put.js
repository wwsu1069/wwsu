module.exports = {
  friendlyName: "PUT /api/config/onesignal",

  description: "Change settings for OneSignal push notifications.",

  inputs: {
    oneSignalAppID: {
      type: "string",
      allowNull: true,
      description: "The ID of the OneSignal app.",
    },

    oneSignalRest: {
      type: "string",
      allowNull: true,
      description: "The REST API key of the OneSignal app.",
    },

    oneSignalCategoryMessage: {
      type: "string",
      allowNull: true,
      description: "The UUID for the message Android category.",
    },

    oneSignalCategoryEvent: {
      type: "string",
      allowNull: true,
      description: "The UUID for the event Android category.",
    },

    oneSignalCategoryAnnouncement: {
      type: "string",
      allowNull: true,
      description: "The UUID for the announcement Android category.",
    },

    oneSignalCategoryRequest: {
      type: "string",
      allowNull: true,
      description: "The UUID for the request Android category.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configbasic.updateOne(
      { ID: 1 },
      {
        oneSignalAppID: inputs.oneSignalAppID || null,
        oneSignalRest: inputs.oneSignalRest ? inputs.oneSignalRest : undefined, // Null = do not change
        oneSignalCategoryMessage: inputs.oneSignalCategoryMessage || null,
        oneSignalCategoryEvent: inputs.oneSignalCategoryEvent || null,
        oneSignalCategoryAnnouncement: inputs.oneSignalCategoryAnnouncement || null,
        oneSignalCategoryRequest: inputs.oneSignalCategoryRequest || null,
      }
    );
  },
};

module.exports = {
  friendlyName: "PUT /api/config/reputation",

  description: "Change settings for how reputation points are calculated.",

  inputs: {
    warningPointsPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 20,
      description:
        "Number of reputation points to subtract for each warning point earned.",
    },

    cancellationPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to subtract for each broadcast cancellation.",
    },

    silenceAlarmPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to subtract each time a broadcast triggers the silence alarm.",
    },

    absencePenalty: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to subtract for each broadcast absence / no-show.",
    },

    unauthorizedBroadcastPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to subtract for each broadcast airing that was not scheduled.",
    },

    missedIDPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to subtract for each time a broadcast fails to take the top of hour ID break.",
    },

    signOnEarlyPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 3,
      description:
        "Number of reputation points to subtract for each time a broadcast goes on the air 5 or more minutes before scheduled start time.",
    },

    signOnLatePenalty: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to subtract for each time a broadcast goes on the air 5 or more minutes after scheduled start time.",
    },

    signOffEarlyPenalty: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to subtract for each time a broadcast signs off 5 or more minutes before scheduled end time.",
    },

    signOffLatePenalty: {
      type: "number",
      min: 0,
      defaultsTo: 3,
      description:
        "Number of reputation points to subtract for each time a broadcast signs off 5 or more minutes after scheduled end time.",
    },

    breakReputation: {
      type: "number",
      min: 0,
      defaultsTo: 2,
      description:
        "Number of reputation points to add if the host took the configured ideal number of breaks per hour.",
    },

    showReputation: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to add for each live broadcast.",
    },

    prerecordReputation: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description: "Number of reputation points to add for each prerecord.",
    },

    remoteReputation: {
      type: "number",
      min: 0,
      defaultsTo: 5,
      description:
        "Number of reputation points to add for each remote broadcast.",
    },

    sportsReputation: {
      type: "number",
      min: 0,
      defaultsTo: 8,
      description:
        "Number of reputation points to add for each sports broadcast.",
    },

    genreReputation: {
      type: "number",
      min: 0,
      defaultsTo: 1,
      description:
        "Number of reputation points to add for each genre broadcast.",
    },

    playlistReputation: {
      type: "number",
      min: 0,
      defaultsTo: 2,
      description:
        "Number of reputation points to add for each playlist broadcast.",
    },
  },

  exits: {},

  fn: async function (inputs) {
    return await sails.models.configanalytics.updateOne(
      { ID: 1 },
      {
        warningPointsPenalty: inputs.warningPointsPenalty,
        cancellationPenalty: inputs.cancellationPenalty,
        silenceAlarmPenalty: inputs.silenceAlarmPenalty,
        absencePenalty: inputs.absencePenalty,
        unauthorizedBroadcastPenalty: inputs.unauthorizedBroadcastPenalty,
        missedIDPenalty: inputs.missedIDPenalty,
        signOnEarlyPenalty: inputs.signOnEarlyPenalty,
        signOnLatePenalty: inputs.signOnLatePenalty,
        signOffEarlyPenalty: inputs.signOffEarlyPenalty,
        signOffLatePenalty: inputs.signOffLatePenalty,
        breakReputation: inputs.breakReputation,
        showReputation: inputs.showReputation,
        prerecordReputation: inputs.prerecordReputation,
        remoteReputation: inputs.remoteReputation,
        sportsReputation: inputs.sportsReputation,
        genreReputation: inputs.genreReputation,
        playlistReputation: inputs.playlistReputation,
      }
    );
  },
};

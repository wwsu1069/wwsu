module.exports = {
  friendlyName: "POST /api/config/radiodjs",

  description: "Add a new RadioDJ instance to system configuration.",

  inputs: {
    name: {
      type: "string",
      required: true,
      description: "Alphanumeric name",
      regex: /^[a-zA-Z0-9-_]+$/
    },

    label: {
      type: "string",
      description: "Friendly name",
    },

    restURL: {
      type: "string",
      isURL: true,
      required: true,
      description: "The URL to the REST server for this RadioDJ",
    },

    restPassword: {
      type: "string",
      required: true,
      description: "The password for the RadioDJ REST server",
    },

    errorLevel: {
      type: "number",
      min: 1,
      max: 5,
      required: true,
      description:
        "The error level that should be reported when this RadioDJ is not working (5 = good, 4 = info, 3 = minor, 2 = major, 1 = critical)",
    },
  },

  exits: {},

  fn: async function (inputs) {
    let record = await sails.models.configradiodjs
      .create({
        name: inputs.name,
        label: inputs.label,
        restURL: inputs.restURL,
        restPassword: inputs.restPassword,
        errorLevel: inputs.errorLevel,
      })
      .fetch();

    return record;
  },
};

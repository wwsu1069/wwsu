module.exports = {
  friendlyName: "DELETE /api/config/radiodjs/:ID",

  description: "Delete a RadioDJ instance from system settings.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The RadioDJ to delete.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.configradiodjs.destroyOne({
      ID: inputs.ID,
    });
    if (!record) throw { notFound: "The provided RadioDJ ID was not found." };

    return record;
  },
};

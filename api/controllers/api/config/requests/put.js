module.exports = {
  friendlyName: "PUT /api/config/requests",

  description: "Change options for the request system.",

  inputs: {
    requestsDailyLimit: {
      description:
        "Each IP address is allowed to make this many track requests per day (reset at midnight). 0 = disable the request system.",
      type: "number",
      min: 0,
    },

    requestsPriorityChange: {
      description:
        "When someone requests a track, its priority in RadioDJ will be adjusted by this amount (positive = increase, negative = decrease). Keep in mind if priority adjustment is set in RadioDJ for a played track, its priority will also be reduced/adjusted by that amount when played.",
      type: "number",
      min: -100,
      max: 100,
    },
  },

  exits: {},

  fn: async function (inputs) {
    let criteria = {
      requestsDailyLimit: inputs.requestsDailyLimit,
      requestsPriorityChange: inputs.requestsPriorityChange,
    };

    return await sails.models.configbasic.updateOne({ ID: 1 }, criteria);
  },
};

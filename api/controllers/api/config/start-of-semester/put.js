module.exports = {
  friendlyName: "PUT api/config/start-of-semester",

  description:
    "Change the date/time of the start of the current semester for analytics.",

  inputs: {
    startOfSemester: {
      description:
        "moment.js compatible date/time indicating the start of the current semester / schedule rotation.",
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      required: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Edit config
    return await sails.models.configanalytics.updateOne(
      { ID: 1 },
      { startOfSemester: inputs.startOfSemester }
    );
  },
};

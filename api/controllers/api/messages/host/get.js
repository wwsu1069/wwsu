module.exports = {
  friendlyName: "GET api/messages/host",

  description:
    "A WWSU client, such as DJ Controls, will use this endpoint to read messages.",

  inputs: {},

  fn: async function (inputs) {
    // Get client IP address
    let fromIP = await sails.helpers.getIp(this.req);

    // Get messages
    let records = await sails.helpers.messages.get(
      this.req.payload.host,
      fromIP,
      this.req.isSocket ? sails.sockets.getId(this.req) : null
    );

    // Subscribe to web socket if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "messages");
      sails.log.verbose("Request was a socket. Joining messages.");
    }

    return records;
  },
};

const sh = require("shorthash");

module.exports = {
  friendlyName: "POST api/messages/host",

  description: "Send messages from WWSU internal clients.",

  inputs: {
    to: {
      type: "string",
      required: true,
    },

    toFriendly: {
      type: "string",
      required: true,
    },

    message: {
      type: "string",
      required: true,
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    internalError: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    // Prevent sending messages to the display signs if host is belongsTo and the specified belongsTo is not on the air
    if (
      inputs.to.startsWith("display-") &&
      !(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload))
    )
      throw {
        forbidden:
          "This host is currently not allowed to send messages to the display signs; this host did not start the current broadcast and does not have admin permission.",
      };

    // Prevent sending messages to any website visitors if host is belongsTo and the specified belongsTo is not on the air
    if (
      (inputs.to === "website" || inputs.to.startsWith("website-")) &&
      !(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload))
    )
      throw {
        forbidden:
          "This host is currently not allowed to send messages publicly or to website visitors; this host did not start the current broadcast and does not have admin permission.",
      };

    let channel;
    let discordMessage;

    // Filter disallowed HTML
    inputs.message = await sails.helpers.sanitize(inputs.message);

    // Filter profanity
    inputs.message = await sails.helpers.filterProfane(inputs.message);

    // Truncate after 1024 characters
    inputs.message = await sails.helpers.truncateText(inputs.message, 1024);

    // Find the host's friendly name
    var stuff = await sails.models.hosts.findOne({
      host: this.req.payload.host,
    });
    if (!stuff) {
      throw { internalError: "The authorized host was not found." };
    }

    let fromFriendly = stuff.friendlyname;

    // Obfuscate the real host
    let host = `computer-${sh.unique(
      this.req.payload.host + sails.config.custom.basic.hostSecret
    )}`;

    // Send public messages in Discord as well if webchat is enabled.
    try {
      if (
        typeof DiscordClient !== "undefined" &&
        DiscordClient &&
        DiscordClient.readyTimestamp &&
        DiscordClient.ws.status !== 5 &&
        inputs.to === "website" &&
        sails.models.meta.memory.webchat
      ) {
        if (sails.models.meta.memory.discordChannel) {
          channel = DiscordClient.channels.resolve(
            sails.models.meta.memory.discordChannel
          );
          if (channel)
            discordMessage = await channel.send(
              `__Message from **${fromFriendly}**__` +
                "\n" +
                `${await sails.helpers.discord.cleanContent(
                  inputs.message,
                  channel
                )}`
            );
        } else if (sails.models.meta.memory.state.startsWith("sports")) {
          // Sports channel
          channel = DiscordClient.channels.resolve(
            sails.config.custom.discord.channelSports
          );
          if (channel)
            discordMessage = await channel.send(
              `__Message from **${fromFriendly}**__` +
                "\n" +
                `${await sails.helpers.discord.cleanContent(
                  inputs.message,
                  channel
                )}`
            );
        } else {
          // General channel
          channel = DiscordClient.channels.resolve(
            sails.config.custom.discord.channelGeneral
          );
          if (channel)
            discordMessage = await channel.send(
              `__Message from **${fromFriendly}**__` +
                "\n" +
                `${await sails.helpers.discord.cleanContent(
                  inputs.message,
                  channel
                )}`
            );
        }
      }
    } catch (e) {
      sails.log.error(e);
      // Just log Discord errors.
    }

    // Create the message
    var records = await sails.models.messages
      .create({
        from: host,
        fromFriendly: fromFriendly,
        to: inputs.to,
        toFriendly: inputs.toFriendly,
        message: inputs.message,
        discordChannel: channel ? channel.id : null,
        discordMessage: discordMessage ? discordMessage.id : null,
      })
      .fetch();

    return records;
  },
};

var sh = require("shorthash");

module.exports = {
  friendlyName: "GET api/discipline/web",

  description:
    "Get an array of discipline assigned to the requested IP that have not been acknowledged yet. Also subscribe to websockets in case they later get discipline when connected.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    var fromIP = await sails.helpers.getIp(this.req);
    var host = await sails.helpers.getHost(this.req);
    var searchto = moment().subtract(1, "days").format("YYYY-MM-DD HH:mm:ss");

    if (this.req.isSocket) {
      // If a ban is issued for this client later on, it is sent through this socket as "discipline-add" event
      sails.sockets.join(this.req, `discipline-${fromIP}`);
      sails.sockets.join(this.req, `discipline-${host}`);
    }

    var records = await sails.models.discipline
      .find({
        where: {
          or: [
            { action: "permaban", active: 1 },
            { action: "dayban", createdAt: { ">": searchto }, active: 1 },
            { action: "showban", active: 1 },
            { action: "permaban", acknowledged: 0 },
            { action: "dayban", acknowledged: 0 },
            { action: "showban", acknowledged: 0 },
          ],
          IP: [fromIP, `website-${host}`],
        },
      })
      .sort(`createdAt DESC`);

    if (records.length > 0) {
      var discipline = [];
      records.map((record) => {
        discipline.push({
          ID: record.ID,
          active: record.active,
          acknowledged: record.acknowledged,
          message: record.message,
          action: record.action,
          createdAt: record.createdAt,
        });
      });
      return discipline;
    } else {
      return [];
    }
  },
};

module.exports = {
  friendlyName: "DELETE api/discipline/host",

  description: "Remove a discipline record from the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the discipline record to remove.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.discipline.destroyOne({ ID: inputs.ID });
    if (!record)
      throw { notFound: "The provided discipline ID was not found." };

    return record;
  },
};

module.exports = {
  friendlyName: "POST api/discipline/host",

  description: "Add a discipline action to a specified host.",

  inputs: {
    active: {
      type: "boolean",
      description: "Whether or not this discipline should be in effect.",
      defaultsTo: true,
    },
    IP: {
      type: "string",
      required: true,
      description:
        "Either the IP address or unique host ID of the user to ban.",
    },
    action: {
      type: "string",
      required: true,
      isIn: ["dayban", "permaban", "showban"],
      description:
        "Type of ban: dayban (24 hours from createdAt), permaban (indefinite), show ban (until the current broadcast ends).",
    },
    message: {
      type: "string",
      defaultsTo: `Unspecified Reason`,
      description: "Reason for the discipline.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    // Prevent adding discipline to website visitors if host is belongsTo and the specified belongsTo is not on the air
    if (!(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload)))
      throw {
        forbidden:
          "This host is currently not allowed to add discipline; this host did not start the current broadcast and does not have admin permission.",
      };

    return await sails.helpers.discipline.add(
      inputs.IP,
      inputs.action,
      inputs.message,
      inputs.active
    );
  },
};

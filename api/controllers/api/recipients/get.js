module.exports = {
  friendlyName: "GET api/recipients",

  description: "Get a list of recipients for messages.",

  inputs: {},

  fn: async function (inputs) {
    // Get recipients
    var records = await sails.models.recipients.find();

    // Subscribe to web socket if applicable
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "recipients");
      sails.log.verbose("Request was a socket. Joining recipients.");
    }

    return records;
  },
};

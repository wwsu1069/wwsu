module.exports = {
  friendlyName: "POST api/recipients/display",

  description:
    "Registers a display sign recipient as online.",

  inputs: {
    host: {
      type: "string",
      required: true,
      description: "Name of the host being registered.",
    },
  },

  fn: async function (inputs) {
    let response = await sails.helpers.recipients.add(
      sails.sockets.getId(this.req),
      inputs.host,
      "display",
      inputs.host
    );

    // Join display-refresh to receive requests to refresh the display signs
    sails.sockets.join(this.req, "display-refresh");

    // Join a messages socket to receive messages sent to this display sign
    sails.sockets.join(this.req, `messages-${inputs.host}`);

    sails.log.verbose(
      "Request was a display host. Joined display-refresh and messages-(host)."
    );

    // Return the nickname for this host as a label object
    return response;
  },
};

var sh = require("shorthash");

module.exports = {
  friendlyName: "PUT api/recipients/web",

  description:
    "Changes a label/nickname or a device ID for a public recipient.",

  inputs: {
    label: {
      type: "string",
      description: "The new label or nickname for this recipient.",
    },
    device: {
      type: "string",
      description: "The new device Id for this user",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    // Get the recipient host
    var host = await sails.helpers.getHost(this.req);

    if (inputs.label) {
      // Filter disallowed HTML
      inputs.label = await sails.helpers.sanitize(inputs.label);

      // Do not allow profane labels
      inputs.label = await sails.helpers.filterProfane(inputs.label);

      // Truncate after 64 characters
      inputs.label = await sails.helpers.truncateText(inputs.label, 64);
    }

    let criteria = {
      label: inputs.label ? `Web (${inputs.label})` : undefined, // Always use format "Web (nickname)" for web labels.
      device: inputs.device,
    };

    let criteriaB = _.cloneDeep(criteria);

    // Update the recipient
    var record = await sails.models.recipients.updateOne(
      { host: `website-${host}` },
      criteriaB
    );
    if (!record)
      throw {
        forbidden:
          "This device has not been registered online yet. Please call POST api/recipients/web and then try this again.",
      };

    return record;
  },
};

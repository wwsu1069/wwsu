var sh = require("shorthash");

module.exports = {
  friendlyName: "PUT api/recipients/host",

  description:
    "Edits a host recipient (can only edit the peer ID from skywayJS / peerJS).",

  inputs: {
    peer: {
      type: "string",
      allowNull: true,
      description: `The PeerJS ID assigned to this recipient. Use null if a peer was removed.`,
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Update the recipient peer ID
    let record = await sails.models.recipients.updateOne(
      {
        host: `computer-${sh.unique(
          this.req.payload.host + sails.config.custom.basic.hostSecret
        )}`,
      },
      { peer: inputs.peer || null }
    );
    if (!record) throw { notFound: "The authorized host was not found." };

    return record;
  },
};

module.exports = {
  friendlyName: "POST api/recipients/host",

  description: "Registers a DJ Controls / host recipient as online.",

  inputs: {},

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Locate the host
    let host = await sails.models.hosts.findOne({
      host: this.req.payload.host,
    });
    if (!host) {
      throw { notFound: "The authorized host was not found." };
    }

    // Add the recipient
    let response = await sails.helpers.recipients.add(
      sails.sockets.getId(this.req),
      this.req.payload.host,
      "computers",
      this.req.payload.host
    );

    // Subscribe to websockets
    if (this.req.isSocket) {
      // Receive pending EAS alerts
      sails.sockets.join(this.req, "eas-pending");

      // Receive events for remote calling if this host can do remote broadcasts
      let canBroadcast = await sails.helpers.call.canStartRemoteBroadcast(
        host.ID
      );
      if (canBroadcast) {
        sails.sockets.join(this.req, "bad-call");
        sails.sockets.join(this.req, "very-bad-call");
        sails.sockets.join(this.req, "silent-call");
        sails.sockets.join(this.req, "finalize-call");
        sails.sockets.join(this.req, "remote-dropped-delay");
      }

      // Both hosts that can start remotes and can be called / receive remote audio should be informed of call quality.
      if (canBroadcast || host.answerCalls) {
        sails.sockets.join(this.req, "call-quality");
      }

      // Host responsible for the delay system should receive dump events
      if (host.delaySystem) {
        sails.sockets.join(this.req, "delay-system-dump");
      }
    }

    // Return the host label object
    return response;
  },
};

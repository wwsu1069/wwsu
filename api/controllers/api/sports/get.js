module.exports = {
  friendlyName: "GET api/sports",

  description: "Get sports information and subscribe to the sports websocket.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "sports");
      sails.log.verbose("Request was a socket. Joining sports.");
    }

    return await sails.models.sports.find();
  },
};

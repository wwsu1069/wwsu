module.exports = {
  friendlyName: "POST api/eas",

  description: "Send an alert through the Emergency Alert System.",

  inputs: {
    source: {
      type: "string",
      required: true,
      description: "The originator of this alert.",
    },

    counties: {
      type: "string",
      required: true,
      description:
        "This alert applies to this comma-delimited list of counties.",
    },

    alert: {
      type: "string",
      required: true,
      description: "Title of the alert",
    },

    severity: {
      type: "string",
      required: true,
      isIn: ["Extreme", "Severe", "Moderate", "Minor"],
      description: `Severity of alert: One of the following in order from highest to lowest ['Extreme', 'Severe', 'Moderate', 'Minor']`,
    },

    starts: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when the alert starts. Recommended ISO string.`,
    },

    expires: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when the alert expires. Recommended ISO string.`,
    },

    color: {
      type: "string",
      regex: /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i,
      description: "Hex color representing this alert.",
      required: true,
    },

    information: {
      type: "string",
      required: true,
      description: "Detailed information about this alert for the public.",
    },

    needsAired: {
      type: "boolean",
      defaultsTo: false,
      description: "Should this alert be sent over the air (text to speech)?",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    // Disallow sending as The National Weather Service or Wright State Alert; these are automatic
    if (
      ["The National Weather Service", "Wright State Alert"].indexOf(
        inputs.source
      ) !== -1
    )
      throw {
        forbidden:
          "Not allowed to send alerts as The National Weather Service or Wright State Alert; these are automatic.",
      };

    // Add the alert to EAS
    let record = await sails.helpers.eas.addAlert(
      moment().valueOf(),
      inputs.source,
      inputs.counties,
      inputs.alert,
      inputs.severity,
      inputs.starts !== null && typeof inputs.starts !== "undefined"
        ? moment(inputs.starts).format("YYYY-MM-DD HH:mm:ss")
        : moment().format("YYYY-MM-DD HH:mm:ss"),
      inputs.expires !== null && typeof inputs.expires !== "undefined"
        ? moment(inputs.expires).format("YYYY-MM-DD HH:mm:ss")
        : moment().add(15, "minutes").format("YYYY-MM-DD HH:mm:ss"),
      inputs.color,
      inputs.information,
      inputs.needsAired
    );

    // Process post tasks (this is what actually pushes the new alert out)
    await sails.helpers.eas.postParse();

    return record;
  },
};

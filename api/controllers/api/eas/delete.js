module.exports = {
  friendlyName: "DELETE api/eas",

  description: "Remove an EAS alert",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the EAS to remove.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Do not allow removal of automatic alerts
    let record = await sails.models.eas.findOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The EAS ID was not found." };
    if (
      ["The National Weather Service", "Wright State Alert"].indexOf(
        record.source
      ) !== -1
    )
      throw { forbidden: "Not allowed to delete an automatic alert." };

    let recordB = await sails.models.eas.destroyOne({ ID: inputs.ID });
    if (!recordB) throw { notFound: "The EAS ID was not found." };
    return recordB;
  },
};

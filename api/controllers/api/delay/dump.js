module.exports = {
  friendlyName: "POST api / delay / dump",

  description:
    "Transmit socket event indicating whichever DJ Controls responsible for the delay system should command the delay system to dump.",

  inputs: {},

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    // Error if the delay system is currently in critical status.
    let delayStatus = await sails.models.status.find({
      name: "delay-system",
    });
    if (delayStatus && delayStatus[0] && delayStatus[0].status === 1)
      throw {
        forbidden:
          "Delay system is currently in critical status / malfunctioning.",
      };

    // Prevent dumping if host has a belongsTo and the specified member is not on the air
    if (!(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload)))
      throw {
        forbidden:
          "This host is currently not allowed to remotely activate the dump button; this host did not start the current broadcast and does not have admin permission.",
      };

    // Transmit dump event through sockets; DJ Controls responsible for dumping should send a dump signal to the delay system via the serial port.
    sails.sockets.broadcast("delay-system-dump", "delay-system-dump", null);

    // Note that we dumped just now
    sails.models.status.lastDump = moment();
  },
};

module.exports = {
  friendlyName: "GET api/status",

  description:
    "Get the current status of all subsystems. If socket request, subscribe to receiving status changes.",

  inputs: {
    filter: {
      type: "json",
      description:
        "If provided (array of strings), we will only return / socket-subscribe statuses with the provided names.",
    },
  },

  fn: async function (inputs) {
    // Get status records
    let records = await sails.models.status.find();

    // Subscribe to websocket if applicable
    if (this.req.isSocket) {
      if (!inputs.filter || inputs.filter.constructor !== Array) {
        sails.sockets.join(this.req, "status");
        sails.log.verbose("Request was a socket. Joining status.");
      } else {
        inputs.filter.forEach((filter) => {
          sails.sockets.join(this.req, `status-${filter}`);
        });
        records = records.filter(
          (record) => inputs.filter.indexOf(record.name) !== -1
        );
      }
    }

    return records;
  },
};

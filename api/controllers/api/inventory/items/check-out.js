module.exports = {
  friendlyName: "POST api/inventory/items/:ID/check-out",

  description: "Check an item out",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the item being checked out",
    },

    name: {
      type: "string",
      required: true,
      description: "The full name of the person checking the item out",
    },

    checkOutDate: {
      type: "string",
      required: true,
      description: "The date/time the item was checked out. Defaults to now.",
      custom: function (value) {
        return moment(value).isValid();
      },
    },

    checkOutCondition: {
      type: "string",
      isIn: ["Excellent", "Very Good", "Good", "Fair", "Poor", "Broken"],
      description:
        "The condition of the item when checked out. Defaults to the condition specified in items.",
    },

    checkOutQuantity: {
      type: "number",
      required: true,
      description:
        "The number of items checked out. Must be less than or equal to the item's available quantity.",
    },

    checkOutNotes: {
      type: "string",
      description: "Any additional check-out notes.",
    },

    checkInDue: {
      type: "string",
      description: "When the item is expected to be checked back in.",
      custom: function (value) {
        return moment(value).isValid();
      },
    },
  },

  exits: {
    badRequest: {
      statusCode: 400,
    },
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Check for item
    let record = await sails.models.items.findOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The item ID provided was not found." };

    // Check if the item may be checked out
    if (!record.canCheckOut)
      throw {
        forbidden:
          "The specified item is not allowed to be checked out; the canCheckOut setting is false.",
      };

    // Check if the specified quantity is available for checking out
    if (
      inputs.checkOutQuantity >
      (await sails.helpers.inventory.getAvailableQuantity(inputs.ID))
    ) {
      throw {
        badRequest:
          "There is not enough available quantity of the specified item to check out the provided amount.",
      };
    }

    var criteria = {
      item: inputs.ID,
      name: inputs.name,
      checkOutDate: moment(inputs.checkOutDate).format("YYYY-MM-DD HH:mm:ss"),
      checkOutCondition: inputs.checkOutCondition || record.condition,
      checkOutQuantity: inputs.checkOutQuantity,
      checkOutNotes: inputs.checkOutNotes,
      checkInDue: inputs.checkInDue
        ? moment(inputs.checkInDue).format("YYYY-MM-DD HH:mm:ss")
        : undefined,
    };

    return await sails.models.checkout.create(criteria).fetch();
  },
};

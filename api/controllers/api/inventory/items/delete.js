module.exports = {
  friendlyName: "DELETE api/inventory/items/:ID",

  description:
    "Remove inventory item from the system (and all its checkout records).",

  inputs: {
    ID: {
      type: "number",
      required: true,
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.items.destroyOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The provided item was not found." };

    // All done.
    return record;
  },
};

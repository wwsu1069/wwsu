module.exports = {
  friendlyName: "DELETE api/inventory/checkout/:ID",

  description: "Remove a checkout record",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the checkout record to remove.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    let record = await sails.models.checkout.destroyOne({ ID: inputs.ID });
    if (!record)
      throw { notFound: "The provided checkout record was not found." };

    return record;
  },
};

module.exports = {
  friendlyName:
    "PUT /api/meta/:metaSecret -or- GET /api/meta/update/:metaSecret",

  description: "Update meta from a non-RadioDJ automation system.",

  inputs: {
    title: {
      type: "string",
      allowNull: true,
      description:
        "Name of the track playing. Send null or empty string when the automation system stops playing anything.",
    },
    artist: {
      type: "string",
      defaultsTo: "Unknown Artist",
      description: "Name of the artist for the track playing.",
    },
    album: {
      type: "string",
      description: "Name of the album for the track playing.",
    },
    label: {
      type: "string",
      description: "Name of the record label for the track playing.",
    },
    eventHosts: {
      type: "string",
      description:
        "When starting a new event and we do not want to use the default hosts in the calendar system, this should be the name of the hosts separated with semicolons.",
    },
    eventName: {
      type: "string",
      allowNull: true,
      description:
        "When starting a new event, this should be provided with the name of the event (as used in the calendar events system). Send null or empty string when no longer airing an event.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs, exits) {
    if (sails.config.custom.radiodjs.length)
      return exits.forbidden(
        "You cannot manually set metadata when using RadioDJ; it is set automatically by the system."
      );

    // Metadata for now playing track
    if (inputs.title === null || inputs.title === "") {
      await sails.helpers.meta.change.with({
        trackArtist: null,
        trackTitle: null,
        trackAlbum: null,
        trackLabel: null,
        trackStamp: null,
      });
    } else if (inputs.title) {
      await sails.helpers.meta.change.with({
        trackArtist: inputs.artist || "Unknown artist",
        trackTitle: inputs.title,
        trackAlbum: inputs.album || null,
        trackLabel: inputs.label || null,
        trackStamp: null,
      });
      // Log the new track playing if the track was new
      await sails.models.logs
        .create({
          attendanceID:
            inputs.changingState || sails.models.meta.memory.changingState
              ? 0
              : sails.models.meta.memory.attendanceID, // If changingState, set attendanceID to 0, which will be updated when changingState is back to null.
          logtype: "track",
          loglevel: "secondary",
          logsubtype: "automation",
          logIcon: `far fa-play-circle`,
          title: `A track played in automation.`,
          event: `${
            sails.models.meta.memory.trackArtist
              ? `Artist: ${sails.models.meta.memory.trackArtist}<br />`
              : ``
          }
${
  sails.models.meta.memory.trackTitle
    ? `Title: ${sails.models.meta.memory.trackTitle}<br />`
    : ``
}
${
  sails.models.meta.memory.trackAlbum
    ? `Album: ${sails.models.meta.memory.trackAlbum}<br />`
    : ``
}
${
  sails.models.meta.memory.trackLabel
    ? `Label: ${sails.models.meta.memory.trackLabel}<br />`
    : ``
}`,
          trackArtist: sails.models.meta.memory.trackArtist || null,
          trackTitle: sails.models.meta.memory.trackTitle || null,
          trackAlbum: sails.models.meta.memory.trackAlbum || null,
          trackLabel: sails.models.meta.memory.trackLabel || null,
        })
        .fetch()
        .tolerate(() => {});
    }

    // Update event on the air
    if (
      inputs.eventName &&
      inputs.eventName !== null &&
      inputs.eventName !== ""
    ) {
      // This is a new event going on the air
      const newEvent = async () => {
        // Get what is on the schedule.
        let events = sails.models.calendar.calendardb.whatShouldBePlaying();

        // See if any of the events match the eventName provided
        let event = events.find(
          (ev) =>
            ev.name === inputs.eventName &&
            [
              "show",
              "remote",
              "sports",
              "prerecord",
              "playlist",
              "genre",
            ].indexOf(ev.type) !== -1
        );

        // If not, try to find its original calendar event
        if (!event)
          event = sails.models.calendar.calendardb.calendar.find(
            (ev) =>
              ev.name === inputs.eventName &&
              [
                "show",
                "remote",
                "sports",
                "prerecord",
                "playlist",
                "genre",
              ].indexOf(ev.type) !== -1 &&
              ev.active,
            true
          );

        // Still no event? Throw forbidden
        if (!event)
          return exits.forbidden(
            "The provided eventName does not exist as an active broadcast event in the calendar."
          );

        // Update meta state
        await sails.helpers.meta.changeDjs(
          `${inputs.eventHosts || event.hosts} - ${event.name}`
        );
        switch (event.type) {
          case "show":
            await sails.helpers.meta.change.with({
              state: "live_on",
              host: null,
              show: `${inputs.eventHosts || event.hosts} - ${event.name}`,
              topic: event.description || `No Description Provided`,
              trackStamp: null,
              webchat: true,
            });
            break;
          case "remote":
            await sails.helpers.meta.change.with({
              state: "remote_on",
              host: null,
              show: `${inputs.eventHosts || event.hosts} - ${event.name}`,
              topic: event.description || `No Description Provided`,
              trackStamp: null,
              webchat: true,
            });
            break;
          case "sports":
            await sails.helpers.meta.change.with({
              state: "sports_on",
              host: null,
              show: event.name,
              topic: event.description || `No Description Provided`,
              trackStamp: null,
              webchat: true,
            });
            break;
          case "prerecord":
            await sails.helpers.meta.change.with({
              state: "prerecord_on",
              host: null,
              show: `${inputs.eventHosts || event.hosts} - ${event.name}`,
              topic: event.description || `No Description Provided`,
              trackStamp: null,
              webchat: true,
            });
            break;
          case "genre":
            await sails.helpers.meta.change.with({
              state: "automation_genre",
              host: null,
              show: ``,
              genre: event.name,
              topic: event.description || `No Description Provided`,
              trackStamp: null,
              webchat: true,
            });
            break;
          case "playlist":
            await sails.helpers.meta.change.with({
              state: "automation_playlist",
              host: null,
              show: `${inputs.eventHosts || event.hosts} - ${event.name}`,
              topic: event.description || `No Description Provided`,
              trackStamp: null,
              webchat: true,
            });
            break;
        }
        await sails.helpers.meta.newShow();
        return;
      };

      if (sails.models.meta.memory.calendarUnique) {
        // Terminating events?
        if (inputs.eventName === null || inputs.eventName === "") {
          await sails.helpers.state.automation();
          // Event in progress?
        } else {
          let eventNow = sails.models.calendar.calendardb.getEventsByUnique(
            sails.models.meta.memory.calendarUnique
          );
          if (!eventNow[0] || eventNow[0].name !== inputs.eventName) {
            await newEvent();
          }
        }
      } else if (inputs.eventName && inputs.eventName !== "") {
        await newEvent();
      }
    } else if (typeof inputs.eventName !== "undefined") {
      // Lock system from any other state changing requests until we are done.
      await sails.helpers.meta.change.with({
        changingState: `Changing to automation`,
      });

      // Actually go to automation
      await sails.helpers.state.automation(inputs.transition);
    }

    return exits.success();
  },
};

// TODO: Test this

module.exports = {
  friendlyName: "POST api/calendar/clockwheels/web",

  description: "Add a clockwheel segment via the DJ web panel.",

  inputs: {
    calendarUnique: {
      type: "string",
      required: true,
      custom: (value) => {
        let temp = value.split("-");

        if (temp.length > 3 || temp.length < 2) return false; // No more than 3 parts, and no less than 2, allowed

        if (!temp[0] || isNaN(parseInt(temp[0]))) return false; // First part must exist and be a number.

        if (!temp[1] || (temp[1] !== "null" && !moment(temp[1]).isValid()))
          return false; // Second part must be null or a valid moment timestamp.

        if (temp[2] && isNaN(parseInt(temp[2]))) return false; // Third part, if provided, must be a valid number.

        return true;
      },
      // TODO: Program this
      description:
        "The unique calendar string this clockwheel pertains to. Use just #-null to supply a default for the given calendar Id. Use #-null-# to supply a default for the given calendar ID and schedule ID.",
    },

    relativeStart: {
      type: "number",
      required: true,
      min: 0,
      description:
        "The number of minutes since the start of the event that this segment starts at.",
    },

    relativeEnd: {
      type: "number",
      required: true,
      min: 0,
      description:
        "The number of minutes since the start of the event that this segment ends at.",
    },

    segmentName: {
      type: "string",
      required: true,
      maxLength: 255,
      description: "The name of the segment",
    },

    segmentColor: {
      type: "string",
      defaultsTo: "#D50000",
      isHexColor: true,
      description: "The hex color to shade on the clockwheel for this segment.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
    badRequest: {
      statusCode: 400,
    },
  },

  fn: async function (inputs) {
    let minutes = {};

    // End should be greater than start. If not, error.
    if (inputs.relativeEnd <= inputs.relativeStart)
      throw { badRequest: "relativeEnd should be greater than relativeStart." };

    // Decipher the unique string.
    let temp = inputs.calendarUnique.split("-");
    let calendarID = parseInt(temp[0]);
    let start = temp[1];
    let scheduleID = parseInt(temp[2]);

    let events;

    // Grab the event to the best of our ability based on the calendarUnique provided.
    try {
      events = sails.models.calendar.calendardb.getEventsByUnique(
        inputs.calendarUnique
      );
    } catch (e) {
      let event = await sails.models.calendar.findOne({
        ID: calendarID,
        or: [
          {
            hostDJ: this.req.payload.ID,
            cohostDJ1: this.req.payload.ID,
            cohostDJ2: this.req.payload.ID,
            cohostDJ3: this.req.payload.ID,
          },
        ],
        active: true,
      });
      if (!event)
        throw {
          notFound:
            "A calendar event with the provided ID in calendarUnique was not found.",
        };

      if (scheduleID) {
        let schedule = await sails.models.schedule.findOne({
          ID: scheduleID,
          calendarID: event.ID,
        });
        if (!schedule)
          throw {
            notFound:
              "A schedule with the provided ID in calendarUnique was not found.",
          };

        events = [sails.models.calendar.calendardb.scheduleToEvent(schedule)];
      } else {
        events = [event];
      }
    }
    events = events.find((event) => !event.scheduleType.startsWith("canceled"));
    if (!events)
      throw {
        notFound:
          "Could not find any active occurrences with the provided calendarUnique.",
      };

    // Make sure there is not already a clockwheel set for any of the given minutes.
    let clockwheels = await sails.models.clockwheels.find({
      calendarUnique: inputs.calendarUnique,
    });
    if (clockwheels && clockwheels.length > 0) {
      clockwheels.map((clockwheel) => {
        for (
          let i = clockwheel.relativeStart;
          i < clockwheel.relativeEnd;
          i++
        ) {
          minutes[i] = true;
        }
      });
    }
    let invalidTime = false;
    for (let i = inputs.relativeStart; i < inputs.relativeEnd; i++) {
      if (minutes[i]) {
        invalidTime = true;
      }
    }
    if (invalidTime)
      throw {
        badRequest:
          "The requested clockwheel segment would overlap another clockwheel segment.",
      };

    // Create the clockwheel
    return await sails.models.clockwheels
      .create({
        calendarID: calendarID,
        scheduleID: scheduleID,
        calendarUnique: inputs.calendarUnique,
        relativeStart: inputs.relativeStart,
        relativeEnd: inputs.relativeEnd,
        segmentName: inputs.segmentName,
        segmentColor: inputs.segmentColor,
      })
      .fetch();
  },
};

module.exports = {
  friendlyName: "PUT api/calendar/clockwheels/web",

  description: "Edit a clockwheel segment via the DJ web panel.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "ID of the clockwheel to edit",
    },

    calendarUnique: {
      type: "string",
      custom: (value) => {
        let temp = value.split("-");

        if (temp.length > 3 || temp.length < 2) return false; // No more than 3 parts, and no less than 2, allowed

        if (!temp[0] || isNaN(parseInt(temp[0]))) return false; // First part must exist and be a number.

        if (!temp[1] || (temp[1] !== "null" && !moment(temp[1]).isValid()))
          return false; // Second part must be null or a valid moment timestamp.

        if (temp[2] && isNaN(parseInt(temp[2]))) return false; // Third part, if provided, must be a valid number.

        return true;
      },
      // TODO: Program this
      description:
        "The unique calendar string this clockwheel pertains to. Use just #-null to supply a default for the given calendar Id. Use #-null-# to supply a default for the given calendar ID and schedule ID.",
    },

    relativeStart: {
      type: "number",
      min: 0,
      description:
        "The number of minutes since the start of the event that this segment starts at.",
    },

    relativeEnd: {
      type: "number",
      min: 0,
      description:
        "The number of minutes since the start of the event that this segment ends at.",
    },

    segmentName: {
      type: "string",
      maxLength: 255,
      description: "The name of the segment",
    },

    segmentColor: {
      type: "string",
      isHexColor: true,
      description: "The hex color to shade on the clockwheel for this segment.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
    badRequest: {
      statusCode: 400,
    },
  },

  fn: async function (inputs) {
    // Get the original record
    let original = await sails.models.clockwheels.findOne({ ID: inputs.ID });
    if (!original) throw "notFound";

    // Prep criteria for editing
    let criteria = {};
    if (typeof inputs.calendarUnique !== "undefined")
      criteria.calendarUnique = inputs.calendarUnique;
    if (typeof inputs.relativeStart !== "undefined")
      criteria.relativeStart = inputs.relativeStart;
    if (typeof inputs.relativeEnd !== "undefined")
      criteria.relativeEnd = inputs.relativeEnd;
    if (typeof inputs.segmentName !== "undefined")
      criteria.segmentName = inputs.segmentName;
    if (typeof inputs.segmentColor !== "undefined")
      criteria.segmentColor = inputs.segmentColor;
    let criteriaB = _.cloneDeep(criteria);

    // Combine original record with requested changes so that we can run checks.
    let modified = Object.assign(original, criteriaB);

    // Decipher the unique string.
    let temp = modified.calendarUnique.split("-");
    let calendarID = parseInt(temp[0]);
    let start = temp[1];
    let scheduleID = parseInt(temp[2]);

    // Set the new calendarID and scheduleID
    modified.calendarID = calendarID;
    modified.scheduleID = scheduleID;
    criteriaB.calendarID = calendarID;
    criteriaB.scheduleID = scheduleID;

    let minutes = {};

    // End should be greater than start. If not, error.
    if (modified.relativeEnd <= modified.relativeStart)
      throw { badRequest: "relativeEnd should be greater than relativeStart." };

    let events;

    // Grab the event to the best of our ability based on the calendarUnique provided.
    try {
      events = sails.models.calendar.calendardb.getEventsByUnique(
        modified.calendarUnique
      );
    } catch (e) {
      let event = await sails.models.calendar.findOne({
        ID: calendarID,
        or: [
          {
            hostDJ: this.req.payload.ID,
            cohostDJ1: this.req.payload.ID,
            cohostDJ2: this.req.payload.ID,
            cohostDJ3: this.req.payload.ID,
          },
        ],
        active: true,
      });
      if (!event)
        throw {
          notFound:
            "A calendar event with the ID in calendarUnique was not found.",
        };

      if (scheduleID) {
        let schedule = await sails.models.schedule.findOne({
          ID: scheduleID,
          calendarID: event.ID,
        });
        if (!schedule)
          throw {
            notFound: "A schedule with the ID in calendarUnique was not found.",
          };

        events = [sails.models.calendar.calendardb.scheduleToEvent(schedule)];
      } else {
        events = [event];
      }
    }
    events = events.find((event) => !event.scheduleType.startsWith("canceled"));
    if (!events)
      throw {
        notFound:
          "Could not find any active occurrences with the calendarUnique.",
      };

    // Make sure there is not already a clockwheel set for any of the given minutes.
    let clockwheels = await sails.models.clockwheels.find({
      calendarUnique: modified.calendarUnique,
      ID: { "!=": inputs.ID }, // Don't include this record since we are modifying it
    });
    if (clockwheels && clockwheels.length > 0) {
      clockwheels.map((clockwheel) => {
        for (
          let i = clockwheel.relativeStart;
          i < clockwheel.relativeEnd;
          i++
        ) {
          minutes[i] = true;
        }
      });
    }
    let invalidTime = false;
    for (let i = modified.relativeStart; i < modified.relativeEnd; i++) {
      if (minutes[i]) {
        invalidTime = true;
      }
    }
    if (invalidTime)
      throw {
        badRequest:
          "The requested clockwheel segment would overlap another clockwheel segment.",
      };

    // Update the clockwheel
    return await sails.models.clockwheels.updateOne(
      { ID: inputs.ID },
      criteriaB
    );
  },
};

module.exports = {
  friendlyName: "PUT api / calendar / event",

  description: "Edit a main calendar event.",

  inputs: {
    ID: {
      required: true,
      type: "number",
    },

    type: {
      type: "string",
      isIn: [
        "show",
        "sports",
        "remote",
        "prerecord",
        "genre",
        "playlist",
        "event",
        "onair-booking",
        "prod-booking",
        "office-hours",
      ],
    },

    priority: {
      type: "number",
      allowNull: true,
    },

    hostDJ: {
      type: "number",
      allowNull: true,
    },

    cohostDJ1: {
      type: "number",
      allowNull: true,
    },

    cohostDJ2: {
      type: "number",
      allowNull: true,
    },

    cohostDJ3: {
      type: "number",
      allowNull: true,
    },

    eventID: {
      type: "number",
      allowNull: true,
    },

    playlistID: {
      type: "number",
      allowNull: true,
    },

    director: {
      type: "number",
      allowNull: true,
    },

    name: {
      type: "string",
      allowNull: true,
    },

    description: {
      type: "string",
      allowNull: true,
    },

    logo: {
      type: "number",
      allowNull: true,
    },

    banner: {
      type: "number",
      allowNull: true,
    },
  },

  exits: {
    badRequest: {
      statusCode: 400,
    },
    success: {
      statusCode: 202, // Accepted
    },
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // Get the original calendar record
    var calendar = await sails.models.calendar.findOne({ ID: inputs.ID });
    if (!calendar) throw { notFound: "The calendar ID was not found." };

    // Bail if we are still processing something for this Calendar ID
    if (sails.models.status.tasks.calendar.has(`calendar`))
      throw {
        conflict:
          "Another calendar operation is in progress. This request was blocked to prevent conflicts. Please try again in a minute.",
      };

    // Reserve the operation to prevent conflicts
    sails.models.status.tasks.calendar.set(`calendar`, true);

    try {
      // Verify the event
      let event = {
        ID: inputs.ID,
        type: inputs.type,
        priority: inputs.priority,
        hostDJ: inputs.hostDJ,
        cohostDJ1: inputs.cohostDJ1,
        cohostDJ2: inputs.cohostDJ2,
        cohostDJ3: inputs.cohostDJ3,
        eventID: inputs.eventID,
        playlistID: inputs.playlistID,
        director: inputs.director,
        name: inputs.name,
        description: inputs.description,
        logo: inputs.logo,
        banner: inputs.banner,
      };

      // Polyfill main calendar info with edits requested
      var tempCal = {};
      for (var stuff in calendar) {
        if (Object.prototype.hasOwnProperty.call(calendar, stuff)) {
          if (
            typeof calendar[stuff] !== "undefined" &&
            calendar[stuff] !== null
          )
            tempCal[stuff] = calendar[stuff];
        }
      }
      for (var stuff in event) {
        if (Object.prototype.hasOwnProperty.call(event, stuff)) {
          if (typeof event[stuff] !== "undefined" && event[stuff] !== null)
            tempCal[stuff] = event[stuff];
        }
      }

      // Verify the edits
      try {
        event = await sails.helpers.calendar.verify(tempCal);
      } catch (e) {
        sails.models.status.tasks.calendar.delete(`calendar`);
        throw { badRequest: e.message };
      }

      // Check for and process event conflicts in the background
      sails.models.calendar.calendardb.checkConflicts(
        async (conflicts) => {
          // Do not continue for conflict errors
          if (conflicts.errors.length > 0) {
            sails.models.status.tasks.calendar.delete(`calendar`);
            sails.log.error(new Error(conflicts.errors.join("; ")));
            return;
          }

          // Edit the event into the calendar
          await sails.models.calendar.updateOne({ ID: inputs.ID }, event);

          // Remove records which should be removed first
          if (conflicts.removals.length > 0) {
            await sails.models.schedule
              .destroy({
                ID: conflicts.removals.map((removal) => removal.scheduleID),
              })
              .fetch();
          }

          // Now, add overrides
          if (conflicts.additions.length > 0) {
            let cfMaps = conflicts.additions.map(async (override) => {
              await sails.models.schedule.create(override).fetch();
            });
            await Promise.all(cfMaps);
          }

          // Finally, re-check the calendar events and update cache after 5 seconds
          setTimeout(async () => {
            await sails.helpers.calendar.check(false, true);
          }, 5000);

          sails.models.status.tasks.calendar.delete(`calendar`);
        },
        [{ updateCalendar: event }]
      );

      return "Request accepted. The event will be edited after conflict resolution is run.";
    } catch (e) {
      sails.models.status.tasks.calendar.delete(`calendar`);
      throw e;
    }
  },
};

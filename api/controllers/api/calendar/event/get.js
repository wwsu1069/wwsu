module.exports = {
  friendlyName: "GET api / calendar / event",

  description:
    "Get calendar events to use in CalendarDb (or get a single one with analytics if ID provided). Also subscribe to sockets if ID not provided.",

  inputs: {
    ID: {
      type: "number",
      description: `If provided, instead of returning an array of events, will return information about the specified event.`,
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // No ID? Return all events and subscribe to sockets.
    if (!inputs.ID) {
      var calendarRecords = await sails.models.calendar.find();

      // Subscribe to sockets if applicable
      if (this.req.isSocket) {
        sails.sockets.join(this.req, "calendar");
        sails.log.verbose("Request was a socket. Joining calendar.");
      }

      return calendarRecords;

      // Return a specific event
    } else {
      var record = await sails.models.calendar.findOne({ ID: inputs.ID });
      if (!record) throw "notFound";

      // Include the date of the start of the semester
      var returnData = {
        event: record,
      };

      // Add attendance records for the event
      returnData.attendance = await sails.models.attendance.find({
        calendarID: inputs.ID,
      });

      // Add analytics for the event to return data
      let weekStats = await sails.helpers.analytics.showtime(
        undefined,
        [inputs.ID],
        moment().subtract(7, "days").toISOString(true),
        undefined,
        true
      );
      let semesterStats = await sails.helpers.analytics.showtime(
        undefined,
        [inputs.ID],
        moment(sails.config.custom.analytics.startOfSemester).toISOString(true),
        undefined,
        true
      );
      let yearStats = await sails.helpers.analytics.showtime(
        undefined,
        [inputs.ID],
        moment().subtract(1, "years").toISOString(true),
        undefined,
        true
      );
      returnData.stats = {
        week: weekStats,
        semester: semesterStats,
        year: yearStats,
      };

      return returnData;
    }
  },
};

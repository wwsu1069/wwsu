module.exports = {
  friendlyName: "PUT api / calendar / event / activate",

  description: "Mark an event in the main calendar active",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID number of the calendar event to mark active.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Controller calendar/active called.");

    let record = await sails.models.calendar.findOne({ ID: inputs.ID });
    if (!record) throw "notFound";

    // verify the event (in case something changed since it was last active, such as hosts being removed)
    try {
      await sails.helpers.calendar.verify(record);
    } catch (e) {
      throw { conflict: e.message };
    }

    // Mark the calendar as active
    let record2 = await sails.models.calendar.updateOne(
      { ID: inputs.ID },
      {
        active: true,
        // Reset lastAired to now to prevent auto cleanup from re-marking inactive. It is assumed when an event is marked active, it will be airing soon.
        lastAired: moment().format("YYYY-MM-DD HH:mm:ss"),
      }
    );

    // Finally, re-check the calendar events and update cache after 5 seconds
    setTimeout(async () => {
      await sails.helpers.calendar.check(false, true);
    }, 5000);

    return record2;
  },
};

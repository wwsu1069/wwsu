module.exports = {
  friendlyName: "GET api / calendar / web",

  description:
    "Retrieve currently-airing radio shows, their schedules, and their cancellations / re-schedules in an easy to use data structure.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    let returnData = [];

    // Get on-air programming events that are active; sort by priority
    let events = await sails.models.calendar
      .find({
        ID: { "!=": 1 },
        active: true,
        type: ["show", "remote", "prerecord", "sports"],
      })
      .sort("priority DESC");

    let maps = events.map(async (event) => {
      // Prep a data object for this radio show
      let data = {
        name: event.name || "Unknown Event",
        type: event.type,
        hosts: event.hosts || "Unknown Hosts",
        description: event.description || undefined,
        logo: event.logo
          ? `${sails.config.custom.baseUrl}/api/uploads/${event.logo}`
          : undefined,
        banner: event.banner
          ? `${sails.config.custom.baseUrl}/api/uploads/${event.banner}`
          : undefined,
        schedules: [],
        updates: [],
      };

      // Get all schedules for this event
      let schedules = await sails.models.schedule.find({
        calendarID: event.ID,
      });

      schedules.forEach((sch) => {
        switch (sch.scheduleType) {
          // Mark re-schedules
          case "updated":
          case "updated-system":
            if (sch.newTime || sch.duration) {
              data.updates.push(
                `RE-SCHEDULED: ${moment(sch.originalTime).format(
                  "llll"
                )} re-scheduled to ${moment(
                  sch.newTime || sch.originalTime
                ).format("llll")}${
                  sch.duration
                    ? ` - ${moment(sch.newTime || sch.originalTime)
                        .add(sch.duration, "minutes")
                        .format("LT")}`
                    : ``
                }`
              );
            }
            break;
          // Mark cancellations
          case "canceled":
          case "canceled-system":
            data.updates.push(
              `CANCELED: ${moment(sch.originalTime).format("llll")}`
            );
            break;
          // Mark regular schedules; generate human readable text for them
          case null:
          case undefined:
            data.schedules.push(
              sails.models.calendar.calendardb.generateScheduleText(sch)
            );
            break;
        }
      });

      if (data.schedules.length > 0) returnData.push(data); // Do not include shows with no regular schedules
    });
    await Promise.all(maps);

    return returnData;
  },
};

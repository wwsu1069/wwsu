module.exports = {
  friendlyName: "DELETE api / calendar / schedule",

  description: "Remove a schedule record from the calendar.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID number of the schedule event to remove.",
    },
  },

  exits: {
    success: {
      statusCode: 202, // Accepted
    },
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // check if the original record exists and bail if not
    if (!(await sails.models.schedule.count({ ID: inputs.ID })))
      throw { notFound: "The schedule ID was not found." };

    // Bail if we are still processing something for this Schedule ID
    if (sails.models.status.tasks.calendar.has(`calendar`))
      throw {
        conflict:
          "Another calendar operation is in progress. This request was blocked to prevent conflicts. Please try again in a minute.",
      };

    // Reserve the operation to prevent conflicts
    sails.models.status.tasks.calendar.set(`calendar`, true);

    try {
      // Check for event conflicts
      sails.models.calendar.calendardb.checkConflicts(
        async (conflicts) => {
          // Do not continue for conflict errors
          if (conflicts.errors.length > 0) {
            sails.models.status.tasks.calendar.delete(`calendar`);
            sails.log.error(new Error(conflicts.errors.join("; ")));
            return;
          }

          sails.sockets.broadcast("schedule", "upbeat", conflicts);

          // Destroy the schedule event
          await sails.models.schedule.destroyOne({ ID: inputs.ID });

          // Remove records which should be removed first
          if (conflicts.removals.length > 0) {
            await sails.models.schedule
              .destroy({
                ID: conflicts.removals.map((removal) => removal.scheduleID),
              })
              .fetch();
          }

          // Now, add overrides
          if (conflicts.additions.length > 0) {
            let cfMaps = conflicts.additions.map(async (override) => {
              await sails.models.schedule.create(override).fetch();
            });
            await Promise.all(cfMaps);
          }

          // Finally, re-check the calendar events and update cache after 5 seconds
          setTimeout(async () => {
            await sails.helpers.calendar.check(false, true);
          }, 5000);

          sails.models.status.tasks.calendar.delete(`calendar`);
        },
        [{ remove: inputs.ID }]
      );

      return "Request accepted. The schedule will be deleted after conflict resolution is run.";
    } catch (e) {
      sails.models.status.tasks.calendar.delete(`calendar`);
      throw e;
    }
  },
};

const sh = require("shorthash");

module.exports = {
  friendlyName: "GET api / auth / Host",

  description: "Authorize a host and get a token.",

  inputs: {
    username: {
      type: "string",
      description: "The host to authorize.",
      required: true,
    },
  },

  exits: {
    success: {
      statusCode: 200,
    },
    wrong: {
      statusCode: 403,
    },
    error: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    // Verify the host first
    let hostRecords = await sails.models.hosts.count();
    let host = await sails.models.hosts.findOrCreate(
      { host: inputs.username },
      {
        host: inputs.username,

        // Automatically authorize the first host in the database and make them an admin
        authorized: (hostRecords <= 0),
        admin: (hostRecords <= 0),

        friendlyname: `Unknown ${inputs.username.substr(
          inputs.username.length - 8
        )}`,
      }
    );

    if (!host || !host.authorized) {
      throw {
        wrong: `The provided host either does not exist or is not authorized. To grant access, please use a DJ Controls with administrator privileges and authorize the host ${inputs.username}`,
      };
    }

    // Sometimes sockets will have an undefined session in Sails, which triggers errors. Make an empty object if this happens.
    //if (typeof this.req.session === "undefined") this.req.session = {};

    // Generate the token valid for 10 minutes
    this.req.session.authHost = {
      ID: host.ID,
      host: host.host,
      admin: host.admin,
      belongsTo: host.belongsTo,
      lockDown: host.lockDown,
      startRemotes: await sails.helpers.call.canStartRemoteBroadcast(host.ID),
      answerCalls: host.authorized && host.answerCalls,
      exp: moment().add(10, "minutes").toISOString(true),
    };

    // Return the token as an object
    return {
      token: sh.unique(`${host.ID}-${this.req.session.authHost.exp}`),
      expires: 60000 * 10,
    };
  },
};

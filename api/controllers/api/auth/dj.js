const bcrypt = require("bcrypt");
const sh = require("shorthash");

module.exports = {
  friendlyName: "GET api / auth / dj",

  description: "Authorize a dj and get a token.",

  inputs: {
    username: {
      type: "string",
      description: "The name of the DJ to authorize.",
      required: true,
    },

    password: {
      type: "string",
      description: "DJ login to authorize.",
      required: true,
    },
  },

  exits: {
    success: {
      statusCode: 200,
    },
    wrong: {
      statusCode: 403,
    },
    error: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    // Verify the DJ exists first
    let dj = await sails.models.djs.findOne({
      name: inputs.username,
      active: true,
      login: { "!=": null },
    });
    if (!dj) {
      throw {
        wrong:
          "The username or password is wrong, or it points to a member that does not exist or is not allowed to authorize.",
      };
    }

    // Now check the password
    let match = await bcrypt.compare(inputs.password, dj.login);

    if (!match) {
      throw {
        wrong:
          "The username or password is wrong, or it points to a member that does not exist or is not allowed to authorize.",
      };
    }

    // Sometimes sockets will have an undefined session in Sails, which triggers errors. Make an empty object if this happens.
    //if (typeof this.req.session === "undefined") this.req.session = {};

    // Generate the token valid for 60 minutes
    this.req.session.authDJ = {
      ID: dj.ID,
      name: dj.name,
      exp: moment().add(60, "minutes").toISOString(true),
    };

    // Return the token as an object
    return {
      token: sh.unique(`${dj.ID}-${this.req.session.authDJ.exp}`),
      expires: 60000 * 60,
    };
  },
};

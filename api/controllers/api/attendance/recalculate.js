module.exports = {
  friendlyName: "PUT /api/attendance/:ID?/recalculate",

  description: "Recalculate analytics for attendance records.",

  inputs: {
    ID: {
      type: "number",
      description:
        "If provided, the attendanceID to recalculate. If not provided, will recalculate all records without stats.",
    },
  },

  fn: async function (inputs) {
    if (inputs.id) {
      sails.log.debug("/api/attendance/recalculate: Calculating " & inputs.ID);
      await sails.helpers.attendance.recalculate(inputs.ID);
      sails.log.debug(
        "/api/attendance/recalculate: DONE calculating " & inputs.ID
      );
      return;
    }

    sails.log.debug(
      "/api/attendance/recalculate: Calculating all non-calculated IDs"
    );
    let records = await sails.models.attendance.find({
      showTime: null,
      actualEnd: { "!=": null },
    });
    let maps = records.map(async (record) => {
      sails.log.debug("/api/attendance/recalculate: Calculating " & record.ID);
      await sails.helpers.attendance.recalculate(record.ID);
    });
    await Promise.all(maps);

    sails.log.debug("/api/attendance/recalculate: DONE");
  },
};

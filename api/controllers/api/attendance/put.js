module.exports = {
  friendlyName: "PUT api / attendance",

  description: "Edit the status of an attendance record.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the attendance record to edit.",
    },

    happened: {
      type: "number",
      description:
        "Change the happening status... 2 = happened but unscheduled, 1 = happened, 0 = unexcused absence, -1 = excused cancellation",
      min: -1,
      max: 2,
    },

    ignore: {
      type: "number",
      description:
        "0 = do not ignore, 1 = do not count towards reputation %, 2 = do not count towards reputation % nor any reputation stats",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Determine what needs updating
    let criteria = {};

    // PATCH; only change what was provided in the body
    if (typeof inputs.happened !== "undefined" && inputs.happened !== null) {
      criteria.happened = inputs.happened;
    }
    if (typeof inputs.ignore !== "undefined" && inputs.ignore !== null) {
      criteria.ignore = inputs.ignore;
    }

    // We must clone the InitialValues object due to how Sails.js manipulates any objects passed as InitialValues.
    let criteriaB = _.cloneDeep(criteria);

    // Edit it
    let record = await sails.models.attendance.updateOne(
      { ID: inputs.ID },
      criteriaB
    );
    if (!record) throw "notFound";
    return record;
  },
};

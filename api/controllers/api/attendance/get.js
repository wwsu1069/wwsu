module.exports = {
  friendlyName: "GET api / attendance",

  description: "Retrieve attendance records.",

  inputs: {
    ID: {
      type: "number",
      description: `If we want to retrieve a specific attendance record, specify it here. Doing so ignores all other fields and returns an object instead of an array.`,
    },
    calendarID: {
      type: "number",
      allowNull: true,
      description: `Specify a calendar ID if you want to retrieve records matching a specific calendar event.`,
    },
    date: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of a date to get logs.`,
    },
    duration: {
      type: "number",
      defaultsTo: 1,
      min: 1,
      max: 14,
      description: `Number of days to get records for if date is provided. Defaults to 1.`,
    },
    dj: {
      type: "number",
      allowNull: true,
      description: `Retrieve attendance records for the specified DJ. If provided, date is ignored.`,
    },
    event: {
      type: "string",
      allowNull: true,
      description: `DEPRECATED (use calendarID instead): Return attendance records where this string is contained within the record's event field. If provided, date is ignored. If DJ is provided, will further filter by the DJ.`,
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Controller attendance/get called.");

    let query = {};

    if (inputs.ID) {
      query.ID = inputs.ID;
    } else {
      if (inputs.calendarID && inputs.calendarID !== null) {
        query.calendarID = inputs.calendarID;
      }

      // No DJ nor event? Filter by date.
      if (
        (!inputs.dj || inputs.dj === null) &&
        (!inputs.event || inputs.event === null)
      ) {
        // Subscribe to sockets if applicable
        if (this.req.isSocket) {
          sails.sockets.join(this.req, "attendance");
          sails.log.verbose("Request was a socket. Joining attendance.");
        }

        if (inputs.date && inputs.date !== null) {
          let start = moment(inputs.date).startOf("day");
          let end = moment(start).add(inputs.duration, "days");
          query.or = [
            {
              scheduledStart: {
                ">=": start.format("YYYY-MM-DD HH:mm:ss"),
                "<": end.format("YYYY-MM-DD HH:mm:ss"),
              },
            },
            {
              actualStart: {
                ">=": start.format("YYYY-MM-DD HH:mm:ss"),
                "<": end.format("YYYY-MM-DD HH:mm:ss"),
              },
            },
          ];
        }
      } else {
        if (inputs.dj && inputs.dj !== null) {
          query.or = [
            { dj: inputs.dj },
            { cohostDJ1: inputs.dj },
            { cohostDJ2: inputs.dj },
            { cohostDJ3: inputs.dj },
          ];
        }

        if (inputs.event && inputs.event !== null) {
          query.event = { contains: inputs.event };
        }
      }
    }

    // Get records
    if (inputs.ID) {
      let record = await sails.models.attendance.findOne(query);
      if (!record) throw "notFound";

      return record;
    } else {
      let records = await sails.models.attendance.find(query);

      if (records) {
        // We want to sort by actualStart if not null, else scheduledStart if not null, else ID. We can't do that in ORM, so use a compare function instead.
        let compare = function (a, b) {
          let theDateA =
            a.actualStart !== null ? a.actualStart : a.scheduledStart;
          let theDateB =
            b.actualStart !== null ? b.actualStart : b.scheduledStart;
          if (moment(theDateA).valueOf() < moment(theDateB).valueOf()) {
            return -1;
          }
          if (moment(theDateA).valueOf() > moment(theDateB).valueOf()) {
            return 1;
          }
          if (a.ID > b.ID) {
            return 1;
          }
          if (b.ID > a.ID) {
            return -1;
          }
          return 0;
        };

        records.sort(compare);
      }

      sails.log.verbose(`Special records returned: ${records.length}`);
      sails.log.silly(records);

      return records;
    }
  },
};

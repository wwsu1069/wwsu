module.exports = {
  friendlyName: "PUT api/logs/acknowledge-all",

  description: "Mark all logs as acknowledged.",

  inputs: {},

  exits: {},

  fn: async function (inputs) {
    await sails.models.logs.update(
      { acknowledged: true },
      { acknowledged: false }
    );
    return;
  },
};

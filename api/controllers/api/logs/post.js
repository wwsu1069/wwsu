module.exports = {
  friendlyName: "POST api/logs",

  description: "Add a log entry into the system.",

  inputs: {
    date: {
      type: "string",
      custom: function (value) {
        return moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of a date in which this log took place. Defaults to now.`,
    },
    logtype: {
      type: "string",
      required: true,
      description: "Category of log. Use manual for DJs adding track logs during their broadcast.",
    },

    loglevel: {
      type: "string",
      required: true,
      isIn: [
        "danger",
        "orange",
        "warning",
        "info",
        "success",
        "primary",
        "secondary",
      ],
      description:
        "Log severity: danger, orange, warning, info, success, primary, or secondary.",
    },

    logsubtype: {
      type: "string",
      allowNull: true,
      description: "Log subcategory / subtype, such as a radio show name.",
    },

    logIcon: {
      type: "string",
      required: true,
      description: "The fontawesome icon associated with this log",
    },

    excused: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If applicable, whether or not this log should be excused from reputation",
    },

    title: {
      type: "string",
      required: true,
      description: "Event title",
    },

    event: {
      type: "string",
      description:
        "The log event / what happened, plus any data (other than track information).",
    },

    trackArtist: {
      type: "string",
      allowNull: true,
      description:
        "If a track was played, the artist of the track, used for spin counts.",
    },

    trackTitle: {
      type: "string",
      allowNull: true,
      description:
        "If a track was played, the title of the track, used for spin counts.",
    },

    trackAlbum: {
      type: "string",
      allowNull: true,
      description: "If a track was played, the album of the track.",
    },

    trackLabel: {
      type: "string",
      allowNull: true,
      description: "If a track was played, the record label of the track.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    // Prevent adding manual logs if host is belongsTo and the specified belongsTo is not on the air
    if (
      inputs.logtype === "manual" &&
      !await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload)
    )
      throw {
        forbidden:
          "This host is currently not allowed to add manual logs; this host did not start the current broadcast and does not have admin permission.",
      };

    // Create the log entry
    let record = await sails.models.logs
      .create({
        attendanceID: sails.models.meta.memory.attendanceID,
        logtype: inputs.logtype,
        loglevel: inputs.loglevel,
        logsubtype: inputs.logsubtype,
        excused: inputs.excused,
        logIcon: inputs.logIcon,
        title: inputs.title,
        event: inputs.event,
        trackArtist: inputs.trackArtist,
        trackTitle: inputs.trackTitle,
        trackAlbum: inputs.trackAlbum,
        trackLabel: inputs.trackLabel,
        createdAt:
          inputs.date !== null && typeof inputs.date !== "undefined"
            ? moment(inputs.date).format("YYYY-MM-DD HH:mm:ss")
            : moment().format("YYYY-MM-DD HH:mm:ss"),
      })
      .fetch();

    // Set manual meta if criteria matches
    if (
      inputs.logtype === "manual" &&
      inputs.trackArtist &&
      inputs.trackArtist.length > 0 &&
      inputs.trackTitle &&
      inputs.trackTitle.length > 0
    ) {
      await sails.helpers.meta.change.with({
        trackArtist: inputs.trackArtist,
        trackTitle: inputs.trackTitle,
        trackAlbum: inputs.trackAlbum,
        trackLabel: inputs.trackLabel,
        trackStamp:
          inputs.date !== null && typeof inputs.date !== "undefined"
            ? moment(inputs.date).format("YYYY-MM-DD HH:mm:ss")
            : moment().format("YYYY-MM-DD HH:mm:ss"),
      });

      // Empty log; clear metadata
    } else if (inputs.logtype === "manual") {
      await sails.helpers.meta.change.with({
        trackArtist: null,
        trackTitle: null,
        trackAlbum: null,
        trackLabel: null,
        trackStamp: null,
      });
    }

    return record;
  },
};

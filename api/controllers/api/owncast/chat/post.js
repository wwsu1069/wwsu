module.exports = {
    friendlyName: "POST api/owncast/chat/:owncastSecret",

    description: "Owncast should POST to this endpoint via a webhook when a user sends a message.",

    inputs: {
        type: {
            type: "string",
            required: true,
            isIn: ["CHAT"], // Should never be anything else; integrity check
            description: "The event type."
        },
        eventData: {
            type: "json",
            required: true,
            custom: (value) => {
                // Ensure structural integrity
                if (typeof value.user === 'undefined') {
                    return false;
                }
                if (typeof value.user.id === 'undefined') {
                    return false;
                }
                if (typeof value.user.displayName === 'undefined') {
                    return false;
                }
                if (typeof value.body === 'undefined') {
                    return false;
                }

                return true;
            },
            description: "The data pertaining to the event."
        }
    },

    fn: async function (inputs) {
        // Prepare data
        let opts = {
            message: inputs.eventData.body,
            fromIP: "owncast-" + inputs.eventData.user.id, // Use user ID as the IP
            nickname: inputs.eventData.user.displayName || null,
            private: false,
            host: inputs.eventData.user.id, // Use user ID as host
        };

        let channel;
        let discordMessage;

        let theid = opts.host;
        // If no nickname provided, use host as the nickname
        if (inputs.nickname === null) {
            inputs.nickname = theid;
        }
        let record = null;

        // Filter disallowed HTML
        opts.message = await sails.helpers.sanitize(opts.message);

        // Filter profanity
        opts.message = await sails.helpers.filterProfane(opts.message);

        // Truncate after 1024 characters
        opts.message = await sails.helpers.truncateText(opts.message, 1024);

        // Create and broadcast the message
        try {
            if (
                typeof DiscordClient !== "undefined" &&
                DiscordClient &&
                DiscordClient.readyTimestamp &&
                DiscordClient.ws.status !== 5 &&
                sails.models.meta.memory.webchat
            ) {
                if (sails.models.meta.memory.discordChannel) {
                    channel = DiscordClient.channels.resolve(
                        sails.models.meta.memory.discordChannel
                    );
                    if (channel)
                        discordMessage = await channel.send(
                            `__Message from **Owncast (${inputs.nickname})**__` +
                            "\n" +
                            `${await sails.helpers.discord.cleanContent(
                                opts.message,
                                channel
                            )}`
                        );
                } else if (sails.models.meta.memory.state.startsWith("sports")) {
                    // Sports channel
                    channel = DiscordClient.channels.resolve(
                        sails.config.custom.discord.channelSports
                    );
                    if (channel)
                        discordMessage = await channel.send(
                            `__Message from **Owncast (${inputs.nickname})**__` +
                            "\n" +
                            `${await sails.helpers.discord.cleanContent(
                                opts.message,
                                channel
                            )}`
                        );
                } else {
                    // General channel
                    channel = DiscordClient.channels.resolve(
                        sails.config.custom.discord.channelGeneral
                    );
                    if (channel)
                        discordMessage = await channel.send(
                            `__Message from **Owncast (${inputs.nickname})**__` +
                            "\n" +
                            `${await sails.helpers.discord.cleanContent(
                                opts.message,
                                channel
                            )}`
                        );
                }
            }
        } catch (e) {
            sails.log.error(e);
            // Log errors but continue execution.
        }

        record = await sails.models.messages
            .create({
                status: "active",
                from: `owncast-${theid}`,
                fromFriendly: `Owncast (${inputs.nickname})`,
                fromIP: opts.fromIP,
                to: "DJ",
                toFriendly: "Public",
                message: opts.message + "\n\n**This message came from Owncast.** To respond, use the Owncast chat at video.wwsu1069.org.", // TODO: fudge code
                discordChannel: channel ? channel.id : null,
                discordMessage: discordMessage ? discordMessage.id : null,
            })
            .fetch();
        delete record.fromIP; // We do not want to broadcast IP addresses!

        return record;
    },
};

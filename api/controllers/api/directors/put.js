const bcrypt = require("bcrypt");
const randomString = require("randomstring");

module.exports = {
  friendlyName: "PUT api/directors",

  description: "Edit one of the directors in the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the director to edit.",
    },

    name: {
      type: "string",
      description: "If provided, the director will be renamed to this.",
    },

    login: {
      type: "string",
      description:
        "If provided, the login for the director will be changed to this.",
    },

    email: {
      type: "string",
      isEmail: true,
      description:
        "If provided, the email for the director will be changed to this.",
    },

    admin: {
      type: "boolean",
      description:
        "If provided, the admin status of the director will be changed to this.",
    },

    assistant: {
      type: "boolean",
      description:
        "If provided, the assistant status of the director will be changed to this.",
    },

    position: {
      type: "string",
      description:
        "If provided, the director position will be changed to this.",
    },

    avatar: {
      type: "number",
      allowNull: true,
      description: "The ID of the Uploads file for the director's avatar",
    },

    profile: {
      type: "string",
      description: "Profile information for the director.",
    },

    canSendEmails: {
      type: "boolean",
      description:
        "Should this director have permission to send emails through the WWSU system?",
    },

    emailEmergencies: {
      type: "boolean",
      description: "Should this director receive emails of critical problems?",
    },

    emailCalendar: {
      type: "boolean",
      description:
        "Should this director receive emails regarding calendar events and shows?",
    },

    emailWeeklyAnalytics: {
      type: "boolean",
      description:
        "Should this director receive emails every week with analytics?",
    },

    emailFlags: {
      type: "boolean",
      description:
        "Should this director receive emails every when a track or broadcast gets flagged?",
    },

    emailBlogApprovals: {
      type: "boolean",
      description:
        "Should this director receive emails when a new blog entry is added (such as by a DJ) that needs approved?",
    },
  },

  exits: {
    badRequest: {
      statusCode: 400,
    },
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // Do not allow editing of the master director if we did not authorize with master director
    if (inputs.ID === 1 && this.req.payload.ID !== 1)
      throw {
        forbidden: "Master director may only be edited by the master director.",
      };

    // If editing master director, do not allow disabling of the admin setting, so forcefully keep it true.
    if (inputs.ID === 1) inputs.admin = true;

    // Determine if we need to lock out of editing admin
    let lockout = await sails.models.directors.count({ admin: true });

    // Block requests to change admin to false if there are 1 or less admin directors.
    // Should NEVER happen because of the master director!
    if (lockout <= 1 && typeof inputs.admin !== "undefined" && !inputs.admin) {
      throw {
        conflict:
          "To prevent accidental lockout, this request was denied because there are 1 or less admin directors. Make another director an admin first before removing admin status from this director.",
      };
    }

    // Determine what needs updating
    let director = await sails.models.directors.findOne({ ID: inputs.ID });
    if (!director)
      throw { notFound: "The director ID provided does not exist." };

    var criteria = {};

    if (typeof inputs.name !== "undefined" && inputs.name !== null) {
      criteria.name = inputs.name;

      // If a name is provided, ensure this name is not already taken.
      if (director && director.name !== inputs.name) {
        let directors2 = await sails.models.directors.find({
          name: inputs.name,
        });
        if (directors2.length > 0) {
          throw {
            badRequest: "A director by the provided name already exists.",
          };
        }
      }
    }

    // Hash the login password if provided
    if (
      typeof inputs.login !== "undefined" &&
      inputs.login !== null &&
      inputs.login !== ""
    ) {
      criteria.login = bcrypt.hashSync(inputs.login, 10);
    }

    if (typeof inputs.email !== "undefined") {
      criteria.email = inputs.email;
    }

    if (typeof inputs.admin !== "undefined" && inputs.admin !== null) {
      criteria.admin = inputs.admin;
    }

    if (typeof inputs.assistant !== "undefined" && inputs.assistant !== null) {
      criteria.assistant = inputs.assistant;
    }

    if (typeof inputs.position !== "undefined" && inputs.position !== null) {
      criteria.position = inputs.position;
    }

    if (typeof inputs.avatar !== "undefined" && inputs.avatar !== null) {
      criteria.avatar = inputs.avatar;
    }

    if (inputs.profile) {
        criteria.profile = await sails.helpers.sanitize(inputs.profile || "");
    }

    if (
      typeof inputs.canSendEmails !== "undefined" &&
      inputs.canSendEmails !== null
    ) {
      criteria.canSendEmails = inputs.canSendEmails;
    }

    if (
      typeof inputs.emailEmergencies !== "undefined" &&
      inputs.emailEmergencies !== null
    ) {
      criteria.emailEmergencies = inputs.emailEmergencies;
    }

    if (
      typeof inputs.emailCalendar !== "undefined" &&
      inputs.emailCalendar !== null
    ) {
      criteria.emailCalendar = inputs.emailCalendar;
    }

    if (
      typeof inputs.emailWeeklyAnalytics !== "undefined" &&
      inputs.emailWeeklyAnalytics !== null
    ) {
      criteria.emailWeeklyAnalytics = inputs.emailWeeklyAnalytics;
    }

    if (
      typeof inputs.emailFlags !== "undefined" &&
      inputs.emailFlags !== null
    ) {
      criteria.emailFlags = inputs.emailFlags;
    }

    if (
      typeof inputs.emailBlogApprovals !== "undefined" &&
      inputs.emailBlogApprovals !== null
    ) {
      criteria.emailBlogApprovals = inputs.emailBlogApprovals;
    }

    // We must clone the InitialValues object due to how Sails.js manipulates any objects passed as InitialValues.
    let criteriaB = _.cloneDeep(criteria);

    // Edit it
    let record = await sails.models.directors.updateOne(
      { ID: inputs.ID },
      criteriaB
    );

    // Also edit directors with the same original name to the new name, if applicable
    if (director && inputs.name) {
      await sails.models.djs
        .update({ realName: director.name }, { realName: inputs.name })
        .fetch();
    }

    delete record.login; // Do not return login hash.
    return record;
  },
};

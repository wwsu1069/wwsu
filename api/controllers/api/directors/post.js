const bcrypt = require("bcrypt");
module.exports = {
  friendlyName: "POST api/directors",

  description: "Add a new director into the system.",

  inputs: {
    name: {
      type: "string",
      required: true,
      description: "The director to add.",
    },

    login: {
      type: "string",
      required: true,
      description: "The login used for the clock-in and clock-out computer.",
    },

    email: {
      type: "string",
      isEmail: true,
      description: "The email address of the director",
    },

    admin: {
      type: "boolean",
      defaultsTo: false,
      description: "Is this director an administrator? Defaults to false.",
    },

    assistant: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Is this director an assistant director opposed to a main director? Defaults to false.",
    },

    position: {
      type: "string",
      required: true,
      description:
        "The description of the position of this director (such as general manager).",
    },

    avatar: {
      type: "number",
      allowNull: true,
      description: "The ID of the Uploads file for the director's avatar",
    },

    profile: {
      type: "string",
      description: "Profile information for the director.",
    },

    canSendEmails: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Should this director have permission to send emails through the WWSU system?",
    },

    emailEmergencies: {
      type: "boolean",
      defaultsTo: false,
      description: "Should this director receive emails of critical problems?",
    },

    emailCalendar: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Should this director receive emails regarding calendar events and shows?",
    },

    emailWeeklyAnalytics: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Should this director receive emails every week with analytics?",
    },

    emailFlags: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Should this director receive emails when a track or broadcast is flagged?",
    },

    emailBlogApprovals: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Should this director receive emails when a new blog entry is added (such as by a DJ) that needs approved?",
    },
  },

  exits: {
    badRequest: {
      statusCode: 400,
    },
  },

  fn: async function (inputs) {
    // Validation; director must first exist as org member
    let dj = await sails.models.djs.find({
      realName: inputs.name,
      active: true,
    });
    if (dj.length === 0) {
      throw {
        badRequest:
          "Director name provided does not match an active org member's realName. Add the director as an org member first.",
      };
    }

    // Add the director and bcrypt the login. Use findOrCreate because we do not want to create a director that already exists.
    let record = await sails.models.directors
      .findOrCreate(
        { name: inputs.name },
        {
          name: inputs.name,
          login: bcrypt.hashSync(inputs.login, 10),
          email: inputs.email,
          admin: inputs.admin,
          assistant: inputs.assistant,
          position: inputs.position,
          present: 0,
          avatar: inputs.avatar,
          profile: await sails.helpers.sanitize(inputs.profile),
          canSendEmails: inputs.canSendEmails,
          emailEmergencies: inputs.emailEmergencies,
          emailCalendar: inputs.emailCalendar,
          emailWeeklyAnalytics: inputs.emailWeeklyAnalytics,
          emailFlags: inputs.emailFlags,
          emailBlogApprovals: inputs.emailBlogApprovals,
          since: moment().format("YYYY-MM-DD HH:mm:ss"),
        }
      )
      .fetch();

      delete record.login; // Do not return login hash.
      return record;
  },
};

module.exports = {
  friendlyName: "POST api/call/bad-call",

  description:
    "Transmit socket event indicating the currently connected call is of bad quality.",

  inputs: {
    bitRate: {
      type: "number",
      required: false,
      description: `If provided, request a new bitrate to use in kbps.`,
    },
  },

  exits: {},

  fn: async function (inputs) {
    // Transmit bad-call event through sockets; DJ Controls should manage from there such as displaying a warning to the host or re-connecting with the new bitRate.
    sails.sockets.broadcast("bad-call", "bad-call", inputs.bitRate);
  },
};

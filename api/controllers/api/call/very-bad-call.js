module.exports = {
  friendlyName: "POST api/call/very-bad-call",

  description:
    "If remote audio continues to be poor quality, call this to trigger very-bad-call socket event.",

  inputs: {},

  exits: {},

  fn: async function () {
    // Transmit very-bad-call event to DJ Controls
    // DJ Controls should ideally either try disconnecting and re-connecting the call, or disconnecting and sending to break with a warning to the host.
    sails.sockets.broadcast("very-bad-call", "very-bad-call", true);
  },
};

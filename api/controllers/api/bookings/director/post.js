module.exports = {
  friendlyName: "POST api / bookings / director",

  description: "Add director booking",

  inputs: {
    type: {
      type: "string",
      isIn: ["onair-booking", "prod-booking"],
      required: true,
      description: "The type of booking",
    },
    reason: {
      type: "string",
      required: true,
      description: "Reason for booking the studio",
    },
    start: {
      type: "ref",
      required: true,
      custom: function (value) {
        return moment(value).isValid();
      },
    },
    duration: {
      type: "number",
      min: 1,
      max: 60 * 24, // Directors are allowed up to system max of 24 hours.
      required: true,
    },
  },

  exits: {
    error: {
      statusCode: 500,
    },
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs, exits) {
    try {
      // Bail if we are still processing a booking for this director
      if (
        sails.models.status.tasks.calendar.has(
          `${inputs.type}-director-${this.req.payload.ID}`
        )
      )
        return exits.conflict(
          "Another booking operation is still processing for this director. Please try again in a minute."
        );

      sails.models.status.tasks.calendar.set(
        `${inputs.type}-director-${this.req.payload.ID}`,
        true
      );

      // Find the booking event
      let record = await sails.models.calendar
        .find({ type: inputs.type })
        .limit(1);
      if (!record || !record[0]) {
        sails.models.status.tasks.calendar.delete(
          `${inputs.type}-director-${this.req.payload.ID}`
        );
        return exits.error(
          `Internal error: Expected a calendar event with the type ${inputs.type} to exist, but it did not.`
        );
      }

      record = record[0];

      // Construct the booking event
      let event = {
        calendarID: record.ID,
        type: inputs.type,
        director: this.req.payload.ID,
        description: inputs.reason,
        oneTime: [moment(inputs.start).format("YYYY-MM-DD")],
        startTime: moment(inputs.start).format("HH:mm"),
        duration: inputs.duration,
      };

      // Verify the event
      try {
        event = await sails.helpers.calendar.verify(event);
      } catch (e) {
        sails.models.status.tasks.calendar.delete(
          `${inputs.type}-director-${this.req.payload.ID}`
        );
        return exits.error(e.message);
      }

      // Check for event conflicts.
      let conflicts = sails.models.calendar.calendardb.checkConflicts(null, [
        { insert: event },
      ]);
      let additions =
        conflicts.additions && conflicts.additions.length
          ? conflicts.additions.filter(
              (addition) => addition.calendarID === record.ID
            )
          : [];
      let errors = conflicts.errors;

      // If there are any event conflicts, then the booking cannot be processed.
      if (errors && errors.length) {
        sails.models.status.tasks.calendar.delete(
          `${inputs.type}-director-${this.req.payload.ID}`
        );
        return exits.forbidden(errors.join());
      } else if (additions && additions.length) {
        sails.models.status.tasks.calendar.delete(
          `${inputs.type}-director-${this.req.payload.ID}`
        );
        return exits.forbidden(
          `You cannot book the studio for the specified date/time; the studio is already booked.`
        );
      }

      // At this point, we can add the booking
      let _event = _.cloneDeep(event);
      let record2 = await sails.models.schedule.create(_event).fetch();

      sails.models.status.tasks.calendar.delete(
        `${inputs.type}-director-${this.req.payload.ID}`
      );
      return exits.success(record2);
    } catch (e) {
      sails.models.status.tasks.calendar.delete(
        `${inputs.type}-director-${this.req.payload.ID}`
      );
      throw e;
    }
  },
};

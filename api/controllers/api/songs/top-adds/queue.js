module.exports = {
  friendlyName: "POST api/songs/top-adds/queue",

  description: "Queue a Top Add into RadioDJ, and play it if necessary.",

  inputs: {},

  exits: {
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCodee: 409,
    },
  },

  fn: async function (inputs) {
    if (!sails.config.custom.radiodjs.length)
    throw {
      forbidden:
        "No RadioDJs are configured in the system.",
    };

    // Block if running an alert
    if (sails.models.meta.memory.altRadioDJ !== null) {
      throw {
        conflict:
          "An alert is currently being broadcast. Please try again in a minute.",
      };
    }

    // Prevent adding tracks if host is belongsTo and the specified belongsTo is not on the air
    if (!(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload)))
      throw {
        forbidden:
          "This host is currently not allowed to queue a top add; this host did not start the current broadcast and does not have admin permission.",
      };

    // Log it
    await sails.models.logs
      .create({
        attendanceID: sails.models.meta.memory.attendanceID,
        logtype: "topadd",
        loglevel: "info",
        logsubtype: sails.models.meta.memory.show,
        logIcon: `fas fa-headphones`,
        title: `Host requested to play a random top add.`,
        event: "",
      })
      .fetch()
      .tolerate((err) => {
        // Do not throw for an error, but log it.
        sails.log.error(err);
      });

    // Queue it
    await sails.helpers.songs.queue(
      sails.config.custom.subcats.adds,
      "Top",
      1,
      "lenientRules"
    );

    // Play it
    await sails.helpers.rest.cmd("EnableAssisted", 0);
    await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);

    return;
  },
};

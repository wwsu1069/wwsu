module.exports = {
  friendlyName: "GET /api/songs/long",

  description:
    "Return an array of songs considered too long, eg. music tracks whose duration exceeds breakCheck. Only returns certain song details.",

  inputs: {},

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    // Disable if not using RadioDJ
    if (!sails.config.custom.radiodjs.length) {
      throw { forbidden: "RadioDJ is not configured / being used." };
    }

    let cats = {};

    // grab RadioDJ categories and put them in memory.
    let cats2 = await sails.models.category.find();
    cats2.map((cat) => {
      cats[cat.ID] = cat.name;
    });

    // Get the long tracks
    let found = await sails.models.songs.find({
      enabled: 1,
      id_subcat: sails.config.custom.subcats.music,
      duration: { ">=": 60 * sails.config.custom.basic.breakCheck },
    });

    // Add additional data.
    let maps = found.map(async (song) => {
      // Get those subcategories
      let subcats2 = await sails.models.subcategory.findOne({
        ID: song.id_subcat,
      });

      let category =
        `${cats[subcats2.parentid]} >> ${subcats2.name}` || "Unknown";

      return {
        ID: song.ID,
        artist: song.artist,
        title: song.title,
        album: song.album,
        category: category
      };
    });
    maps = await Promise.all(maps);

    return maps;
  },
};

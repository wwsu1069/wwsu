module.exports = {
  friendlyName: "PUT /api/songs/long/:ID?",

  description: "Disable long tracks.",

  inputs: {
    ID: {
      type: "number",
      description:
        "If provided, will only disable the provided song ID (if it was detected as long, else a 404 will be thrown).",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Disable if not using RadioDJ
    if (!sails.config.custom.radiodjs.length) {
      throw { forbidden: "RadioDJ is not configured / being used." };
    }

    // Single song
    if (inputs.ID) {
      let found = await sails.models.songs.updateOne(
        {
          ID: inputs.ID,
          enabled: 1,
          id_subcat: sails.config.custom.subcats.music,
          duration: { ">=": 60 * sails.config.custom.basic.breakCheck },
        },
        { enabled: 0 }
      );
      if (!found)
        throw {
          notFound:
            "The provided song ID was either not found or not detected as too long.",
        };

      return found;
    }

    // All songs
    let found = await sails.models.songs.update(
      {
        enabled: 1,
        id_subcat: sails.config.custom.subcats.music,
        duration: { ">=": 60 * sails.config.custom.basic.breakCheck },
      },
      { enabled: 0 }
    );

    return found;
  },
};

module.exports = {
  friendlyName: "GET api/songs/genres",

  description: 'Get array of objects of genres. {ID: "genre"}.',

  inputs: {},

  exits: {
    success: {
      statusCode: 200,
    },
    notFound: {
      statusCode: 404,
    },
    internalError: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    // Bail if not using RadioDJ
    if (sails.config.custom.radiodjs.length <= 0) {
      throw {
        internalError: "The system is not configured to use RadioDJ.",
      };
    }

    let returnData = [];

    // Retrieve a list of genres.
    let genres = await sails.models.genre.find({}).sort("name ASC");

    // Push the genres out
    genres.map((genre) => {
      let temp = {};
      temp.ID = genre.ID;
      temp.name = genre.name;
      returnData.push(temp);
    });

    return returnData;
  },
};

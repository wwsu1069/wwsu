module.exports = {
  friendlyName: "GET api/songs/sorter",

  description:
    "Returns all non-problematic songs, as well as categories, subcategories, genres, etc, for automatic sorting.",

  inputs: {},

  exits: {
    success: {
      statusCode: 200,
    },
    notFound: {
      statusCode: 404,
    },
    internalError: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    // Bail if not using RadioDJ
    if (sails.config.custom.radiodjs.length <= 0) {
      throw {
        internalError: "The system is not configured to use RadioDJ.",
      };
    }

    var returnData = {
      categories: [],
      subcategories: [],
      genres: [],
      songs: [],
    };

    // First, get all songs with enabled 0 or more
    returnData.songs = await sails.models.songs.find({ enabled: { ">=": 0 } });

    // Next, populate genres
    var temp = await sails.models.genre.find();
    if (temp.length > 0) {
      temp.map((genre) => {
        returnData.genres.push({ ID: genre.ID, name: genre.name });
      });
    }

    // Next, populate categories
    temp = await sails.models.category.find();
    if (temp.length > 0) {
      temp.map((category) => {
        returnData.categories.push({ ID: category.ID, name: category.name });
      });
    }

    // Populate subcategories
    temp = await sails.models.subcategory.find();
    if (temp.length > 0) {
      temp.map((subcategory) => {
        var category = `Unknown Category`;
        returnData.categories
          .filter((cat) => cat.ID === subcategory.parentid)
          .map((cat) => {
            category = cat.name;
          });
        returnData.subcategories.push({
          ID: subcategory.ID,
          category: category,
          name: subcategory.name,
        });
      });
    }

    return returnData;
  },
};

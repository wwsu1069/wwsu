module.exports = {
  friendlyName: "POST api/songs/psas/queue",

  description:
    "Queue a PSA into RadioDJ... often used during sports broadcasts.",

  inputs: {
    duration: {
      type: "number",
      defaultsTo: 30,
      description:
        "The number of seconds the PSA should be, +/- 5 seconds. Defaults to 30.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    if (!sails.config.custom.radiodjs.length)
      throw {
        forbidden:
          "No RadioDJs are configured in the system.",
      };

    // Prevent adding tracks if host is belongsTo and the specified belongsTo is not on the air
    if (!(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload)))
      throw {
        forbidden:
          "This host is currently not allowed to queue a PSA; this host did not start the current broadcast and does not have admin permission.",
      };

    // Queue applicable PSA
    await sails.helpers.songs.queue(
      sails.config.custom.subcats.PSAs,
      "Top",
      1,
      "lenientRules",
      inputs.duration
    );

    return;
  },
};

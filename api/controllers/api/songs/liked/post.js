module.exports = {
  friendlyName: "POST api/songs/liked/:ID?",

  description:
    "Registers a like for the song ID. This in-turn bumps its priority in RadioDJ.",

  inputs: {
    ID: {
      type: "number",
      description: "The ID of the track being liked.",
    },
    track: {
      type: "string",
      description: "The name of the track (if not providing an ID)",
    },
  },

  exits: {
    badRequest: {
      statusCode: 400,
    },
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    sails.log.debug("Controller songs/like called.");

    // Get the hosts's IP address first
    let fromIP = await sails.helpers.getIp(this.req);

    // First, get the track record from the database (and reject if it does not exist)
    if (inputs.ID) {
      if (!sails.config.custom.radiodjs.length) {
        throw {
          forbidden: "Cannot like a specific track ID when not using RadioDJ.",
        };
      }

      let track = await sails.models.songs.findOne({ ID: inputs.ID });
      if (!track) {
        throw { notFound: "The provided track ID was not found" };
      }

      let query = { IP: fromIP, trackID: inputs.ID };

      // If config specifies users can like tracks multiple times, add a date condition.
      if (sails.config.custom.basic.songsLikedCooldown > 0) {
        query.createdAt = {
          ">=": moment()
            .subtract(sails.config.custom.basic.songsLikedCooldown, "days")
            .format("YYYY-MM-DD HH:mm:ss"),
        };
      }

      // First, check if the client already liked the track recently. Error if it cannot be liked again at this time.
      let records = await sails.models.songsliked.count(query);
      if (records && records > 0) {
        throw {
          forbidden:
            "This IP cannot like the provided track again at this time.",
        };
      }

      // Next, check to see this track ID actually played recently. We will allow a 30-minute grace. Any tracks not played within the last 30 minutes cannot be liked.
      let canLike = false;
      records = await sails.models.history.count({
        trackID: inputs.ID,
        createdAt: {
          ">=": moment().subtract(30, "minutes").format("YYYY-MM-DD HH:mm:ss"),
        },
      });
      if (records > 0) canLike = true;

      if (!canLike) {
        throw {
          forbidden:
            "The provided track has not aired in the last 30 minutes and therefore cannot be liked.",
        };
      }

      // At this point, the track can be liked, so like it
      let record = await sails.models.songsliked
        .create({
          IP: fromIP,
          trackID: inputs.ID,
        })
        .fetch();

      // Update track weight if applicable / configured to change weight on a track like
      if (sails.config.custom.basic.songsLikedPriorityChange !== 0) {
        await sails.models.songs.update(
          { ID: inputs.ID },
          {
            weight:
              track.weight + sails.config.custom.basic.songsLikedPriorityChange,
          }
        );
      }

      // Log the request
      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "website-likedtrack",
          loglevel: "info",
          logsubtype: ``,
          logIcon: `fas fa-thumbs-up`,
          title: `Someone liked a track on the website.`,
          event: `Track: ${track.artist} - ${track.title} (ID ${inputs.ID})`,
        })
        .fetch()
        .tolerate((err) => {
          sails.log.error(err);
        });

      return record;
    } else {
      if (!inputs.track)
        throw { badRequest: "Must provide track when not providing ID." };

      // Check if the provided track was played in memory recently
      let record = sails.models.meta.memory.history.find(
        (track) =>
          !track.ID &&
          track.track === inputs.track &&
          moment(track.time).add(30, "minutes").isAfter(moment())
      );
      if (!record)
        throw {
          forbidden:
            "The provided track was either not found, did not play in the last 30 minutes, or was not one of the last 5 tracks that played.",
        };

      // Check if this IP already liked the track
      let records = await sails.models.songsliked.count({
        IP: fromIP,
        track: inputs.track,
        createdAt:
          sails.config.custom.basic.songsLikedCooldown > 0
            ? {
                ">=": moment()
                  .subtract(
                    sails.config.custom.basic.songsLikedCooldown,
                    "days"
                  )
                  .format("YYYY-MM-DD HH:mm:ss"),
              }
            : { ">=": "2002-01-01 00:00:00" },
      });
      if (records && records > 0) {
        throw {
          forbidden:
            "This IP cannot like the provided track again at this time.",
        };
      }

      record = await sails.models.songsliked
        .create({
          IP: fromIP,
          track: inputs.track,
        })
        .fetch();

      await sails.models.logs
        .create({
          attendanceID: sails.models.meta.memory.attendanceID,
          logtype: "website-likedtrack",
          loglevel: "info",
          logsubtype: ``,
          logIcon: `fas fa-thumbs-up`,
          title: `Someone liked a track on the website.`,
          event: `Track: ${inputs.track}`,
        })
        .fetch()
        .tolerate((err) => {
          sails.log.error(err);
        });

      return record;
    }
  },
};

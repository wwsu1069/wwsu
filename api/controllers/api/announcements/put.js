module.exports = {
  friendlyName: "PUT api / announcements",

  description: "Edit an existing announcement.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the record to edit.",
    },

    type: {
      type: "string",
      description:
        "The type of announcement; determines which subsystems receive the announcement.",
    },

    level: {
      type: "string",
      isIn: ["danger", "warning", "info", "success", "secondary"],
      description:
        "Announcement warning level. Must be danger, warning, info, or trivial.",
    },

    title: {
      type: "string",
      required: true,
      description: "The announcement title.",
    },

    announcement: {
      type: "string",
      description: "The announcement text.",
    },

    displayTime: {
      type: "number",
      min: 5,
      max: 60,
    },

    starts: {
      type: "string",
      custom: function (value) {
        return value === null || moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when the announcement starts. Recommended ISO string.`,
    },

    expires: {
      type: "string",
      custom: function (value) {
        return value === null || moment(value).isValid();
      },
      allowNull: true,
      description: `moment() parsable string of when the announcement expires. Defaults to the year 3000. Recommended ISO string.`,
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // We must clone the InitialValues object due to how Sails.js manipulates any objects passed as InitialValues.
    let criteriaB = _.cloneDeep(inputs);

    if (inputs.type !== null && typeof inputs.type !== "undefined") {
      // If the type changed, issue a remove websocket event to the previous type.
      let record = await sails.models.announcements.findOne({ ID: inputs.ID });
      if (record && record.type !== inputs.type) {
        sails.sockets.broadcast(
          `announcements-${record.type}`,
          "announcements",
          { remove: inputs.ID }
        );
      }
    }

    // Update the announcement
    let record = await sails.models.announcements.updateOne(
      { ID: inputs.ID },
      criteriaB
    );
    if (!record) throw "notFound";

    return record;
  },
};

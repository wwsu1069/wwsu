module.exports = {
  friendlyName: "DELETE api / announcements",

  description: "Remove an announcement.",

  inputs: {
    ID: {
      type: "number",
      required: true,
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Destroy the announcement
    let record = await sails.models.announcements.destroyOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The announcement ID was not found." };
  },
};

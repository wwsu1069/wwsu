module.exports = {
  friendlyName: "PUT api/silence/inactive",

  description:
    "DJ Controls should call this endpoint after a silence issue has been resolved, and silence is no longer detected.",

  inputs: {},

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    // Bail if in maintenance mode
    if (sails.config.custom.basic.maintenance)
      throw { forbidden: "Request rejected; system is in maintenance mode." };

    // Status for silence set to good
    await sails.helpers.status.modify.with({
      name: `silence`,
      status: 5,
      label: `Silence`,
      summary: `Audio levels are acceptable.`,
      data: `Audio levels are reported acceptable by DJ Controls.`,
    });

    return;
  },
};

module.exports = {
  friendlyName: "PUT api/djs/host/:ID/deactivate",

  description: "Mark a DJ as inactive in the system.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The DJ ID to mark inactive.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Do not allow marking master member inactive
    if (inputs.ID === 1)
      throw { forbidden: "Not allowed to deactivate the default DJ (ID 1)." };

    // Mark DJ as inactive.
    let record = await sails.models.djs.updateOne(
      { ID: inputs.ID },
      { active: false }
    );
    if (!record) throw { notFound: "The provided DJ ID was not found." };

    return record;
  },
};

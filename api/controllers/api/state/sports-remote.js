module.exports = {
  friendlyName: "POST /api/state/remote/sports",

  description: "Request to begin a remote sports broadcast.",

  inputs: {
    topic: {
      type: "string",
      defaultsTo: "",
      description:
        "A string containing a short blurb about this sports broadcast.",
    },

    sport: {
      type: "string",
      required: true,
      custom: (value) => {
        return sails.config.custom.sports.find((sport) => sport.name === value);
      },
      description: "Name of the sport that is being broadcast.",
    },

    webchat: {
      type: "boolean",
      defaultsTo: true,
      description:
        "Should the web chat be enabled during this broadcast? Defaults to true.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // Do not allow starting a remote broadcast if delay system status is 1 (critical).
    let delayStatus = await sails.models.status.find({
      name: "delay-system",
    });
    if (delayStatus && delayStatus[0] && delayStatus[0].status === 1)
      throw {
        forbidden:
          "Remote broadcasts are not allowed when the delay system is in critical status.",
      };

    // Disallow starting a remote sports broadcast if there is no permission
    if (
      !(await sails.helpers.canStartRemoteBroadcast(
        this.req.payload.ID,
        "sports"
      ))
    ) {
      throw {
        forbidden:
          "Rejected; the org member this host belongs to does not have permission to start remote sports broadcasts.",
      };
    }

    // Disallow starting a sports remote broadcast if the host has belongsTo and there is no scheduled sports broadcast at this time
    if (this.req.payload.belongsTo !== null) {
      let record = sails.models.calendar.calendardb.whatShouldBePlaying(
        null,
        false
      );
      record = record.filter(
        (event) =>
          event.type === "sports" && event.name.startsWith(inputs.sport)
      );
      if (record.length < 1) {
        throw {
          forbidden:
            "Rejected; there is no sports broadcast scheduled at this time.",
        };
      }
    }

    // Do not continue if not in automation mode; client should request automation before requesting sports
    if (
      !sails.models.meta.memory.state.startsWith("automation_") &&
      !sails.models.meta.memory.state.startsWith("prerecord_")
    ) {
      throw {
        forbidden:
          "Not allowed to start a remote sports broadcast outside of automation or prerecord. Please go to automation first.",
      };
    }

    // Block this request if we are already switching states
    if (sails.models.meta.memory.changingState !== null) {
      throw {
        conflict:
          "Rejected; system is currently changing states. Please try again in several seconds.",
      };
    }

    // Block if running an alert
    if (sails.models.meta.memory.altRadioDJ !== null) {
      throw {
        conflict:
          "Rejected; system is airing an alert. Please try again in a minute.",
      };
    }

    try {
      // Lock so that any other state changing requests are blocked until we are done
      await sails.helpers.meta.change.with({
        changingState: `Switching to sports-remote`,
      });

      // Filter profanity
      if (inputs.topic !== "") {
        inputs.topic = await sails.helpers.filterProfane(inputs.topic);
        inputs.topic = await sails.helpers.sanitize(inputs.topic);
      }

      // Skip RadioDJ stuff and go immediately to live if not using RadioDJ.
      if (!sails.config.custom.radiodjs.length) {
        await sails.helpers.meta.change.with({
          hostDJ: null,
          cohostDJ1: null,
          cohostDJ2: null,
          cohostDJ3: null,
          state: "sportsremote_on",
          host: this.req.payload.ID,
          show: inputs.sport,
          topic: inputs.topic,
          trackStamp: null,
          webchat: inputs.webchat,
        });
        await sails.helpers.meta.newShow();
        return;
      }

      // Set meta to prevent accidental messages in DJ Controls
      await sails.helpers.meta.change.with({
        state: "automation_sportsremote",
        host: this.req.payload.ID,
        show: inputs.sport,
        topic: inputs.topic,
        trackStamp: null,
      });

      // await sails.helpers.error.count('goLive');

      // Operation: Remove all music tracks, queue a station ID, queue an opener if one exists for this sport, and start the next track if current track is music.
      await sails.helpers.rest.cmd("EnableAutoDJ", 0);
      await sails.helpers.songs.remove(
        true,
        sails.config.custom.subcats.noClearShow,
        false,
        true
      );
      await sails.helpers.rest.cmd("EnableAssisted", 1);
      await sails.helpers.break.checkClockwheel(false);
      await sails.helpers.break.executeArray(
        sails.config.custom.breaks.filter(
          (record) => record.type === "sports" && record.subtype === "start"
        ),
        "Sports Start"
      );

      // Queue a Sports opener if there is one
      if (typeof sails.config.custom.sportscats[inputs.sport] !== "undefined") {
        await sails.helpers.songs.queue(
          [sails.config.custom.sportscats[inputs.sport]["Sports Openers"]],
          "Bottom",
          1
        );
      }

      await sails.helpers.rest.cmd("EnableAssisted", 0);

      var queueLength = await sails.helpers.songs.calculateQueueLength();

      // If the radioDJ queue is unacceptably long, try to reduce it.
      if (queueLength >= sails.config.custom.basic.maxQueueSports) {
        await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Try to Disable autoDJ again in case it was mistakenly still active
        await sails.helpers.songs.remove(
          true,
          sails.config.custom.subcats.noClearShow,
          false,
          true
        );
        if (
          sails.config.custom.subcats.noClearShow &&
          sails.config.custom.subcats.noClearShow.indexOf(
            sails.models.meta.memory.trackIDSubcat
          ) === -1
        ) {
          await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
        } // Skip currently playing track if it is not a noClearShow track
      }

      // Add a station ID
      await sails.helpers.songs.queue(
        sails.config.custom.subcats.IDs,
        "Top",
        1
      );

      queueLength = await sails.helpers.songs.calculateQueueLength();

      // Change meta
      await sails.helpers.meta.change.with({
        hostDJ: null,
        cohostDJ1: null,
        cohostDJ2: null,
        cohostDJ3: null,
        queueFinish: moment().add(queueLength, "seconds").toISOString(true),
        show: inputs.sport,
        topic: inputs.topic,
        trackStamp: null,
        webchat: inputs.webchat,
      });

      await sails.helpers.meta.change.with({ changingState: null });
    } catch (e) {
      await sails.helpers.meta.change.with({
        state: "automation_on",
        host: null,
        show: "",
        topic: "",
        trackStamp: null,
        changingState: null,
      });
      throw e;
    }
  },
};

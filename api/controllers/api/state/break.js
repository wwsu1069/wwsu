module.exports = {
  friendlyName: "POST api/state/break",

  description: "Go to a break.",

  inputs: {
    halftime: {
      type: "boolean",
      defaultsTo: false,
      description:
        "Halftime is true if this is an extended or halftime sports break, rather than a standard one.",
    },

    problem: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, will play a configured technicalIssue liner as the break begins, such as if the break was triggered because of an issue. Defaults to false.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // Prevent state changing if host is belongsTo and the specified belongsTo is not on the air.
    // EXCEPTION: We are doing a remote broadcast, the host has answerCalls or can start a broadcast, and inputs.problem = true
    if (
      !(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload))
    ) {
      if (
        !inputs.problem ||
        (!sails.models.meta.memory.state.startsWith("remote_") &&
          !sails.models.meta.memory.state.startsWith("sportsremote_")) ||
        (!(await sails.helpers.call.canStartRemoteBroadcast(
          this.req.payload.ID
        )) &&
          !this.req.payload.answerCalls)
      ) {
        throw {
          forbidden:
            "This host is currently not allowed to go to break; this host did not start the current broadcast and does not have admin permission.",
        };
      }
    }

    // API NOTE: Although these checks are already made in the goBreak helper, we should do them again (first) here to return proper HTTP codes.

    // Block if we are in the process of changing states
    if (sails.models.meta.memory.changingState !== null) {
      throw {
        conflict:
          "Request rejected; the system is in the process of changing states right now. Please try again in several seconds.",
      };
    }

    // Block if running an alert
    if (sails.models.meta.memory.altRadioDJ !== null) {
      throw {
        conflict: "Request rejected; an alert is currently being broadcast.",
      };
    }

    // Block the request if we are already in break and problem = true.
    if (sails.models.meta.memory.state.endsWith("_break") && inputs.problem)
      throw { forbidden: "Already in a break." };

    await sails.helpers.state.goBreak(inputs.halftime, inputs.problem);

    return;
  },
};

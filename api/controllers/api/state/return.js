module.exports = {
  friendlyName: "POST api/state/return",

  description: "Return from a break.",

  inputs: {},

  exits: {
    forbidden: {
      statusCode: 403,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // Prevent adding tracks if host is belongsTo and the specified belongsTo is not on the air
    if (!(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload)))
      throw {
        forbidden:
          "This host is currently not allowed to return from break; this host did not start the current broadcast and does not have admin permission.",
      };

    // Block this request if we are changing states right now
    if (sails.models.meta.memory.changingState !== null) {
      throw {
        conflict:
          "Rejected; system is currently changing states. Please try again in several seconds.",
      };
    }

    // Block if running an alert
    if (sails.models.meta.memory.altRadioDJ !== null) {
      throw {
        conflict:
          "Rejected; system is airing an alert. Please try again in a minute.",
      };
    }

    try {
      let queueLength;

      // Lock so that other state changing requests get blocked until we are done
      await sails.helpers.meta.change.with({
        changingState: `Returning from break`,
      });

      // log it
      (async () => {
        await sails.models.logs
          .create({
            attendanceID: sails.models.meta.memory.attendanceID,
            logtype: "return",
            loglevel: "info",
            logsubtype: sails.models.meta.memory.show,
            logIcon: `fas fa-check`,
            title: `Host requested to resume the broadcast.`,
            event: "",
          })
          .fetch()
          .tolerate((err) => {
            // Don't throw errors, but log them
            sails.log.error(err);
          });
      })();

      // If not using RadioDJ, skip all the remaining stuff and go immediately back on.
      if (!sails.config.custom.radiodjs.length) {
        // Assume a top of hour ID was aired during the break if we returned between :55 and :05
        // We have no way of checking for IDs airing when not using RadioDJ. So this is arbitrary.
        if (moment().minute() >= 55 || moment().minute() < 5) {
          await sails.helpers.meta.change.with({
            lastID: moment().format("YYYY-MM-DD HH:mm:ss"),
          });
        }
        switch (sails.models.meta.memory.state) {
          case "live_break":
            await sails.helpers.meta.change.with({
              state: "live_on",
            });
            break;
          case "sports_break":
          case "sports_halftime":
            await sails.helpers.meta.change.with({
              state: "sports_on",
            });
            break;
          case "remote_break":
          case "remote_break_disconnected":
            await sails.helpers.meta.change.with({
              state: "remote_on",
            });
            break;
          case "sportsremote_break":
          case "sportsremote_break_disconnected":
          case "sportsremote_halftime":
            await sails.helpers.meta.change.with({
              state: "sportsremote_on",
            });
            break;
        }

        return;
      }

      await sails.helpers.rest.cmd("EnableAssisted", 1);

      // Remove clearBreak tracks to speed up the return
      await sails.helpers.songs.remove(
        false,
        sails.config.custom.subcats.clearBreak,
        false,
        true
      );

      // Perform the break

      // If returning from a halftime break...
      if (sails.models.meta.memory.state.includes("halftime")) {
        // Queue a legal ID
        await sails.helpers.songs.queue(
          sails.config.custom.subcats.IDs,
          "Bottom",
          1
        );
        sails.models.status.errorCheck.prevID = moment();
        sails.models.status.errorCheck.prevBreak = moment();
        await sails.helpers.error.count("stationID");

        await sails.helpers.break.executeArray(
          sails.config.custom.breaks.filter(
            (record) => record.type === "sports" && record.subtype === "after"
          ),
          "Sports After"
        );

        // Queue a sports return
        if (
          typeof sails.config.custom.sportscats[
            sails.models.meta.memory.show
          ] !== "undefined"
        ) {
          await sails.helpers.songs.queue(
            [
              sails.config.custom.sportscats[sails.models.meta.memory.show][
                "Sports Returns"
              ],
            ],
            "Bottom",
            1
          );
        }

        queueLength = await sails.helpers.songs.calculateQueueLength();

        // If queue is unacceptably long, try to speed the process up.
        if (queueLength >= sails.config.custom.basic.maxQueueSportsReturn) {
          await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Try to Disable autoDJ again in case it was mistakenly still active
          await sails.helpers.songs.remove(
            false,
            sails.config.custom.subcats.clearBreak,
            false,
            true
          );
          if (
            sails.config.custom.subcats.clearBreak &&
            sails.config.custom.subcats.clearBreak.indexOf(
              sails.models.meta.memory.trackIDSubcat
            ) !== -1
          ) {
            await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
          } // Skip currently playing track if it is a clearBreak track

          queueLength = await sails.helpers.songs.calculateQueueLength();
        }

        // await sails.helpers.error.count('sportsReturnQueue');

        // Change state
        if (
          sails.models.meta.memory.state === "sportsremote_halftime" ||
          sails.models.meta.memory.state ===
            "sportsremote_halftime_disconnected"
        ) {
          await sails.helpers.meta.change.with({
            queueFinish: moment().add(queueLength, "seconds").toISOString(true),
            state: "sportsremote_returning",
          });
        } else {
          await sails.helpers.meta.change.with({
            queueFinish: moment().add(queueLength, "seconds").toISOString(true),
            state: "sports_returning",
          });
        }
      } else {
        // Do stuff depending on the state
        switch (sails.models.meta.memory.state) {
          case "live_break":
            // Queue after break
            await sails.helpers.break.executeArray(
              sails.config.custom.breaks.filter(
                (record) => record.type === "live" && record.subtype === "after"
              ),
              "Live After"
            );

            // Queue a show return if there is one
            if (
              typeof sails.config.custom.showcats[
                sails.models.meta.memory.show.split(" - ")[1]
              ] !== "undefined"
            ) {
              await sails.helpers.songs.queue(
                [
                  sails.config.custom.showcats[
                    sails.models.meta.memory.show.split(" - ")[1]
                  ]["Show Returns"],
                ],
                "Bottom",
                1
              );
            } else if (
              typeof sails.config.custom.showcats["Default"] !==
              "undefined"
            ) {
              await sails.helpers.songs.queue(
                [sails.config.custom.showcats["Default"]["Show Returns"]],
                "Bottom",
                1
              );
            }

            await sails.helpers.meta.change.with({
              queueFinish: moment()
                .add(
                  await sails.helpers.songs.calculateQueueLength(),
                  "seconds"
                )
                .toISOString(true),
              state: "live_returning",
            });
            break;
          case "sports_break":
            // Queue after break
            await sails.helpers.break.executeArray(
              sails.config.custom.breaks.filter(
                (record) =>
                  record.type === "sports" && record.subtype === "after"
              ),
              "Sports After"
            );
            // Queue a sports liner
            if (
              typeof sails.config.custom.sportscats[
                sails.models.meta.memory.show
              ] !== "undefined"
            ) {
              await sails.helpers.songs.queue(
                [
                  sails.config.custom.sportscats[sails.models.meta.memory.show][
                    "Sports Returns"
                  ],
                ],
                "Bottom",
                1
              );
            }

            queueLength = await sails.helpers.songs.calculateQueueLength();

            if (queueLength >= sails.config.custom.basic.maxQueueSportsReturn) {
              await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Try to Disable autoDJ again in case it was mistakenly still active
              await sails.helpers.songs.remove(
                false,
                sails.config.custom.subcats.clearBreak,
                false,
                true
              );
              if (
                sails.config.custom.subcats.clearBreak &&
                sails.config.custom.subcats.clearBreak.indexOf(
                  sails.models.meta.memory.trackIDSubcat
                ) !== -1
              ) {
                await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
              } // Skip currently playing track if it is a clearBreak track

              queueLength = await sails.helpers.songs.calculateQueueLength();
            }

            // await sails.helpers.error.count('sportsReturnQueue');
            await sails.helpers.meta.change.with({
              queueFinish: moment()
                .add(queueLength, "seconds")
                .toISOString(true),
              state: "sports_returning",
            });
            break;
          case "remote_break":
          case "remote_break_disconnected":
            // Queue after break
            await sails.helpers.break.executeArray(
              sails.config.custom.breaks.filter(
                (record) =>
                  record.type === "remote" && record.subtype === "after"
              ),
              "Remote After"
            );
            // Queue a show return if there is one
            if (
              typeof sails.config.custom.showcats[
                sails.models.meta.memory.show.split(" - ")[1]
              ] !== "undefined"
            ) {
              await sails.helpers.songs.queue(
                [
                  sails.config.custom.showcats[
                    sails.models.meta.memory.show.split(" - ")[1]
                  ]["Show Returns"],
                ],
                "Bottom",
                1
              );
            } else if (
              typeof sails.config.custom.showcats["Default"] !==
              "undefined"
            ) {
              await sails.helpers.songs.queue(
                [sails.config.custom.showcats["Default"]["Show Returns"]],
                "Bottom",
                1
              );
            }
            await sails.helpers.meta.change.with({
              queueFinish: moment()
                .add(
                  await sails.helpers.songs.calculateQueueLength(),
                  "seconds"
                )
                .toISOString(true),
              state: "remote_returning",
            });
            break;
          case "sportsremote_break":
          case "sportsremote_break_disconnected":
            // Queue after break
            await sails.helpers.break.executeArray(
              sails.config.custom.breaks.filter(
                (record) =>
                  record.type === "sports" && record.subtype === "after"
              ),
              "Sports After"
            );
            // Queue a sports liner
            if (
              typeof sails.config.custom.sportscats[
                sails.models.meta.memory.show
              ] !== "undefined"
            ) {
              await sails.helpers.songs.queue(
                [
                  sails.config.custom.sportscats[sails.models.meta.memory.show][
                    "Sports Returns"
                  ],
                ],
                "Bottom",
                1
              );
            }

            queueLength = await sails.helpers.songs.calculateQueueLength();

            if (queueLength >= sails.config.custom.basic.maxQueueSportsReturn) {
              await sails.helpers.rest.cmd("EnableAutoDJ", 0); // Try to Disable autoDJ again in case it was mistakenly still active
              await sails.helpers.songs.remove(
                false,
                sails.config.custom.subcats.clearBreak,
                false,
                true
              );
              if (
                sails.config.custom.subcats.clearBreak &&
                sails.config.custom.subcats.clearBreak.indexOf(
                  sails.models.meta.memory.trackIDSubcat
                ) !== -1
              ) {
                await sails.helpers.rest.cmd("PlayPlaylistTrack", 0);
              } // Skip currently playing track if it is a clearBreak track

              queueLength = await sails.helpers.songs.calculateQueueLength();
            }

            // await sails.helpers.error.count('sportsReturnQueue');
            await sails.helpers.meta.change.with({
              queueFinish: moment()
                .add(queueLength, "seconds")
                .toISOString(true),
              state: "sportsremote_returning",
            });
            break;
        }
      }

      await sails.helpers.rest.cmd("EnableAssisted", 0);

      await sails.helpers.meta.change.with({ changingState: null });
    } catch (e) {
      await sails.helpers.meta.change.with({ changingState: null });
      throw e;
    }
  },
};

module.exports = {
  friendlyName: "POST api/state/radiodj/change",

  description: "Switch which radioDJ is currently active.",

  inputs: {},

  exits: {
    forbidden: {
      statusCode: 403
    }
  },

  fn: async function (inputs) {
    try {
      if (!sails.config.custom.radiodjs.length)
        throw {forbidden: "No RadioDJs are configured in the system"};

      // Lock state change
      await sails.helpers.meta.change.with({
        changingState: `Switching radioDJ instances`,
      });

      // Try to stop the current automation, then switch to another and execute post error tasks to get it going
      await sails.helpers.rest.cmd("EnableAssisted", 1, 0);
      await sails.helpers.rest.cmd("EnableAutoDJ", 1, 0);
      await sails.helpers.rest.cmd("StopPlayer", 0, 0);
      
      let queue = sails.models.meta.automation;
      await sails.helpers.rest.changeRadioDj();
      await sails.helpers.rest.cmd("ClearPlaylist", 1);
      await sails.helpers.error.post(queue);

      await sails.helpers.meta.change.with({ changingState: null });
      return;
    } catch (e) {
      await sails.helpers.meta.change.with({ changingState: null });
      throw e;
    }
  },
};

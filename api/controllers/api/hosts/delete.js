var sh = require("shorthash");

module.exports = {
  friendlyName: "DELETE api/hosts/:ID",

  description: "Remove a host from the database.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the director to edit.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
    conflict: {
      statusCode: 409,
    },
  },

  fn: async function (inputs) {
    // First, determine if we need to lock out of editing authorized and admin
    let lockout = await sails.models.hosts.count({
      authorized: true,
      admin: true,
    });

    let toDestroy = await sails.models.hosts.findOne({ ID: inputs.ID });
    if (!toDestroy) throw { notFound: "The host ID was not found." };

    // Block requests to remove this host if there are 1 or less authorized admin hosts and this host is an authorized admin.
    if (lockout <= 1 && toDestroy.authorized && toDestroy.admin) {
      throw {
        forbidden:
          "To prevent accidental lockout, this request was denied because there are 1 or less authorized admin hosts. Make another host an authorized admin first before removing this host.",
      };
    }

    // Destroy it
    let hostRecord = await sails.models.hosts.destroyOne({ ID: inputs.ID });
    await sails.models.recipients
      .update(
        {
          host: `computer-${sh.unique(
            hostRecord.host + sails.config.custom.basic.hostSecret
          )}`,
        },
        { answerCalls: false, belongsTo: 0, startRemotes: false }
      )
      .fetch();

    // Destroy the status records for this host as well
    await sails.models.status
      .destroy({
        name: `host-${sh.unique(
          hostRecord.host + sails.config.custom.basic.hostSecret
        )}`,
      })
      .fetch();

    // All done.
    return hostRecord;
  },
};

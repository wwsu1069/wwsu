module.exports = {
  friendlyName: "PUT api/hosts/:ID/log-out",

  description: "Log out of a locked down host",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the host from which this log comes.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Integrity check for existing host. Do not check if locked down because we might be checking out after a previously lockdown host is no longer lockdown.
    let host = await sails.models.hosts.findOne({
      ID: inputs.ID,
    });
    if (!host) throw { notFound: "The provided host ID was not found." };

    let records = await sails.models.lockdown
      .update(
        {
          host: inputs.ID,
          clockOut: null,
        },
        { clockOut: moment().format("YYYY-MM-DD HH:mm:ss") }
      )
      .fetch();

    if (!records || records.length === 0)
      throw { forbidden: "No one was logged in to the provided host." };

    // All done.
    return records;
  },
};

module.exports = {
  friendlyName: "DELETE api/djnotes",

  description: "Remove djnotes entry.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The ID of the record to remove.",
    },
  },

  exits: {
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    // Remove this record
    let record = await sails.models.djnotes.destroyOne({ ID: inputs.ID });
    if (!record) throw { notFound: "The DJ note ID was not found." };

    return record;
  },
};

module.exports = {
  friendlyName: "GET /api/categories",

  description:
    "Get a mapping of RadioDJ categories and subcategories for use in categories configuration.",

  inputs: {},

  exits: {
    success: {
      statusCode: 200,
    },
    notFound: {
      statusCode: 404,
    },
    internalError: {
      statusCode: 500,
    },
  },

  fn: async function (inputs) {
    // Bail if not using RadioDJ
    if (sails.config.custom.radiodjs.length <= 0) {
      throw {
        internalError:
          "The system is not configured to use RadioDJ.",
      };
    }

    let returnData = {};

    // Load main categories in memory
    let categories = await sails.models.category.find();

    // Load subcategories
    let subcategories = await sails.models.subcategory.find();

    // Map subcategories with categories.
    subcategories.map((subcategory) => {
      let category = categories.find((cat) => cat.ID === subcategory.parentid);
      if (category) {
        if (typeof returnData[category.name] === "undefined") {
          returnData[category.name] = [];
        }
        returnData[category.name].push(subcategory.name);
      }
    });

    return returnData;
  },
};

const sh = require("shorthash");

module.exports = {
  friendlyName: "POST api/requests",

  description: "Place a request.",

  inputs: {
    ID: {
      required: true,
      type: "number",
      description: "ID number of the song to request.",
    },

    name: {
      type: "string",
      defaultsTo: "anonymous",
      description: "Name provided of the person making the request.",
    },

    message: {
      type: "string",
      defaultsTo: "",
      description: "Message provided regarding the request.",
    },

    device: {
      type: "string",
      allowNull: true,
      description:
        "If requested from the mobile app, provide the device ID so they can receive a push notification when the request plays.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
    notFound: {
      statusCode: 404,
    },
  },

  fn: async function (inputs) {
    if (!sails.config.custom.radiodjs.length)
      throw {
        forbidden:
          "The request system is currently disabled. Please post your request in the chat instead if a show is on the air.",
      };

    // Get the client IP address
    let fromIP = await sails.helpers.getIp(this.req);

    // First, confirm the track can actually be requested.
    let requestable = await sails.helpers.requests.checkRequestable(
      inputs.ID,
      fromIP
    );

    // If not, reject with an error.
    if (!requestable.requestable) {
      throw { forbidden: requestable.message };
    }

    let host = await sails.helpers.getHost(this.req);

    // Filter disallowed HTML
    inputs.name = await sails.helpers.sanitize(inputs.name);
    inputs.message = await sails.helpers.sanitize(inputs.message);

    // Filter profanity
    inputs.name = await sails.helpers.filterProfane(inputs.name);
    inputs.message = await sails.helpers.filterProfane(inputs.message);

    // Truncate
    inputs.name = await sails.helpers.truncateText(inputs.name, 64);
    inputs.message = await sails.helpers.truncateText(inputs.message, 1024);

    // Get the song data
    let record2 = await sails.models.songs.findOne({ ID: inputs.ID });
    if (!record2) {
      throw { notFound: "The track ID was not found." };
    }

    // Create the request
    let request = await sails.models.requests
      .create({
        songID: inputs.ID,
        username: inputs.name,
        userIP: fromIP,
        message: inputs.message,
        requested: moment().format("YYYY-MM-DD HH:mm:ss"),
        played: 0,
      })
      .fetch();

    // Load the request in memory
    sails.models.requests.pending.push(inputs.ID);

    // Bump priority if configured
    if (sails.config.custom.basic.requestsPriorityChange !== 0) {
      await sails.models.songs.updateOne(
        { ID: inputs.ID },
        {
          weight:
            record2.weight + sails.config.custom.basic.requestsPriorityChange,
        }
      );
    }

    // Log the request
    await sails.models.logs
      .create({
        attendanceID: sails.models.meta.memory.attendanceID,
        logtype: "website-request",
        loglevel: "info",
        logsubtype: `${sails.models.meta.memory.show}`,
        logIcon: `fas fa-record-vinyl`,
        title: `A track was requested online.`,
        event: `Track: ${record2.artist} - ${record2.title} (ID ${inputs.ID})<br />Requested By: ${inputs.name}<br />Message: ${inputs.message}`,
      })
      .fetch()
      .tolerate((err) => {
        sails.log.error(err);
      });

    let returndata = {
      requested: true,
      message: `Request placed! Requests are queued at every break. If a show is live, it is up to the host's discretion.`,
    };

    // Add a push notification subscription if a device was provided
    if (inputs.device && inputs.device !== null) {
      await sails.models.subscribers.findOrCreate(
        { device: inputs.device, type: `request`, subtype: request.ID },
        {
          host: `website-${host}`,
          device: inputs.device,
          type: `request`,
          subtype: request.ID,
          description: `Track request: ${record2.artist} - ${record2.title} (will be auto-removed when the request plays and notification sent out)`,
        }
      );
      returndata.message = `Request placed! Requests are queued at every break. If a show is live, it is up to the host's discretion.<br />
                                      <strong>You will receive a push notification when your request begins playing.</strong>`;
    }

    // Finish it
    return returndata;
  },
};

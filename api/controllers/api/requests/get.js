module.exports = {
  friendlyName: "GET api/requests",

  description: "Get pending track requests and subscribe to sockets.",

  inputs: {
    offset: {
      type: "number",
      defaultsTo: 0,
      description: "Only return requests with an ID greater than this."
    },
  },

  fn: async function (inputs) {
    // Get requests
    let response = await sails.helpers.requests.get(inputs.offset);

    // If applicable, subscribe to the requests socket
    if (this.req.isSocket) {
      sails.sockets.join(this.req, "requests");
      sails.log.verbose("Request was a socket. Joining requests.");
    }

    return response;
  },
};

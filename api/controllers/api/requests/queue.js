const { info } = require("winston");

module.exports = {
  friendlyName: "POST api/requests/:ID/queue",

  description: "Queue or play a request.",

  inputs: {
    ID: {
      type: "number",
      required: true,
      description: "The Request ID number.",
    },
  },

  exits: {
    forbidden: {
      statusCode: 403,
    },
  },

  fn: async function (inputs) {
    if (!sails.config.custom.radiodjs.length)
      throw {
        forbidden: "Request system disabled; no RadioDJs configured.",
      };

    // Prevent queuing requests if host is belongsTo and the specified belongsTo is not on the air
    if (!(await sails.helpers.hostAuthorizedDuringBroadcast(this.req.payload)))
      throw {
        forbidden:
          "This host is currently not allowed to queue a request; this host did not start the current broadcast and does not have admin permission.",
      };

    // Queue the request
    let response = await sails.helpers.requests.queue(
      1,
      false,
      false,
      inputs.ID
    );

    if (!response) throw { forbidden: "Unable to queue that request." };

    return;
  },
};

const Gtts = require("gtts");

// Stream new alerts through text-to-speech
module.exports = async function speakEAS(req, res) {
  // Set response content type to MP3
  res.set({ "Content-Type": "audio/mpeg" });

  // Prepare what to speak
  let toSpeak = ``;

  // Grab alerts active
  let records = await sails.models.eas.find({ needsAired: true });

  // If no alerts in effect, send nothing and exit.
  if (records.length === 0) {
    return res.send();
  }

  toSpeak += records
    .map((alert) => {
      return `${alert.source || `An Unknown Source`} has issued, ${
        alert.alert
      }. ${alert.information}.`;
    })
    .join(" ");

  // Generate TTS and stream the text to speech audio to response
  let speech = new Gtts(toSpeak, "en-us");
  speech.stream().pipe(res);

  // Mark all alerts as spoken
  await sails.models.eas
    .update({ needsAired: true }, { needsAired: false })
    .fetch();
};

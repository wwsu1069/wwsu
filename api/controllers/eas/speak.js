const Gtts = require("gtts");

// Stream the currently active alerts as a text to speech MP3 file.
module.exports = async function speakEAS(req, res) {
  // Set response content type to MP3
  res.set({ "Content-Type": "audio/mpeg" });

  // Prepare what to speak
  let toSpeak = ``;

  // Grab alerts active
  let records = await sails.models.eas.find();

  if (records.length === 0) {
    toSpeak = `There are currently no active alerts for the WWSU listening area.`;
  } else {
    toSpeak += records
      .map((alert) => {
        return `${alert.source || `An Unknown Source`} has issued, ${
          alert.alert
        }, in effect for ${alert.counties || "Wright State University"}${
          alert.expires ? `, until ${moment(alert.expires).format("dddd MMMM Do, LT")}` : ``
        }.`;
      })
      .join(" ");
  }

  // Generate TTS and stream the text to speech audio to response
  let speech = new Gtts(toSpeak, "en-us");
  speech.stream().pipe(res);
};

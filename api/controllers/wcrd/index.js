module.exports = {
  friendlyName: "wcrd/",

  description: "wcrd page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/wcrd",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `WWSU is a proud participant in World College Radio Day. Learn about the event, WWSU's accomplishments, and the importance of college radio.`,
        author: "World College Radio Day - WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/wcrd`,
        "og:type": "video",
        "og:description": `WWSU is a proud participant in World College Radio Day. Learn about the event, WWSU's accomplishments, and the importance of college radio.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};

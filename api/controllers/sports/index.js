module.exports = {
  friendlyName: "sports/",

  description: "Sports page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/sports",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `View our upcoming schedule of live sports broadcasts / coverage of the Wright State Raiders.`,
        author: "Upcoming Sports Broadcasts - WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/sports`,
        "og:type": "website",
        "og:description": `View our upcoming schedule of live sports broadcasts / coverage of the Wright State Raiders.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};
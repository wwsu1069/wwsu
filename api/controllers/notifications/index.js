module.exports = {
    friendlyName: "notifications/",
  
    description: "Notifications page",
  
    inputs: {},
  
    exits: {
      success: {
        responseType: "view",
        viewTemplatePath: "website/notifications",
      },
    },
  
    fn: async function (inputs) {
      // All done.
      return {
        layout: "website/layout",
        meta: {
          description: `Manage your push notification subscriptions on WWSU.`,
          author: "WWSU 106.9 FM",
          "og:title": `Manage Notifications - WWSU 106.9 FM`,
          "og:url": `${sails.config.custom.baseUrl}/notifications`,
          "og:type": "website",
          "og:description": `Manage your push notification subscriptions on WWSU.`,
          "og:site_name": "WWSU 106.9 FM",
        },
      };
    },
  };
  
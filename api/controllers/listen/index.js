module.exports = {
  friendlyName: "listen/",

  description: "Now playing page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/nowplaying",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
    };
  },
};
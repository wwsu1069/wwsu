module.exports = {
  friendlyName: "video/",

  description: "Video page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/video",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `Watch our live video stream when we (or a DJ) are streaming.`,
        author: "Video Stream - WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/video`,
        "og:type": "video",
        "og:description": `Watch our live video stream when we (or a DJ) are streaming.`,
        "og:site_name": "WWSU 106.9 FM",
        "og:video": "https://wwsuvideo.pdstig.me"
      },
    };
  },
};
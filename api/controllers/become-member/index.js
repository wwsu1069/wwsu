module.exports = {
  friendlyName: "become-member/",

  description: "Become Member page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/member",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `Learn how you can become a member of WWSU, get involved, and even host your own radio shows.`,
        author: "Become Org Member - WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/become-member`,
        "og:type": "website",
        "og:description": `Learn how you can become a member of WWSU, get involved, and even host your own radio shows.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};
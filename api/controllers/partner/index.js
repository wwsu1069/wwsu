module.exports = {
  friendlyName: "partner/",

  description: "Partner page",

  inputs: {},

  exits: {
    success: {
      responseType: "view",
      viewTemplatePath: "website/partner",
    },
  },

  fn: async function (inputs) {
    // All done.
    return {
      layout: "website/layout",
      meta: {
        description: `Learn about the various services we offer at WWSU: music / DJing for events, on-air underwritings and sponsorships, and display sign promotion.`,
        author: "Partner With Us - WWSU 106.9 FM",
        "og:title": `WWSU 106.9 FM`,
        "og:url": `${sails.config.custom.baseUrl}/partner`,
        "og:type": "website",
        "og:description": `Learn about the various services we offer at WWSU: music / DJing for events, on-air underwritings and sponsorships, and display sign promotion.`,
        "og:site_name": "WWSU 106.9 FM",
      },
    };
  },
};
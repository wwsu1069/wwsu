/**
 * Configdiscord.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    token: {
      type: "string",
      allowNull: true,
      description: "The Discord bot token from the developer portal.",
      encrypt: true,
    },

    clientOptions: {
      type: "json",
      description: "Discord.js client options for the bot.",
      defaultsTo: {
        messageCacheMaxSize: 10000,
        invalidRequestWarningInterval: 10,
        partials: ["USER", "MESSAGE", "CHANNEL", "GUILD_MEMBER", "REACTION"],
        intents: [
          "GUILDS",
          "GUILD_MESSAGES",
          "GUILD_MEMBERS",
          "GUILD_PRESENCES",
          "GUILD_MESSAGE_REACTIONS",
          "DIRECT_MESSAGES",
        ],
      },
    },

    guildWWSU: {
      type: "string",
      defaultsTo: "",
      description: "The snowflake ID of the WWSU discord server.",
    },

    guildWNF: {
      type: "string",
      defaultsTo: "",
      description: "The snowflake ID of the Wright News Feeds discord server.",
    },

    channelLive: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the text channel to post broadcasts that go on the air.",
    },

    channelScheduleChanges: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the text channel to post all broadcast schedule changes.",
    },

    channelRadio: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the voice channel for the bot to join to play the internet radio stream.",
    },

    channelSports: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the text channel where general sports discussions take place; this is where sports-related stuff will be posted / read.",
    },

    channelGeneral: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the text channel where general discussions take place (messages posted in here when a show is not broadcasting will be displayed in the messages / chat system).",
    },

    channelBlog: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the text channel where published blog posts should be posted.",
    },

    channelAnalytics: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the text channel where weekly analytics should be posted.",
    },

    messageAnalytics: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the message where weekly analytics should be posted / edited.",
    },

    channelWNF: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the text channel in the Wright News feeds to post things.",
    },

    categoryShowDefault: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the channel category to place new text channels for shows added to the calendar.",
    },

    categoryGeneral: {
      type: "string",
      defaultsTo: "",
      description:
        "The snowflake ID of the channel category where general discussions take place.",
    },
  },

  initialize: async function () {
    sails.config.custom.discord = await sails.models.configdiscord
      .findOrCreate({ ID: 1 }, { ID: 1 })
      .decrypt();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-discord socket: ${data}`);
    sails.sockets.broadcast("config-discord", "config-discord", data);

    // Re-load config
    (async () => {
      sails.config.custom.discord = await sails.models.configdiscord
        .findOrCreate({ ID: 1 }, { ID: 1 })
        .decrypt();

      proceed();
    })();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-discord socket: ${data}`);
    sails.sockets.broadcast("config-discord", "config-discord", data);

    // Re-load config
    (async () => {
      sails.config.custom.discord = await sails.models.configdiscord
        .findOrCreate({ ID: 1 }, { ID: 1 })
        .decrypt();

      proceed();
    })();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-discord socket: ${data}`);
    sails.sockets.broadcast("config-discord", "config-discord", data);

    // Re-load config
    (async () => {
      sails.config.custom.discord = await sails.models.configdiscord
        .findOrCreate({ ID: 1 }, { ID: 1 })
        .decrypt();

      proceed();
    })();
  },
};

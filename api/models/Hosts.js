/**
 * Hosts.js
 *
 * @description :: Hosts contains the computers that use DJ Controls, their friendly name, and which kinds of messages they should receive.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    host: {
      type: "string",
      required: true,
      unique: true,
    },

    friendlyname: {
      type: "string",
      defaultsTo: "Unknown Host",
    },

    app: {
      type: "string",
      allowNull: true,
    },

    authorized: {
      type: "boolean",
      defaultsTo: false,
    },

    admin: {
      type: "boolean",
      defaultsTo: false,
    },

    belongsTo: {
      type: "number",
      allowNull: true,
      defaultsTo: 0, // null = WWSU computer, 0 = neither WWSU computer nor org member, other # = ID of the Djs host belongs to.
    },

    lockDown: {
      type: "string",
      isIn: ["prod", "onair", "director"],
      allowNull: true,
    },

    answerCalls: {
      type: "boolean",
      defaultsTo: false,
    },

    silenceDetection: {
      type: "boolean",
      defaultsTo: false,
    },

    recordAudio: {
      type: "boolean",
      defaultsTo: false,
    },

    delaySystem: {
      type: "boolean",
      defaultsTo: false,
    },

    EAS: {
      type: "boolean",
      defaultsTo: false,
    },

    requests: {
      type: "boolean",
      defaultsTo: false,
    },

    emergencies: {
      type: "boolean",
      defaultsTo: false,
    },

    accountability: {
      type: "boolean",
      defaultsTo: false,
    },

    webmessages: {
      type: "boolean",
      defaultsTo: false,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`hosts socket: ${data}`);
    sails.sockets.broadcast("hosts", "hosts", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`hosts socket: ${data}`);
    sails.sockets.broadcast("hosts", "hosts", data);

    (async () => {
      // Edit the status of recorder if necessary
      if (!(await sails.models.hosts.count({ recordAudio: true })))
        await sails.helpers.status.modify.with({
          name: `recorder`,
          status: 2,
          label: `Recorder`,
          summary: `No hosts configured to record on-air programming. Manual recording will be necessary.`,
          data: `There are no hosts currently set for recording audio. <strong>Be prepared to manually record your broadcasts</strong> until this is resolved.
          <br /><strong>TO FIX:</strong>
          <ol>
            <li>Go in administration DJ Controls -> Hosts.</li>
            <li>Edit the host that should record audio and check the box for record audio.</li>
            <li>On the DJ Controls recording audio, in DJ Controls Settings -> Audio, make sure the appropriate input audio devices receiving on-air audio have "Record Audio" checked. Also make sure the Recorder Settings are correct.</li>
          </ol>`,
        });
    })();

    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`hosts socket: ${data}`);
    sails.sockets.broadcast("hosts", "hosts", data);

    (async () => {
      // Edit the status of recorder if necessary
      if (!(await sails.models.hosts.count({ recordAudio: true })))
        await sails.helpers.status.modify.with({
          name: `recorder`,
          status: 2,
          label: `Recorder`,
          summary: `No hosts configured to record on-air programming. Manual recording will be necessary.`,
          data: `There are no hosts currently set for recording audio. <strong>Be prepared to manually record your broadcasts</strong> until this is resolved.
          <br /><strong>TO FIX:</strong>
          <ol>
            <li>Go in administration DJ Controls -> Hosts.</li>
            <li>Edit the host that should record audio and check the box for record audio.</li>
            <li>On the DJ Controls recording audio, in DJ Controls Settings -> Audio, make sure the appropriate input audio devices receiving on-air audio have "Record Audio" checked. Also make sure the Recorder Settings are correct.</li>
          </ol>`,
        });
    })();

    return proceed();
  },
};

/**
 * Configcategories.js
 *
 * @description :: Configuration that maps system categories with RadioDJ subcategories.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",

  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      required: true,
      unique: true,
    },

    categories: {
      type: "json",
      required: true,
      description:
        "Map of RadioDJ categories and subcategories assigned to this system category. Should be an object... each key is the name of the RadioDJ main category, and value is an array of subcategory names in the main category (or empty array for all of them).",
      custom: (value) => {
        if (typeof value !== "object") return false;

        let valid = true;
        let hasOneKey = false; // There must exist at least one key

        for (let key in value) {
          if (!Object.prototype.hasOwnProperty.call(value, key)) continue;

          // All property keys must be a string
          if (typeof key !== "string") {
            valid = false;
            break;
          }

          hasOneKey = true;

          // Values must be an array
          if (value[key].constructor !== Array) {
            valid = false;
            break;
          }

          // Every item in the value array must be a string
          if (value[key].length) {
            value[key].forEach((subcat) => {
              if (typeof subcat !== "string") valid = false;
            });
          }
        }

        return hasOneKey && valid;
      },
    },
  },

  initialize: async function () {
    // Create any missing categories as defined in systemCategories
    for (let key in sails.config.custom.systemCategories) {
      if (
        !Object.prototype.hasOwnProperty.call(
          sails.config.custom.systemCategories,
          key
        )
      )
        continue;

      // Create the category if it does not exist with default values
      let record = await sails.models.configcategories.findOrCreate(
        { name: key },
        { name: key, categories: sails.config.custom.systemCategories[key] }
      );
    }

    // Load full category configuration
    sails.config.custom.categories = await sails.models.configcategories.find();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-categories socket: ${data}`);
    sails.sockets.broadcast("config-categories", "config-categories", data);

    // Add break to config
    sails.config.custom.categories.push(newlyCreatedRecord);

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-categories socket: ${data}`);
    sails.sockets.broadcast("config-categories", "config-categories", data);

    // Re-run initialize in case we lost a system category (and to update full categories in memory)
    module.exports.initialize().then(() => proceed());
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-categories socket: ${data}`);
    sails.sockets.broadcast("config-categories", "config-categories", data);

    // Re-run initialize in case we lost a system category (and to update full categories in memory)
    module.exports.initialize().then(() => proceed());
  },
};

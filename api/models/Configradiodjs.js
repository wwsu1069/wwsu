/**
 * Configradiodjs.js
 *
 * @description :: A configuration of each RadioDJ used by the system.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      required: true,
      unique: true,
      description: "Alphanumeric name",
      regex: /^[a-zA-Z0-9-_]+$/,
    },

    label: {
      type: "string",
      description: "Friendly name",
    },

    restURL: {
      type: "string",
      isURL: true,
      required: true,
      description: "The URL to the REST server for this RadioDJ",
    },

    restPassword: {
      type: "string",
      required: true,
      encrypt: true,
      description: "The password for the RadioDJ REST server",
    },

    errorLevel: {
      type: "number",
      min: 1,
      max: 5,
      required: true,
      description:
        "The error level that should be reported when this RadioDJ is not working (5 = good, 4 = info, 3 = minor, 2 = major, 1 = critical)",
    },
  },

  initialize: async function () {
    // Load full configuration
    sails.config.custom.radiodjs = await sails.models.configradiodjs
      .find()
      .decrypt();
  },

  // Websockets standards
  // TODO: Do any necessary maintenance of specific setting changes
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };

    // Delete sensitive records so they do not go out over sockets
    delete data.insert.restPassword;

    sails.log.silly(`config-radiodjs socket: ${data}`);
    sails.sockets.broadcast("config-radiodjs", "config-radiodjs", data);

    (async () => {
      // Reload configuration
      sails.config.custom.radiodjs = await sails.models.configradiodjs
        .find()
        .decrypt();

      proceed();

      // Add radioDJ to the status system
      await sails.models.status
        .create({
          name: `radiodj-${newlyCreatedRecord.name}`,
          label: `RadioDJ ${newlyCreatedRecord.label}`,
          status: newlyCreatedRecord.errorLevel,
          summary: "This RadioDJ has not reported online since initialization.",
          data: "This RadioDJ has not reported online since initialization.",
          time: null,
        })
        .fetch();
    })();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };

    // Delete sensitive records so they do not go out over sockets
    delete data.update.restPassword;

    sails.log.silly(`config-radiodjs socket: ${data}`);
    sails.sockets.broadcast("config-radiodjs", "config-radiodjs", data);

    (async () => {
      // Reload configuration
      sails.config.custom.radiodjs = await sails.models.configradiodjs
        .find()
        .decrypt();

      proceed();

      // Change active RadioDJ if we could no longer find the active one in configuration.
      if (
        !sails.config.custom.radiodjs.find(
          (instance) => instance.name === sails.models.meta.memory.radiodj
        )
      ) {
        await sails.helpers.meta.change.with({ radiodj: `` });
        await sails.helpers.rest.changeRadioDj();
        await sails.helpers.rest.cmd("StopPlayer", null, 10000, updatedRecord);
      }

      // Update all RadioDJs in the status system
      sails.config.custom.radiodjs.map(async (radiodj) => {
        sails.models.status
          .findOrCreate(
            { name: `radiodj-${radiodj.name}` },
            {
              name: `radiodj-${radiodj.name}`,
              label: `RadioDJ ${radiodj.label}`,
              status: radiodj.errorLevel,
              summary: "This RadioDJ has not reported online since initialization.",
              data: "This RadioDJ has not reported online since initialization.",
              time: null,
            }
          )
          .exec(async (err, record, created) => {
            if (err) throw err;

            if (!created) {
              await sails.models.status
                .update(
                  { name: `radiodj-${radiodj.name}` },
                  {
                    label: `RadioDJ ${radiodj.label}`,
                  }
                )
                .fetch();
            }
          });
      });

      // Delete radioDJs that no longer exist in the status system
      let statuses = await sails.models.status.find({
        name: { startsWith: `radiodj-` },
      });
      statuses.map(async (status) => {
        if (
          !sails.config.custom.radiodjs.find(
            (radiodj) => status.name === `radiodj-${radiodj.name}`
          )
        )
          await sails.models.status.destroy({ ID: status.ID }).fetch();
      });
    })();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-radiodjs socket: ${data}`);
    sails.sockets.broadcast("config-radiodjs", "config-radiodjs", data);

    (async () => {
      // Reload configuration
      sails.config.custom.radiodjs = await sails.models.configradiodjs
        .find()
        .decrypt();

      proceed();

      // Change active RadioDJ if the one deleted was the one active.
      if (destroyedRecord.name === sails.models.meta.memory.radiodj) {
        await sails.helpers.meta.change.with({ radiodj: `` });
        await sails.helpers.rest.changeRadioDj();
        await sails.helpers.rest.cmd(
          "StopPlayer",
          null,
          10000,
          destroyedRecord
        );
      }

      // Destroy the status record
      await sails.models.status
        .destroy({ name: `radiodj-${destroyedRecord.name}` })
        .fetch();
    })();
  },
};

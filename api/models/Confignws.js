/**
 * Confignws.js
 *
 * @description :: Configuration for NWS CAPS alerts in the internal EAS.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    code: {
      type: "string",
      required: true,
      description: "The CAPS county or zone code",
    },

    name: {
      type: "string",
      required: true,
      description:
        "The human readable name for this location, such as the name of the county.",
    },
  },

  initialize: async function () {
    // Load full NWS configuration
    sails.config.custom.nws = await sails.models.confignws.find();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-nws socket: ${data}`);
    sails.sockets.broadcast("config-nws", "config-nws", data);

    // Add to config
    sails.config.custom.nws.push(newlyCreatedRecord);

    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-nws socket: ${data}`);
    sails.sockets.broadcast("config-nws", "config-nws", data);

    // Update nws in config
    (async () => {
      let findRecord = sails.config.custom.nws.findIndex(
        (record) => record.ID === updatedRecord.ID
      );
      if (findRecord < 0) {
        sails.config.custom.nws = await sails.models.confignws.find();
      } else {
        sails.config.custom.nws[findRecord] = updatedRecord;
      }
      proceed();
    })();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-nws socket: ${data}`);
    sails.sockets.broadcast("config-nws", "config-nws", data);

    // Remove nws in config
    (async () => {
      let findRecord = sails.config.custom.nws.findIndex(
        (record) => record.ID === destroyedRecord.ID
      );
      if (findRecord < 0) {
        sails.config.custom.nws = await sails.models.confignws.find();
      } else {
        delete sails.config.custom.nws[findRecord];
      }
      proceed();
    })();
  },
};

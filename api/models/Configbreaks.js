/**
 * Configbreaks.js
 *
 * @description :: Configuration for clockwheel breaks.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    type: {
      type: "string",
      required: true,
      description: "Type of the break",
      isIn: ["clockwheel", "automation", "live", "remote", "sports"],
    },

    subtype: {
      type: "string",
      required: true,
      custom: (value) => {
        // If the value is a number, it must be between 0 and 59 (for the minute of the hour)
        if (!isNaN(parseInt(value))) {
          let numericValue = parseInt(value);
          if (numericValue < 0 || numericValue > 59) return false;
          return true;
        }

        // If the value is otherwise an actual string, it must be an allowed value
        if (
          [
            "start",
            "before",
            "during",
            "duringHalftime",
            "after",
            "end",
          ].indexOf(value) === -1
        )
          return false;
        return true;
      },
      description:
        "The subtype (for clockwheel, the minute of the hour... for all other types, either start, before, during, duringHalftime, after, or end).",
    },

    order: {
      type: "number",
      defaultsTo: 1,
      description:
        "Tasks are executed in order from lowest to highest (ID is used when records have the same order)",
    },

    task: {
      type: "string",
      required: true,
      isIn: [
        "log",
        "queue",
        "queueDuplicates",
        "queueUnderwritings",
        "queueRequests",
        "queueAlerts",
      ],
    },

    event: {
      type: "string",
      allowNull: true,
      description: "For log tasks, this is the text to be logged.",
    },

    category: {
      type: "string",
      allowNull: true,
      description:
        "For queue tasks, the category (configured in the node server) to queue tracks from.",
    },

    quantity: {
      type: "number",
      defaultsTo: 1,
      description: "The number of tracks to queue for any queue related tasks.",
    },

    rules: {
      type: "string",
      isIn: ["noRules", "lenientRules", "strictRules"],
      defaultsTo: "noRules",
      description:
        "noRules = do not follow playlist rotation rules; lenientRules = follow rotation rules unless there are no more tracks that conform; strictRules = abandon queuing any more tracks if no more conform to rotation rules",
    },

    doWhen: {
      type: "json",
      defaultsTo: [],
      custom: (value) => {
        if (value.constructor !== Array) return false; // Must be an array

        let invalid = value.find(
          (item) =>
            typeof item !== "string" ||
            [
              "show",
              "remote",
              "sports",
              "prerecord",
              "genre",
              "playlist",
              "default",
            ].indexOf(item) === -1
        );
        return !invalid;
      },
      description:
        "Only execute this task during the specified types of broadcasts (empty array = all of them, the default). Accepted values: show, remote, sports, prerecord, genre, playlist, default (Default automation rotation).",
    },
  },

  initialize: async function () {
    // Initialize clockwheel 0 ID break if it does not exist.
    await sails.models.configbreaks.findOrCreate(
      { type: "clockwheel", subtype: "0", task: "queue", category: "IDs" },
      {
        type: "clockwheel",
        subtype: "0",
        order: 0,
        task: "queue",
        category: "IDs",
        quantity: 1,
        rules: "noRules",
      }
    );

    // Load full break configuration
    sails.config.custom.breaks = await sails.models.configbreaks
      .find()
      .sort([{ order: "ASC" }, { ID: "ASC" }]);
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-breaks socket: ${data}`);
    sails.sockets.broadcast("config-breaks", "config-breaks", data);

    // Re-load config
    (async () => {
      sails.config.custom.breaks = await sails.models.configbreaks
        .find()
        .sort([{ order: "ASC" }, { ID: "ASC" }]);

      proceed();
    })();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-breaks socket: ${data}`);
    sails.sockets.broadcast("config-breaks", "config-breaks", data);

    // Did we get rid of the top of hour ID task? Add a new one if we did!
    (async () => {
      await sails.models.configbreaks.findOrCreate(
        { type: "clockwheel", subtype: "0", task: "queue", category: "IDs" },
        {
          type: "clockwheel",
          subtype: "0",
          order: 0,
          task: "queue",
          category: "IDs",
          quantity: 1,
          rules: "noRules",
        }
      );

      // Re-load config
      sails.config.custom.breaks = await sails.models.configbreaks
        .find()
        .sort([{ order: "ASC" }, { ID: "ASC" }]);

      proceed();
    })();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-breaks socket: ${data}`);
    sails.sockets.broadcast("config-breaks", "config-breaks", data);

    // Did we get rid of the top of hour ID task? Add a new one if we did!
    (async () => {
      await sails.models.configbreaks.findOrCreate(
        { type: "clockwheel", subtype: "0", task: "queue", category: "IDs" },
        {
          type: "clockwheel",
          subtype: "0",
          order: 0,
          task: "queue",
          category: "IDs",
          quantity: 1,
          rules: "noRules",
        }
      );

      // Re-load config
      sails.config.custom.breaks = await sails.models.configbreaks
        .find()
        .sort([{ order: "ASC" }, { ID: "ASC" }]);

      proceed();
    })();
  },
};

/**
 * sails.models.attendance.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    calendarID: {
      type: "number",
      allowNull: true,
    },

    unique: {
      type: "string",
    },

    dj: {
      type: "number",
      allowNull: true,
    },

    cohostDJ1: {
      type: "number",
      allowNull: true,
    },

    cohostDJ2: {
      type: "number",
      allowNull: true,
    },

    cohostDJ3: {
      type: "number",
      allowNull: true,
    },

    event: {
      type: "string",
    },

    happened: {
      type: "number",
      defaultsTo: 1,
      min: -1,
      max: 1,
    },

    happenedReason: {
      type: "string",
      allowNull: true,
      columnType: "text",
    },

    ignore: {
      type: "number",
      defaultsTo: 0,
      min: 0,
      max: 2,
    },

    showTime: {
      type: "number",
      allowNull: true,
    },

    tuneIns: {
      type: "number",
      allowNull: true,
    },

    listenerMinutes: {
      type: "number",
      allowNull: true,
    },

    listenerPeak: {
      type: "number",
      allowNull: true,
    },

    listenerPeakTime: {
      type: "ref",
      columnType: "datetime",
    },

    videoTime: {
      type: "number",
      allowNull: true,
    },

    viewerTuneIns: {
      type: "number",
      allowNull: true,
    },

    viewerMinutes: {
      type: "number",
      allowNull: true,
    },

    viewerPeak: {
      type: "number",
      allowNull: true,
    },

    viewerPeakTime: {
      type: "ref",
      columnType: "datetime",
    },

    webMessages: {
      type: "number",
      allowNull: true,
    },

    missedIDs: {
      type: "json",
      defaultsTo: [],
    },

    breaks: {
      type: "number",
      defaultsTo: 0,
    },

    cancellation: {
      type: "boolean",
      defaultsTo: false,
    },

    absent: {
      type: "boolean",
      defaultsTo: false,
    },

    unauthorized: {
      type: "boolean",
      defaultsTo: false,
    },

    silence: {
      type: "json",
      defaultsTo: [],
    },

    signedOnEarly: {
      type: "boolean",
      defaultsTo: false,
    },

    signedOnLate: {
      type: "boolean",
      defaultsTo: false,
    },

    signedOffEarly: {
      type: "boolean",
      defaultsTo: false,
    },

    signedOffLate: {
      type: "boolean",
      defaultsTo: false,
    },

    badPlaylist: {
      type: "boolean",
      defaultsTo: false,
    },

    scheduledStart: {
      type: "ref",
      columnType: "datetime",
    },

    scheduledEnd: {
      type: "ref",
      columnType: "datetime",
    },

    actualStart: {
      type: "ref",
      columnType: "datetime",
    },

    actualEnd: {
      type: "ref",
      columnType: "datetime",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    [
      "listenerPeakTime",
      "viewerPeakTime",
      "scheduledStart",
      "scheduledEnd",
      "actualStart",
      "actualEnd",
      "createdAt",
      "updatedAt",
    ].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    [
      "listenerPeakTime",
      "viewerPeakTime",
      "scheduledStart",
      "scheduledEnd",
      "actualStart",
      "actualEnd",
      "createdAt",
      "updatedAt",
    ].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`attendance socket: ${data}`);
    sails.sockets.broadcast("attendance", "attendance", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`attendance socket: ${data}`);
    sails.sockets.broadcast("attendance", "attendance", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`attendance socket: ${data}`);
    sails.sockets.broadcast("attendance", "attendance", data);
    return proceed();
  },

  weeklyAnalytics: {
    topShows: [],
    topGenre: { name: "None", score: 0 },
    topPlaylist: { name: "None", score: 0 },
    onAir: 0,
    listeners: 0,
    onAirListeners: 0,
    listenerPeak: 0,
    listenerPeakTime: null,
    viewers: 0,
    onAirViewers: 0,
    viewerPeak: 0,
    viewerPeakTime: null,
    tracksLiked: 0,
    tracksRequested: 0,
    webMessagesExchanged: 0,
    discordMessagesExchanged: 0,
  },
};

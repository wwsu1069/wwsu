/**
 * Clockwheels.js
 *
 * @description :: Container containing scheduled clockwheels.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    calendarID: {
      type: "number",
    },

    scheduleID: {
      type: "number",
    },

    relativeStart: {
      type: "number",
      min: 0,
    },

    relativeEnd: {
      type: "number",
      min: 0,
    },

    segmentName: {
      type: "string",
    },

    segmentColor: {
      type: "string",
      defaultsTo: "#D50000",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  initialize: async function () {
    sails.models.calendar.calendardb.query(
      "clockwheels",
      await sails.models.clockwheels.find(),
      true
    );
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`clockwheels socket: ${data}`);
    sails.models.calendar.calendardb.query("clockwheels", data);
    sails.sockets.broadcast("clockwheels", "clockwheels", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`Clockwheels socket: ${data}`);
    sails.models.calendar.calendardb.query("clockwheels", data);
    sails.sockets.broadcast("clockwheels", "clockwheels", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`clockwheels socket: ${data}`);
    sails.models.calendar.calendardb.query("clockwheels", data);
    sails.sockets.broadcast("clockwheels", "clockwheels", data);
    return proceed();
  },
};

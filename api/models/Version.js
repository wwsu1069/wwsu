/**
 * version.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    app: {
      type: "string",
      required: true,
      unique: true,
    },

    version: {
      type: "string",
      required: true,
    },

    downloadURL: {
      type: "string",
      required: true,
      isURL: true,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  initialize: async function () {
    // Create app records if they do not exist
    await sails.models.version.findOrCreate(
      { app: "wwsu-dj-controls" },
      {
        app: "wwsu-dj-controls",
        version: "0.0.0",
        downloadURL: "http://localhost",
      }
    );
    await sails.models.version.findOrCreate(
      { app: "wwsu-timesheets" },
      {
        app: "wwsu-timesheets",
        version: "0.0.0",
        downloadURL: "http://localhost",
      }
    );
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`version socket: ${data}`);
    sails.sockets.broadcast(
      `version-${newlyCreatedRecord.app}`,
      "version",
      data
    );
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`version socket: ${data}`);
    sails.sockets.broadcast(`version-${updatedRecord.app}`, "version", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`version socket: ${data}`);
    sails.sockets.broadcast(`version-${destroyedRecord.app}`, "version", data);
    return proceed();
  },
};

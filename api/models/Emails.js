/**
 * Emails.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    to: {
      type: "json",
      required: true,
    },

    cc: {
      type: "json",
    },

    bcc: {
      type: "json",
    },

    subject: {
      type: "string",
    },

    text: {
      type: "string",
      columnType: "text",
    },

    sendAt: {
      type: "ref",
      columnType: "datetime",
    },

    sent: {
      type: "boolean",
      defaultsTo: false,
    },

    status: {
      type: "json",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["sendAt", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["sendAt", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },
};

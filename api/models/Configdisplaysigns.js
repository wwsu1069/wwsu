/**
 * Configdisplaysigns.js
 *
 * @description :: Display sign configuration
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusailsconfig",
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    name: {
      type: "string",
      required: true,
      unique: true,
      description: "Alphanumeric name",
    },

    label: {
      type: "string",
      description: "Friendly name",
    },

    instances: {
      type: "number",
      defaultsTo: 0,
      min: 0,
      description:
        "How many of this type of display sign we expect to be connected?",
    },

    errorLevel: {
      type: "number",
      min: 1,
      max: 5,
      defaultsTo: 3,
      description:
        "The error level that should be reported when less than (instances) display signs are connected (5 = good, 4 = info, 3 = minor, 2 = major, 1 = critical)",
    },
  },

  initialize: async function () {
    // Generate missing records
    await sails.models.configdisplaysigns.findOrCreate(
      { name: "internal" },
      { name: "internal", label: "Internal" }
    );
    await sails.models.configdisplaysigns.findOrCreate(
      { name: "public" },
      { name: "public", label: "Public" }
    );

    // Load full configuration
    sails.config.custom.displaysigns =
      await sails.models.configdisplaysigns.find();
  },

  // Websockets standards
  // TODO: Do any necessary maintenance of specific setting changes
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.log.silly(`config-displaysigns socket: ${data}`);
    sails.sockets.broadcast("config-displaysigns", "config-displaysigns", data);

    // Reload configuration
    (async () => {
      sails.config.custom.displaysigns =
        await sails.models.configdisplaysigns.find();

      proceed();
    })();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.log.silly(`config-displaysigns socket: ${data}`);
    sails.sockets.broadcast("config-displaysigns", "config-displaysigns", data);

    (async () => {
      // Generate missing records
      await sails.models.configdisplaysigns.findOrCreate(
        { name: "internal" },
        { name: "internal", label: "Internal" }
      );
      await sails.models.configdisplaysigns.findOrCreate(
        { name: "public" },
        { name: "public", label: "Public" }
      );

      // Load full configuration
      sails.config.custom.displaysigns =
        await sails.models.configdisplaysigns.find();

      proceed();
    })();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.log.silly(`config-displaysigns socket: ${data}`);
    sails.sockets.broadcast("config-displaysigns", "config-displaysigns", data);

    (async () => {
      // Generate missing records
      await sails.models.configdisplaysigns.findOrCreate(
        { name: "internal" },
        { name: "internal", label: "Internal" }
      );
      await sails.models.configdisplaysigns.findOrCreate(
        { name: "public" },
        { name: "public", label: "Public" }
      );

      // Load full configuration
      sails.config.custom.displaysigns =
        await sails.models.configdisplaysigns.find();

      proceed();
    })();
  },
};

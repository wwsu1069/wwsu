/**
 * Playlists_list.js
 *
 * @description :: A list of tracks mapped to playlists.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "radiodj",
  // migrate: "safe", // NOT SUPPORTED by Sails :(
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    pID: {
      //type: "number",
      model: "playlists",
    },

    sID: {
      //type: "number",
      model: "songs",
    },

    cstart: {
      type: "number",
    },
    cnext: {
      type: "number",
    },
    cend: {
      type: "number",
    },
    fin: {
      type: "number",
    },
    fout: {
      type: "number",
    },
    swID: {
      type: "number",
    },
    swplay: {
      type: "number",
    },
    vtID: {
      type: "number",
    },
    vtplay: {
      type: "number",
    },
    swfirst: {
      type: "string",
    },
    ord: {
      type: "number",
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },
};

/**
 * Rss.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "wwsusails",
  ignoreEmptyTable: true,
  attributes: {
    ID: {
      type: "number",
      autoIncrement: true,
    },

    entryID: {
      type: "string",
      required: true,
      unique: true,
    },

    source: {
      type: "string",
      required: true,
    },

    date: {
      type: "ref",
      columnType: "datetime",
    },

    title: {
      type: "string",
      required: true,
    },

    summary: {
      type: "string",
      allowNull: true,
      columnType: "text",
    },

    author: {
      type: "string",
      allowNull: true,
    },
  },

  // MariaDB does not allow ISO strings
  beforeCreate: function (criteria, proceed) {
    ["date", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // MariaDB does not allow ISO strings
  beforeUpdate: function (criteria, proceed) {
    ["date", "createdAt", "updatedAt"].map((key) => {
      if (criteria[key])
        criteria[key] = moment(criteria[key]).format("YYYY-MM-DD HH:mm:ss.SSS");
    });

    proceed();
  },

  // Websockets standards
  afterCreate: function (newlyCreatedRecord, proceed) {
    var data = { insert: newlyCreatedRecord };
    sails.sockets.broadcast(`rss-${newlyCreatedRecord.source}`, "rss", data);
    return proceed();
  },

  afterUpdate: function (updatedRecord, proceed) {
    var data = { update: updatedRecord };
    sails.sockets.broadcast(`rss-${updatedRecord.source}`, "rss", data);
    return proceed();
  },

  afterDestroy: function (destroyedRecord, proceed) {
    var data = { remove: destroyedRecord.ID };
    sails.sockets.broadcast(`rss-${destroyedRecord.source}`, "rss", data);
    return proceed();
  },
};

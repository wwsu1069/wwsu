module.exports = async function (req, res, next) {
    if (req.param("owncastSecret")) {
      if (req.param("owncastSecret") !== sails.config.custom.basic.owncastSecret) {
        return res.status(403).json("The specified owncastSecret is incorrect.");
      }
    } else {
      return res
        .status(401)
        .json("You must provide an owncastSecret to use this endpoint.");
    }
  
    return next();
  };
  
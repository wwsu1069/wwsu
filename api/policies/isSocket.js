/**
 * isAuthorized
 *
 * @description :: Policy to check if using a websocket connection.
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = async function (req, res, next) {
  if (!req.isSocket) {
    return res
      .status(426)
      .append('Upgrade', 'websocket')
      .send("Disallowed protocol: you must use a websocket when calling this endpoint.");
  }

  next();
};

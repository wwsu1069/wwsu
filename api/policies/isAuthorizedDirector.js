/**
 * isAuthorized
 *
 * @description :: Policy to check if user is authorized with JSON web token
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = async function (req, res, next) {
  // Sometimes sockets will have an undefined session in Sails, which triggers errors. Make an empty object if this happens.
  //if (typeof req.session === "undefined") req.session = {};

  let authorized = req.session.authDirector;

  // This should never happen, but if there is no ID, we should error.
  if (!authorized || !authorized.ID || moment().isAfter(moment(authorized.exp)))
    return res
      .status(401)
      .send(
        "This endpoint requires auth/director authorization. This session has not authorized or the authorization is expired."
      );

  req.payload = authorized;

  return next();
};

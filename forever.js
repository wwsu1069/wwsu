const forever = require("forever-monitor");

const child = new forever.Monitor("app.js", {
  max: 10, // Allow process to restart a maximum of 10 times.
  uid: "wwsu-sails",
  silent: true,
  args: [],
  'env': { 'NODE_ENV': 'production' },
});

child.on('watch:restart', function(info) {
  console.info('Restarting sails because ' + info.file + ' changed');
});

child.on('restart', function() {
  console.info('Restarting sails for the ' + child.times + ' time');
});

child.on("exit", function () {
  console.error(
    new Error("sails has exited permanently!")
  );
});

child.on('exit:code', function(code) {
  console.error('Forever detected sails exited with code ' + code);
});

child.start();
